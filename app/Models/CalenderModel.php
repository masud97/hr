<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CalenderModel extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    protected $fillable = ['employee_login_id', 'title', 'start_date', 'start_time', 'end_date', 'end_time', 'des', 'status', 'insert_date_time', 'update_date_time', 'update_emp_id', 'reminder_for', 'remind_before', 'color'];
    protected $table = 'hr_calendar';

}
