<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TotalLeaveModel extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    protected  $fillable=['total_casual_leave','total_sick_leave','halfday'];
    protected $table = 'total_leave';

}
