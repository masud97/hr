<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NotificationManagerModel extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    protected  $fillable=['employee_login_id','message','url','purpose','insert_date_time','update_date_time','status','type','related_id'];
    protected $table = 'notifications_manager';

}
