<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApproverModel extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    protected  $fillable=['employee_login_id','supervisor_login_id','approver_login_id','final_approver_login_id'];
    protected $table = 'assign_approver';

}
