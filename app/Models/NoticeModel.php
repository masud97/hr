<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NoticeModel extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    protected  $fillable=['published_by','subject','body','publish_on','reissue','priority','priority','seen_by','notice_for','insert_date','status'];
    protected $table = 'notice';

}