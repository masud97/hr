<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeDetailsModel extends Model
{
     /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
    protected $fillable =  ['employee_login_id','full_name','email_personal','email_work','mobile_personal','mobile_work','mobile_emergency','emergency_contact_name_and_relation','persent_address','permanent_address','blood_group','date_of_birth','marital_status','gender','religion','highest_degree_obtained','company_name','designation','employee_category','joining_date','image','document_upload','department','national_id'];
    protected $table = 'employee_details';
}
