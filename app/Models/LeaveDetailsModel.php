<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LeaveDetailsModel extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    protected $table = 'leave_details';
    protected $fillable =  ['employee_login_id','waiting_for_employee_login_id','leave_purpose','leave_type','leave_from','leave_to','leave_amount','note','approve_deny_by','status','attachment','apply_date'];

}
