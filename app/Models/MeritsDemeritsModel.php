<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MeritsDemeritsModel extends Model {

    public $timestamps = false;
    protected $fillable = ['employee_login_id','type', 'insert_date','description','status'];
    protected $table = 'merits_demerits';

}
