<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityReportModel extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    protected $fillable = ['employee_login_id', 'report_from', 'report_to', 'report_type', 'self_a', 'activity_complete_this_week', 'long_term_project', 'issue_for_immediate_action', 'key_team', 'note', 'status', 'insert_date', 'rating', 'who_will_rate', 'who_seen'];
    protected $table = 'hr_activity_report';

}
