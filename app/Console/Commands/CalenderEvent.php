<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\CalenderModel as CalenderModel;
use App\Library\EmployeeDetailsLib as empLib;

class CalenderEvent extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:event';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This function check for event and notify user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {

        $event_array = NULL;
        $select = array('id', 'employee_login_id', 'title', 'start_date', 'start_time', 'end_date', 'end_time', 'reminder_for', 'remind_before', 'des');
        //  $temp = $this->calendar_model->select_where_descending($select, $where);
        $temp = CalenderModel::select($select)->orderBy('id', 'DESC')->where(array('status' => 'active', 'email_status' => 0))->get();
        if (!count($temp) > 0) {
            $temp = NULL;
        }
        if (!is_null($temp)) {
            $i = 0;
            foreach ($temp as $value) {

                /////
                $start_date_ = $request->input('start_date');
                $start_date = str_replace('/', '-', $start_date_);
                $start_date = date("Y-m-d", strtotime($start_date));

                /////////
                $start_time_ = $request->input('start_time');
                $start_time = date("H:i", strtotime($start_time_));
                $remind_before = $value->remind_before;

                $timeDiff = CalenderTimeDiff($start_date, $start_time);
                if ($start_date == date('Y-m-d') and $timeDiff <= $remind_before) {
                    if ($value->reminder_for != 'all') {
                        $reminder_for_emp = NULL;
                        $reminder_for_temp = explode('|:|', $value->reminder_for);

                        foreach ($reminder_for_temp as $val) {

                            $mail_array = array(
                                'email' => empLib::getEmployeeWorkEmailById($val),
                                'name' => empLib::getEmployeeName($val),
                                'title' => $value->title,
                                'des' => $value->des,
                                'start_time_' => date("h:i a", strtotime($value->start_time))
                            );

                            Mail::send('email-body.event', $mail_array, function($message) use ($mail_array) {

                                $message->to($mail_array['email'], $mail_array['name'])->subject('Event Alert');
                            });
                            $notification = array(
                                'employee_login_id' => $val,
                                'message' => "You have a new event today. Please check by click on this notification. An email has been set to you for details.",
                                'url' => "/all-event",
                                'purpose' => 'event',
                                'insert_date_time' => date('Y-m-d h:i:s a'),
                                'update_date_time' => NUll,
                                'status' => 'unseen',
                                'type' => 'info',
                                'related_id' => $val
                            );
                            \App\Models\NotificationManagerModel ::create($notification);
                            CalenderModel::where('id', $value->id)->update(array('email_status' => 1));
                        }
                    } else if ($value->reminder_for == 'all') {

                        $all_emp = empLib::all_active_emp_id_name();
                        if (!is_null($all_emp)) {
                            foreach ($all_emp as $val) {

                                $mail_array = array(
                                    'email' => empLib::getEmployeeWorkEmailById($val->id),
                                    'name' => empLib::getEmployeeName($val->id),
                                    'title' => $value->title,
                                    'des' => $value->des,
                                    'start_time_' => date("h:i a", strtotime($value->start_time))
                                );

                                Mail::send('email-body.event', $mail_array, function($message) use ($mail_array) {

                                    $message->to($mail_array['email'], $mail_array['name'])->subject('Event Alert');
                                });
                                $notification = array(
                                    'employee_login_id' => $val->id,
                                    'message' => "You have a new event today. Please check by click on this notification. An email has been set to you for details.",
                                    'url' => "/all-event",
                                    'purpose' => 'event',
                                    'insert_date_time' => date('Y-m-d h:i:s a'),
                                    'update_date_time' => NUll,
                                    'status' => 'unseen',
                                    'type' => 'info',
                                    'related_id' => $val->id
                                );
                                \App\Models\NotificationManagerModel ::create($notification);
                                CalenderModel::where('id', $value->id)->update(array('email_status' => 1));
                            }
                        }
                    }
                }
            }
        }
    }

}
