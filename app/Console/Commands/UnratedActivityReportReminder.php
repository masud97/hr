<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\ActivityReportModel as ActivityReportModel;

class UnratedActivityReportReminder extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'remind:report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This function give reminder to aprover about unrated report';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {


        $temp = ActivityReportModel::select(array('who_will_rate', 'insert_date'))
                ->where('status', 'active')
                ->where('rating', '=', NULL)
                ->get();
        $c = 0;
        $i = 0;
        $first = TRUE;
        if (count($temp) > 0) {
            $emp_id = array();
            foreach ($temp as $val) {

                $inser_date = date('Y-m-d', strtotime($val->insert_date));
                $start = strtotime($inser_date);
                $end = strtotime(date('Y-m-d'));
                $days_between = ceil(abs($end - $start) / 86400);

                if ($days_between >= 7) {
                    if (in_array($val->who_will_rate, $emp_id)) {
                        $emp_id[$i] = $val->who_will_rate;
                        $c = $c + 1;
                        $count[$i] = $count;
                        $first = FALSE;
                    } else {
                        if (!$first) {
                            $c = 0;
                            $i = $i + 1;
                        }

                        $emp_id[$i] = $val->who_will_rate;
                        $c = $c + 1;
                        $count[$i] = $c;
                        $first = FALSE;
                    }
                }
            }


            if (isset($emp_id)) {
                $emp_id = array_values($emp_id);
                $count = array_values($count);
                for ($i = 0; $i < sizeof($emp_id); $i++) {
                    $mail_array = array(
                        'email' => empLib::getEmployeeWorkEmailById($emp_id[$i]),
                        'name' => empLib::getEmployeeName($emp_id[$i]),
                        'count' => $count[$i]
                    );

                    Mail::send('email-body.report-reminder-approver', $mail_array, function($message) use ($mail_array) {

                        $message->to($mail_array['email'], $mail_array['name'])->subject('Event Alert');
                    });
                    $notification = array(
                        'employee_login_id' => $emp_id[$i],
                        'message' => "You have " . $count[$i] . " unrated pending report. Please check by click on this notification.",
                        'url' => "/pending-report",
                        'purpose' => 'event',
                        'insert_date_time' => date('Y-m-d h:i:s a'),
                        'update_date_time' => NUll,
                        'status' => 'unseen',
                        'type' => 'info',
                        'related_id' => $emp_id[$i]
                    );
                    \App\Models\NotificationManagerModel ::create($notification);
                }
            }
        }
    }

}
