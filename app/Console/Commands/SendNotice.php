<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\NoticeModel as NoticeModel;
use App\Models\NotificationManagerModel as Notify;
use Illuminate\Support\Facades\Mail;

class SendNotice extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:notice';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This function check the schedule notice and send it';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $notices = NoticeModel::select('id', 'subject', 'body', 'notice_for')
                ->where('publish_on', '<=', date('Y-m-d'))
                ->where('status', 'pending')
                ->get();

        if (!count($notices) > 0)
        {
            /* no notice found so exit the function */

            exit();
        }

        foreach ($notices as $not)
        {
            $notifications = NULL;
            $noticeFor = explode(',', $not->notice_for);
            $noticeId = $not->id;
            $subject = $not->subject;
            $body = $not->body;
            foreach ($noticeFor as $empId)
            {
                $notifications [] = array(
                    'employee_login_id' => $empId,
                    'type' => 'success',
                    'related_id' => $noticeId,
                    'message' => 'You have a new notice',
                    'url' => '/notice/view/' . $noticeId,
                    'purpose' => 'notice',
                    'insert_date_time' => date('Y-m-d'),
                    'status' => 'unseen'
                );

                $mail = array(
                    'email' => getUserEmail($empId),
                    'name' => getUserFullName($empId),
                    'subject' => $subject,
                    'body' => $body
                );

                Mail::send('email-body.notice', $mail, function ($message) use ($mail) {
                    $message->to($mail['email'], $mail['name'])->subject($mail['subject']);
                });
            }

            //insert notifications
            Notify::insert($notifications);

            /* now change the status of the notice to active */
            NoticeModel::where('id', $noticeId)->update(array('status' => 'active'));
        }
    }

}
