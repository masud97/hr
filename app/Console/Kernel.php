<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\SendNotice::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule) {
//        $schedule->command('send:notice')
//                ->daily();
        $schedule->command('send:notice')
                ->everyMinute();
        $schedule->command('clear:token')
                ->everyFiveMinutes();
        $schedule->command('get:event')
                ->everyTenMinutes();
        $schedule->command('remind:report')
                ->daily();
    }

}
