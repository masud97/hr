<?php

use App\Library\EmployeeDetailsLib as EmpDetails;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Route;

/* ------------------------------------------------------------------------------
 * NO Cash
 * -----------------------------------------------------------------------------
 * 
 * This function remove the browser cash. When user press the back button then the browser load the last 
 * Visited link from cash. This is a problem becasuse when a user successfully login to your site and
 * press the back button thn it show the login page again. To avoid this problem use this function before load the page
 * 
 * 
 * @author: Arnob
 * @email: mdarnob007@gamil.com
 * 
 */
if (!function_exists("no_cash"))
{

    function no_cash() {
        header("Expires: Mon, 26 Jul 1990 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
    }

}


/* ------------------------------------------------------------------------------
 * Last Query
 * -----------------------------------------------------------------------------
 * 
 * This function is use for print the last query executed by using laravel db or Eloquent
 * 
 * Note: please enable query log before run the query DB::enableQueryLog(); after query is execude call this function
 * 
 * @author: Arnob
 * @email: mdarnob007@gamil.com
 */


if (!function_exists("last_query"))
{

    function last_query() {
        $queries = DB::getQueryLog();
        $last_query = end($queries);

        printIt($last_query);
    }

}


/* ------------------------------------------------------------------------------
 * Print It
 * -----------------------------------------------------------------------------
 * 
 * print_it is a simple function for print your array by using pre.
 * When we recive some data from user or db we want to print it for verify that
 * we recived the valid data or data that we want. so every time we need to write 
 * a branch of code. For avoid this problem use this function just pass the array 
 * and it will show your expected result
 * 
 * @author : Arnob
 * @email : mdarnob007@gamil.com
 * 
 * @param : array 
 * @param : bool, pass true if you want to print by using var_dum function
 * 
 */


if (!function_exists("printIt"))
{

    function printIt($data, $bool = false, $die = FALSE) {
        echo "<pre>";

        if ($bool)
        {
            var_dump($data);
        }
        else
        {
            print_r($data);
        }


        echo "</pre>";
        if ($die)
        {
            die();
        }
    }

}

/* ------------------------------------------------------------------------------
 * convertTimeZone
 * -----------------------------------------------------------------------------
 * 
 * 
 */

if (!function_exists("convertTimeZone"))
{

    function convertTimeZone($dateTime, $target_timezone, $format = 'Y-m-d h:i:s A', $currentTimeZone = NULL) {
        if (is_null($currentTimeZone))
        {
            $currentTimeZone = date_default_timezone_get();
        }
        $sdatetime = new DateTime($dateTime, new DateTimeZone($currentTimeZone));
        $sdatetime->setTimeZone(new DateTimeZone($target_timezone));
        return $sdatetime->format($format);
    }

}


if (!function_exists("changeDateFormat"))
{

    function changeDateFormat($date, $format = 'Y-m-d') {
        if (!$date)
        {
            return '';
        }
        $fromDate = new DateTime($date);
        return $fromDate->format($format);
    }

}

if (!function_exists("addDayWithDate"))
{

    function addDayWithDate($date, $day) {
        if (!$date)
        {
            return '';
        }
        $fromDate = new DateTime($date);
        $fromDate->modify("+{$day} day");
        return $fromDate->format('Y-m-d');
    }

}

if (!function_exists("checkIfAnyActiveTokenExist"))
{

    function checkIfAnyActiveTokenExist() {
        $data = $this->CI->forget_password_model->select_where('id', array('email' => $this->email, 'status' => 1));
        if (!is_null($data))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

}

/* ------------------------------------------------------------------------------
 * Get User image
 * ------------------------------------------------------------------------------
 * 
 * This function returnt the url of current login user image
 * 
 */

if (!function_exists('getUserImageUrl'))
{

    function getUserImageUrl($userId) {
        $data = EmpDetails::getUserInfo('image', array('employee_login_id' => $userId));
        $url = asset('assets/img/user-image/noimagefound.jpg');
        if (count($data) > 0)
        {
            if (!is_null($data->image))
            {

                $url = asset('assets/img/user-image/' . $data->image);
            }
        }

        return $url;
    }

}

if (!function_exists('getUserFullName'))
{

    function getUserFullName($userId) {
        $data = EmpDetails::getUserInfo('full_name', array('employee_login_id' => $userId));

        if (count($data) > 0)
        {
            return $data->full_name;
        }
        else
        {
            return "Unknown";
        }
    }

}
if (!function_exists('getUserEmail'))
{

    function getUserEmail($userId) {
        $data = EmpDetails::getUserInfo('email_work', array('employee_login_id' => $userId));

        if (count($data) > 0)
        {
            return $data->email_work;
        }
        else
        {
            return "";
        }
    }

}


/* ------------------------------------------------------------------------------
 * Time Diff
 * -----------------------------------------------------------------------------
 * 
 * This function return the difference between two date in min
 * 
 */

if (!function_exists("timeDiff"))
{

    function timeDiff($start_date, $end_time) {

//        $date1 = new \DateTime($start_date);
//        $date2 = new \DateTime($start_time);
//        $interval = $date1->diff($date2);
//        return $interval->days;
        $dStart = new DateTime($start_date);
        $dEnd = new DateTime($end_time);
        $dDiff = $dStart->diff($dEnd);

        //if user select same date this function return the diff is zero. So we need to add +1 for leave amount
        //the invert is 1 if result is negative
        if ($dDiff->invert)
        {
            /* date will be negative */
            $dDiff = $dDiff->format('%a');
            $dDiff++;
            return $dDiff * (-1);
        }
        else
        {
            /* every thing is ok */

            $dDiff = $dDiff->format('%a');
            return ++$dDiff;
        }
    }

}


if (!function_exists('validateDate'))
{

    function validateDate($date, $format = 'Y-m-d H:i:s') {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

}


if (!function_exists("hash_password"))
{

    function hash_password($password) {
        $options = [
            'cost' => 10,
            'salt' => mcrypt_create_iv(26, MCRYPT_DEV_URANDOM),
        ];
        return password_hash($password, PASSWORD_BCRYPT, $options);
    }

}

if (!function_exists("ck_password"))
{

    function ck_password($password, $hash) {
        //echo "password: ".$password."<br/>Hash: ".$hash;
        if (password_verify($password, $hash))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

}

if (!function_exists("hasPrivilege"))
{

    function hasPrivilege($privilegeId) {
        return \App\Library\PrivilegeCheckLib::checkPrivilege($privilegeId, Session::get('id'));
    }

}

if (!function_exists("get_public_holiday"))
{

    function get_public_holiday() {


        $result = file_get_contents("http://www.timeanddate.com/holidays/bangladesh/");

        $result = entre2v2($result, '<table class="zebra fw tb-cl tb-hover">', '</table>');
        //printIt($result);
        $result = explode('<tbody>', $result);
        //printIt($result);
        $result = explode('<tr', $result[1]);

        $final = array();
        for ($i = 1; $i < sizeof($result); $i++)
        {

            $result2 = explode('<th class="nw" >', $result[$i]);

            $result3[0] = date('Y-m-d', strtotime(explode('</th>', $result2[1])[0]));

            $result3[1] = entre2v2($result2[1], '">', '</a>');
            $result3[2] = entre2v2($result2[1], '</a></td><td>', '</td>');

            if ($result3[2] == 'Public Holiday' or $result3[2] == 'National holiday')
            {
                $final[] = $result3;
            }
        }

        //printIt($final);
        if (isset($final))
        {
            $public_array = array();
            for ($i = 0; $i < sizeof($final); $i++)
            {


                $public_array[$i][0] = NULL;

                $public_array[$i][1] = NULL;

                $public_array[$i][2] = $final[$i][1];

                $public_array[$i][3] = $final[$i][0];
                $public_array[$i][4] = "00:00:00";
                $public_array[$i][5] = $final[$i][0];
                $public_array[$i][6] = "00:00:00";
                $public_array[$i][7] = NULL;
                $public_array[$i][8] = "#ff0000";
            }
        }
        //printIt($public_array);
        if (isset($public_array))
        {
            $public_array = array_values($public_array);
            return $public_array;
        }
        else
        {
            return NULL;
        }
    }

}

if (!function_exists("entre2v2"))
{

    function entre2v2($text, $marqueurDebutLien, $marqueurFinLien) {
        $ar0 = explode($marqueurDebutLien, $text);
        $ar1 = explode($marqueurFinLien, $ar0[1]);
        $ar = trim($ar1[0]);
        return $ar;
    }

}


if (!function_exists("getCurrentUrl"))
{

    function getCurrentUrl() {

        return Route::getFacadeRoot()->current()->uri();
    }

}

if (!function_exists("custom_current_week_for_report"))
{

    function custom_current_week_for_report() {
        $date = date("Y-m-d");
        $newdate = strtotime('+2 day', strtotime($date));
        $newdate = date('Y-m-d', $newdate);

        $current_week = date("W", strtotime($newdate));

        return $current_week;
    }

}


if (!function_exists("CalenderTimeDiff")) {

    function CalenderTimeDiff($start_date, $start_time) {
        date_default_timezone_set("Asia/Dhaka");

// convert to unix timestamps
        $firstTime = strtotime("$start_date $start_time");
        $lastTime = strtotime(date('Y-m-d H:i'));

// perform subtraction to get the difference (in seconds) between times
        $interval = abs($lastTime - $firstTime);

        $minutes = round($interval / 60);

// return the difference
        return $minutes;
    }

}
