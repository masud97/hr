<?php

namespace App\Library;

use Illuminate\Support\Facades\Route;
use App\Models\LoginModel as LoginModel;

class PrivilegeCheckLib {

    public static function hasPrivilege($id) {

        $currentUrl = Route::getFacadeRoot()->current()->uri();

        switch ($currentUrl) {
            case 'notice/new':
                return PrivilegeCheckLib::checkPrivilege(9, $id);
            case 'notice/new/submit':
                return PrivilegeCheckLib::checkPrivilege(9, $id);
            case 'leave/approve-deny':
                return PrivilegeCheckLib::checkPrivilege(2, $id);
            case 'leave-history':
                return PrivilegeCheckLib::checkPrivilege(1, $id);
            case 'leave-history/search':
                return PrivilegeCheckLib::checkPrivilege(1, $id);
            case 'leave/all-pending-applications':
                return PrivilegeCheckLib::checkPrivilege(2, $id);
            case 'create-employee':
                return PrivilegeCheckLib::checkPrivilege(3, $id);
            case 'create-employee-submit':
                return PrivilegeCheckLib::checkPrivilege(3, $id);
            case 'terminate_employee':
                return PrivilegeCheckLib::checkPrivilege(6, $id);
            case 'terminate-employee-submit':
                return PrivilegeCheckLib::checkPrivilege(6, $id);
            case 'reset-password-submit':
                return PrivilegeCheckLib::checkPrivilege(7, $id);
            case 'add-merits-demerits':
                return PrivilegeCheckLib::checkPrivilege(5, $id);
            case 'edit-merits-demerits':
                return PrivilegeCheckLib::checkPrivilege(5, $id);
            case 'edit-employee-submit':
                return PrivilegeCheckLib::checkPrivilege(3, $id);
            case 'file-upload-admin':
                return PrivilegeCheckLib::checkPrivilege(11, $id);
            case 'archive-employee-list':
                return PrivilegeCheckLib::checkPrivilege(12, $id);
            case 'app-config':
                return PrivilegeCheckLib::checkPrivilege(14, $id);
            case 'app-config/submit':
                return PrivilegeCheckLib::checkPrivilege(14, $id);
            default :
                return FALSE;
        }
    }

    /* This function return true or false. If privileged exists it return true else false */

    public static function checkPrivilege($privilegeId, $id) {
        return LoginModel::whereRaw('FIND_IN_SET(' . $privilegeId . ',privilege)')->where(array('id' => $id, 'status' => 'active'))->exists();
    }

    public static function hasActive($id) {
        return LoginModel::where(array('id' => $id, 'status' => 'active'))->exists();
    }

}
