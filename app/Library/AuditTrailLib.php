<?php

namespace App\Library;
use DB;
use App\Models\AuditTrailModel;
use Request;



class AuditTrailLib{
    
    public static function addTrail($access_by,$description,$type)
    {
       
        AuditTrailModel::create(array(
            'ip'=>Request::ip(),
            'access_by'=>$access_by,
            'description'=>$description,
            'type'=>$type,
            'date'=>date('Y-m-d H:i:s')
        ));
    }
    
}