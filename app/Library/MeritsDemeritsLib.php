<?php

namespace App\Library;

use Illuminate\Support\Facades\Session;
use App\Models\MeritsDemeritsModel as MeritsDemeritsModel;
use DB;

class MeritsDemeritsLib {

    public static function add_merits_demerits($request) {

        $data = array(
            'employee_login_id' => $request->input('employee_id'),
            'type' => $request->input('type'),
            'description' => $request->input('details'),
            'insert_date' => time(),
            'status' => 'active'
        );


        if (MeritsDemeritsModel::create($data)) {

            return TRUE;
        } else {

            return FALSE;
        }
    }

    public static function all_merits_demerits($employee_id = NULL,$type='active') {

        if (is_null($employee_id)) {
            $employee_id = Session::get('id');
        }
        $where = array(
            'employee_login_id' => $employee_id,
            'status' => $type,
        );
        $data = MeritsDemeritsModel::select(array('id', 'employee_login_id', 'type', 'description'))
                ->where($where)
                ->get();

        if (count($data) > 0) {
            $i = 0;
            foreach ($data as $val) {
                $merits_demerits_data[$i]['id'] = $val->id;
                $merits_demerits_data[$i]['employee_login_id'] = $val->employee_login_id;
                $merits_demerits_data[$i]['type'] = $val->type;
                $merits_demerits_data[$i++]['description'] = $val->description;
            }
            return array('status' => TRUE, 'value' => $merits_demerits_data);
        } else {
            return array('status' => FALSE, 'msg' => 'No data found');
        }
    }

    public static function edit_merits_demerits($request) {

        $data = array(
            'description' => $request->input('details')
        );
        if (MeritsDemeritsModel::where('id', $request->input('id'))->update($data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public static function remove_merits_demerits($id) {

        $data = array(
            'status' => 'deactive'
        );
        if (MeritsDemeritsModel::where('id',$id)->update($data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
