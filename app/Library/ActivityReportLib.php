<?php

namespace App\Library;

use DB;
use App\Models\ActivityReportModel as ActivityReportModel;
use Request;

class ActivityReportLib {

    public static function get_max_week_for_report($employee_id) {
       
        $year = date('Y');


        $select = 'week_no';

        $where = array(
            "employee_login_id" => $employee_id,
            "status" => 1,
        );

        $d = ActivityReportModel::select($select)->where($where)->whereYear('insert_date', '=', $year)->orderBy('insert_date', 'DESC')->get(); //max week

        if (!count($d) > 0) {
            return NULL;
        } else {
            $max_week = $d->week_no;

            return $max_week;
        }
    }

}
