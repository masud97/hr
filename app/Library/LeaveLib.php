<?php

namespace App\Library;

use Illuminate\Support\Facades\Session;
use App\Models\LeaveDetailsModel as LeaveDetailsModel;
use App\Models\ApproverModel as ApproverModel;
use App\Models\TotalLeaveModel as TotalLeaveModel;
use App\Models\NotificationManagerModel as NotificationManagerModel;
use App\Library\EmployeeDetailsLib as EmployeeLib;
use DB;

class LeaveLib {

    static function casulaLeaveFull($leaveFrom, $leaveTo, $description, $attachment) {
        //check if apply leave amount is greater then zero
        //check if user has a pending leave appliaction
        //check if leaveTakehn and leave apply amount is greater then total leave then return an error
        $leaveAmount = LeaveLib::common($leaveFrom, $leaveTo);

        if ($leaveAmount['status'] === 'error') {
            return $leaveAmount;
        }

        $watingFor = LeaveLib::getWatingForId();
        if ($watingFor['status'] === 'error') {
            return $watingFor;
        }


        $totalLeave = LeaveLib::totalLeave();
        $leaveTaken = LeaveLib::totalLeaveTaken('casual');


        if (($leaveTaken + $leaveAmount['value']) > $totalLeave['casual']) {
            return array('status' => 'error', 'message' => 'You have not enough leave remain');
        }

        /* Now make an array and store it into the leave details table */
        $leaveDetails = array(
            'employee_login_id' => Session::get('id'),
            'waiting_for_employee_login_id' => $watingFor['value'],
            'leave_purpose' => 'casual',
            'leave_type' => 'full',
            'leave_from' => $leaveFrom,
            'leave_to' => $leaveTo,
            'leave_amount' => $leaveAmount['value'],
            'note' => $description,
            'status' => 'pending',
            'attachment' => $attachment,
            'apply_date' => date('Y-m-d')
        );

        $lastInsertedId = LeaveDetailsModel::create($leaveDetails)->id;
        if ($lastInsertedId) {
            LeaveLib::insertLeaveNotification($watingFor['value'], 'You have a pending application from ' . EmployeeLib::getEmployeeName(Session::get('id')), $lastInsertedId);
            return array('status' => 'success', 'value' => 'Your appliaction has been sent', 'watingFor' => $watingFor['value'], 'leave_amount' => $leaveAmount['value']);
        } else {
            return array('status' => 'error', 'message' => 'Unable to store data. Please try again later');
        }
    }

    static function sickLeaveFull($leaveFrom, $leaveTo, $description, $attachment) {

        $leaveAmount = LeaveLib::common($leaveFrom, $leaveTo);


        if ($leaveAmount['status'] === 'error') {
            return $leaveAmount;
        }

        $watingFor = LeaveLib::getWatingForId();
        if ($watingFor['status'] === 'error') {
            return $watingFor;
        }

        $totalLeave = LeaveLib::totalLeave();
        $leaveTaken = LeaveLib::totalLeaveTaken('sick');

        if (($leaveTaken + $leaveAmount['value']) > $totalLeave['sick']) {
            return array('status' => 'error', 'message' => 'You have not enough leave remain');
        }

        /* Now make an array and store it into the leave details table */
        $leaveDetails = array(
            'employee_login_id' => Session::get('id'),
            'waiting_for_employee_login_id' => $watingFor['value'],
            'leave_purpose' => 'sick',
            'leave_type' => 'full',
            'leave_from' => $leaveFrom,
            'leave_to' => $leaveTo,
            'leave_amount' => $leaveAmount['value'],
            'note' => $description,
            'status' => 'pending',
            'attachment' => $attachment,
            'apply_date' => date('Y-m-d')
        );

        $lastInsertedId = LeaveDetailsModel::create($leaveDetails)->id;
        if ($lastInsertedId) {
            LeaveLib::insertLeaveNotification($watingFor['value'], 'You have a pending application from ' . EmployeeLib::getEmployeeName(Session::get('id')), $lastInsertedId);
            return array('status' => 'success', 'value' => 'Your appliaction has been sent', 'watingFor' => $watingFor['value'], 'leave_amount' => $leaveAmount['value']);
        } else {
            return array('status' => 'error', 'message' => 'Unable to store data. Please try again later');
        }
    }

    static function casualLeaveHalf($leaveFrom, $leaveTo, $description, $attachment) {

        //check if apply leave amount is greater then zero
        //check if user has a pending leave appliaction
        //check if leaveTakehn and leave apply amount is greater then total leave then return an error

        if ($leaveFrom !== $leaveTo) {
            return array('status' => 'error', 'message' => 'On half day application, leave from date and leave to date will be same');
        }
        $totalLeave = LeaveLib::totalLeave();



        $totalHalfDay = LeaveLib::countHalfDay();

        if ($totalLeave['half'] <= $totalHalfDay) {
            return array('status' => 'error', 'message' => 'Your half day application limit is over');
        }

        $leaveAmount = LeaveLib::common($leaveFrom, $leaveTo);

        if ($leaveAmount['status'] === 'error') {
            return $leaveAmount;
        }

        if ($leaveAmount['value'] > 1) {
            /* you must specify the date */
            return array('status' => 'error', 'message' => 'On half day application, leave from date and leave to date will be same');
        }

        $leaveAmount['value'] = 0.5;

        $watingFor = LeaveLib::getWatingForIdHalf();
        if ($watingFor['status'] === 'error') {
            return $watingFor;
        }



        $leaveTaken = LeaveLib::totalLeaveTaken('casual');


        if (($leaveTaken + $leaveAmount['value']) > $totalLeave['casual']) {
            return array('status' => 'error', 'message' => 'You have not enough leave remain');
        }

        /* Now make an array and store it into the leave details table */
        $leaveDetails = array(
            'employee_login_id' => Session::get('id'),
            'waiting_for_employee_login_id' => $watingFor['value'],
            'leave_purpose' => 'casual',
            'leave_type' => 'half',
            'leave_from' => $leaveFrom,
            'leave_to' => $leaveTo,
            'leave_amount' => $leaveAmount['value'],
            'note' => $description,
            'status' => 'pending',
            'attachment' => $attachment,
            'apply_date' => date('Y-m-d')
        );

        $lastInsertedId = LeaveDetailsModel::create($leaveDetails)->id;
        if ($lastInsertedId) {
            LeaveLib::insertLeaveNotification($watingFor['value'], 'You have a pending application from ' . EmployeeLib::getEmployeeName(Session::get('id')), $lastInsertedId);
            return array('status' => 'success', 'value' => 'Your appliaction has been sent', 'watingFor' => $watingFor['value'], 'leave_amount' => $leaveAmount['value']);
        } else {
            return array('status' => 'error', 'message' => 'Unable to store data. Please try again later');
        }
    }

    static function sickLeaveHalf($leaveFrom, $leaveTo, $description, $attachment) {

        //check if apply leave amount is greater then zero
        //check if user has a pending leave appliaction
        //check if leaveTakehn and leave apply amount is greater then total leave then return an error

        if ($leaveFrom !== $leaveTo) {
            return array('status' => 'error', 'message' => 'On half day application, leave from date and leave to date will be same');
        }
        $totalLeave = LeaveLib::totalLeave();


        $totalHalfDay = LeaveLib::countHalfDay();
        if ($totalLeave['half'] <= $totalHalfDay) {
            return array('status' => 'error', 'message' => 'Your half day application limit is over');
        }

        $leaveAmount = LeaveLib::common($leaveFrom, $leaveTo);

        if ($leaveAmount['status'] === 'error') {
            return $leaveAmount;
        }

        if ($leaveAmount['value'] > 1) {
            /* you must specify the date */
            return array('status' => 'error', 'message' => 'On half day application, leave from date and leave to date will be same');
        }

        $leaveAmount['value'] = 0.5;

        $watingFor = LeaveLib::getWatingForIdHalf();
        if ($watingFor['status'] === 'error') {
            return $watingFor;
        }



        $leaveTaken = LeaveLib::totalLeaveTaken('sick');


        if (($leaveTaken + $leaveAmount['value']) > $totalLeave['sick']) {
            return array('status' => 'error', 'message' => 'You have not enough leave remain');
        }

        /* Now make an array and store it into the leave details table */
        $leaveDetails = array(
            'employee_login_id' => Session::get('id'),
            'waiting_for_employee_login_id' => $watingFor['value'],
            'leave_purpose' => 'sick',
            'leave_type' => 'half',
            'leave_from' => $leaveFrom,
            'leave_to' => $leaveTo,
            'leave_amount' => $leaveAmount['value'],
            'note' => $description,
            'status' => 'pending',
            'attachment' => $attachment,
            'apply_date' => date('Y-m-d')
        );

        $lastInsertedId = LeaveDetailsModel::create($leaveDetails)->id;
        if ($lastInsertedId) {
            LeaveLib::insertLeaveNotification($watingFor['value'], 'You have a pending application from ' . EmployeeLib::getEmployeeName(Session::get('id')), $lastInsertedId);
            return array('status' => 'success', 'value' => 'Your appliaction has been sent', 'watingFor' => $watingFor['value'], 'leave_amount' => $leaveAmount['value']);
        } else {
            return array('status' => 'error', 'message' => 'Unable to store data. Please try again later');
        }
    }

    static function specialLeave($leaveFrom, $leaveTo, $description, $attachment) {
        //check if apply leave amount is greater then zero
        //check if user has a pending leave appliaction
        //check if leaveTakehn and leave apply amount is greater then total leave then return an error
        $leaveAmount = timeDiff($leaveFrom, $leaveTo);
        /* check if user has any pending special leave application */
        if (LeaveDetailsModel::where(array('status' => 'pending', 'leave_type' => 'special'))->exists()) {
            return array('status' => 'error', 'message' => 'You have a pending application');
        }
        if ($leaveAmount <= 0) {
            /* leave amount must be greater then zero */
            return array('status' => 'error', 'message' => 'Leave amount must be greater then zero');
        }

        $watingFor = 14; //this will be fixed for md sir;

        /* Now make an array and store it into the leave details table */
        $leaveDetails = array(
            'employee_login_id' => Session::get('id'),
            'waiting_for_employee_login_id' => $watingFor,
            'leave_purpose' => 'special',
            'leave_type' => 'special',
            'leave_from' => $leaveFrom,
            'leave_to' => $leaveTo,
            'leave_amount' => $leaveAmount,
            'note' => $description,
            'status' => 'pending',
            'attachment' => $attachment,
            'apply_date' => date('Y-m-d')
        );

        $lastInsertedId = LeaveDetailsModel::create($leaveDetails)->id;
        if ($lastInsertedId) {
            LeaveLib::insertLeaveNotification($watingFor, 'You have a pending application from ' . EmployeeLib::getEmployeeName(Session::get('id')), $lastInsertedId);
            return array('status' => 'success', 'value' => 'Your appliaction has been sent', 'watingFor' => $watingFor, 'leave_amount' => $leaveAmount);
        } else {
            return array('status' => 'error', 'message' => 'Unable to store data. Please try again later');
        }
    }

    public static function insertLeaveNotification($notificationFor, $message, $applicationId) {
        $notification = array(
            'employee_login_id' => $notificationFor,
            'message' => $message,
            'url' => '/pending-applicatoins/application-view/' . $applicationId,
            'purpose' => 'leave',
            'insert_date_time' => date('Y-m-d h:i:s a'),
            'update_date_time' => NUll,
            'status' => 'unseen',
            'type' => 'success',
            'related_id' => $applicationId
        );


        NotificationManagerModel::create($notification);
    }

    public static function denyLeave($waitingForId, $applicationId, $note, $notificationId) {
        $application = LeaveLib::getApplication($applicationId, $waitingForId);
        if (!count($application) > 0) {
            return array('status' => 'error', 'message' => 'Invalid application id');
        }

        $oldNote = $application->note;
        $myName = EmployeeLib::getEmployeeName(Session::get('id'));

        $oldNote .= "<br/><br/>" . $myName . " : " . $note;

        $data = array(
            'note' => $oldNote,
            'status' => 'deny',
            'approve_deny_by' => $myName
        );

        $notification = array(
            'employee_login_id' => $application->employee_login_id,
            'message' => 'Your application has been deny by ' . $myName,
            'url' => "/profile/leave-details",
            'purpose' => 'leave',
            'insert_date_time' => date('Y-m-d h:i:s a'),
            'update_date_time' => NUll,
            'status' => 'unseen',
            'type' => 'danger',
            'related_id' => $applicationId
        );


        if (LeaveDetailsModel::where(array('id' => $applicationId))->update($data)) {
            //update old notification
            NotificationManagerModel::where(array('id' => $notificationId, 'related_id' => $applicationId))->update(array('status' => 'seen'));

            //create new notification
            NotificationManagerModel::create($notification);

            return array('status' => 'success', 'message' => 'Application deny', 'employee_id' => $application->employee_login_id);
        } else {
            return array('status' => 'error', 'message' => 'Unknown error occurred. Please try again later.');
        }
    }

    public static function approveLeave($waitingForId, $applicationId, $notificationId, $leaveFrom, $leaveTo, $note) {
        $application = LeaveLib::getApplication($applicationId, $waitingForId);
        if (!count($application) > 0) {
            return array('status' => 'error', 'message' => 'Invalid application id');
        }

        $oldNote = $application->note;
        $myName = EmployeeLib::getEmployeeName(Session::get('id'));

        $oldNote .= "<br/><br/>" . $myName . " : " . $note;

        $employeeId = $application->employee_login_id;
        
        if($application->leave_type === 'half')
        {
            $leaveAmount = 0.5;
        }
        else
        {
            $leaveAmount = timeDiff($leaveFrom, $leaveTo);
        }
        


        if ($leaveAmount <= 0) {
            /* leave amount must be greater then zero */
            return array('status' => 'error', 'message' => 'Leave amount must be greater then zero');
        }

        $totalLeaveTaken = LeaveLib::totalLeaveTaken($application->leave_purpose, $employeeId);
        $totalLeave = LeaveLib::totalLeave();

        if (($leaveAmount + $totalLeaveTaken) > $totalLeave[$application->leave_purpose]) {
            /**/
            return array('status' => 'error', 'message' => 'Not enough leave remain');
        }


        $allApprover = LeaveLib::getAllApprover($employeeId);
        if (!count($allApprover) > 0) {
            return array('status' => 'error', 'message' => 'No superviso or approver or final approver found. Please contact your HR manager');
        }

        $myId = Session::get('id');
        $nowWatingForWillBE = null;
        if ($allApprover->supervisor_login_id === $myId) {
            /* i am a supervisor.
             * so now return the approver id.
             * bcz when supervisor approve this application, its time for approver to approve or deny this application
             */
            $nowWatingForWillBE = $allApprover->approver_login_id;
        } else if ($allApprover->approver_login_id === $myId) {
            /* i am a approver.
             * so now return the final approver id.
             * bcz when approver approve this application, its time for final approver to approve or deny this application
             */
            $nowWatingForWillBE = $allApprover->final_approver_login_id;
        } else if ($allApprover->final_approver_login_id === $myId) {
            /*
             * I am final approver.
             * so application approved
             */
            $nowWatingForWillBE = 'approved';
        } else {

            /* i am noting. this application is not for me. */
            return array('status' => 'error', 'message' => 'Unknown error occurred. Please contact with your HR manager, to check your approval again.');
        }


        if ($nowWatingForWillBE === 'approved') {
            $data = array(
                'leave_from' => $leaveFrom,
                'leave_to' => $leaveTo,
                'leave_amount' => $leaveAmount,
                'note' => $oldNote,
                'status' => 'approve',
                'approve_deny_by' => $myName,
            );

            $notification = array(
                'employee_login_id' => $employeeId,
                'message' => 'Your application has been approved by ' . $myName,
                'url' => "/profile/leave-details",
                'purpose' => 'leave',
                'insert_date_time' => date('Y-m-d h:i:s a'),
                'update_date_time' => NUll,
                'status' => 'unseen',
                'type' => 'success',
                'related_id' => $applicationId
            );
        } else {
            $data = array(
                'waiting_for_employee_login_id' => $nowWatingForWillBE,
                'leave_from' => $leaveFrom,
                'leave_to' => $leaveTo,
                'leave_amount' => $leaveAmount,
                'note' => $oldNote
            );

            $notification = array(
                'employee_login_id' => $nowWatingForWillBE,
                'message' => 'You have a pending application from ' . EmployeeLib::getEmployeeName($employeeId),
                'url' => "/pending-applicatoins/application-view/" . $applicationId,
                'purpose' => 'leave',
                'insert_date_time' => date('Y-m-d h:i:s a'),
                'update_date_time' => NUll,
                'status' => 'unseen',
                'type' => 'success',
                'related_id' => $applicationId
            );
        }




        if (LeaveDetailsModel::where(array('id' => $applicationId))->update($data)) {
            /* seen old notifications */
            NotificationManagerModel::where(array('id' => $notificationId, 'related_id' => $applicationId))->update(array('status' => 'seen'));
            /* create new notifications */
            NotificationManagerModel::create($notification);
            return array('status' => 'success', 'message' => 'Application approved', 'nowWatingForWillBE' => $nowWatingForWillBE, 'employeeId' => $employeeId, 'purpose' => $application->leave_purpose, 'type' => $application->leave_type, 'from' => $leaveFrom, 'to' => $leaveTo, 'amount' => $leaveAmount);
        } else {
            return array('status' => 'error', 'message' => 'Unknown error occurred. Please try again later.');
        }
    }

    public static function changeApproveApplicationLeaveDate($leaveFrom, $leaveTo, $applicationId, $purpose, $employeeId) {
        $leaveAmount = timeDiff($leaveFrom, $leaveTo);


        if ($leaveAmount <= 0) {
            /* leave amount must be greater then zero */
            return array('status' => 'error', 'message' => 'Leave amount must be greater then zero');
        }

        $totalLeaveTaken = LeaveLib::totalLeaveTaken($purpose, $employeeId);
        $totalLeave = LeaveLib::totalLeave();

        if (($leaveAmount + $totalLeaveTaken) > $totalLeave[$purpose]) {
            /**/
            return array('status' => 'error', 'message' => 'You have not enough leave remain');
        }


        /* now every thing is fine. so update the leave date */
        $data = array(
            'leave_from' => $leaveFrom,
            'leave_to' => $leaveTo,
            'leave_amount' => $leaveAmount
        );

        if (LeaveDetailsModel::where(array('id' => $applicationId))->update($data)) {
            return array('status' => 'success', 'message' => 'Application date has been successfully changed', 'amount' => $leaveAmount);
        } else {
            return array('status' => 'error', 'message' => 'Unknown error occurred. Please Try again later.');
        }
    }

    public static function getLeaveHistroy($employeeLoginId, $year) {
        return LeaveDetailsModel::select('id', 'waiting_for_employee_login_id', 'leave_type', 'leave_purpose', 'leave_from', 'leave_to', 'leave_amount', 'note', 'status', 'attachment', 'apply_date')
                        ->where(array('employee_login_id' => $employeeLoginId))
                        ->whereYear('leave_from', '=', $year)
                        ->orderBy('apply_date', 'DESC')
                        ->get();
    }

    public static function getWatingForIdName($applicationId) {
        //employee_details
        //leave_details
        $WatingDetails = DB::table('leave_details')
                ->join('employee_details', 'leave_details.waiting_for_employee_login_id', '=', 'employee_details.employee_login_id')
                ->where('leave_details.id', $applicationId)
                ->select('leave_details.employee_login_id', 'employee_details.full_name')
                ->first();

        if (count($WatingDetails) > 0) {
            return array('status' => 'success', 'value' =>
                array('id' => $WatingDetails->employee_login_id,
                    'name' => $WatingDetails->full_name));
        } else {
            return array('status' => 'error', 'message' => 'No data found');
        }
    }

    private static function common($leaveFrom, $leaveTo) {
        /* convert date to number of days */
        $leaveAmount = timeDiff($leaveFrom, $leaveTo);


        if ($leaveAmount <= 0) {
            /* leave amount must be greater then zero */
            return array('status' => 'error', 'message' => 'Leave amount must be greater then zero');
        }


        /* Now check if employee have an pending application */

        $pendingLeave = LeaveDetailsModel::where(array('status' => 'pending', 'employee_login_id' => Session::get('id')))
                ->exists();

        if ($pendingLeave) {
            /* Error: Pending application found. User can not apply if he/she has an pending application */
            return array('status' => 'error', 'message' => 'You have a pending application');
        }

        return array('status' => 'success', 'value' => $leaveAmount);
    }

    private static function getWatingForId() {
        $approver = ApproverModel::select('supervisor_login_id', 'approver_login_id', 'final_approver_login_id')
                        ->where(array('employee_login_id' => Session::get('id')))->first();

        if (count($approver) > 0) {
            if (!is_null($approver->supervisor_login_id)) {
                return array('status' => 'success', 'value' => $approver->supervisor_login_id);
            } else if (!is_null($approver->approver_login_id)) {
                return array('status' => 'success', 'value' => $approver->approver_login_id);
            } else if (!is_null($approver->final_approver_login_id)) {
                return array('status' => 'success', 'value' => $approver->final_approver_login_id);
            } else {
                /* Error: No superviso or approver or final approver found */
                return array('status' => 'error', 'message' => 'No superviso or approver or final approver found. Please contact your HR manager');
            }
        } else {
            /* Error: No superviso or approver or final approver found */
            return array('status' => 'error', 'message' => 'No superviso or approver or final approver found. Please contact your HR manager');
        }
    }

    private static function getWatingForIdHalf() {
        $approver = ApproverModel::select('approver_login_id', 'final_approver_login_id')
                        ->where(array('employee_login_id' => Session::get('id')))->first();

        if (count($approver) > 0) {
            if (!is_null($approver->approver_login_id)) {
                return array('status' => 'success', 'value' => $approver->approver_login_id);
            } else if (!is_null($approver->final_approver_login_id)) {
                return array('status' => 'success', 'value' => $approver->final_approver_login_id);
            } else {
                /* Error: No superviso or approver or final approver found */
                return array('status' => 'error', 'message' => 'No superviso or approver or final approver found. Please contact your HR manager');
            }
        } else {
            /* Error: No superviso or approver or final approver found */
            return array('status' => 'error', 'message' => 'No superviso or approver or final approver found. Please contact your HR manager');
        }
    }

    private static function getAllApprover($employeeId) {
        return ApproverModel::select('supervisor_login_id', 'approver_login_id', 'final_approver_login_id')
                        ->where(array('employee_login_id' => $employeeId))->first();
    }

//    private static function whoAmIForThisEmployee($employeeId)
//    {
//        $myId = Session::get('id');
//        ApproverModel::
//    }

    public static function getWatingApplication() {

        $applications = LeaveDetailsModel::select('id', 'employee_login_id', 'leave_purpose', 'leave_type', 'leave_from', 'leave_to', 'leave_amount', 'note', 'attachment', 'apply_date')
                ->where(array('waiting_for_employee_login_id' => Session::get('id'), 'status' => 'pending'))
                ->get();
    }

    public static function getWatingApplicationShort($id) {
        return DB::table('leave_details')
                        ->join('employee_details', 'employee_details.employee_login_id', '=', 'leave_details.employee_login_id')
                        ->select('employee_details.full_name', 'leave_details.id', 'leave_details.employee_login_id', 'leave_details.leave_purpose', 'leave_details.leave_type', 'leave_details.leave_amount', 'leave_details.apply_date', 'leave_details.leave_from', 'leave_details.leave_to')
                        ->where(array('waiting_for_employee_login_id' => $id, 'status' => 'pending'))
                        ->get();
    }

    public static function getApplication($applicationId, $waitingForId) {
        return DB::table('leave_details')
                        ->join('employee_details', 'employee_details.employee_login_id', '=', 'leave_details.employee_login_id')
                        ->select('employee_details.full_name', 'employee_details.email_work', 'employee_details.image', 'leave_details.employee_login_id', 'leave_details.leave_purpose', 'leave_details.leave_type', 'leave_details.leave_from', 'leave_details.leave_to', 'leave_details.leave_amount', 'leave_details.attachment', 'leave_details.note', 'leave_details.apply_date')
                        ->where(array('leave_details.waiting_for_employee_login_id' => $waitingForId, 'leave_details.id' => $applicationId, 'leave_details.status' => 'pending'))
                        ->first();
    }

    public static function totalLeaveTaken($purpose, $employee_id = NULL) {
        if (is_null($employee_id)) {
            $employee_id = Session::get('id');
        }
        return LeaveDetailsModel::where(array('employee_login_id' => $employee_id, 'leave_purpose' => $purpose, 'status' => 'approve'))
                        ->whereYear('leave_from', '=', date('Y'))
                        ->sum('leave_amount');
    }

    public static function totalLeave() {
        $totalLeave = TotalLeaveModel::select('total_casual_leave', 'total_sick_leave', 'halfday')->first();
        if (count($totalLeave) > 0) {
            return array('casual' => $totalLeave->total_casual_leave, 'sick' => $totalLeave->total_sick_leave, 'half' => $totalLeave->halfday);
        } else {
            return array('casual' => 0, 'sick' => 0, 'half' => 0);
        }
    }

    public static function countHalfDay($employee_id = NULL) {
        if (is_null($employee_id)) {
            $employee_id = Session::get('id');
        }
        return LeaveDetailsModel::where(array('employee_login_id' => $employee_id, 'status' => 'approve', 'leave_type' => 'half'))
                        ->whereYear('leave_from', '=', date('Y'))
                        ->count();
    }

    public static function withdrawApplication($id, $appId) {
        return LeaveDetailsModel::where(array('employee_login_id' => $id, 'status' => 'pending', 'id' => $appId))
                        ->update(array('status' => 'withdraw'));
    }

    public static function get_leave_interval_yearly_calender() {

        $select = array('employee_login_id', 'leave_from', 'leave_to');

        $where = array(
            'status' => 'approve'
        );
        $temp_leave_for_emp = LeaveDetailsModel::select($select)->where($where)->whereYear('apply_date', '=', date('Y'))->get();
        if (!count($temp_leave_for_emp) > 0) {

            $temp_leave_for_emp = NULL;
        }

        if (!is_null($temp_leave_for_emp)) {

            $emp_leave_array = NULL;
            $i = 0;

            foreach ($temp_leave_for_emp as $value) {
                if (!is_null(EmployeeLib::check_employee_status_by_login_id($value->employee_login_id))) {
                    $emp_leave_array[$i][0] = $value->employee_login_id;
                    $emp_leave_array[$i][1] = NULL;
                    $emp_leave_array[$i][2] = EmployeeLib::getEmployeeName($value->employee_login_id) . "'s leave";
                    $emp_leave_array[$i][3] = $value->leave_from;
                    $emp_leave_array[$i][4] = "00:00:00";
                    $emp_leave_array[$i][5] = $value->leave_to;
                    $emp_leave_array[$i][6] = "23:59:59";
                    $emp_leave_array[$i][7] = NULL;
                    $emp_leave_array[$i][8] = "#396CCF";
                    $i++;
                }
            }
        }
        if (isset($emp_leave_array)) {
            $emp_leave_array = array_values($emp_leave_array);
            return $emp_leave_array;
        } else {
            return NULL;
        }
    }

}
