<?php

namespace App\Library;

use App\Models\EmployeeDetailsModel as EmployeeDetailsModel;
use App\Models\LoginModel as LoginModel;
use App\Models\ApproverModel as ApproverModel;
//use App\Models\ApproverModel as ApproverModel;
use DB;

class EmployeeDetailsLib {

    static function getUserInfo($selectedItem = null, $where) {
        if (is_null($selectedItem)) {
            $userInfo = EmployeeDetailsModel::where($where)->first();
        } else {
            $userInfo = EmployeeDetailsModel::select($selectedItem)->where($where)->first();
        }


        return $userInfo;
    }

    static function getAllActiveUserInfo($selectedItem = null) {
        $userInfo = EmployeeDetailsModel::select($selectedItem)
                ->join('login', 'employee_details.employee_login_id', '=', 'login.id')
                ->where('login.status', 'active')
                ->get();

        return $userInfo;
    }

    static function all_active_emp_id_name() {
        $all_emp = DB::table('login')
                ->join('employee_details', 'login.id', '=', 'employee_details.employee_login_id')
                ->where(array('status' => 'active'))
                ->select('employee_details.employee_login_id', 'employee_details.full_name')
                ->get();
        $all_emp_ = NULL;
        $i = 0;
        foreach ($all_emp as $val) {

            $all_emp_[$i]['id'] = $val->employee_login_id;
            $all_emp_[$i++]['name'] = $val->full_name;
        }

        if (count($all_emp_) > 0) {
            return $all_emp_;
        }

        return NULL;
    }

    static function get_company_name() {
        $company = EmployeeDetailsModel::select('company_name')->groupBy('company_name')->get();
        $companyName = null;
        if (count($company) > 0) {
            foreach ($company as $row) {
                $companyName[] = $row->company_name;
            }
        }

        return $companyName;
    }

    static function getEmployeeNameByUsingCompanyName($companyName) {
        return EmployeeDetailsModel::select('employee_login_id')->where(array('company_name' => $companyName))->get();
    }

    static function get_employee_id_by_login_id($id) {
        $emp = DB::table('login')
                ->where(array('status' => 'active', 'id' => $id))
                ->select('employee_id')
                ->first();


        if (is_null($emp)) {
            return NULL;
        } else {
            return $emp->employee_id;
        }
    }

    static function get_deactive_employee_id_by_login_id($id) {
        $emp = DB::table('login')
                ->where(array('status' => 'deactive', 'id' => $id))
                ->select('employee_id')
                ->first();


        if (is_null($emp)) {
            return NULL;
        } else {
            return $emp->employee_id;
        }
    }

    static function check_employee_status_by_login_id($id) {
        $emp = DB::table('login')
                ->where(array('status' => 'active', 'id' => $id))
                ->select('id')
                ->first();

        if (!count($emp) > 0) {
            return NULL;
        } else {
            return $emp->id;
        }
    }

    static function file_upload($request, $employee_id = NULL) {
        
        $attachment_doc = null;
        if ($request->hasFile('file_upload')) {
            if ($request->file_upload->getClientOriginalExtension() === 'pdf') {

                if ($request->file('file_upload')->isValid()) {
                    $folder = base_path() . '/public/assets/user-doc';
                    $file = $request->file('file_upload');
                    $type = $file->getClientOriginalExtension();
                    $attachment_doc = time() . $request->input('employee_id') . '.' . $type;
                    $request->file('file_upload')->move($folder, $attachment_doc);
                    return $attachment_doc;
                } else {
                    return FALSE;
                }
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }





        $prev_file_json = DB::table('login')
                ->where(array('status' => 'active', 'employee_login_id' => $request->input('employee_id')))
                ->select('document_upload')
                ->first();

        $updated_data = array(
            array(
                "category" => $request->input('file_category'),
                "title" => $request->input('file_title'),
                "file_name" => $attachment_doc
            )
        );
        if (is_null($prev_file_json)) {
            $data = json_encode($data);
        } else {
            $temp_array = json_decode($prev_file_json->document_upload);
            $data = array_values(array_merge($temp_array, $updated_data));
            $data = json_encode($data);
        }

        $update_document = EmployeeDetailsModel::where(array('employee_login_id' => $request->input('employee_id')))->update($data);
        if ($update_document) {
            return TRUE;
        }
        return FALSE;
    }

    static function getEmployeeName($id) {
        $emp = DB::table('employee_details')
                ->where(array('employee_login_id' => $id))
                ->select('full_name')
                ->first();

        if (!count($emp) > 0) {
            return NULL;
        } else {
            return $emp->full_name;
        }
    }

    static function getEmployeeAssignApproverById($id) {
        $emp = DB::table('assign_approver')
                ->where(array('employee_login_id' => $id))
                ->select('id', 'supervisor_login_id', 'approver_login_id', 'final_approver_login_id')
                ->first();

        if (!count($emp) > 0) {
            return NULL;
        } else {
            return $emp;
        }
    }

    static function getEmployeeWorkEmailById($id) {
        $emp = DB::table('employee_details')
                ->where(array('employee_login_id' => $id))
                ->select('email_work')
                ->first();

        if (!count($emp) > 0) {
            return NULL;
        } else {
            return $emp->email_work;
        }
    }

    static function get_emp_birthday_monthly() {

        $temp = EmployeeDetailsLib::getAllActiveUserInfo(array('employee_login_id', 'full_name', 'date_of_birth'));

        if (count($temp) > 0) {

            $emp_bd_array = NULL;
            $i = 0;
            foreach ($temp as $value) {
                $temp_bd = $value->date_of_birth;



                if (!is_null($temp_bd)) {
                    $date = date('d', strtotime($temp_bd));
                    $month = date('m', strtotime($temp_bd));
                    //   if ($date == date('d') and $month == date('m')) {


                    $temp_emp_details = $value->full_name;
                    //   printIt($temp_emp_details);

                    $emp_bd_array[$i][0] = $value->employee_login_id;

                    $emp_bd_array[$i][1] = NULL;
                    if (!is_null($temp_emp_details)) {
                        $emp_bd_array[$i][2] = $value->full_name . "'s Birthday";
                    } else {
                        $emp_bd_array[$i][2] = NULL;
                    }
                    $emp_bd_array[$i][3] = date("Y") . "-" . $month . "-" . $date;
                    $emp_bd_array[$i][4] = "00:00:00";
                    $emp_bd_array[$i][5] = date("Y") . "-" . $month . "-" . $date;
                    $emp_bd_array[$i][6] = "23:59:59";
                    $emp_bd_array[$i][7] = NULL;
                    $emp_bd_array[$i][8] = "#F89406";
                    $i++;
                }
            }
        }
        // printIt($emp_bd_array);
        if (isset($emp_bd_array)) {
            $emp_bd_array = array_values($emp_bd_array);
            return $emp_bd_array;
        } else {
            return NULL;
        }
    }

    static function edit_approver_checking($existingApprover, $employee_id, $supervisor, $approver, $final_approver, $request) {
        ////\\\\\



        if ($existingApprover->final_approver_login_id != $final_approver) {
            $temp_check = ApproverModel::select('id')
                    ->where('supervisor_login_id', $existingApprover->final_approver_login_id)
                    ->where('approver_login_id', '!=', $existingApprover->final_approver_login_id)
                    ->where('final_approver_login_id', '!=', $existingApprover->final_approver_login_id)
                    ->first();
            if (count($temp_check) > 0) {
                $tempPriv = '1,2,4,5,7,8,10,11,13';


                LoginModel::where('id', $existingApprover->final_approver_login_id)
                        ->update(array('privilege' => $tempPriv));
            } else {
                $temp_check = ApproverModel::select('id')
                        ->where('supervisor_login_id', '!=', $existingApprover->final_approver_login_id)
                        ->where('approver_login_id', '!=', $existingApprover->final_approver_login_id)
                        ->where('final_approver_login_id', '!=', $existingApprover->final_approver_login_id)
                        ->first();
                if (count($temp_check) > 0) {
                    $tempPriv = NULL;


                    LoginModel::where('id', $existingApprover->final_approver_login_id)
                            ->update(array('privilege' => $tempPriv));
                }
            }
        }
        if ($existingApprover->approver_login_id != $approver) {
            $temp_check = ApproverModel::select('id')
                    ->where('supervisor_login_id', $existingApprover->approver_login_id)
                    ->where('approver_login_id', '!=', $existingApprover->approver_login_id)
                    ->where('final_approver_login_id', '!=', $existingApprover->approver_login_id)
                    ->first();
            if (count($temp_check) > 0) {
                $tempPriv = '1,2,4,5,7,8,10,11,13';
                LoginModel::where('id', $existingApprover->approver_login_id)
                        ->update(array('privilege' => $tempPriv));
            } else {
                $temp_check = ApproverModel::select('id')
                        ->where('supervisor_login_id', '!=', $existingApprover->approver_login_id)
                        ->where('approver_login_id', '!=', $existingApprover->approver_login_id)
                        ->where('final_approver_login_id', '!=', $existingApprover->approver_login_id)
                        ->first();
                if (count($temp_check) > 0) {
                    $tempPriv = NULL;
                    LoginModel::where('id', $existingApprover->approver_login_id)
                            ->update(array('privilege' => $tempPriv));
                }
            }
        }




        ////\\\
        //        update supervisor , approver , final approver privilege

        if (!is_null($supervisor)) {

            $tempPriv = array(
                1, 2, 4, 5, 7, 8, 10, 11, 13
            );

            $supOldPriv = LoginModel::select('privilege')->where(array('status' => 'active', 'id' => $supervisor))->first();
            if (!count($supOldPriv) > 0) {
                return redirect()->back()->withErrors('Selected employee is not a valid employee.');
            }

            if ($supOldPriv->privilege == NULL) {
                $supOldPriv = array();
            } else {
                $supOldPriv = explode(',', $supOldPriv->privilege);
            }

            $supPriv = array_merge($tempPriv, $supOldPriv);
            $supPriv = array_unique($supPriv);
            sort($supPriv);
            $supPriv = array_values($supPriv);
            $supPriv = implode(',', $supPriv);


            LoginModel::where('id', $supervisor)
                    ->update(array('privilege' => $supPriv));
        }
        if (!is_null($approver)) {
            $appPriv = '1,2,3,4,5,6,7,8,9,10,11,12,13,14';
            LoginModel::where('id', $approver)
                    ->update(array('privilege' => $appPriv));
        }
        if (!is_null($final_approver)) {
            $fAppPriv = '1,2,3,4,5,6,7,8,9,10,11,12,13,14';
            LoginModel::where('id', $final_approver)
                    ->update(array('privilege' => $fAppPriv));
        }
    }

}
