<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */


//test 
//Route::get('login', 'Login\LoginController@login');
Route::get('/get-name-and-image/{employeeId}', 'Login\LoginController@getNameAndImage')->where(array('employeeId' => "[0-9]+"));
Route::get('cli', 'BackgroundProcess@index');
/* Login route */
Route::get('/', 'Login\LoginController@login');
Route::post('/login-check', 'Login\LoginController@loginCheck');
Route::get('/logout', 'Login\LoginController@logout');

/* Forget password route */
Route::post('send-token', 'ForgetPassword\ForgetPasswordController@sendToken');
Route::get('forget-password-reset', 'ForgetPassword\ForgetPasswordController@reset');
Route::post('forget-password-reset-submit', 'ForgetPassword\ForgetPasswordController@reset_submit');


Route::get('test', 'Test@index');


Route::group(['middleware' => ['loginCheck']], function() {
    Route::get('mark-all-notification-as-read','Ajax@mark_all_as_read');
    
    Route::post('create-new-company', 'ConfigController@add_new_company');
    Route::get('/dashboard', 'DashboardController@index');

//leave open for all
    Route::get('/apply-for-leave', 'LeaveController@index');
    Route::post('/apply-for-leave/submit', 'LeaveController@submit');
    Route::get('/profile/leave-details/{notificationId?}', 'LeaveController@leaveDetails')->where(array('notificationId' => '[0-9]+'));
    Route::get('profile/leave-details/withdraw-appliaction/{applicationId}', 'LeaveController@withdrawApplication')->where('applicationId', '[0-9]+');

//this route is not open for all. but ther is some technical issue thats why we mange the privilege in controller not in middelware
    Route::get('/pending-applicatoins/application-view/{applicationId}/{notificationId?}', 'LeaveController@applicationView')->where(array('applicationId' => '[0-9]+', 'notificationId' => '[0-9]+'));
    Route::get('/profile/leave-details/cancle-approve-application/{applicationId}/{employeeId}', 'LeaveController@cancleLeaveApplication')->where(array('applicationId' => '[0-9]+', 'employeeId' => '[0-9]+'));
    Route::post('/profile/leave-details/change-approve-application-date/{applicationId}/{employeeId}', 'LeaveController@cahngeApproveApplicationDate')->where(array('applicationId' => '[0-9]+', 'employeeId' => '[0-9]+'));
    Route::get('admin-employee-leave-details/{employee_id}', 'LeaveController@admin_leaveDetails_of_employee')->where('employee_id', '[0-9]+');




//    employee 
    Route::get('all-employee', 'Employee@all_emp');


//terminate 
//this route is not open for all. but ther is some technical issue thats why we mange the privilege in controller not in middelware
    Route::get('terminate-employee/{employee_id}', 'Employee@terminate_employee')->where('employee_id', '[0-9]+');




//document

    Route::get('document/{employee_id}', 'Employee@document')->where('employee_id', '[0-9]+');

    Route::post('change-password-submit', 'Employee@change_password_submit');

//this route is not open for all. but ther is some technical issue thats why we mange the privilege in controller not in middelware
    Route::get('admin-employee-profile-view/{employee_id}/{notificationId?}', 'Employee@admin_employee_profile_view')->where('employee_id', '[0-9]+')->where('notificationId', '[0-9]+');

//this route is not open for all. but ther is some technical issue thats why we mange the privilege in controller not in middelware
    Route::post('change-profile-picture/{employee_id}', 'Employee@change_profile_pic_admin')->where('employee_id', '[0-9]+');

    Route::post('change-profile-picture', 'Employee@change_profile_pic_employee');
//profile
    Route::get('/profile', 'Employee@profile');
    Route::get('/edit-profile', 'Employee@edit_profile');
//this route is not open for all. but ther is some technical issue thats why we mange the privilege in controller not in middelware
    Route::get('/admin-edit-employee-profile/{employee_id}', 'Employee@admin_edit_profile')->where('employee_id', '[0-9]+');

    Route::get('/edit-employee-profile/{notifiaction_id?}', 'Employee@edit_profile_emp')->where('notifiaction_id','[0-9]+');

    Route::post('edit-employee-by-emp-submit', 'Employee@edit_employee_by_emp_submit');
//Merits- Demerits
//this route is not open for all. but ther is some technical issue thats why we mange the privilege in controller not in middelware
    Route::get('/admin-view-merits-demerits/{employee_id}', 'Employee@view_merits_demerits_admin_of_employee')->where('employee_id', '[0-9]+');
//this route is not open for all. but ther is some technical issue thats why we mange the privilege in controller not in middelware
    Route::get('/remove-merits-demerits/{id}', 'Employee@remove_merits_demerits')->where('id', '[0-9]+');
    Route::get('/view-merits-demerits', 'Employee@view_merits_demerits');



//approver
    Route::post('edit-approver/{employee_id}', 'Employee@edit_approver_submit')->where('employee_id', '[0-9]+');

//notice; this section is open for employee and admin
    Route::get('/notice', 'Notice\NoticeController@allNotice');
    Route::get('/notice/view/{noticeId}/{notificatonId?}', 'Notice\NoticeController@viewNotice')->where(array('noticeId' => '[0-9]+', 'notificationId' => '[0-9]+'));
//this route is not open for all. but ther is some technical issue thats why we mange the privilege in controller not in middelware
    Route::get('notice/withdraw/{notificatonId}', 'Notice\NoticeController@withdrawNotice')->where(array('noticeId' => '[0-9]+'));

//Calender
    Route::get('calender', 'Calender@index');
    Route::get('all-event/{id?}', 'Calender@all_event_list')->where('id', '[0-9]+');
    Route::get('remove-event/{id}', 'Calender@remove_event')->where('id', '[0-9]+');
    Route::post('create-calender', 'Calender@create_calender_submit');


//ajax call only

    Route::get('/ajax/get-notifications', 'Ajax@checkNotifications');
    Route::get('/ajax/get-notifications-leave', 'Ajax@checkNotificationsLeave');

    //gallery
    Route::get('gallery/{employeeId?}', 'GalleryController@index')->where(array('employeeId' => '[0-9]+'));


    //archive employee profile 
    Route::get('/archive-employee-profile-view/{emplyee_id}', 'ArchiveEmployee@profile_view')->where('employee_id', '[0-9]+');
    Route::get('/archive-view-merits-demerits/{emplyee_id}', 'ArchiveEmployee@view_merits_demerits_of_employee')->where('employee_id', '[0-9]+');
    Route::get('/restore-employee/{emplyee_id}', 'ArchiveEmployee@restore_employee')->where('employee_id', '[0-9]+');


    /// activity report


    Route::get('/admin-activity-report', 'ActivityReport@report_list_for_admin');
    Route::get('/activity-report-employee/{id?}', 'ActivityReport@report_list_for_emp')
            ->where('id', '[0-9]+');
    Route::get('/add-new-report', 'ActivityReport@add_new_report');
    Route::get('/edit-report/{id}', 'ActivityReport@edit_report')->where('id', '[0-9]+');
    Route::post('/activity-report-submit', 'ActivityReport@report_submit');
    Route::post('/edit-report-submit', 'ActivityReport@edit_report_submit');
    Route::get('/report-details/{id}/{employee_id?}/{notification_id?}', 'ActivityReport@report_details')
            ->where('id', '[0-9]+')
            ->where('emplyee_id', '[0-9]+')
            ->where('notification_id', '[0-9]+');
    Route::get('/pending-report/{id?}', 'ActivityReport@pending_for_approver')
            ->where('id', '[0-9]+');
    Route::post('/rate-report', 'ActivityReport@rate_report');
    Route::get('/activity-report-employee-notify/{id?}', 'ActivityReport@employee_notify')
            ->where('id', '[0-9]+');







    /*     * **********************************************Middleware****************************************** */





    Route::group(['middleware' => ['checkPrivileges']], function() {

        //config
        Route::get('/app-config', 'ConfigController@index');
        Route::post('/app-config/submit', 'ConfigController@submit');

//notice
        Route::get('/notice/new', 'Notice\NoticeController@newNotice');
        Route::post('/notice/new/submit', 'Notice\NoticeController@submitNotice');

//leave
        Route::post('/leave/approve-deny', 'LeaveController@approveOrDenyApplication');
        Route::get('/leave-history', 'LeaveController@leaveHistroy');
        Route::get('/leave-history/search', 'LeaveController@leaveHistorySearch');
        Route::get('/leave/all-pending-applications', 'LeaveController@allPendingApplications');

//employee
        Route::get('create-employee', 'Employee@create_emp');
        Route::post('create-employee-submit', 'Employee@create_emp_submit');

//reset password
        Route::post('reset-password-submit', 'Employee@reset_password_submit');



//terminate 
        Route::post('terminate-employee-submit', 'Employee@terminate_employee_submit');
//add merits/ demerits 
        Route::post('/add-merits-demerits', 'Employee@add_merits_demerits');
// edit merits demerits
        Route::post('/edit-merits-demerits', 'Employee@edit_merits_demerits');
//edit emp admin 

        Route::post('edit-employee-submit', 'Employee@edit_employee_submit');
//file upload
        Route::post('/file-upload-admin', 'Employee@file_upload_admin');
// edit emp submit 
        Route::post('edit-employee-submit', 'Employee@edit_employee_submit');

        //archive emp
        Route::get('archive-employee-list', 'ArchiveEmployee@all_emp');
    });
});


