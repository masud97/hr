<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Requests;

class ConfigController extends Controller {

    public function __construct() {
        
    }

    public function index() {
        $totalLeave = \App\Library\LeaveLib::totalLeave();
        $total_com = \App\Models\CompanyModel::select(array('companyName'))
                ->where('status', 'active')
                ->first();
        if (!count($total_com) > 0) {
            $total_com = NULL;
        } else {
            
        }
        $data = array(
            'title' => 'Leave Config',
            'customJs' => 'app-config',
            'totalLeave' => $totalLeave,
            'totalCom' => $total_com
        );
        return view('content.config', $data);
    }

    public function submit(Request $request) {
        $this->validate($request, [

            'casual-leave' => 'required|int|min:0',
            'sick-leave' => 'required|int|min:0',
            'half-day' => 'required|int|min:0'
        ]);

        $data = \App\Models\TotalLeaveModel::select('id')->first();
        if (count($data) > 0) {
            $updateData = array(
                'total_casual_leave' => $request->input('casual-leave'),
                'total_sick_leave' => $request->input('sick-leave'),
                'halfday' => $request->input('half-day')
            );
            /* data exist so update the data */
            \App\Models\TotalLeaveModel::where(array('id' => $data->id))->update($updateData);

            return redirect()->back()->withInput()->with('success', 'Data successfully updated');
        } else {
            /* insert new data bcz no data found in db */
            $InsertData = array(
                'total_casual_leave' => $request->input('casual-leave'),
                'total_sick_leave' => $request->input('sick-leave'),
                'halfday' => $request->input('half-day')
            );

            \App\Models\TotalLeaveModel::create($InsertData);

            return redirect()->back()->withInput()->with('success', 'Data successfully inserted');
        }
    }
    
    public function add_new_company(Request $request) {
        if (!hasPrivilege(14)) {
            return redirect()
                            ->withErrors('Access Denied');
        }
        $this->validate($request, [

            'companyName' => 'required'
        ]);
        $total_com = \App\Models\CompanyModel::select(array('id', 'companyName'))
                ->where('status', 'active')
                ->first();
        if (!count($total_com) > 0) {
       
            $data = array(
                'companyName' => $request->input('companyName'),
                'insert_date_time' => time(),
                'status' => 'active'
            );

            if (\App\Models\CompanyModel::create($data)) {
                return redirect()
                                ->back()
                                ->with('success', 'New Company created');
            } else {
                return redirect()
                                ->back()
                                ->withErrors('New company creation failed');
            }
        } else {
            
            $data = array(
                'companyName' => $request->input('companyName'),
                'update_date_time' => time(),
            );

            if (\App\Models\CompanyModel::where('id', $total_com->id)->update($data)) {
                return redirect()
                                ->back()
                                ->with('success', 'Company updated');
            } else {
                return redirect()
                                ->back()
                                ->withErrors('Company update failed');
            }
        }
    }

}
