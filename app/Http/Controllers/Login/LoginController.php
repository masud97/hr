<?php

namespace App\Http\Controllers\Login;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\Models\LoginModel as LoginModel;
use App\Library\EmployeeDetailsLib as EmpLib;

class LoginController extends Controller {

    public function __construct() {
        /* no_cash is a helper function. Its remove the browser cash. */
        no_cash();
    }

    public function index(Request $request) {
        if ($request->session()->get('is_login')) {
            return redirect('/dashboard');
        }

       

        return view('login'); 
    }

    public function login(Request $request) {
        if ($request->session()->get('is_login')) {
            return redirect('/dashboard');
        }


 $title = array('title' => 'Human Resource - Login');
        return view('login_advance',$title);
    }

    public function getNameAndImage(Request $request, $employeeId) {
        $data = DB::table('login')
                        ->join('employee_details', 'login.id', '=', 'employee_details.employee_login_id')
                        ->select('employee_details.full_name', 'employee_details.image')
                        ->where(array('login.employee_id' => $employeeId))->first();
        if (count($data) > 0) {
            if (is_null($data->image)) {
                $url = asset('assets/img/user-image/noimagefound.jpg');
            } else {
                $url = asset('assets/img/user-image/' . $data->image);
            }
            $d = array('status' => TRUE, 'full_name' => $data->full_name, 'image' => $url);
        } else {
            $d = array('status' => FALSE, 'message' => 'Invalid empoyee id');
        }

        echo json_encode($d);
    }

    public function loginCheck(Request $request) {

        if ($request->session()->get('is_login')) {
            return redirect('/dashboard');
        }
        $title = array('title' => 'Human Resource - Login');
        $this->validate($request, [
            'employee_id' => 'numeric|required|min:3',
            'password' => 'required|max:20'
        ]);

        $user = LoginModel::select('id', 'password')
                ->where(array('status' => 'active', 'employee_id' => $request->employee_id))
                ->first();


        if ($user) {
            $check = ck_password($request->password, $user->password);
            if ($check) {
                /* get employee full name */
                $result = EmpLib::getUserInfo('full_name', array('employee_login_id' => $user->id));

                if (!is_null($result)) {
                    $fullName = $result->full_name;
                } else {
                    $fullName = "No Name Found";
                }

                $data = array(
                    'name' => $fullName,
                    'employee_id' => $request->employee_id,
                    'id' => $user->id,
                    'is_login' => TRUE
                );
                $request->session()->put($data);
                \App\Library\AuditTrailLib::addTrail($fullName, 'Employee has been successfully logged in.', 'success');
                return redirect('/dashboard');
            } else {


                \App\Library\AuditTrailLib::addTrail('Unknown', 'Invalid password. Employee id provided = ' . $request->employee_id, 'error');
                return view('login_advance', $title)->withErrors('Invalid employee id or password.');
            }
        } else {
            \App\Library\AuditTrailLib::addTrail('Unknown', 'Invalid employee id. Employee id provided = ' . $request->employee_id, 'error');
            return view('login_advance', $title)->withErrors('Invalid employee id or password');
        }
    }

    public function logout(Request $request) {
        //destroy session
        $request->session()->flush();
        return redirect('/');
    }

}
