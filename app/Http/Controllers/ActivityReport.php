<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\ApproverModel as ApproverModel;
use App\Models\EmployeeDetailsModel as EmpModel;
use App\Library\EmployeeDetailsLib as empLib;
use App\Models\ActivityReportModel as ActivityReportModel;
use App\Library\ActivityReportLib as ActivityReportLib;
use Illuminate\Support\Facades\Validator;
use DB;
use Illuminate\Support\Facades\Mail;

class ActivityReport extends Controller {

    public function __construct() {
        DB::enableQueryLog();
    }

//    public function index() {
//
//        if (hasPrivilege(13)) {
//            ActivityReport::report_list_for_admin();
//        } else {
//            ActivityReport::report_list_for_emp();
//        }
//    }

    public function report_list_for_admin(Request $request) {

        $employee_id = $request->session()->get('id');




        $temp = ApproverModel::select(array('employee_login_id'))
                ->where('supervisor_login_id', $employee_id)
                ->orWhere('approver_login_id', $employee_id)
                ->orWhere('final_approver_login_id', $employee_id)
                ->get();

        if (count($temp) > 0) {
            // echo "masi"; die();
            $emp_list_array = array();

            $i = 0;
            foreach ($temp as $value) {
                $temp_emp_check = \App\Models\LoginModel::where(array('status' => 'active', 'id' => $value->employee_login_id))->first();


                if (count($temp_emp_check)) {

                    $emp_list_array[$i][0] = $value->employee_login_id;
                    $emp_list_array[$i][1] = empLib::getEmployeeName($value->employee_login_id);

                    $temp_rate = ActivityReportModel::where('employee_login_id', $value->employee_login_id)->get()->sum('rating');
                    if (count($temp_rate) > 0) {
                        $emp_list_array[$i][2] = $temp_rate;
                    } else {
                        $emp_list_array[$i][2] = NULL;
                    }

                    $temp_row_num = ActivityReportModel::where('employee_login_id', $value->employee_login_id)->get()->count();
                    if (!is_null($temp_row_num)) {
                        $emp_list_array[$i][3] = $temp_row_num * 10;
                    } else {
                        $emp_list_array[$i][3] = NULL;
                    }


                    $temp_photo = EmpModel::select(array('image', 'company_name'))->where('employee_login_id', $value->employee_login_id)->first();
                    // echo "<pre>";               print_r($temp);
                    if (count($temp_photo) > 0) {
                        $emp_list_array[$i][4] = $temp_photo->image;
                        $emp_list_array[$i++][5] = $temp_photo->company_name;
                    } else {
                        $emp_list_array[$i][4] = NULL;
                        $emp_list_array[$i++][5] = NULL;
                    }
                }
            }
        } else {
            $emp_list_array = NULL;
        }

        if (!isset($emp_list_array)) {
            $emp_list_array = NULL;
        }
        //  printIt($emp_list_array);

        $data = array(
            'emp_list_array' => $emp_list_array,
            'title' => "Activity Report",
            'customJs' => 'js_for_activity_report',
        );

        return view('content.activity_report.admin-home', $data);
    }

    public function report_list_for_emp(Request $request, $employee_id = NULL) {
        $emp = NULL;
        if (is_null($employee_id)) {
            $employee_id = $request->session()->get('id');
        } else {
            $emp = $employee_id;
        }
        //$employee_id = 24;
        $report_list = ActivityReportModel::select(array('id', 'report_from', 'report_to', 'insert_date', 'rating'))
                ->where(array('employee_login_id' => $employee_id, 'status' => 'active'))
                ->whereYear('insert_date', '=', date('Y'))
                ->orderBy('id', 'DESC')
                ->get();

        $report_list_array = NULL;
        if (count($report_list) > 0) {
            $i = 0;
            foreach ($report_list as $value) {
                $report_list_array[$i]['id'] = $value->id;
                $report_list_array[$i]['report_from'] = changeDateFormat($value->report_from, 'l jS \of F');
                $report_list_array[$i]['report_to'] = changeDateFormat($value->report_to, 'l jS \of F');
                $report_list_array[$i]['insert_date'] = changeDateFormat($value->insert_date, 'l jS \of F  h:i A');

                if (!is_null($value->rating)) {
                    $report_list_array[$i]['rating'] = $value->rating;
                } else {
                    $report_list_array[$i]['rating'] = '-';
                }
                $i++;
            }
        } else {
            $report_list_array = NULL;
        }

        if (!isset($report_list_array)) {
            $report_list_array = NULL;
        }
        // printIt($report_list_array);

        $data = array(
            'emp' => $emp,
            'report_list_array' => $report_list_array,
            'title' => "Activity Report",
            'customJs' => 'js_for_activity_report',
        );

        return view('content.activity_report.employee-home', $data);
    }

    public function pending_for_approver(Request $request, $notification_id = NULL) {

        $employee_id = $request->session()->get('id');
        if (!is_null($notification_id)) {
            \App\Models\NotificationManagerModel::
                    where(array('id' => $notification_id, 'related_id' => $employee_id))
                    ->update(array('status' => 'seen', 'update_date_time' => date('Y-m-d H:i:s')));
        }

        $report_list = ActivityReportModel::select(array('id', 'employee_login_id', 'report_from', 'report_to', 'insert_date'))
                ->where(array('who_will_rate' => $employee_id, 'status' => 'active'))
                ->whereYear('insert_date', '=', date('Y'))
                ->where('rating', '=', NULL)
                ->orderBy('id', 'DESC')
                ->get();

        $report_list_array = NULL;
        if (count($report_list) > 0) {
            $i = 0;
            foreach ($report_list as $value) {
                if (is_null($value->rating)) {
                    $report_list_array[$i]['id'] = $value->id;
                    $report_list_array[$i]['employee_login_id'] = $value->employee_login_id;
                    $report_list_array[$i]['report_from'] = changeDateFormat($value->report_from, 'l jS \of F');
                    $report_list_array[$i]['report_to'] = changeDateFormat($value->report_to, 'l jS \of F');
                    $report_list_array[$i++]['insert_date'] = changeDateFormat($value->insert_date, 'l jS \of F  h:i A');
                }
            }
        } else {
            $report_list_array = NULL;
        }

        if (!isset($report_list_array)) {
            $report_list_array = NULL;
        }
        //  printIt($emp_list_array);

        $data = array(
            'report_list_array' => $report_list_array,
            'title' => "Pending Report",
            'customJs' => 'js_for_activity_report',
        );

        return view('content.activity_report.pending-report', $data);
    }

    public function add_new_report(Request $request) {

        $employee_id = $request->session()->get('id');

        //  $data_2 = ActivityReportLib::get_max_week_for_report($employee_id);
        //$custom_current_week = custom_current_week_for_report();
//        if (is_null($data_2)) {
//            $check = 0; /// new report
//        } else {
//            if ($data_2 == $custom_current_week) {
//                $check = 1; // curent so revised  
//            } else {
//                $check = 2; // missing list
//            }
//        }
        $data = array(
            // 'data_2' => $data_2,
            // 'check' => $check,
            'title' => "Activity Report",
            'customJs' => 'add-new-report-js',
                // 'custom_current_week' => $custom_current_week
        );
        // printIt($data); die();
        return view('content.activity_report.add-new-report', $data);
    }

    public function report_submit(Request $request) {

        $employee_id = $request->session()->get('id');

        $validator = Validator::make($request->all(), [
                    'report_from' => 'required',
                    'report_to' => 'required',
                    'self_a' => 'required',
                    'report_from' => 'required',
                    'report_to' => 'required'
        ]);



        if ($validator->fails()) {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'report submit - validation', 'error');
            return redirect()->back()
                            ->withErrors($validator)
                            ->withInput();
        }


        $who_will_rate = NULL;
        $supervisor_ = ApproverModel::select(array('supervisor_login_id', 'approver_login_id', 'final_approver_login_id'))->where('employee_login_id', $employee_id)->first();
        if (count($supervisor_) > 0) {
            if (!is_null($supervisor_->supervisor_login_id)) {
                $who_will_rate = $supervisor_->supervisor_login_id;
            } else {
                if (!is_null($supervisor_->approver_login_id)) {
                    $who_will_rate = $supervisor_->approver_login_id;
                } else {
                    $who_will_rate = $supervisor_->final_approver_login_id;
                }
            }
        } else {
            $who_will_rate = NULL;

            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'report submit - approver data not found', 'error');
            return redirect()->back()->withErrors('You do not have any approver assigned. please contact with admin')->withInput();
        }

        $data_insert = array(
            'employee_login_id' => $employee_id,
            'report_from' => changeDateFormat(str_replace('/', '-', $request->input('report_from')), 'Y-m-d'),
            'report_to' => changeDateFormat(str_replace('/', '-', $request->input('report_to')), 'Y-m-d'),
            'self_a' => $request->input('self_a'),
            'activity_complete_this_week' => $request->input('activity_complete_this_week'),
            'long_term_project' => $request->input('long_term_project'),
            'issue_for_immediate_action' => $request->input('$issue_for_immediate_action'),
            'key_team' => $request->input('key_team_'),
            'note' => $request->input('note_'),
            'status' => 'active',
            'insert_date' => date('Y:m:d H:i:s'),
            'who_will_rate' => $who_will_rate,
        );

        $temp_insert = ActivityReportModel::create($data_insert);

        if ($temp_insert->id) {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'report submit - successfully', 'success');

            $notification = array(
                'employee_login_id' => $who_will_rate,
                'message' => "New activity report from " . empLib::getEmployeeName($employee_id),
                'url' => "/report-details/$temp_insert->id/$employee_id",
                'purpose' => 'profile',
                'insert_date_time' => date('Y-m-d h:i:s a'),
                'update_date_time' => NUll,
                'status' => 'unseen',
                'type' => 'warning',
                'related_id' => $who_will_rate
            );
            \App\Models\NotificationManagerModel ::create($notification);

            $mail_array = array(
                'name' => empLib::getEmployeeName($who_will_rate),
                'emp_name' => empLib::getEmployeeName($employee_id),
                'insert_date_time' => date('d-m-Y h:i a'),
                'email' => empLib::getEmployeeWorkEmailById($who_will_rate)
            );
            $template = 'new-activity-repor';
            ActivityReport::sendEmail($mail_array, $template);


            return redirect('/activity-report-employee')->with('success', 'Activity Report Submitted Successfully');
        } else {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'report submit - approver data not found', 'error');
            return redirect()->back()->withErrors('Unknown error occured, please try agin later.')->withInput();
        }
    }

    public function edit_report(Request $request, $id) {

        $employee_id = $request->session()->get('id');

        $report_list = ActivityReportModel::select(array('id', 'report_from', 'self_a', 'activity_complete_this_week', 'long_term_project', 'issue_for_immediate_action', 'key_team', 'report_to', 'note', 'rating', 'who_will_rate'))
                ->where(array('employee_login_id' => $employee_id, 'id' => $id, 'status' => 'active'))
                ->whereYear('insert_date', '=', date('Y'))
                ->orderBy('id', 'DESC')
                ->first();

        $report_list_array = NULL;
        if (!count($report_list) > 0) {
            $report_list_array = NULL;
            return redirect()->back()->withErrors('No data found');
        } else {
            if (!is_null($report_list->rating)) {
                return redirect()->back()->withErrors('This report is already rated by your approver you can not change it ');
            }
        }

        $report_list->report_from = changeDateFormat($report_list->report_from, 'd/m/Y');
        $report_list->report_to = changeDateFormat($report_list->report_to, 'd/m/Y');
        $data = array(
            'report_list_array' => $report_list,
            'title' => "Activity Report",
            'customJs' => 'add-new-report-js',
        );

        return view('content.activity_report.edit-form', $data);
    }

    public function edit_report_submit(Request $request) {

        $id = $request->input('id');
        $employee_id = $request->session()->get('id');
        $old_data = ActivityReportModel::select(array('report_from', 'report_to', 'self_a', 'activity_complete_this_week', 'long_term_project', 'issue_for_immediate_action', 'key_team', 'note'))
                ->where(array('id' => $id, 'employee_login_id' => $employee_id))
                ->first();

        if (!count($old_data) > 0) {
            return redirect()
                            ->back()
                            ->withErrors('Invalid id');
        }

        $who_will_rate = $request->input('who_will_rate');



        $validator = Validator::make($request->all(), [
                    'report_from' => 'required',
                    'report_to' => 'required',
                    'self_a' => 'required',
                    'report_from' => 'required',
                    'report_to' => 'required'
        ]);



        if ($validator->fails()) {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'report edit submit - validation', 'error');
            return redirect()->back()
                            ->withErrors($validator)
                            ->withInput();
        }




        $data_insert = array(
            'report_from' => changeDateFormat(str_replace('/', '-', $request->input('report_from')), 'Y-m-d'),
            'report_to' => changeDateFormat(str_replace('/', '-', $request->input('report_to')), 'Y-m-d'),
            'self_a' => $request->input('self_a'),
            'activity_complete_this_week' => $request->input('activity_complete_this_week'),
            'long_term_project' => $request->input('long_term_project'),
            'issue_for_immediate_action' => $request->input('issue_for_immediate_action'),
            'key_team' => $request->input('key_team_'),
            'note' => $request->input('note_'),
            'update_date_time' => date('Y:m:d H:i:s'),
        );


        if (ActivityReportModel::where(array('id' => $id, 'employee_login_id' => $employee_id))
                        ->update($data_insert)) {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'report edit submit - successfully', 'success');

            $notification = array(
                'employee_login_id' => $who_will_rate,
                'message' => "Activity report edited by " . empLib::getEmployeeName($employee_id) . ". check your email to see the comparison of new and old data.",
                'url' => "/report-details/$id/$employee_id",
                'purpose' => 'profile',
                'insert_date_time' => date('Y-m-d h:i:s a'),
                'update_date_time' => NUll,
                'status' => 'unseen',
                'type' => 'warning',
                'related_id' => $who_will_rate
            );
            \App\Models\NotificationManagerModel ::create($notification);

            $mail_array = array(
                'old_data' => $old_data,
                'new_data' => $data_insert,
                'name' => empLib::getEmployeeName($who_will_rate),
                'emp_name' => empLib::getEmployeeName($employee_id),
                'insert_date_time' => date('d-m-Y h:i a'),
                'email' => empLib::getEmployeeWorkEmailById($who_will_rate)
            );
            $template = 'edit-activity-repor';
            ActivityReport::sendEmail($mail_array, $template);


            return redirect('/activity-report-employee')->with('success', 'Activity report updated successfully');
        } else {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'report edit submit - data update failed', 'error');
            return redirect()->back()->withErrors('Unknown error occured, please try agin later.')->withInput();
        }
    }

    public function report_details(Request $request, $id, $employee_id = NULL, $notification_id = NULL) {
        $emp = NULL;
        if (is_null($employee_id)) {
            $employee_id = $request->session()->get('id');
        } else {
            $emp = $employee_id;
        }

        if (!is_null($notification_id)) {
            \App\Models\NotificationManagerModel::
                    where(array('id' => $notification_id, 'related_id' => $request->session()->get('id')))
                    ->update(array('status' => 'seen', 'update_date_time' => date('Y-m-d H:i:s')));
        }

        $report_list = ActivityReportModel::select(array('id', 'employee_login_id', 'report_from', 'self_a', 'activity_complete_this_week', 'long_term_project', 'issue_for_immediate_action', 'key_team', 'report_to', 'note', 'rating', 'who_will_rate', 'insert_date'))
                ->where(array('employee_login_id' => $employee_id, 'id' => $id, 'status' => 'active'))
                ->whereYear('insert_date', '=', date('Y'))
                ->first();

        $report_list_array = NULL;
        if (!count($report_list) > 0) {
            $report_list_array = NULL;
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'report details - data not found', 'error');

            return redirect()->back()->withErrors('No data found');
        }

        $report_list->report_from = changeDateFormat($report_list->report_from, 'l jS \of F Y');
        $report_list->report_to = changeDateFormat($report_list->report_to, 'l jS \of F Y');
        $report_list->insert_date = changeDateFormat($report_list->insert_date, 'l jS \of F Y');

        $publish_by_name_and_image = EmpModel::
                        select(array('image', 'full_name'))
                        ->where('employee_login_id', $employee_id)->first();


        $data = array(
            'emp' => $emp,
            'report_list_array' => $report_list,
            'publish_by_name_and_image' => $publish_by_name_and_image,
            'title' => "Activity Report",
            'customJs' => 'activity-report-details',
        );

        return view('content.activity_report.report-details', $data);
    }

    public function rate_report(Request $request) {
        
        $employee_id = $request->session()->get('id');

        $id = $request->input('id');
        if (!empLib::check_employee_status_by_login_id($request->input('employee_login_id'))) {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'rate report- employee status is not active.', 'error');
            return redirect()->back()->withErrors('This employee id has been deactivated');
        }
        $this->validate($request, [
            'rate' => 'required|min:0|max:10',
            'id' => 'required',
            'who_will_rate' => 'required',
        ]);

        if ($request->input('who_will_rate') != $employee_id) {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'rate report - Authorization ', 'error');

            return redirect()
                            ->back()
                            ->withErrors('You are not authorised for this action.');
        }

        $data = array(
            'rating' => $request->input('rate')
        );
        $where = array(
            'employee_login_id' => $request->input('employee_login_id'),
            'who_will_rate' => $employee_id,
            'id' => $request->input('id')
        );
        ActivityReportModel::where($where)
                ->where('rating', '=', NULL)
                ->update($data);
        \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'rate report - rated successfully ', 'success');

        $notification = array(
            'employee_login_id' => $request->input('employee_login_id'),
            'message' => "Your activity report has been rated ",
            'url' => "/activity-report-employee-notify",
            'purpose' => 'report',
            'insert_date_time' => date('Y-m-d h:i:s a'),
            'update_date_time' => NUll,
            'status' => 'unseen',
            'type' => 'warning',
            'related_id' => $request->input('employee_login_id')
        );
        \App\Models\NotificationManagerModel ::create($notification);

        $approver = ApproverModel::select(array('approver_login_id', 'final_approver_login_id'))
                        ->where('employee_login_id', $request->input('employee_login_id'))->first();

        if (count($approver) > 0) {
            if (!is_null($approver->approver_login_id) and $approver->approver_login_id != $employee_id) {
                $mail = array(
                    'name' => empLib::getEmployeeName($approver->approver_login_id),
                    'rate_emp_name' => empLib::getEmployeeName($employee_id),
                    'emp_name' => empLib::getEmployeeName($request->input('employee_login_id')),
                    'rate' => $request->input('rate'),
                    'insert_date_time' => date('d-m-Y h:i a'),
                    'email' => empLib::getEmployeeWorkEmailById($approver->approver_login_id)
                );
                ActivityReport::sendEmail($mail, 'rate-report');
                $notification = array(
                    'employee_login_id' => $approver->approver_login_id,
                    'message' => empLib::getEmployeeName($request->input('employee_login_id')) . "'s activity report has been rated by " . empLib::getEmployeeName($employee_id),
                    'url' => "/report-details/$id/" . $request->input('employee_login_id'),
                    'purpose' => 'report',
                    'insert_date_time' => date('Y-m-d h:i:s a'),
                    'update_date_time' => NUll,
                    'status' => 'unseen',
                    'type' => 'warning',
                    'related_id' => $approver->approver_login_id
                );
                \App\Models\NotificationManagerModel ::create($notification);
            }
            if (!is_null($approver->final_approver_login_id) and $approver->final_approver_login_id != $employee_id) {
                $mail = array(
                    'name' => empLib::getEmployeeName($approver->final_approver_login_id),
                    'rate_emp_name' => empLib::getEmployeeName($employee_id),
                    'emp_name' => empLib::getEmployeeName($request->input('employee_login_id')),
                    'rate' => $request->input('rate'),
                    'insert_date_time' => date('d-m-Y h:i a'),
                    'email' => empLib::getEmployeeWorkEmailById($approver->final_approver_login_id)
                );
                ActivityReport::sendEmail($mail, 'rate-report');
                $notification = array(
                    'employee_login_id' => $approver->final_approver_login_id,
                    'message' => empLib::getEmployeeName($request->input('employee_login_id')) . "'s activity report has been rated by " . empLib::getEmployeeName($employee_id),
                    'url' => "/report-details/$id/" . $request->input('employee_login_id'),
                    'purpose' => 'report',
                    'insert_date_time' => date('Y-m-d h:i:s a'),
                    'update_date_time' => NUll,
                    'status' => 'unseen',
                    'type' => 'warning',
                    'related_id' => $approver->final_approver_login_id
                );
                \App\Models\NotificationManagerModel ::create($notification);
            }
        }
        \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'rate report - successfully ', 'success');

        return redirect()->back()->with('success', 'Rated successfully');
    }

    private static function sendEmail($data, $template) {
        Mail::send("email-body.$template", $data, function ($message) use ($data) {
            $message->to($data['email'], $data['emp_name'])->subject('Activity Report');
        });
    }

    public function employee_notify(Request $request, $notification_id = NULL) {

        $employee_id = $request->session()->get('id');
        if (!is_null($notification_id)) {
            \App\Models\NotificationManagerModel::
                    where(array('id' => $notification_id, 'related_id' => $employee_id))
                    ->update(array('status' => 'seen', 'update_date_time' => date('Y-m-d H:i:s')));
        }

        return redirect('/activity-report-employee');
    }

}
