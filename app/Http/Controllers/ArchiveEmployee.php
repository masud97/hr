<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\EmployeeDetailsModel as empDetails;
use DB;
use App\Models\LoginModel as LoginModel;
use App\Models\ApproverModel as ApproverModel;
use Illuminate\Support\Facades\Validator;
use App\Library\EmployeeDetailsLib as empLib;
use App\Library\LeaveLib as LeaveLib;
use Illuminate\Support\Facades\Input;
use App\Library\MeritsDemeritsLib as MeritsDemeritsLib;
use Illuminate\Support\Facades\Mail;

class ArchiveEmployee extends Controller {

    public function __construct() {
        DB::enableQueryLog();
    }

//    public function index() {
//        return view('content.apply-for-leave', array('title' => 'Apply For Leave'));
//    }



    public function profile_view(Request $request, $employee_id) {
        if (!hasPrivilege(12)) {

            return redirect()->back()->withErrors('Access denied');
        }
        if (!\App\Library\PrivilegeCheckLib::checkPrivilege(8, $request->session()->get('id'))) {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'archive profile view - privilige issue.', 'error');
            return redirect()->back()->withErrors("Access deny");
        }


        $totalLeave = LeaveLib::totalLeave();
        $leaveTaken['casual'] = LeaveLib::totalLeaveTaken('casual', $employee_id);
        $leaveTaken['sick'] = LeaveLib::totalLeaveTaken('sick', $employee_id);
        $leaveTaken['half'] = LeaveLib::countHalfDay($employee_id);

        //get user details
        $selectedItem = array("id", "full_name", "email_personal", 'email_work', 'mobile_personal', 'mobile_work', 'mobile_emergency', 'emergency_contact_name_and_relation', 'persent_address', 'permanent_address', 'blood_group', 'date_of_birth', 'marital_status', 'gender', 'religion', 'highest_degree_obtained', 'company_name', 'designation', 'employee_category', 'joining_date', 'image');
        $where = array('employee_login_id' => $employee_id);
        $userDetails = empLib::getUserInfo($selectedItem, $where);

        if (!count($userDetails) > 0) {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'archive profile view - employee details not found.', 'error');
            return redirect('/all-employee')->withErrors('Invalid employee id');
        }



        $assign_approver = empLib::getEmployeeAssignApproverById($employee_id);
        if (count($assign_approver) > 0) {
            $supId = $assign_approver->supervisor_login_id;
            $apId = $assign_approver->approver_login_id;
            $fapId = $assign_approver->final_approver_login_id;
            $assignApprover['supervisor'] = NULL;
            $assignApprover['approver'] = NULL;
            $assignApprover['finalApprover'] = NULL;

            if (!is_null($supId)) {
                $assignApprover['supervisor']['name'] = empLib::getEmployeeName($supId);
            }
            if (!is_null($apId)) {
                $assignApprover['approver']['name'] = empLib::getEmployeeName($apId);
            }
            if (!is_null($fapId)) {
                $assignApprover['finalApprover']['name'] = empLib::getEmployeeName($fapId);
            }
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'archive profile view - approver details found.', 'success');
        } else {
            $assignApprover = NULL;
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'archive profile view - approver details not found.', 'error');
        }

        $emp_id_1 = empLib::get_deactive_employee_id_by_login_id($employee_id);
        if (!count($emp_id_1) > 0) {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'archive profile view - employee id not found.', 'error');
        }

        $data = array(
            'title' => 'Profile',
            'customJs' => 'archive-emp-profile-js',
            'totalLeave' => $totalLeave,
            'leaveTaken' => $leaveTaken,
            'userDetails' => $userDetails,
            'assignApprover' => $assignApprover,
            'emp_id' => $emp_id_1,
            'only_profile' => 'admin-employee-profile-view',
            'employee_id' => $employee_id,
        );

        //printIt($data,FALSE,TRUE);

        return view('content.archive.profile', $data);
    }

    public function all_emp(Request $request) {
        if (!hasPrivilege(12)) {

            return redirect()->back()->withErrors('Access denied');
        }
        $all_emp = NULL;
        $all_emp = DB::table('login')
                ->join('employee_details', 'login.id', '=', 'employee_details.employee_login_id')
                ->where(array('status' => 'deactive'))
                ->select('login.employee_id', 'employee_details.id', 'employee_details.employee_login_id', 'employee_details.image', 'employee_details.full_name', 'employee_details.company_name', 'employee_details.designation', 'employee_details.mobile_work', 'employee_details.mobile_emergency', 'employee_details.emergency_contact_name_and_relation', 'employee_details.blood_group')
                ->get();

        //printIt($all_emp,FALSE,TRUE);
        if (count($all_emp) > 0) {
            $i = 0;
            foreach ($all_emp as $val) {
                // printIt($val, FALSE, FALSE);
                $all_emp_val[$i]['id'] = $val->id;
                $all_emp_val[$i]['employee_login_id'] = $val->employee_id;
                $all_emp_val[$i]['employee_id'] = $val->employee_login_id;
                $all_emp_val[$i]['full_name'] = $val->full_name;
                $all_emp_val[$i]['company_name'] = $val->company_name;
                $all_emp_val[$i]['designation'] = $val->designation;
                $all_emp_val[$i++]['image'] = $val->image;
            }
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'archive all employee list- data  found', 'success');
        } else {
            $all_emp_val = NULL;
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'archive all employee list- data not found', 'error');
        }
        //  printIt($all_emp_val); die();
        return view('content.archive.all-emp-list', array('title' => 'All Employee', 'customJs' => 'archive-emp-js', 'all_emp_val' => $all_emp_val));
    }

    public function view_merits_demerits_of_employee($employee_id) {
        if (!hasPrivilege(12)) {

            return redirect()->back()->withErrors('Access denied');
        }


        $employeDetails = empLib::getUserInfo(array('full_name', 'email_work', 'image'), array('employee_login_id' => $employee_id));
        $data = MeritsDemeritsLib::all_merits_demerits($employee_id, 'deactive');

        if (!$data['status']) {
            return redirect()
                            ->back()
                            ->withErrors('No data found');
        }

        // printIt($data['value'],FALSE, TRUE);

        return view('content.archive.all-merits-demerits', array('title' => 'Merits/Demerits', 'customJs' => 'all-merits-demerits-js', 'userDetails' => $employeDetails, 'employee_id' => $employee_id, 'merits_demerits_array' => $data['value']));
    }

    public function restore_employee($employee_id) {

        if (!hasPrivilege(12)) {

            return redirect()->back()->withErrors('Access denied');
        }

        LoginModel::where('id', $employee_id)->update(array('status' => 'active'));

       

        return redirect('/archive-employee-list')->with('success', 'Employee restored successfully');
    }

}
