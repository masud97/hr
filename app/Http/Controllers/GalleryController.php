<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class GalleryController extends Controller {

    public function __construct() {
        
    }

    public function index(Request $request,$employeeId=null) {
        if(is_null($employeeId))
        {
            /*its employee request*/
            $employeeId = $request->session()->get('id');
            $emp = NULL;
        }
        else
        {
            /*Its admin request. So check the privilege*/
            if(!hasPrivilege(8))
            {
                return redirect()->back()->withErrors('Access deny');
            }
            
            /*check if this emloyee is active or deactive*/
            $temp = \App\Library\EmployeeDetailsLib::check_employee_status_by_login_id($employeeId);
            if(is_null($temp))
            {
                return redirect()->back()->withErrors("Invalid employee id");
            }
            $emp = $employeeId;
        }
        
        
        
        /*Now get all document for this employee*/
        
       $employeDetails = \App\Models\EmployeeDetailsModel::select('full_name', 'email_work', 'image','document_upload')->where(array('employee_login_id'=>$employeeId))->first();
        
       if(!count($employeDetails)>0)
       {
           $documents = NULL;
       }
       else
       {
           $documents = $employeDetails->document_upload;
       }
       
       
       
       if(!is_null($documents))
       {
           $documents = json_decode($documents,true);
       }
       
       /*Now get the leave document*/
       $attachments = \App\Models\LeaveDetailsModel::select('attachment')->where(array('employee_login_id'=>$employeeId))->get();
       
       if(count($attachments)>0)
       {
           foreach ($attachments as $row)
           {
               if(!is_null($row->attachment))
               {
                   $attachmes2[]=$row->attachment;
               }
               
           }
              
       }
       
       if(!isset($attachmes2))
       {
           $attachmes2 = NULL;
       }
        
        $data = array(
            'title' => 'Gallery',
            'customJs' => 'gallery',
            'userDetails' => $employeDetails,
            'documents'=>$documents,
            'attachments' => $attachmes2,
            'employee_id' => $emp
        );
        //\App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'Successfully view employee gallery. For employee id = ' . $employeeId, 'success');
        return view('content.employee.gallery', $data);
    }

}
