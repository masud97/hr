<?php

namespace App\Http\Controllers\ForgetPassword;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\EmployeeDetailsLib as emp_details;
use App\Models\ForgetPasswordModel;
use Illuminate\Support\Facades\Mail;
use App\Models\LoginModel;

class ForgetPasswordController extends Controller {

    public function __construct() {
        /* no_cash is a helper function. Its remove the browser cash. */
        no_cash();
    }

    public function index() {
        
    }

    public function sendToken(Request $request) {


        $this->validate($request, [
            'email' => 'required|email'
        ]);

        $temp_emp = emp_details::getUserInfo(array('employee_login_id', 'full_name'), array('email_work' => $request->email));

        if (count($temp_emp) > 0)
        {
            $token = $this->get_token();
            $temp = ForgetPasswordModel::select('id')->where(array('email' => $request->email, 'status' => 'active'))->first();
            if (count($temp) > 0)
            {

                $expiry_time=strtotime(date("d-m-Y H:i:s", strtotime(date('Y-m-d H:i:s')) + (2 * 3600)));
                $updateData = array(
                    'employee_login_id' => $temp_emp->employee_login_id,
                    'email' => $request->email,
                    'token' => $token,
                    'token_create_time' => time(),
                    'token_expiry' => $expiry_time,
                    'status' => 'active'
                );

                ForgetPasswordModel::where(array('id' => $temp->id))->update($updateData);

                $data = array(
                    'expiry_time' => $expiry_time,
                    'token' => $token,
                     'email' => $request->input('email'),
                     'name' => $temp_emp->full_name,
                );

                Mail::send('email-body.mailer', $data, function($message) use ($data) {

                    $message->to($data['email'], $data['name'])->subject('Forget Password');
                });

                return redirect('/forget-password-reset')->with('success', 'An email has been sent with token to your provided email address.');

            }
            else
            {

	        $forget_password_info = new ForgetPasswordModel;
                $forget_password_info->token = $token;
                $forget_password_info->token_create_time = time();
                
                // add two more hours to check expiry 
                //echo date("d-m-Y H:i:s"); die();

                $expiry_time = date("d-m-Y h:i:s a", time() + (2 * 3600));
                $update_time = strtotime(date("d-m-Y h:i:s a", strtotime(date('Y-m-d H:i:s')) + (2 * 3600)));
                $forget_password_info->token_expiry = $update_time;
                $forget_password_info->status = 'active';
                $forget_password_info->email = $request->input('email');
                $forget_password_info->employee_login_id = $temp_emp->employee_login_id;
                
                
                if ($forget_password_info->save())
                {
                    $data = array(
                        'expiry_time' => $expiry_time,
                        'token' => $token,
                        'email' => $request->input('email'),
                        'name' => $temp_emp->full_name,
                    );

                    Mail::send('email-body.mailer', $data, function($message) use ($data) {

                        $message->to($data['email'], $data['name'])->subject('Forget Password');
                    });

                    return redirect('/forget-password-reset')->with('success', 'An email has been sent with token to your provided email address.');
                }
                else
                {
                    return redirect('/')->withErrors('Token update failed');
                }
            }
        }
        else
        {

            return redirect('/')->withErrors('Invalid employee email');
        }
    }

    public function get_token() {
        while (1) {
            $token = bin2hex(openssl_random_pseudo_bytes(25));
            /* check token exist/not */
            $data = ForgetPasswordModel::select('id')->where('token', $token)->first();
            //$data = $this->CI->forget_password_model->select_where('id', array('token' => $token));
            if (is_null($data))
            {
                return $token;
            }
        }
    }

    public function reset() {
        return view('forget-password.reset-form', array('title' => 'Reset Password'));
    }

    public function reset_submit(Request $request) {

        $this->validate($request, [
            'token' => 'required',
            'new_password' => 'required',
            'new_confirm_password' => 'required'
        ]);

        $new_password = $request->new_password;
        $confirm_password = $request->new_confirm_password;


        if ($new_password != $confirm_password)
        {


            return redirect('/forget-password-reset')->withErrors('Password does not match');
        }


        $data = $this->checkTokenFromDb($request->token);


        if (!$data)
        {

            return redirect('/forget-password-reset')->withErrors('Invalid Token');
        }

        /// if token is valid 
        // $newPasswordHash = $this->hash_password($pass);

        $newPasswordHash = hash_password($new_password);

        $abc = $this->updateUserPassword($newPasswordHash, $data['employee_login_id']);

        if ($abc)
        {
            /* password update successfully. Now desable the token */
            $disable_token = $this->disable_token($data['id']);
            return redirect('/')->with('success', 'password update successfully');
        }
        else
        {
            return redirect('/forget-password-reset')->withErrors('password update failed. Please try again');
        }
    }

    public function checkTokenFromDb($token) {

        $where = array(
            'token' => trim($token),
            'status' => 'active',
        );

        /* check token exist/not */
        $data = ForgetPasswordModel::select('id', 'employee_login_id', 'email')
                ->where($where)
                ->where('token_expiry', '>=', time())
                ->first();
                
                
                
        //printIt($data, FALSE, TRUE);

        if (!is_null($data))
        {
            $value['id'] = $data->id;
            $value['employee_login_id'] = $data->employee_login_id;
            $value['email'] = $data->email;
            return $value;
        }
        return FALSE;
    }

    public function updateUserPassword($password, $user_id) {
        $check_user = LoginModel::find($user_id);

        if (!$check_user)
        {
            return redirect('/forget-password-reset')->withErrors('Wrong employee id');
        }

        $check_user->password = $password;

        return $check_user->save();
    }

    public function disable_token($id) {
        $check_token = ForgetPasswordModel::find($id);

        if (!$check_token)
        {
            return redirect('/forget-password-reset')->withErrors('Token updation failed');
        }

        $check_token->status = 'deactive';

        return $check_token->save();
    }

}
