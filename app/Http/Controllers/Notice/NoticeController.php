<?php

namespace App\Http\Controllers\Notice;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\NoticeModel as NoticeModel;
use App\Models\NotificationManagerModel as Notify;
use App\Models\EmployeeDetailsModel as EmpModel;
use DB;
use Illuminate\Support\Facades\Mail;

class NoticeController extends Controller {

    public function __construct() {
        DB::enableQueryLog();
    }

    public function allNotice(Request $request) {
        //privilege id 9 for notice
        $hasPrivilege = \App\Library\PrivilegeCheckLib::checkPrivilege(9, $request->session()->get('id'));
        if ($hasPrivilege)
        {
            $allNotice = NoticeModel::select('id', 'subject', 'insert_date', 'publish_on', 'reissue', 'status')
                    ->orderBy('id', 'DESC')
                    ->get();
        }
        else
        {
            $id = $request->session()->get('id');
            $allNotice = NoticeModel::select('id', 'subject', 'insert_date', 'publish_on', 'reissue', 'status')
                    ->where('status', 'active')
                    ->whereRaw('FIND_IN_SET(' . $id . ',notice_for)')
                    ->orderBy('id', 'DESC')
                    ->get();
        }
        /* get all notice */

        $data = array(
            'customJs' => 'all-notice',
            'title' => 'All Notice',
            'allNotice' => $allNotice,
            'hasPrivilege' => $hasPrivilege
        );

        return view('content.notice.all-notice', $data);
    }

    public function newNotice() {
        /* get employee and company name */
        $company = \App\Library\EmployeeDetailsLib::get_company_name();
        $activeEmployee = \App\Library\EmployeeDetailsLib::all_active_emp_id_name();

        $data = array(
            'customJs' => 'new-notice',
            'title' => 'New Notice',
            'company' => $company,
            'activeEmployee' => $activeEmployee
        );

        return view('content.notice.new-notice', $data);
    }

    public function submitNotice(Request $request) {

        $this->validate($request, [
            'employee_or_company' => 'required',
            'subject' => 'required',
            'publish_date' => 'required',
            'notice_body' => 'required',
        ]);



        //priority
        $employee_or_company = $request->input('employee_or_company');
//        $priority = $request->input('priority');
        $subject = $request->input('subject');
        $publish_date = str_replace('/', '-', $request->input('publish_date'));
        $notice_body = $request->input('notice_body');


        if (!is_array($employee_or_company))
        {
            return redirect()->back()->withErrors('Invalid Employee or caompany')->withInput();
        }

        $noticeFor = '';
        foreach ($employee_or_company as $row)
        {
            if (is_numeric($row))
            {
                /* this is an employee id */
                $noticeFor .= $row;
                $noticeFor .= ",";
            }
            else if ($row === 'all')
            {
                /* you select all employee */
                //get all employee
                $activeEmployee = \App\Library\EmployeeDetailsLib::all_active_emp_id_name();
                if (count($activeEmployee) > 0)
                {
                    foreach ($activeEmployee as $emp)
                    {
                        $nf[] = $emp['id'];
                    }
                    $noticeFor = implode(',', $nf);
                }
                else
                {
                    $noticeFor = "";
                }

                break;
            }
            else
            {
                /* select company name */
                /* get employee id by using the compnay name */
                $employeeIds = \App\Library\EmployeeDetailsLib::getEmployeeNameByUsingCompanyName($row);
                if (count($employeeIds) > 0)
                {
                    foreach ($employeeIds as $emp)
                    {
                        $noticeFor .= $emp->employee_login_id;
                        $noticeFor .= ',';
                    }
                }
            }
        }


        if ($noticeFor === '')
        {
            return redirect()->back()->withErrors('No employee selected')->withInput();
        }

        $noticeFor = rtrim($noticeFor, ",");

        $publishOn = changeDateFormat($publish_date, 'Y-m-d');
        if ($publishOn === date('Y-m-d'))
        {
            $status = 'active';
        }
        else
        {
            $status = 'pending';
        }


        $notice = array(
            'published_by' => $request->session()->get('id'),
            'subject' => $subject,
            'body' => $notice_body,
            'publish_on' => $publishOn,
            'priority' => NULL,
            'notice_for' => $noticeFor,
            'insert_date' => date('Y-m-d'),
            'status' => $status
        );



        $id = NoticeModel::create($notice);
        $noticeId = $id->id;


        if ($noticeId)
        {
            if ($status == 'active')
            {

                /* notice is published now so genereate notification for employee */
                $employeeList = explode(',', $noticeFor);
                foreach ($employeeList as $id)
                {
                    $notifications [] = array(
                        'employee_login_id' => $id,
                        'type' => 'success',
                        'related_id' => $noticeId,
                        'message' => 'You have a new notice',
                        'url' => '/notice/view/' . $noticeId,
                        'purpose' => 'notice',
                        'insert_date_time' => date('Y-m-d'),
                        'status' => 'unseen'
                    );


                    //send email
                    $mail = array(
                        'email' => getUserEmail($id),
                        'name' => getUserFullName($id),
                        'subject' => $subject,
                        'body' => $notice_body
                    );

                    NoticeController::sendEmail($mail);
                }

                Notify::insert($notifications);
                return redirect('/notice')->with('success', 'Notice has been successfully published');
            }
            else
            {

                return redirect('/notice')->with('success', 'Notice has been successfully inserted');
            }
        }
        else
        {
            return redirect()->back()->withErrors('Unknown error occurred. Please try again later.')->withInput();
        }
    }

    private static function sendEmail($data) {
        Mail::send('email-body.notice', $data, function ($message) use ($data) {
            $message->to($data['email'], $data['name'])->subject($data['subject']);
        });
    }

    public function viewNotice(Request $request, $noticeId, $notificationId = null) {

        //check if this notice is generated for this user
        $id = $request->session()->get("id");
        $hasPrivilege = \App\Library\PrivilegeCheckLib::checkPrivilege(9, $id);

        if (!NoticeModel::whereRaw('FIND_IN_SET(' . $id . ',notice_for)')->where(array('id' => $noticeId, 'status' => 'active'))->exists())
        {
            if (!$hasPrivilege)
            {
                return redirect()->back()->withErrors("Notice is no longer active for you.");
            }
        }



        //get notice
        $notice = NoticeModel::select('subject', 'published_by', 'body', 'publish_on', 'notice_for')->where(array('id' => $noticeId))->first();
        if (!count($notice) > 0)
        {
            return redirect()->back()->withErrors('No notice found. Invalid notice id');
        }
        
       $publish_by_name_and_image = EmpModel::select('full_name', 'image')->where(array('employee_login_id' => $notice->published_by))->first();

        if (!count($publish_by_name_and_image) > 0)
        {
            return redirect()->back()->withErrors("Invalid publish by user id");
        }


        /* make this notification seen */
        Notify::where(array('id' => $notificationId, 'related_id' => $noticeId, 'employee_login_id' => $request->session()->get('id')))->update(array('status' => 'seen'));


        $nFempId = explode(',', $notice->notice_for);
        $noticeFor = "";
        foreach ($nFempId as $i)
        {
            $noticeFor .= getUserFullName($i) . "; ";
        }


        $data = array(
            'title' => 'View Notice',
            'customJs' => 'view-notice',
            'notice' => $notice,
            'noticeFor' => $noticeFor,
            'publish_by_name_and_image' => $publish_by_name_and_image
        );
        return view('content.notice.view-notice', $data);
    }

    public function withdrawNotice(Request $request, $noticeId) {
        if (NoticeModel::where(array('id' => $noticeId))->where('status', '!=', 'withdrawn')->update(array('status' => 'withdrawn')))
        {
            return redirect()->back()->with('success', 'Notice has been successfully withdrawn');
        }
        else
        {
            return redirect()->back()->withErrors('Unknown error occurred. Please try again later');
        }
    }

}
