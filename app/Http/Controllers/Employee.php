<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\EmployeeDetailsModel as empDetails;
use DB;
use App\Models\LoginModel as LoginModel;
use App\Models\ApproverModel as ApproverModel;
use Illuminate\Support\Facades\Validator;
use App\Library\EmployeeDetailsLib as empLib;
use App\Library\LeaveLib as LeaveLib;
use Illuminate\Support\Facades\Input;
use App\Library\MeritsDemeritsLib as MeritsDemeritsLib;
use Illuminate\Support\Facades\Mail;

class Employee extends Controller {

    public function __construct() {
        DB::enableQueryLog();
    }

//    public function index() {
//        return view('content.apply-for-leave', array('title' => 'Apply For Leave'));
//    }

    public function profile(Request $request) {
        $employee_id = $request->session()->get('id');
        $totalLeave = LeaveLib::totalLeave();
        $leaveTaken['casual'] = LeaveLib::totalLeaveTaken('casual');
        $leaveTaken['sick'] = LeaveLib::totalLeaveTaken('sick');
        $leaveTaken['half'] = LeaveLib::countHalfDay();

        //get user details
        $selectedItem = array('id', "full_name", "email_personal", 'email_work', 'mobile_personal', 'mobile_work', 'mobile_emergency', 'emergency_contact_name_and_relation', 'persent_address', 'permanent_address', 'blood_group', 'date_of_birth', 'marital_status', 'gender', 'religion', 'highest_degree_obtained', 'company_name', 'designation', 'employee_category', 'joining_date', 'image', 'department', 'national_id');
        $where = array('employee_login_id' => $request->session()->get('id'));
        $userDetails = empLib::getUserInfo($selectedItem, $where);

        //
        \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'profile details found.', 'success');
        //
        if (!count($userDetails) > 0) {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'profile details not found.', 'error');
            return redirect('/all-employee')->withErrors('Invalid employee id');
        }
        $assignApprover = NULL;
        $assign_approver = empLib::getEmployeeAssignApproverById($employee_id);
        if (count($assign_approver) > 0) {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'profile details - approver details found.', 'success');

            $supId = $assign_approver->supervisor_login_id;
            $apId = $assign_approver->approver_login_id;
            $fapId = $assign_approver->final_approver_login_id;
            $assignApprover['supervisor'] = NULL;
            $assignApprover['approver'] = NULL;
            $assignApprover['finalApprover'] = NULL;
            $assignApprover['row_id'] = $assign_approver->id;
            if (!is_null($supId)) {
                $assignApprover['supervisor']['name'] = empLib::getEmployeeName($supId);
                $assignApprover['supervisor']['id'] = $supId;
            }
            if (!is_null($apId)) {
                $assignApprover['approver']['name'] = empLib::getEmployeeName($apId);
                $assignApprover['approver']['id'] = $apId;
            }
            if (!is_null($fapId)) {
                $assignApprover['finalApprover']['name'] = empLib::getEmployeeName($fapId);
                $assignApprover['finalApprover']['id'] = $fapId;
            }
        } else {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'profile details - approver details not found.', 'error');
            $assignApprover = NULL;
        }
        $emp_id = empLib::get_employee_id_by_login_id($request->session()->get('id'));
        if (count($emp_id) > 0) {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'profile details - employee id found.', 'success');
        } else {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'profile details - employee id not found.', 'error');
        }
        $data = array(
            'title' => 'Profile',
            'customJs' => 'profile',
            'totalLeave' => $totalLeave,
            'leaveTaken' => $leaveTaken,
            'userDetails' => $userDetails,
            'assignApprover' => $assignApprover,
            'only_profile' => $request->segment(1),
            'emp_id' => $emp_id
        );

        return view('content.employee.profile', $data);
    }

    public function admin_employee_profile_view(Request $request, $employee_id, $notificationId = NULL) {

        if (!is_null($notificationId)) {
            \App\Models\NotificationManagerModel::where(array('id' => $notificationId, 'related_id' => $employee_id))->update(array('status' => 'seen', 'update_date_time' => date('Y-m-d H:i:s')));
        }

        if (!\App\Library\PrivilegeCheckLib::checkPrivilege(8, $request->session()->get('id'))) {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'admin profile view - privilige issue.', 'error');
            return redirect('/all-employee')->withErrors("Access deny");
        }

        if (!empLib::check_employee_status_by_login_id($employee_id)) {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'admin profile view - employee status is not active.', 'error');
            return redirect()->back()->withErrors('Invalid employee id');
        }

        $totalLeave = LeaveLib::totalLeave();
        $leaveTaken['casual'] = LeaveLib::totalLeaveTaken('casual', $employee_id);
        $leaveTaken['sick'] = LeaveLib::totalLeaveTaken('sick', $employee_id);
        $leaveTaken['half'] = LeaveLib::countHalfDay($employee_id);

        //get user details
        $selectedItem = array("id", "full_name", "email_personal", 'email_work', 'mobile_personal', 'mobile_work', 'mobile_emergency', 'emergency_contact_name_and_relation', 'persent_address', 'permanent_address', 'blood_group', 'date_of_birth', 'marital_status', 'gender', 'religion', 'highest_degree_obtained', 'company_name', 'designation', 'employee_category', 'joining_date', 'image', 'national_id', 'department');
        $where = array('employee_login_id' => $employee_id);
        $userDetails = empLib::getUserInfo($selectedItem, $where);

        if (!count($userDetails) > 0) {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'admin profile view - employee details not found.', 'error');
            return redirect('/all-employee')->withErrors('Invalid employee id');
        }



        $assign_approver = empLib::getEmployeeAssignApproverById($employee_id);
        if (count($assign_approver) > 0) {
            $supId = $assign_approver->supervisor_login_id;
            $apId = $assign_approver->approver_login_id;
            $fapId = $assign_approver->final_approver_login_id;
            $assignApprover['supervisor'] = NULL;
            $assignApprover['approver'] = NULL;
            $assignApprover['finalApprover'] = NULL;
            $assignApprover['row_id'] = $assign_approver->id;
            if (!is_null($supId)) {
                $assignApprover['supervisor']['name'] = empLib::getEmployeeName($supId);
                $assignApprover['supervisor']['id'] = $supId;
            }
            if (!is_null($apId)) {
                $assignApprover['approver']['name'] = empLib::getEmployeeName($apId);
                $assignApprover['approver']['id'] = $apId;
            }
            if (!is_null($fapId)) {
                $assignApprover['finalApprover']['name'] = empLib::getEmployeeName($fapId);
                $assignApprover['finalApprover']['id'] = $fapId;
            }
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'admin profile view - approver details found.', 'success');
        } else {
            $assignApprover = NULL;
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'admin profile view - approver details not found.', 'error');
        }

        //printIt($assignApprover, FALSE, TRUE);

        $all_emp = empLib::all_active_emp_id_name();
        $emp_id_1 = empLib::get_employee_id_by_login_id($employee_id);
        if (!count($emp_id_1) > 0) {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'admin profile view - employee id not found.', 'error');
        }

        $data = array(
            'title' => 'Profile',
            'customJs' => 'profile',
            'totalLeave' => $totalLeave,
            'leaveTaken' => $leaveTaken,
            'userDetails' => $userDetails,
            'assignApprover' => $assignApprover,
            'emp_id' => $emp_id_1,
            'only_profile' => 'admin-employee-profile-view',
            'employee_id' => $employee_id,
            'all_emp' => $all_emp
        );

        //printIt($data,FALSE,TRUE);

        return view('content.employee.profile', $data);
    }

    public function all_emp(Request $request) {
        $all_emp = NULL;
        $all_emp = DB::table('login')
                ->join('employee_details', 'login.id', '=', 'employee_details.employee_login_id')
                ->where(array('status' => 'active'))
                ->select('employee_details.id', 'employee_details.employee_login_id', 'employee_details.image', 'employee_details.full_name', 'employee_details.company_name', 'employee_details.designation', 'employee_details.mobile_work', 'employee_details.mobile_emergency', 'employee_details.emergency_contact_name_and_relation', 'employee_details.blood_group')
                ->get();

        //printIt($all_emp,FALSE,TRUE);
        if (count($all_emp) > 0) {
            $i = 0;
            foreach ($all_emp as $val) {
                // printIt($val, FALSE, FALSE);
                $all_emp_val[$i]['id'] = $val->id;
                $all_emp_val[$i]['employee_login_id'] = $val->employee_login_id;
                $all_emp_val[$i]['employee_id'] = empLib::get_employee_id_by_login_id($val->employee_login_id);
                $all_emp_val[$i]['full_name'] = $val->full_name;
                $all_emp_val[$i]['company_name'] = $val->company_name;
                $all_emp_val[$i]['designation'] = $val->designation;
                $all_emp_val[$i]['mobile_work'] = $val->mobile_work;
                $all_emp_val[$i]['mobile_emergency'] = $val->mobile_emergency;
                $all_emp_val[$i]['emergency_contact_name_and_relation'] = $val->emergency_contact_name_and_relation;
                $all_emp_val[$i]['image'] = $val->image;
                $all_emp_val[$i++]['blood_group'] = $val->blood_group;
            }
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'all employee list- data  found', 'success');
        } else {
            $all_emp_val = NULL;
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'all employee list- data not found', 'error');
        }

        return view('content.employee.all-emp-list', array('title' => 'All Employee', 'customJs' => 'all-emp-js', 'all_emp_val' => $all_emp_val));
    }

    public function create_emp(Request $request) {
        if (!hasPrivilege(3)) {
            return redirect()->back()->withErrors('Access Denied');
        }
        $all_emp = empLib::all_active_emp_id_name();
        $temp_com = \App\Models\CompanyModel::select('companyName')->where('status', 'active')->first();
        if (count($temp_com) > 0) {
            $company_array = explode(',', $temp_com->companyName);
        } else {
            $company_array = NULL;
        }
        if (!count($all_emp) > 0) {
            $all_emp = NULL;
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'create employee - data not found', 'error');
        }

        return view('content.employee.create-employee-form', array('title' => 'Create Employee', 'customJs' => 'employeeJs', 'all_emp' => $all_emp, 'company_array' => $company_array));
    }

    public function create_emp_submit(Request $request) {

        if (!hasPrivilege(3)) {
            return redirect()->back()->withErrors('Access Denied');
        }
//echo $request->input('employee_id'); die();
        $validator = Validator::make($request->all(), [
                    'full_name' => 'required',
                    'employee_id' => 'required|min:3|max:6|Unique:login',
                    'company_name' => 'required',
                    'password' => 'required',
                    'confirm_password' => 'required',
                    //'blood_group' => 'required',
                    // 'mobile_personal' => 'required',
                    // 'mobile_emergency' => 'required|different:mobile_personal',
                    // 'emergency_contact_name_and_relation' => 'required',
                    'permanent_address' => 'required',
                    //  'blood_group' => 'required',
                    //  'date_of_birth' => 'required',
                    'company_name' => 'required',
                    'joining_date' => 'required',
                    //'profile_img' => 'mimes:jpeg,jpg',
                    'document_upload' => 'mimes:pdf',
                    'department' => 'required',
                    'designation' => 'required',
                    'employee_category' => 'required',
                    'national_id' => 'required'
        ]);



        if ($validator->fails()) {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'create employee submit - validation', 'error');
            return redirect()->back()
                            ->withErrors($validator)
                            ->withInput(Input::except('password', 'confirm_password'));
        }

        $supervisor = $request->input('supervisor_login_id');
        $approver = $request->input('approver_login_id');
        $final_approver = $request->input('final_approver_login_id');



        if ($supervisor != 0) {
            if (!empLib::check_employee_status_by_login_id($supervisor)) {
                return redirect()->back()->withErrors('Invalid supervisor id');
            }
        }

        if ($approver != 0) {
            if (!empLib::check_employee_status_by_login_id($approver)) {
                return redirect()->back()->withErrors('Invalid approver id');
            }
        }
        if ($final_approver != 0) {
            if (!empLib::check_employee_status_by_login_id($final_approver)) {
                return redirect()->back()->withErrors('Invalid final approver id');
            }
        }




        if ($final_approver != 0) {
            if ((($supervisor != 0) && ($approver == 0))) {
                \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'create employee submit - approver is mandatory ', 'error');
                return redirect()->back()
                                ->withErrors('Approver is mandatory')
                                ->withInput(Input::except('password', 'confirm_password'));
            } elseif ((($supervisor == 0) && ($approver != 0))) {
                if ($approver == $final_approver) {
                    \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'create employee submit - approver should be not similer ', 'error');
                    return redirect()->back()->withErrors('Supervisor, approver and final approver should be different person')->withInput(Input::except('password', 'confirm_password'));
                } else {
                    $supervisor = NULL;
                }
            } elseif ((($supervisor == 0) && ($approver == 0))) {

                $supervisor = NULL;
                $approver = NULL;
            } elseif ((($supervisor != 0) && ($approver != 0) && ($final_approver != 0))) {
                if (($supervisor == $approver) or ( $supervisor == $final_approver) or ( $approver == $final_approver)) {
                    \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'create employee submit - approver should be not similer ', 'error');
                    return redirect()->back()->withErrors('Supervisor, approver and final approver should be different person')->withInput(Input::except('password', 'confirm_password'));
                }
            }
        } else {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'create employee submit - final approver not selected', 'error');

            return redirect()->back()
                            ->withErrors('Final approver must be selected')->withInput(Input::except('password', 'confirm_password'));
        }


        $password = trim($request->input('password'));

        $confirmPass = trim($request->input('confirm_password'));

        if ($password != $confirmPass) {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'create employee submit - password and confirm password not matched', 'error');
            return redirect()->back()
                            ->withErrors('Password does not match.')
                            ->withInput(Input::except('password', 'confirm_password'));
        }

        $data = array(
            'employee_id' => $request->input('employee_id'),
            'password' => hash_password($password),
            'status' => 'active'
        );

        $temp = LoginModel::create($data);

        if ($temp) {
            $attachment_doc = NULL;
            $attachment_img = NULL;

            $employeeLoginId = $temp->id;

            if ($request->hasFile('profile_img')) {
                if ($request->profile_img->getClientOriginalExtension() === 'jpeg' or $request->profile_img->getClientOriginalExtension() === 'jpg') {

                    if ($request->file('profile_img')->isValid()) {

                        $folder = base_path() . '/assets/img/user-image';
                        $file = $request->file('profile_img');
                        $type = $file->getClientOriginalExtension();
                        $attachment_img = time() . $request->session()->get('id') . '.' . $type;
                        $request->file('profile_img')->move($folder, $attachment_img);
                    }
                } else {
                    $attachment_img = 'nopic.png';
                }
            }
            if ($request->hasFile('document_upload')) {
                if ($request->file('document_upload')->isValid()) {
                    $folder = base_path() . '/assets/user-doc';
                    $file = $request->file('document_upload');
                    $type = $file->getClientOriginalExtension();
                    $attachment_doc = time() . $request->session()->get('id') . '.' . $type;
                    $request->file('document_upload')->move($folder, $attachment_doc);
                }
            }



            $insertEmpDetails = array(
                'employee_login_id' => $employeeLoginId,
                'full_name' => $request->input('full_name'),
                'email_personal' => $request->input('email_personal'),
                'email_work' => $request->input('email_work'),
                'mobile_personal' => $request->input('mobile_personal'),
                'mobile_work' => $request->input('mobile_work'),
                'mobile_emergency' => $request->input('mobile_emergency'),
                'emergency_contact_name_and_relation' => $request->input('emergency_contact_name_and_relation'),
                'persent_address' => $request->input('persent_address'),
                'permanent_address' => $request->input('permanent_address'),
                'blood_group' => $request->input('blood_group'),
                'date_of_birth' => changeDateFormat($request->input('date_of_birth'), 'Y-m-d'),
                'marital_status' => $request->input('marital_status'),
                'gender' => $request->input('gender'),
                'religion' => $request->input('religion'),
                'highest_degree_obtained' => $request->input('highest_degree_obtained'),
                'company_name' => $request->input('company_name'),
                'designation' => $request->input('designation'),
                'employee_category' => $request->input('employee_category'),
                'joining_date' => changeDateFormat($request->input('joining_date'), 'Y-m-d'),
                'image' => $attachment_img,
                'document_upload' => $attachment_doc,
                'department' => $request->input('department'),
                'national_id' => $request->input('national_id')
            );


            if (empDetails::create($insertEmpDetails)) {


                $approverDetails = array(
                    'employee_login_id' => $employeeLoginId,
                    'supervisor_login_id' => $supervisor,
                    'approver_login_id' => $approver,
                    'final_approver_login_id' => $final_approver
                );

                if (!is_null($supervisor)) {

                    $tempPriv = array(
                        1, 2, 4, 5, 7, 8, 10, 11, 13
                    );

                    $supOldPriv = LoginModel::select('privilege')->where(array('status' => 'active', 'id' => $supervisor))->first();
                    if (!count($supOldPriv) > 0) {
                        return redirect()->back()->withErrors('Selected employee is not a valid employee.');
                    }

                    if ($supOldPriv->privilege == NULL) {
                        $supOldPriv = array();
                    } else {
                        $supOldPriv = explode(',', $supOldPriv->privilege);
                    }

                    $supPriv = array_merge($tempPriv, $supOldPriv);
                    $supPriv = array_unique($supPriv);
                    sort($supPriv);
                    $supPriv = array_values($supPriv);
                    $supPriv = implode(',', $supPriv);


                    LoginModel::where('id', $supervisor)
                            ->update(array('privilege' => $supPriv));
                }
                if (!is_null($approver)) {
                    $appPriv = '1,2,3,4,5,6,7,8,9,10,11,12,13,14';
                    LoginModel::where('id', $approver)
                            ->update(array('privilege' => $appPriv));
                }
                if (!is_null($final_approver)) {
                    $fAppPriv = '1,2,3,4,5,6,7,8,9,10,11,12,13,14';
                    LoginModel::where('id', $final_approver)
                            ->update(array('privilege' => $fAppPriv));
                }


                if (ApproverModel::create($approverDetails)) {
                    \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'create employee submit', 'success');

                    $notification = array(
                        'employee_login_id' => $employeeLoginId,
                        'message' => "Please update your profile information",
                        'url' => "/edit-employee-profile",
                        'purpose' => 'profile',
                        'insert_date_time' => date('Y-m-d h:i:s a'),
                        'update_date_time' => NUll,
                        'status' => 'unseen',
                        'type' => 'warning',
                        'related_id' => $employeeLoginId
                    );
                    \App\Models\NotificationManagerModel ::create($notification);



                    return redirect('/all-employee')
                                    ->with('success', 'Employee created successfully');
                } else {
                    \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'create employee submit - approver data insertion failed', 'error');
                    return redirect('/all-employee')
                                    ->withErrors('Approver data insertion failed');
                }
            } else {
                \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'create employee submit failed', 'error');
                return redirect('create-employee')->back()
                                ->withErrors('Employee creation failed.')
                                ->withInput(Input::except('password', 'confirm_password'));
            }
        } else {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'create employee submit - login info update failed', 'error');
            return redirect('create-employee')->withErrors('Employess login information update failed.');
        }
    }

    public function admin_edit_profile(Request $request, $employee_id) {
        if (!hasPrivilege(3)) {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'admin edit profile - privilege problem  ', 'error');
            return redirect()->back()->withErrors('Access denied');
        }

        $select = array('id', 'employee_login_id', 'full_name', 'email_personal', 'email_work', 'mobile_personal', 'mobile_work', 'mobile_emergency', 'emergency_contact_name_and_relation', 'persent_address', 'permanent_address', 'blood_group', 'date_of_birth', 'marital_status', 'gender', 'religion', 'highest_degree_obtained', 'company_name', 'designation', 'employee_category', 'joining_date', 'image', 'department', 'national_id');
        $where = array(
            'employee_login_id' => $employee_id
        );
        $temp_emp = empLib::getUserInfo($select, $where);
        $temp_com = \App\Models\CompanyModel::select('companyName')->where('status', 'active')->first();
        if (count($temp_com) > 0) {
            $company_array = explode(',', $temp_com->companyName);
        } else {
            $company_array = NULL;
        }

        if (!count($temp_emp) > 0) {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'admin edit profile - user info not found', 'error');
            return redirect()
                            ->back()
                            ->withErrors('No data found');
        }

        $employeDetails = empLib::getUserInfo(array('full_name', 'email_work', 'image'), array('employee_login_id' => $employee_id));
        if (!count($employeDetails) > 0) {
            $employeDetails = NULL;
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'admin edit profile - employee details not found', 'error');
        }
        $employeeId = empLib::get_employee_id_by_login_id($employee_id);
        if (!count($employeeId)) {
            $employeeId = NULL;
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'admin edit profile - employee id not found', 'error');
        }
        $assign_approver = empLib::getEmployeeAssignApproverById($employee_id);
        if (!count($assign_approver) > 0) {
            $assign_approver = NULL;
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'admin edit profile - approver  not found', 'error');
        }
        $all_emp = empLib::all_active_emp_id_name();
        if (!count($all_emp) > 0) {
            $all_emp = NULL;
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'admin edit profile - all active employee data  not found', 'error');
        }

        $data = array(
            'title' => 'Edit employee profile',
            'userDetails' => $employeDetails,
            'customJs' => 'edit-profile-js',
            'assign_approver' => $assign_approver,
            'employeeId' => $employeeId,
            'employee_data' => $temp_emp,
            'employee_id' => $employee_id,
            'all_emp' => $all_emp,
            'company_array' => $company_array
        );



        return view('content.employee.edit-emp-form', $data);
    }

    public function edit_profile_emp(Request $request, $notificationId = NULL) {
        $employee_id = $request->session()->get('id');



        $select = array('id', 'employee_login_id', 'full_name', 'email_personal', 'email_work', 'mobile_personal', 'mobile_work', 'mobile_emergency', 'emergency_contact_name_and_relation', 'persent_address', 'permanent_address', 'blood_group', 'date_of_birth', 'marital_status', 'gender', 'religion', 'highest_degree_obtained', 'company_name', 'designation', 'employee_category', 'joining_date', 'image', 'department');
        $where = array(
            'employee_login_id' => $employee_id
        );
        $temp_emp = empLib::getUserInfo($select, $where);

        if (is_null($temp_emp)) {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'edit profile - data  not found', 'error');
            return redirect()
                            ->back()
                            ->withErrors('No data found');
        }

        $employeDetails = empLib::getUserInfo(array('full_name', 'email_work', 'image'), array('employee_login_id' => $employee_id));
        if (!count($employeDetails) > 0) {
            $employeDetails = NULL;
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'edit profile - employee details data  not found', 'error');
        }
        $employeeId = empLib::get_employee_id_by_login_id($employee_id);
        if (!count($employeeId) > 0) {
            $employeeId = NULL;
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'edit profile - employee id  not found', 'error');
        }
        $assign_approver = empLib::getEmployeeAssignApproverById($employee_id);
        if (!count($assign_approver) > 0) {
            $assign_approver = NULL;
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'edit profile - approver not found', 'error');
        }
        $all_emp = empLib::all_active_emp_id_name();
        if (!count($all_emp) > 0) {
            $all_emp = NULL;
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'edit profile - all employee not found', 'error');
        }
        $data = array(
            'title' => 'Edit employee profile',
            'userDetails' => $employeDetails,
            'customJs' => 'edit-profile-js',
            'assign_approver' => $assign_approver,
            'employeeId' => $employeeId,
            'employee_data' => $temp_emp,
            'all_emp' => $all_emp,
            'notificationId' => $notificationId
        );



        return view('content.employee.edit-emp-form', $data);
    }

    public function edit_employee_submit(Request $request) {

        $employeeLoginId = NULL;

        $employeeLoginId = $request->input('employee_login_id');


        $validator = Validator::make($request->all(), [
                    'full_name' => 'required',
                    'employee_id' => 'required|max:6',
                    'company_name' => 'required',
                    'mobile_personal' => 'required',
                    'permanent_address' => 'required',
                    'date_of_birth' => 'required',
                    'joining_date' => 'required',
                    'department' => 'required',
                    'blood_group' => 'required',
                    'mobile_emergency' => 'required|different:mobile_personal',
                    'emergency_contact_name_and_relation' => 'required',
                    'designation' => 'required',
                    'employee_category' => 'required',
                    'national_id' => 'required',
                    'gender' => 'required',
                    'persent_address' => 'required'
        ]);

        if ($validator->fails()) {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'edit profile submit - validation problem ', 'error');
            return redirect()->back()
                            ->withErrors($validator)
            ;
        }
        $supervisor = $request->input('supervisor_login_id');
        $approver = $request->input('approver_login_id');
        $final_approver = $request->input('final_approver_login_id');



        if ($supervisor != 0) {
            if (!empLib::check_employee_status_by_login_id($supervisor)) {
                return redirect()->back()->withErrors('Invalid supervisor id');
            }
        }

        if ($approver != 0) {
            if (!empLib::check_employee_status_by_login_id($approver)) {
                return redirect()->back()->withErrors('Invalid approver id');
            }
        }
        if ($final_approver != 0) {
            if (!empLib::check_employee_status_by_login_id($final_approver)) {
                return redirect()->back()->withErrors('Invalid final approver id');
            }
        }



        if ($final_approver != 0) {
            if ((($supervisor != 0) && ($approver == 0))) {
                \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'edit profile submit - approver is mandatory', 'error');

                return redirect()->back()
                                ->withErrors('Approver is mandatory')
                ;
            } elseif ((($supervisor == 0) && ($approver != 0))) {
                if ($approver == $final_approver) {
                    \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'edit profile submit - approver should be different', 'error');
                    return redirect()->back()->withErrors('Supervisor, approver and final approver should be different person');
                } else {
                    $supervisor = NULL;
                }
            } elseif ((($supervisor == 0) && ($approver == 0))) {

                $supervisor = NULL;
                $approver = NULL;
            } elseif ((($supervisor != 0) && ($approver != 0) && ($final_approver != 0))) {
                if (($supervisor == $approver) or ( $supervisor == $final_approver) or ( $approver == $final_approver)) {
                    \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'edit profile submit - approver should be different', 'error');
                    return redirect()->back()->withErrors('Supervisor, approver and final approver should be different person');
                }
            }
        } else {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'edit profile submit - approver should be different', 'error');
            return redirect()->back()
                            ->withErrors('Final approver must be selected');
        }

        if ($request->input('employee_id') != $request->input('existing_employee_id')) {
            $validator = Validator::make($request->all(), [

                        'employee_id' => 'Unique:login',
            ]);
            if ($validator->fails()) {
                \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'edit profile submit - login id validation', 'error');
                return redirect()->back()
                                ->withErrors($validator)
                ;
            }

            if (!LoginModel::where(array('id' => $employeeLoginId))->update(array('employee_id' => $request->input('employee_id')))) {
                \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'edit profile submit - login id update failed', 'error');
                return redirect()->back()->withErrors('Employee id update failed');
            }
        }


        $attachment_img = NULL;



        if ($request->hasFile('profile_img')) {
            if ($request->profile_img->getClientOriginalExtension() === 'jpeg' or $request->profile_img->getClientOriginalExtension() === 'jpg') {

                if ($request->file('profile_img')->isValid()) {
                    $folder = base_path() . '/assets/img/user-image';
                    $file = $request->file('profile_img');
                    $type = $file->getClientOriginalExtension();
                    $attachment_img = time() . $request->session()->get('id') . '.' . $type;
                    $request->file('profile_img')->move($folder, $attachment_img);
                }
            } else {
                $attachment_img = $request->input('existing_image');
            }
        } else {
            $attachment_img = $request->input('existing_image');
        }

        $updateEmpDetails = array(
            'full_name' => $request->input('full_name'),
            'email_personal' => $request->input('email_personal'),
            'email_work' => $request->input('email_work'),
            'mobile_personal' => $request->input('mobile_personal'),
            'mobile_work' => $request->input('mobile_work'),
            'mobile_emergency' => $request->input('mobile_emergency'),
            'emergency_contact_name_and_relation' => $request->input('emergency_contact_name_and_relation'),
            'persent_address' => $request->input('persent_address'),
            'permanent_address' => $request->input('permanent_address'),
            'blood_group' => $request->input('blood_group'),
            'date_of_birth' => changeDateFormat($request->input('date_of_birth'), 'Y-m-d'),
            'marital_status' => $request->input('marital_status'),
            'gender' => $request->input('gender'),
            'religion' => $request->input('religion'),
            'highest_degree_obtained' => $request->input('highest_degree_obtained'),
            'company_name' => $request->input('company_name'),
            'designation' => $request->input('designation'),
            'employee_category' => $request->input('employee_category'),
            'joining_date' => changeDateFormat($request->input('joining_date'), 'Y-m-d'),
            'image' => $attachment_img,
            'department' => $request->input('department'),
            'national_id' => $request->input('national_id')
        );

        $empId = $request->input('id');
        $url = "admin-employee-profile-view/" . $employeeLoginId;
        // echo $empId . "<br>" . $employeeLoginId;
        // die();

        empDetails::where(array('id' => $empId, 'employee_login_id' => $employeeLoginId))->update($updateEmpDetails);
        \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'edit profile submit - details info updated ', 'success');

// if (empDetails::where(array('id' => $empId, 'employee_login_id' => $employeeLoginId))->update($updateEmpDetails)) {



        $approverDetails = array(
            'employee_login_id' => $employeeLoginId,
            'supervisor_login_id' => $supervisor,
            'approver_login_id' => $approver,
            'final_approver_login_id' => $final_approver
        );
        if (!is_null($request->input('approver_row_id'))) {
            $row_id = $request->input('approver_row_id');
        } else {
            $row_id = $request->input('row_id');
        }

        $existingApprover = ApproverModel::select(array('supervisor_login_id', 'approver_login_id', 'final_approver_login_id'))
                ->where(array('id' => $row_id, 'employee_login_id' => $employeeLoginId))
                ->first();
        if (!count($existingApprover) > 0) {
            return redirect()->back()->withErrors('Invalid employee id');
        }
        empLib::edit_approver_checking($existingApprover, $employeeLoginId, $supervisor, $approver, $final_approver, $request);



        ApproverModel::where(array('id' => $request->input('approver_row_id'), 'employee_login_id' => $employeeLoginId))->update($approverDetails);
        \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'edit profile submit - approver info updated', 'success');
        $temp_check = ApproverModel::select('id')
                ->where('supervisor_login_id', $existingApprover->supervisor_login_id)
                ->orWhere('approver_login_id', $existingApprover->supervisor_login_id)
                ->orWhere('final_approver_login_id', $existingApprover->supervisor_login_id)
                ->first();
        if (!count($temp_check) > 0) {
            $tempPriv = NULL;

            LoginModel::where('id', $existingApprover->supervisor_login_id)
                    ->update(array('privilege' => $tempPriv));
        }

        return redirect($url)
                        ->with('success', 'Profile successfully updated');
    }

    public function edit_employee_by_emp_submit(Request $request) {

        $employeeLoginId = NULL;
        $employeeLoginId = $request->session()->get('id');
        $old_data_temp = NULL;
        $old_data_temp = empDetails::select('full_name', 'email_personal', 'mobile_personal', 'mobile_emergency', 'emergency_contact_name_and_relation', 'persent_address', 'blood_group', 'date_of_birth', 'marital_status', 'gender', 'religion', 'highest_degree_obtained', 'image', 'gender')
                        ->where('employee_login_id', $employeeLoginId)->first();
        if (!count($old_data_temp) > 0) {
            $old_data_temp = NULL;
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'edit profile submit by employee - employee old data found', 'error');
        }
        $validator = Validator::make($request->all(), [
                    'full_name' => 'required',
                    'blood_group' => 'required',
                    'mobile_personal' => 'required',
                    'mobile_emergency' => 'required|different:mobile_personal',
                    'emergency_contact_name_and_relation' => 'required',
                    'date_of_birth' => 'required',
                    'profile_img' => 'mimes:jpeg,jpg',
                    'mobile_work' => 'required',
                    'email_work' => 'required',
                    'emergency_contact_name_and_relation' => 'required'
        ]);
        if ($validator->fails()) {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'edit profile submit by employee - validation', 'error');

            return redirect()->back()
                            ->withErrors($validator)
            ;
        }

        $notificationId = $request->input('notification_id');

        if (!is_null($notificationId)) {
            \App\Models\NotificationManagerModel::where(array('id' => $notificationId, 'related_id' => $employeeLoginId))->update(array('status' => 'seen', 'update_date_time' => date('Y-m-d H:i:s')));
        }

        $attachment_img = NULL;



        if ($request->hasFile('profile_img')) {
            if ($request->profile_img->getClientOriginalExtension() === 'jpeg' or $request->profile_img->getClientOriginalExtension() === 'jpg') {

                if ($request->file('profile_img')->isValid()) {
                    $folder = base_path() . '/assets/img/user-image';
                    $file = $request->file('profile_img');
                    $type = $file->getClientOriginalExtension();
                    $attachment_img = time() . $request->session()->get('id') . '.' . $type;
                    $request->file('profile_img')->move($folder, $attachment_img);
                }
            } else {
                $attachment_img = $request->input('existing_image');
            }
        } else {
            $attachment_img = $request->input('existing_image');
        }

        $updateEmpDetails = array(
            'full_name' => $request->input('full_name'),
            'email_personal' => $request->input('email_personal'),
            'email_work' => $request->input('email_work'),
            'mobile_personal' => $request->input('mobile_personal'),
            'mobile_emergency' => $request->input('mobile_emergency'),
            'emergency_contact_name_and_relation' => $request->input('emergency_contact_name_and_relation'),
            'persent_address' => $request->input('persent_address'),
            'blood_group' => $request->input('blood_group'),
            'date_of_birth' => changeDateFormat($request->input('date_of_birth'), 'Y-m-d'),
            'marital_status' => $request->input('marital_status'),
            'gender' => $request->input('gender'),
            'religion' => $request->input('religion'),
            'highest_degree_obtained' => $request->input('highest_degree_obtained'),
            'image' => $attachment_img,
            'mobile_work' => $request->input('mobile_work'),
            'mobile_personal' => $request->input('mobile_personal'),
            'mobile_emergency' => $request->input('mobile_emergency'),
            'emergency_contact_name_and_relation' => $request->input('emergency_contact_name_and_relation')
        );

        $empId = $request->input('id');
        $url = "/profile";
        // echo $empId . "<br>" . $employeeLoginId;
        // die();

        empDetails::where(array('id' => $empId, 'employee_login_id' => $employeeLoginId))->update($updateEmpDetails);
        \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'edit profile submit by employee - details info update', 'success');
        $aprover_email = empLib::getEmployeeAssignApproverById($employeeLoginId);
        if (!count($aprover_email) > 0) {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'edit profile submit by employee - approver email not found', 'error');
        }
        if (!is_null(empLib::getEmployeeWorkEmailById($aprover_email->supervisor_login_id))) {
            $aprover_email_array[0]['id'] = $aprover_email->supervisor_login_id;
            $aprover_email_array[0]['email'] = empLib::getEmployeeWorkEmailById($aprover_email->supervisor_login_id);
            $aprover_email_array[0]['name'] = empLib::getEmployeeName($aprover_email->supervisor_login_id);
        }
        if (!is_null(empLib::getEmployeeWorkEmailById($aprover_email->approver_login_id))) {
            $aprover_email_array[1]['id'] = $aprover_email->approver_login_id;
            $aprover_email_array[1]['email'] = empLib::getEmployeeWorkEmailById($aprover_email->approver_login_id);
            $aprover_email_array[1]['name'] = empLib::getEmployeeName($aprover_email->approver_login_id);
        }
        if (!is_null(empLib::getEmployeeWorkEmailById($aprover_email->final_approver_login_id))) {
            $aprover_email_array[2]['id'] = $aprover_email->final_approver_login_id;
            $aprover_email_array[2]['email'] = empLib::getEmployeeWorkEmailById($aprover_email->final_approver_login_id);
            $aprover_email_array[2]['name'] = empLib::getEmployeeName($aprover_email->final_approver_login_id);
        }
        $aprover_email_array = array_values($aprover_email_array);

        foreach ($aprover_email_array as $value) {
            $mail_array = array(
                'employee_id' => $employeeLoginId,
                'old_data_array' => $old_data_temp,
                'updateEmpDetails' => $updateEmpDetails,
                'email' => $value['email'],
                'admin_name' => $value['name'],
                'name' => empLib::getEmployeeName($employeeLoginId)
            );


            Mail::send('email-body.employee-profile-update', $mail_array, function($message) use ($mail_array) {
                // printIt($mail_array['email']); die();
                $message->to($mail_array['email'], 'Admin')->subject('Employee profile has been edited');
            });

            $notification = array(
                'employee_login_id' => $value['id'],
                'message' => $mail_array['name'] . " profile has been edited",
                'url' => "/admin-employee-profile-view/$employeeLoginId",
                'purpose' => 'profile',
                'insert_date_time' => date('Y-m-d h:i:s a'),
                'update_date_time' => NUll,
                'status' => 'unseen',
                'type' => 'warning',
                'related_id' => $employeeLoginId
            );
            \App\Models\NotificationManagerModel ::create($notification);
        }

        \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'edit profile submit by employee - profile update', 'success');
        return redirect($url)
                        ->with('success', "Profile successfully updated , an email has been sent to all of your approver");
    }

    public function change_password_submit(Request $request) {
        // printIt($request->session()->all());
        // 

        $empId = $request->session()->get('id');


        $oldPassword = $request->old_password;
        $newPassword = $request->new_password;
        $confirmNewPassword = $request->confirm_new_password;

        $validator = Validator::make($request->all(), [
                    'old_password' => 'required',
                    'new_password' => 'required|min:6',
                    'confirm_new_password' => 'required|min:6',
        ]);
        if ($validator->fails()) {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'change passwrood - validation problem', 'error');

            return redirect()->back()
                            ->withErrors($validator)
            ;
        }

        $temp_old_password = LoginModel::select('password')->where('id', $empId)->first();

        if (!ck_password($oldPassword, $temp_old_password->password)) {
            return redirect()
                            ->back()
                            ->withErrors('Old password is wrong');
        }

        if (!is_null($temp_old_password)) {
            if ($newPassword != $confirmNewPassword) {
                return redirect()
                                ->back()
                                ->withErrors('New password and confirm password does not match');
            } else {

                if (!LoginModel::find($empId)) {
                    return redirect()->back()->withErrors('Invalid employee id');
                }

                if (LoginModel::where('id', $empId)->update(array('password' => hash_password($newPassword)))) {

                    return redirect()->back()->with('success', 'Password changed succesfully');
                }
            }
        } else {
            return redirect()
                            ->back()
                            ->withErrors('Invalid employee id');
        }
    }

    public function reset_password_submit(Request $request) {

        $newPassword = $request->input('new_password');
        $confirmNewPassword = $request->input('confirm_new_password');

        $validator = Validator::make($request->all(), [

                    'new_password' => 'required',
                    'confirm_new_password' => 'required',
        ]);
        if (!empLib::check_employee_status_by_login_id($request->input('employee_id'))) {
            return redirect()->back()->withErrors('Invalid employee id');
        }

        if ($validator->fails()) {
            return redirect()->back()
                            ->withErrors($validator)
            ;
        }

        if ($newPassword != $confirmNewPassword) {
            return redirect()
                            ->back()
                            ->withErrors('New password and confirm password does not match');
        } else {

            if (!LoginModel::where(array('id' => $request->input('employee_id'), 'status' => 'active'))
                            ->exists()) {
                return redirect()->back()->withErrors('Invalid employee id');
            }

            $data = array(
                'password' => hash_password($newPassword)
            );

            if (LoginModel::where(array('id' => $request->input('employee_id')))->update($data)) {
                return redirect()->back()->with('success', 'Password reset successfully for (' . empLib::getEmployeeName($request->input('employee_id')) . ')');
            } else {
                return redirect()->back()->withErrors('Password reset failed');
            }
        }
    }

    public function add_merits_demerits(Request $request) {


        $employee_id = $request->employee_id;



        if (!is_numeric($employee_id)) {
            return json_encode(array('status' => False, 'msg' => "Invalid employee id"));
        }

        if (!empLib::check_employee_status_by_login_id($employee_id)) {
            return json_encode(array('status' => False, 'msg' => "Invalid employee id"));
        }

        $data = MeritsDemeritsLib::add_merits_demerits($request);

        if ($data) {

            return json_encode(array('status' => TRUE, 'msg' => "Employee " . $request->input('type') . " update succesfully"));
        } else {
            return json_encode(array('status' => FALSE, 'msg' => "Employee " . $request->input('type') . " update failed"));
        }
    }

    public function edit_merits_demerits(Request $request) {

        $this->validate($request, [
            'id' => 'required|integer',
            'details' => 'required',
        ]);
        $data = MeritsDemeritsLib::edit_merits_demerits($request);

        if ($data) {

            return redirect()
                            ->back()
                            ->with('success', $request->input('type') . ' updated');
        } else {
            return redirect()
                            ->back()
                            ->withErrors($request->input('type') . "update failed");
        }
    }

    public function remove_merits_demerits($id) {

        if (!hasPrivilege(5)) {

            return redirect()->back()->withErrors('Access denied');
        }

        $data = MeritsDemeritsLib::remove_merits_demerits($id);

        if ($data) {

            return redirect()
                            ->back()
                            ->with('success', 'Removed successfully');
        } else {
            return redirect()
                            ->back()
                            ->withErrors('Remove failed');
        }
    }

    public function file_upload_admin(Request $request) {

        $employee_id = $request->employee_id;

        $row_id = $request->input('row_id');

        if (!empLib::check_employee_status_by_login_id($employee_id)) {
            return redirect()->back()->withErrors('Invalid employee id');
        }
        $validator = Validator::make($request->all(), [
                    'file_title' => 'required',
                    'file_category' => 'required',
                    'file_upload' => 'mimes:pdf|required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors('Invalid file type');
        }

        $data = empLib::file_upload($request, $employee_id);

        if ($data) {

            $document_array = array(
                array(
                    "Title" => $request->input('file_title'),
                    "Category" => $request->input('file_category'),
                    "file" => $data
                )
            );

            $temp_document = empDetails::select('document_upload')->where(array('id' => $row_id, 'employee_login_id' => $employee_id))->first();

            if (!is_null($temp_document->document_upload)) {
                $temp_document_array = json_decode($temp_document->document_upload);
                $merged_array = array_values(array_merge($temp_document_array, $document_array));
                $merged = json_encode($merged_array);
            } else {
                $merged = json_encode($document_array);
            }

            if (empDetails::where(array('id' => $row_id, 'employee_login_id' => $employee_id))->update(array('document_upload' => $merged))) {
                return redirect()->back()->with('success', 'File uploaded successfully');
            } else {
                return redirect()->back()->withErrors('File upload failed');
            }
            return redirect()->back()->with('success', 'File uploaded successfully');
        } else {
            return redirect()->back()->withErrors('File upload failed');
        }
    }

    public function view_merits_demerits(Request $request) {

        $employee_id = $request->session()->get('id');



        if (!empLib::check_employee_status_by_login_id($employee_id)) {
            return redirect()
                            ->back()
                            ->withErrors('Your account has been deactivated');
        }

        $data = MeritsDemeritsLib::all_merits_demerits($employee_id);

        if (!$data['status']) {
            return redirect()
                            ->back()
                            ->withErrors('No data found');
        }
        $employeDetails = empLib::getUserInfo(array('full_name', 'email_work', 'image'), array('employee_login_id' => $employee_id));


        // printIt($data['value'],FALSE, TRUE);

        return view('content.employee.all-merits-demerits', array('userDetails' => $employeDetails, 'title' => 'Merits/Demerits', 'customJs' => 'all-merits-demerits-js', 'userDetails' => $employeDetails, 'merits_demerits_array' => $data['value']));
    }

    public function view_merits_demerits_admin_of_employee($employee_id) {
        if (!hasPrivilege(5)) {

            return redirect()->back()->withErrors('Access denied');
        }

        if (!empLib::check_employee_status_by_login_id($employee_id)) {
            return redirect()->back()
                            ->withErrors('This employee id has been deactivated');
        }
        $employeDetails = empLib::getUserInfo(array('full_name', 'email_work', 'image'), array('employee_login_id' => $employee_id));
        $data = MeritsDemeritsLib::all_merits_demerits($employee_id);

        if (!$data['status']) {
            return redirect()
                            ->back()
                            ->withErrors('No data found');
        }

        // printIt($data['value'],FALSE, TRUE);

        return view('content.employee.all-merits-demerits', array('title' => 'Merits/Demerits', 'customJs' => 'all-merits-demerits-js', 'userDetails' => $employeDetails, 'employee_id' => $employee_id, 'merits_demerits_array' => $data['value']));
    }

    public function change_profile_pic_admin(Request $request, $employee_id) {
        if (!hasPrivilege(3)) {
            return redirect()->back()->withErrors('Access denied');
        }


        if (!empLib::check_employee_status_by_login_id($employee_id)) {
            return redirect()->back()
                            ->withErrors('This employee id has been deactivated');
        }

        $validator = Validator::make($request->all(), [
                    'profile_img' => 'mimes:jpeg,jpg|required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors('Invalid file type');
        }

        if ($request->hasFile('profile_img')) {
            if ($request->profile_img->getClientOriginalExtension() === 'jpeg' or $request->profile_img->getClientOriginalExtension() === 'jpg') {

                if ($request->file('profile_img')->isValid()) {
                    $folder = base_path() . '/assets/img/user-image';
                    $file = $request->file('profile_img');
                    $type = $file->getClientOriginalExtension();
                    $attachment_img = time() . $request->session()->get('id') . '.' . $type;
                    $request->file('profile_img')->move($folder, $attachment_img);
                }
            } else {
                $attachment_img = $request->input('existing_image');
            }
        } else {
            $attachment_img = $request->input('existing_image');
        }


        if (empDetails::where(array('id' => $request->input('id'), 'employee_login_id' => $employee_id))
                        ->update(array('image' => $attachment_img))) {
            return redirect()->back()->with('success', 'Profile picture updated successfully');
        } else {
            return redirect()->back()->withErrors('Profile picture update failed');
        }
    }

    public function change_profile_pic_employee(Request $request) {

        $validator = Validator::make($request->all(), [
                    'profile_img' => 'mimes:jpeg,jpg|required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors('Invalid file type');
        }
        $employee_id = $request->session()->get('employee_id');

        $where = array(
            'id' => $request->session()->get('id'),
            'status' => 'active'
        );

        $check = LoginModel::select('employee_id')->where($where)->first();
        //printIt($request->session()->all()); die();
        if (count($check) > 0) {
            if ($check->employee_id != $employee_id) {

                return redirect()->back()->withErrors("You can not change another employee profile picture");
            }
        } else {
            return redirect()->back()->withErrors("Wrong employee id");
        }


        if ($request->hasFile('profile_img')) {
            if ($request->profile_img->getClientOriginalExtension() === 'jpeg' or $request->profile_img->getClientOriginalExtension() === 'jpg') {
                if ($request->file('profile_img')->isValid()) {
                    $folder = base_path() . '/assets/img/user-image';
                    $file = $request->file('profile_img');
                    $type = $file->getClientOriginalExtension();
                    $attachment_img = time() . $request->session()->get('id') . '.' . $type;
                    $request->file('profile_img')->move($folder, $attachment_img);
                }
            } else {
                $attachment_img = $request->input('existing_image');
            }
        } else {
            $attachment_img = $request->input('existing_image');
        }

        if (empDetails::where(array('id' => $request->input('id'), 'employee_login_id' => $request->session()->get('id')))
                        ->update(array('image' => $attachment_img))) {
            return redirect()->back()->with('success', 'Profile picture updated successfully');
        } else {
            return redirect()->back()->withErrors('Profile picture update failed');
        }
    }

    public function document($employee_id) {

        if (!empLib::check_employee_status_by_login_id($employee_id)) {
            return redirect()->back()
                            ->withErrors('This employee id has been deactivated');
        }

        $temp = empDetails::select('document_upload')->where('employee_login_id')->first();
        if (!count($temp) > 0) {
            return redirect()->back()
                            ->withErrors('No data found');
        }
        echo $temp->document_upload;
        die();

        $data = array(
            'title' => 'All Document',
            'file_array' => $file_array
        );


        return view('content.all-document-list', $data);
    }

    public function edit_approver_submit(Request $request, $employee_id) {
        if (!hasPrivilege(3)) {
            return redirect()->back()->withErrors('Access denied');
        }
        if (!empLib::check_employee_status_by_login_id($employee_id)) {
            return redirect()->back()->withErrors('Invalid employee id');
        }


        $supervisor = $request->input('supervisor_login_id');
        $approver = $request->input('approver_login_id');
        $final_approver = $request->input('final_approver_login_id');


        if ($supervisor != 0) {
            if (!empLib::check_employee_status_by_login_id($supervisor)) {
                return redirect()->back()->withErrors('Invalid supervisor id');
            }
        }

        if ($approver != 0) {
            if (!empLib::check_employee_status_by_login_id($approver)) {
                return redirect()->back()->withErrors('Invalid approver id');
            }
        }
        if ($final_approver != 0) {
            if (!empLib::check_employee_status_by_login_id($final_approver)) {
                return redirect()->back()->withErrors('Invalid final approver id');
            }
        }



        if ($final_approver != 0) {
            if ((($supervisor != 0) && ($approver == 0))) {
                return redirect()->back()
                                ->withErrors('Approver is mandatory');
            } elseif ((($supervisor == 0) && ($approver != 0))) {
                if ($approver == $final_approver) {
                    return redirect()->back()->withErrors('Supervisor, approver and final approver should be different person');
                } else {
                    $supervisor = NULL;
                }
            } elseif ((($supervisor == 0) && ($approver == 0))) {

                $supervisor = NULL;
                $approver = NULL;
            } elseif ((($supervisor != 0) && ($approver != 0) && ($final_approver != 0))) {
                if (($supervisor == $approver) or ( $supervisor == $final_approver) or ( $approver == $final_approver)) {
                    return redirect()->back()->withErrors('Supervisor, approver and final approver should be different person');
                }
            }
        } else {
            return redirect()->back()
                            ->withErrors('Final approver must be selected');
        }

        if (!is_null($request->input('approver_row_id'))) {
            $row_id = $request->input('approver_row_id');
        } else {
            $row_id = $request->input('row_id');
        }

        $existingApprover = ApproverModel::select(array('supervisor_login_id', 'approver_login_id', 'final_approver_login_id'))
                ->where(array('id' => $row_id, 'employee_login_id' => $employee_id))
                ->first();
        if (!count($existingApprover) > 0) {
            return redirect()->back()->withErrors('Invalid employee id');
        }
        empLib::edit_approver_checking($existingApprover, $employee_id, $supervisor, $approver, $final_approver, $request);

        $data = array(
            'supervisor_login_id' => $supervisor,
            'approver_login_id' => $approver,
            'final_approver_login_id' => $final_approver
        );

        $insert = array(
            'employee_login_id' => $employee_id,
            'supervisor_login_id' => $supervisor,
            'approver_login_id' => $approver,
            'final_approver_login_id' => $final_approver
        );

        $abc = ApproverModel::select('id')->where('id', $request->input('row_id'))->first();



        if (count($abc) > 0) {
            /* update */
            if (ApproverModel::where(array('id' => $request->input('row_id'), 'employee_login_id' => $employee_id))->update($data)) {
                $temp_check = ApproverModel::select('id')
                        ->where('supervisor_login_id', $existingApprover->supervisor_login_id)
                        ->orWhere('approver_login_id', $existingApprover->supervisor_login_id)
                        ->orWhere('final_approver_login_id', $existingApprover->supervisor_login_id)
                        ->first();
                if (!count($temp_check) > 0) {
                    $tempPriv = NULL;

                    LoginModel::where('id', $existingApprover->supervisor_login_id)
                            ->update(array('privilege' => $tempPriv));
                }
                return redirect()->back()->with('success', 'Approver information updated');
            } else {
                return redirect()->back()->withErrors('Approver information update failed');
            }
        } else {
            /* Insert */
            if (ApproverModel::create($insert)) {
                return redirect()->back()->with('success', 'Approver information inserted');
            } else {
                return redirect()->back()->withErrors('Approver information insert failed');
            }
        }
    }

    public function terminate_employee($employee_id) {

        if (!hasPrivilege(6)) {
            return redirect()->back()->withErrors('Access denied');
        }
        $all_emp = empLib::all_active_emp_id_name();
        $data = array(
            'employee_id' => $employee_id,
            'title' => 'Terminate Employee',
            'all_emp' => $all_emp,
            'employee_name' => empLib::getEmployeeName($employee_id)
        );



        return view('content.employee.terminate_employee', $data);
    }

    public function terminate_employee_submit(Request $request) {


        $employee_id = $request->input('employee_id');

        $sup = ApproverModel::select(array('id', 'employee_login_id'))->where('supervisor_login_id', $employee_id)->get();
        if (count($sup) > 0) {
            foreach ($sup as $val) {
                ApproverModel::where('id', $val->id)->update(array('supervisor_login_id' => $request->input('replace_login_id')));
            }
        }
        $app = ApproverModel::select(array('id', 'employee_login_id', 'supervisor_login_id'))->where('approver_login_id', $employee_id)->get();
        if (count($app) > 0) {
            foreach ($app as $val2) {
                if ($val2->supervisor_login_id == $request->input('replace_login_id')) {
                    ApproverModel::where('id', $val2->id)->update(array('supervisor_login_id' => NULL, 'approver_login_id' => $request->input('replace_login_id')));
                } else {
                    ApproverModel::where('id', $val2->id)->update(array('approver_login_id' => $request->input('replace_login_id')));
                }
            }
        }
        $fapp = ApproverModel::select(array('id', 'employee_login_id', 'approver_login_id'))->where('final_approver_login_id', $employee_id)->get();

        if (count($fapp) > 0) {
            foreach ($fapp as $val3) {
                if ($val3->approver_login_id == $request->input('replace_login_id')) {
                    ApproverModel::where('id', $val3->id)->update(array('approver_login_id' => NULL, 'final_approver_login_id' => $request->input('replace_login_id')));
                } else {
                    ApproverModel::where('id', $val3->id)->update(array('final_approver_login_id' => $request->input('replace_login_id')));
                }
            }
        }

        $terminate_employee_priv = LoginModel::select('privilege')->where(array('status' => 'active', 'id' => $employee_id))->first();
        if (count($terminate_employee_priv) > 0) {
            if ($terminate_employee_priv->privilege == NULL) {
                $terminate_employee_priv = array();
            } else {
                $terminate_employee_priv = explode(',', $terminate_employee_priv->privilege);
            }
        } else {
            $terminate_employee_priv = array();
        }


        $transfer_emp_priv = LoginModel::select('privilege')->where(array('status' => 'active', 'id' => $request->input('replace_login_id')))->first();
        if (!count($transfer_emp_priv) > 0) {
            return redirect()->back()->withErrors('Selected employee is not a valid employee (To transfer privilege)');
        } else {

            if ($transfer_emp_priv->privilege == NULL) {
                $transfer_emp_priv = array();
            } else {
                $transfer_emp_priv = explode(',', $transfer_emp_priv->privilege);
            }
        }


        $transfer_emp_priv = array_merge($terminate_employee_priv, $transfer_emp_priv);
        $transfer_emp_priv = array_unique($transfer_emp_priv);
        sort($transfer_emp_priv);
        $transfer_emp_priv = array_values($transfer_emp_priv);
        $transfer_emp_priv = implode(',', $transfer_emp_priv);

        $terminate_data = array(
            'terminate_by_whom' => $request->session()->get('id'),
            'privilege' => NULL,
            'status' => 'deactive',
        );
        LoginModel::where('id', $employee_id)->update($terminate_data);

        $data = array(
            'privilege' => $transfer_emp_priv,
        );


        LoginModel::where('id', $request->input('replace_login_id'))->update($data);




        return redirect('/all-employee')->with('success', 'Employee Has been terminated');
    }

}
