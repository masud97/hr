<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Library\LeaveLib as LeaveLib;
use App\Models\NotificationManagerModel as Notify;
use App\Http\Requests;

class Ajax extends Controller {

    public function checkNotifications(Request $request) {
        /* get pending application wating for this user */
        $id = $request->session()->get('id');
        $watingApplications = Notify::select('id', 'type', 'related_id', 'message', 'url', 'purpose')->where(array('employee_login_id' => $id, 'status' => 'unseen'))->where('purpose', '!=', 'leave')->get();
        if (count($watingApplications) > 0) {
            $i = 0;
            foreach ($watingApplications as $row) {
                $data[$i]['purpose'] = $row->purpose;
                $data[$i]['type'] = $row->type;
                $data[$i]['relatedId'] = $row->related_id;
                $data[$i]['url'] = $row->url;
                $data[$i]['message'] = $row->message;
                $data[$i++]['id'] = $row->id;
            }

            echo json_encode(array('status' => true, 'data' => $data));
        } else {
            echo json_encode(array('status' => false, 'message' => 'No data Found'));
        }
    }

    public function checkNotificationsLeave(Request $request) {
        /* get pending application wating for this user */
        $id = $request->session()->get('id');
        $watingApplications = Notify::select('id', 'type', 'related_id', 'message', 'url', 'purpose')->where(array('employee_login_id' => $id, 'status' => 'unseen', 'purpose' => 'leave'))->get();
        if (count($watingApplications) > 0) {
            $i = 0;
            foreach ($watingApplications as $row) {
                $data[$i]['purpose'] = $row->purpose;
                $data[$i]['type'] = $row->type;
                $data[$i]['relatedId'] = $row->related_id;
                $data[$i]['url'] = $row->url;
                $data[$i]['message'] = $row->message;
                $data[$i++]['id'] = $row->id;
            }

            echo json_encode(array('status' => true, 'data' => $data));
        } else {
            echo json_encode(array('status' => false, 'message' => 'No data Found'));
        }
    }

    public function mark_all_as_read(Request $request) {

        $employee_id = $request->session()->get('id');

        $data = array(
            'status' => 'seen'
        );
        $where = array(
            'employee_login_id' => $employee_id,
            'status' => 'unseen',
        );
        Notify::where($where)->where('purpose', '!=', 'leave')->update($data);

        return redirect()->back();
    }

}
