<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class DashboardController extends Controller
{
    public function __construct() {
       // no_cash();
    }
    
    public function index(Request $request)
    {
        return redirect('/calender');

        //\App\Library\AuditTrailLib::addTrail(getUserFullName($request->session()->get('id')), 'Now in dashboard');
        return view('content.dashboard',array('title'=>'Home'));
    }
}
