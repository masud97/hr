<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\EmployeeDetailsModel as empDetails;
use DB;
use App\Models\LoginModel as LoginModel;
use Illuminate\Support\Facades\Validator;
use App\Library\EmployeeDetailsLib as empLib;
use App\Library\LeaveLib as LeaveLib;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use App\Models\CalenderModel as CalenderModel;

class Calender extends Controller {

    public function __construct() {
        DB::enableQueryLog();
    }

    public function index() {
        //$employee_id = $request->session()->get('id');


        $company = \App\Library\EmployeeDetailsLib::get_company_name();
        $activeEmployee = \App\Library\EmployeeDetailsLib::all_active_emp_id_name();

        $event_array = array();
        $select = array('id', 'title', 'start_date', 'start_time', 'end_date', 'end_time', 'des', 'color');
        //  $temp = $this->calendar_model->select_where_descending($select, $where);
        $temp = CalenderModel::select($select)->where('status', 'active')->get();
        if (!count($temp) > 0) {
            $temp = NULL;
        }
        if (!is_null($temp)) {
            $i = 0;
            foreach ($temp as $value) {
                $event_array[$i][0] = $value->id;
                $event_array[$i][1] = $value->employee_id;
                $event_array[$i][2] = $value->title;
                $event_array[$i][3] = $value->start_date;
                $event_array[$i][4] = $value->start_time;
                $event_array[$i][5] = $value->end_date;
                $event_array[$i][6] = $value->end_time;
                $event_array[$i][7] = $value->des;
                if (is_null($value->color)) {
                    $event_array[$i][8] = "#CF1521";
                } else {
                    $event_array[$i][8] = $value->color;
                }
                $i++;
            }
        } else {
            $event_array = array();
        }

        $temp = empLib::get_emp_birthday_monthly();
        if (!is_null($temp)) {
            $event_array = array_merge($event_array, $temp);
        }
       
        $temp2 = LeaveLib::get_leave_interval_yearly_calender();
        //printIt($temp2); 
        if (!is_null($temp2)) {
            $event_array = array_merge($event_array, $temp2);
        }
        $temp3 = get_public_holiday();
        if (!is_null($temp3)) {
            $event_array = array_merge($event_array, $temp3);
        }

        //  printIt($event_array); die();
        $data = array(
            'company' => $company,
            'activeEmployee' => $activeEmployee,
            'title' => 'Calender',
            'customJs' => 'calender-js',
            'event_array' => $event_array
        );

        // echo changeDateFormat("2016-08-16 00:00:00", DATE_RFC822); die();
        return view('content.calender.calender_view', $data);
    }

    public function create_calender_submit(Request $request) {

        $event_array = NULL;
        $employee_id = $request->session()->get('id');

        $validator = Validator::make($request->all(), [
                    'title' => 'required',
                    'start_date' => 'required',
                    'selected_emp' => 'required',
                    'end_date' => 'required',
        ]);



        if ($validator->fails()) {
            return redirect()->back()
                            ->withErrors($validator)
                            ->withInput();
        }

        ///color picker
        $color = $request->input('event_color');

        if ($color == "#F89406" or $color == "#396CCF" or $color == "#ff0000") {
            return redirect()->back()
                            ->withErrors('Please select another color code')
                            ->withInput();
        }



        $title_ = $request->input('title');


        /////
        $start_date_ = $request->input('start_date');
        $start_date = str_replace('/', '-', $start_date_);
        $start_date = date("Y-m-d", strtotime($start_date));

        /////////
        $start_time_ = $request->input('start_time');
        $start_time = date("H:i", strtotime($start_time_));

        //////
        $end_date_ = $request->input('end_date');
        $end_date = str_replace('/', '-', $end_date_);
        $end_date = date("Y-m-d", strtotime($end_date));
        /////////////////
        $end_time_ = $request->input('end_time');
        $end_time = date("H:i", strtotime($end_time_));
        $des = $request->input('des');

        $selected_emp = $request->input('selected_emp');
        $remind_before = $request->input('remind_before');




        $timeDiff = CalenderTimeDiff($start_date, $start_time);


        //    $activeEmployee = EmployeeDetailsLib::all_active_emp_id_name();
        //masud
        $temp_all_company = empLib::get_company_name();


        $all_company_name = NULL;
        if (!is_null($temp_all_company)) {

            $all_company_name = $temp_all_company;
        } else {
            return redirect()->back()
                            ->withErrors('Company name not found')
                            ->withInput();
        }
        //end masud

        $reminder_for = "";
        foreach ($selected_emp as $emp) {
            $check = 0;
            if ($emp === 'all') {
                $reminder_for = 'all';
                break;
            } else {

                $company_emp_id_for_notice = NULL;
                for ($j = 0; $j < sizeof($selected_emp); $j++) {
                    $data = $selected_emp[$j];
                    if (in_array($data, $all_company_name)) {
                        $check = 1;
                        $temp_companyemp_id = empDetails::select('employee_login_id')->where('company_name', $selected_emp[$j])->get();

                        foreach ($temp_companyemp_id as $value) {
                            $company_emp_id_for_notice[] = $value->employee_login_id;
                        }
                    } else {
                        $company_emp_id_for_notice[] = $data;
                    }
                }
                if ($check != 1) {
                    $reminder_for = $reminder_for . $emp . '|:|';
                } else if ($check == 1) {
                    $reminder_for = implode('|:|', $company_emp_id_for_notice);
                }
            }
        }

        $data = array(
            'remind_before' => $remind_before,
            'reminder_for' => $reminder_for,
            'employee_login_id' => $employee_id,
            'title' => $title_,
            'start_date' => $start_date,
            'start_time' => $start_time,
            'end_date' => $end_date,
            'end_time' => $end_time,
            'des' => $des,
            'status' => 'active',
            'insert_date_time' => date('Y-m-d h:i'),
            'email_status' => 0,
            'color' => $color,
            'insert_date_time' => time()
        );
        //printIt($data); die();
        $insert_data = CalenderModel::create($data);
        if ($insert_data) {

            $id = $insert_data->id;

            if ($start_date == date('Y-m-d') and $timeDiff <= $remind_before) {


                if ($reminder_for === 'all' and $check == 0) {
                    $all_emp_email = empLib::getAllActiveUserInfo(array('employee_login_id', 'email_work'));
                    if (count($all_emp_email) > 0) {
                        foreach ($all_emp_email as $val) {
                            $mail_array = array(
                                'email' => $val->email_work,
                                'name' => empLib::getEmployeeName($val->employee_login_id),
                                'title' => $title_,
                                'des' => $des,
                                'start_time_' => $start_time_
                            );

                            Mail::send('email-body.event', $mail_array, function($message) use ($mail_array) {

                                $message->to($mail_array['email'], $mail_array['name'])->subject('Event Alert');
                            });
                            $notification = array(
                                'employee_login_id' => $val->employee_login_id,
                                'message' => "New event has been added",
                                'url' => "/all-event",
                                'purpose' => 'event',
                                'insert_date_time' => date('Y-m-d h:i:s a'),
                                'update_date_time' => NUll,
                                'status' => 'unseen',
                                'type' => 'info',
                                'related_id' => $val->employee_login_id
                            );
                            \App\Models\NotificationManagerModel ::create($notification);
                        }
                    }
                    CalenderModel::where('id', $id)->update(array('email_status' => 1));
                } else if ($check == 1 and ! is_null($company_emp_id_for_notice)) {

                    for ($z = 0; $z < sizeof($company_emp_id_for_notice); $z++) {

                        $mail_array = array(
                            'email' => empLib::getEmployeeWorkEmailById($company_emp_id_for_notice[$z]),
                            'name' => empLib::getEmployeeName($company_emp_id_for_notice[$z]),
                            'title' => $title_,
                            'des' => $des,
                            'start_time_' => $start_time_
                        );

                        Mail::send('email-body.event', $mail_array, function($message) use ($mail_array) {

                            $message->to($mail_array['email'], $mail_array['name'])->subject('Event Alert');
                        });
                        $notification = array(
                            'employee_login_id' => $company_emp_id_for_notice[$z],
                            'message' => "New event has been added",
                            'url' => "/all-event",
                            'purpose' => 'event',
                            'insert_date_time' => date('Y-m-d h:i:s a'),
                            'update_date_time' => NUll,
                            'status' => 'unseen',
                            'type' => 'info',
                            'related_id' => $company_emp_id_for_notice[$z]
                        );
                        \App\Models\NotificationManagerModel ::create($notification);
                    }
                    CalenderModel::where('id', $id)->update(array('email_status' => 1));
                } else if ($check == 0) {
                    for ($z = 0; $z < sizeof($selected_emp); $z++) {

                        $mail_array = array(
                            'email' => empLib::getEmployeeWorkEmailById($selected_emp[$z]),
                            'name' => empLib::getEmployeeName($selected_emp[$z]),
                            'title' => $title_,
                            'des' => $des,
                            'start_time_' => $start_time_
                        );

                        Mail::send('email-body.event', $mail_array, function($message) use ($mail_array) {

                            $message->to($mail_array['email'], $mail_array['name'])->subject('Event Alert');
                        });
                        $notification = array(
                            'employee_login_id' => $selected_emp[$z],
                            'message' => "New event has been added",
                            'url' => "/all-event",
                            'purpose' => 'event',
                            'insert_date_time' => date('Y-m-d h:i:s a'),
                            'update_date_time' => NUll,
                            'status' => 'unseen',
                            'type' => 'info',
                            'related_id' => $selected_emp[$z]
                        );
                        \App\Models\NotificationManagerModel ::create($notification);
                    }
                    CalenderModel::where('id', $id)->update(array('email_status' => 1));
                }
            }

            return redirect()->back()->with('success', 'Event added successfully');
        } else {
            return redirect()->back()->withInput()->withErrors('Event added failed');
        }
    }

    public function all_event_list(Request $request, $notificationId = NULL) {
        $employee_id = $request->session()->get('id');
        if (!is_null($notificationId)) {
            \App\Models\NotificationManagerModel::where(array('id' => $notificationId, 'related_id' => $employee_id))->update(array('status' => 'seen', 'update_date_time' => date('Y-m-d H:i:s')));
        }
        $event_array = NULL;
        $select = array('id', 'status', 'employee_login_id', 'title', 'start_date', 'start_time', 'end_date', 'end_time', 'des', 'reminder_for', 'remind_before');
        //  $temp = $this->calendar_model->select_where_descending($select, $where);
        $temp = CalenderModel::select($select)->orderBy('id', 'DESC')->get();
        if (!count($temp) > 0) {
            $temp = NULL;
        }
        if (!is_null($temp)) {
            $i = 0;
            foreach ($temp as $value) {
                $reminder_for_temp_emp_name = NULL;
                $event_array[$i][0] = $value->id;
                $event_array[$i][1] = empLib::getEmployeeName($value->employee_login_id);
                $event_array[$i][2] = $value->title;
                $event_array[$i][3] = changeDateFormat($value->start_date, 'd-m-Y');
                $event_array[$i][4] = date("h:i a", strtotime($value->start_time));
                $event_array[$i][5] = changeDateFormat($value->end_date, 'd-m-Y');
                $event_array[$i][6] = date("h:i a", strtotime($value->end_time));
                $event_array[$i][7] = $value->des;
                $event_array[$i][11] = NULL;
                if ($value->reminder_for != 'all') {
                    $reminder_for_temp = explode('|:|', $value->reminder_for);

                    foreach ($reminder_for_temp as $val) {
                        $reminder_for_temp_emp_name[] = empLib::getEmployeeName($val);
                        if ($employee_id == $val) {
                            $event_array[$i][11] = 'mine';
                        }
                    }


                    $event_array[$i][8] = rtrim(implode(',', $reminder_for_temp_emp_name), ',');
                } else {
                    $event_array[$i][8] = $value->reminder_for;
                    $event_array[$i][11] = 'mine';
                }
                $event_array[$i][9] = $value->remind_before;
                $event_array[$i][10] = $value->status;

                $i++;
            }
        } else {
            $event_array = NULL;
        }


        $data = array(
            'title' => 'Calender',
            'customJs' => 'calender-all-event',
            'event_array' => $event_array
        );

        // echo changeDateFormat("2016-08-16 00:00:00", DATE_RFC822); die();
        return view('content.calender.all-event-list', $data);
    }

    public function remove_event(Request $request, $id) {

        $employee_id = $request->session()->get('id');

        $data = array(
            'status' => 'deactive',
            'update_emp_id' => $employee_id,
            'update_date_time' => time()
        );

        if (CalenderModel::where('id', $id)->update($data)) {
            return redirect()->back()->with('success', 'Event removed successfully');
        } else {
            return redirect()->back()->withErrors('Event remove failed');
        }
    }

}
