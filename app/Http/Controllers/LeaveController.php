<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\LeaveDetailsModel as LeaveDetailsModel;
use App\Library\LeaveLib as LeaveLib;
use App\Library\EmployeeDetailsLib as EmployeeLib;
use App\Models\NotificationManagerModel as Notify;
use Illuminate\Support\Facades\Mail;

class LeaveController extends Controller {

    public function __construct() {
        
    }

    public function index(Request $request) {

        /* get total leave */
        $totalLeave = LeaveLib::totalLeave();
        $leaveTaken['casual'] = LeaveLib::totalLeaveTaken('casual');
        $leaveTaken['sick'] = LeaveLib::totalLeaveTaken('sick');
        $leaveTaken['half'] = LeaveLib::countHalfDay();
        $data = array(
            'title' => 'Apply For Leave',
            'customJs' => 'apply-for-leave',
            'totalLeave' => $totalLeave,
            'leaveTaken' => $leaveTaken
        );
        return view('content.apply-for-leave', $data);
    }

    public function submit(Request $request) {

        $leaveType = $request->input('leave_type');
        $leavePurpos = $request->input('leave_purpose');
        $from = str_replace('/', '-', $request->input('leave_from'));
        $to = str_replace('/', '-', $request->input('leave_to'));
        $leaveFrom = changeDateFormat($from, 'Y-m-d');
        $leaveTo = changeDateFormat($to, 'Y-m-d');
        $description = $request->input('leave_note');
        $attachment = NULL;

        $this->validate($request, [
            'leave_type' => 'required|max:7',
            'leave_purpose' => 'required|max:7',
            'attachment' => 'mimes:jpeg,jpg,pdf'
        ]);


        if ($request->hasFile('attachment')) {
            if ($request->attachment->getClientOriginalExtension() === 'pdf' or $request->attachment->getClientOriginalExtension() === 'jpg' or $request->attachment->getClientOriginalExtension() === 'jpeg') {
                if ($request->file('attachment')->isValid()) {
                    $attachment = time() . $request->session()->get('id');
                    $folder = base_path() . '/assets/leave-doc';
                    $file = $request->file('attachment');
                    $type = $file->getClientOriginalExtension();
                    $attachment = time() . $request->session()->get('id') . '.' . $type;
                    $request->file('attachment')->move($folder, $attachment);
                }
            }
        }

        /* form validation will be goes here */


        if ($leaveType === 'full') {
            if ($leavePurpos === 'casual') {
                $return = LeaveLib::casulaLeaveFull($leaveFrom, $leaveTo, $description, $attachment);

                if ($return['status'] === 'error') {
                    \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), $return['message'], 'error');

                    return redirect('/apply-for-leave')->withErrors($return['message'])->withInput();
                } else {
                    \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'Your full day casual application has been sent.', 'success');
                    //send email to watingfor
                    $mail = array(
                        'email' => getUserEmail($return['watingFor']),
                        'name' => getUserFullName($return['watingFor']),
                        'subject' => 'Pending leave application',
                        'body' => '<h4>Dear ' . getUserFullName($return['watingFor']) . '</h4><p>You have a pending full day casual leave application from ' . getUserFullName($request->session()->get('id')) . ' (' . $request->input('leave_from') . ' to ' . $request->input('leave_to') . ') for ' . $return['leave_amount'] . ' day(s)</p>'
                    );

                    LeaveController::sendEmail($mail);

                    return redirect('/apply-for-leave')->with('success', 'Your appliaction has been sent');
                }
            } else if ($leavePurpos === 'sick') {
                $return = LeaveLib::sickLeaveFull($leaveFrom, $leaveTo, $description, $attachment);
                if ($return['status'] === 'error') {
                    \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), $return['message'], 'error');
                    return redirect('/apply-for-leave')->withErrors($return['message'])->withInput();
                } else {
                    \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'Your full day sick application has been sent.', 'success');

                    $mail = array(
                        'email' => getUserEmail($return['watingFor']),
                        'name' => getUserFullName($return['watingFor']),
                        'subject' => 'Pending leave application',
                        'body' => '<h4>Dear ' . getUserFullName($return['watingFor']) . '</h4><p>You have a pending full day sick leave application from ' . getUserFullName($request->session()->get('id')) . ' (' . $request->input('leave_from') . ' to ' . $request->input('leave_to') . ') for ' . $return['leave_amount'] . ' day(s)</p>'
                    );

                    LeaveController::sendEmail($mail);


                    return redirect('/apply-for-leave')->with('success', 'Your appliaction has been sent');
                }
            } else {
                \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'Invalid leave purpose. Purpose provided = ' . $leavePurpos, 'error');
                return redirect('apply-for-leave')->withErrors('Invalid leave Purpose')->withInput();
            }
        } else if ($leaveType === 'half') {
            if ($leavePurpos === 'casual') {
                $return = LeaveLib::casualLeaveHalf($leaveFrom, $leaveTo, $description, $attachment);
                if ($return['status'] === 'error') {
                    \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), $return['message'], 'error');
                    return redirect('/apply-for-leave')->withErrors($return['message'])->withInput();
                } else {
                    \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'Your half day casual application has been sent.', 'success');

                    $mail = array(
                        'email' => getUserEmail($return['watingFor']),
                        'name' => getUserFullName($return['watingFor']),
                        'subject' => 'Pending leave application',
                        'body' => '<h4>Dear ' . getUserFullName($return['watingFor']) . '</h4><p>You have a pending half day casual leave application from ' . getUserFullName($request->session()->get('id')) . ' (' . $request->input('leave_from') . ' to ' . $request->input('leave_to') . ') for ' . $return['leave_amount'] . ' day(s)</p>'
                    );

                    LeaveController::sendEmail($mail);

                    return redirect('/apply-for-leave')->with('success', 'Your appliaction has been sent');
                }
            } else if ($leavePurpos === 'sick') {
                $return = LeaveLib::sickLeaveHalf($leaveFrom, $leaveTo, $description, $attachment);
                if ($return['status'] === 'error') {
                    \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), $return['message'], 'error');
                    return redirect('/apply-for-leave')->withErrors($return['message'])->withInput();
                } else {
                    \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'Your half day sick application has been sent.', 'success');

                    $mail = array(
                        'email' => getUserEmail($return['watingFor']),
                        'name' => getUserFullName($return['watingFor']),
                        'subject' => 'Pending leave application',
                        'body' => '<h4>Dear ' . getUserFullName($return['watingFor']) . '</h4><p>You have a pending half day sick leave application from ' . getUserFullName($request->session()->get('id')) . ' (' . $request->input('leave_from') . ' to ' . $request->input('leave_to') . ') for ' . $return['leave_amount'] . ' day(s)</p>'
                    );

                    LeaveController::sendEmail($mail);

                    return redirect('/apply-for-leave')->with('success', 'Your appliaction has been sent');
                }
            } else {
                \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'Invalid leave purpose. Purpose provied = ' . $leavePurpos, 'error');
                return redirect('apply-for-leave')->withErrors('Invalid leave Purpose')->withInput();
            }
        } else if ($leaveType === 'special') {
            $return = LeaveLib::specialLeave($leaveFrom, $leaveTo, $description, $attachment);
            if ($return['status'] === 'error') {
                \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), $return['message'], 'error');
                return redirect('/apply-for-leave')->withErrors($return['message'])->withInput();
            } else {
                \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'Your special leave application has been sent.', 'success');

                $mail = array(
                    'email' => getUserEmail($return['watingFor']),
                    'name' => getUserFullName($return['watingFor']),
                    'subject' => 'Pending leave application',
                    'body' => '<h4>Dear ' . getUserFullName($return['watingFor']) . '</h4><p>You have a special leave application from ' . getUserFullName($request->session()->get('id')) . ' (' . $request->input('leave_from') . ' to ' . $request->input('leave_to') . ') for ' . $return['leave_amount'] . ' day(s)</p>'
                );

                LeaveController::sendEmail($mail);


                return redirect('/apply-for-leave')->with('success', 'Your appliaction has been sent');
            }
        } else {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'Invalid leave type. Leave type  provided = ' . $leaveType, 'error');
            return redirect('apply-for-leave')->withErrors('Invalid leave type')->withInput();
        }
    }

    public function leaveDetails(Request $request, $notificationId = null) {
        //'employee_details.full_name','leave_details.id','leave_details.employee_login_id','leave_details.leave_purpose','leave_details.leave_type','leave_details.leave_amount','leave_details.apply_date'

        if (!is_null($notificationId)) {
            //user come here from notification section
            //so make this notification seen
            Notify::where(array('id' => $notificationId, 'employee_login_id' => $request->session()->get('id'), 'status' => 'unseen'))->update(array('status' => 'seen'));
        }

        $employeDetails = EmployeeLib::getUserInfo(array('full_name', 'email_work', 'image'), array('employee_login_id' => $request->session()->get('id')));
        $leaveDetails = LeaveLib::getLeaveHistroy($request->session()->get('id'), date('Y'));

        $data = array(
            'title' => 'Leave Details',
            'customJs' => 'leave-details',
            'leaveDetails' => $leaveDetails,
            'userDetails' => $employeDetails
        );

        return view('content.leave-details', $data);
    }

    public function admin_leaveDetails_of_employee(Request $request, $employee_id) {

        $hasPrivilege = \App\Library\PrivilegeCheckLib::checkPrivilege(1, $request->session()->get('id'));
        if (!$hasPrivilege) {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'Access deny. Want to view employee leave details. For employee id = ' . $employee_id, 'error');
            return redirect()->back()->withErrors('Access deny');
        }

        if (!is_numeric($employee_id)) {
            return redirect()->back()->withErrors('Invalid employee id');
        }
        $employeDetails = EmployeeLib::getUserInfo(array('full_name', 'email_work', 'image'), array('employee_login_id' => $employee_id));
        $leaveDetails = LeaveLib::getLeaveHistroy($employee_id, date('Y'));
        $lastApprovedApplicationId = LeaveDetailsModel::select('id', 'leave_from', 'leave_to')->where('employee_login_id', $employee_id)->orderBy('apply_date', 'DESC')->first();
        if (!count($lastApprovedApplicationId) > 0) {
            $id = '';
            $from = '';
            $to = '';
        } else {
            $id = $lastApprovedApplicationId->id;
            $from = $lastApprovedApplicationId->leave_from;
            $to = $lastApprovedApplicationId->leave_to;
        }

        $data = array(
            'title' => 'Leave Details',
            'customJs' => 'leave-details',
            'leaveDetails' => $leaveDetails,
            'userDetails' => $employeDetails,
            'lastApprovedApplicationId' => $id,
            'leaveDate' => array('leave_from' => $from, 'leave_to' => $to),
            'employee_id' => $employee_id
        );
        \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'Successfully view employee details. For employee id = ' . $employee_id, 'success');
        return view('content.leave-details', $data);
    }

    public function withdrawApplication(Request $request, $applicationId) {
        $id = $request->session()->get('id');
        if (LeaveLib::withdrawApplication($id, $applicationId)) {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'Leave application has been withdrawn successfully. Applications id = ' . $applicationId, 'success');
            return redirect()->back()->with('success', 'Your application has been withdrawn successfully');
        } else {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'Application withdraw failed. Application id = ' . $applicationId, 'error');
            return redirect()->back()->withErrors('Application withdraw failed');
        }
    }

    public function applicationView(Request $request, $applicationId, $notificationsId = null) {
        $employeeId = $request->session()->get('id');

        $hasPrivilege = \App\Library\PrivilegeCheckLib::checkPrivilege(2, $employeeId);
        if (!$hasPrivilege) {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'Access deny. Want to view the pending applications. Application id = ' . $applicationId, 'error');
            return redirect()->back()->withErrors('Access deny');
        }


        if (!is_null($notificationsId)) {
            /* get notifications id from notification manager */
            $temp = Notify::select('id')->where(array('employee_login_id' => $employeeId, 'related_id' => $applicationId))->first();
            if (!count($temp) > 0) {
                \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'Invalid notification id. notifications id = ' . $notificationsId, 'error');
                return redirect()->back()->withErrors("No notifications id found");
            } else {
                $notificationsId = $temp->id;
            }
        }


        $leaveDetails = LeaveLib::getApplication($applicationId, $employeeId);

        if (!count($leaveDetails) > 0) {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'Invalid application id = ' . $applicationId, 'error');
            //remove the notification id 
            Notify::where(array('related_id' => $applicationId, 'employee_login_id' => $employeeId, 'purpose' => 'leave'))->update(array('status' => 'seen'));
            return redirect()->back()->withErrors('Invalid application id or application has been approved or denied');
        }

        $leaveTaken['casual'] = LeaveLib::totalLeaveTaken('casual', $leaveDetails->employee_login_id);
        $leaveTaken['sick'] = LeaveLib::totalLeaveTaken('sick', $leaveDetails->employee_login_id);
        $leaveTaken['half'] = LeaveLib::countHalfDay($leaveDetails->employee_login_id);
        $totalLeave = LeaveLib::totalLeave();


        $data = array(
            'title' => 'Application View',
            'leaveDetails' => $leaveDetails,
            'totalLeave' => $totalLeave,
            'leaveTaken' => $leaveTaken,
            'applicatoinId' => $applicationId,
            'notificationId' => $notificationsId,
            'customJs' => 'pending-application-view'
        );
        \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'Application view. Application id= ' . $applicationId, 'success');

        return view('content.pending-application-view', $data);
    }

    public function approveOrDenyApplication(Request $request) {

        $applicationId = $request->input('application_id');
        $watingForEmpId = $request->session()->get('id');
        $this->validate($request, [
            'from' => 'required',
            'to' => 'required',
            'application_id' => 'required|numeric',
            'action' => 'required|max:7',
            'notificationId' => 'numeric'
        ]);

        if ($request->input('action') === 'deny') {
            /* deny application */
            $return = LeaveLib::denyLeave($watingForEmpId, $applicationId, $request->input('leave_des'), $request->input('notificationId'));
            if ($return['status'] === 'error') {
                \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), $return['message'] . ". Application id = " . $applicationId, 'error');

                return redirect()->back()->withErrors($return['message']);
            } else {
                \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'Application deny. Application id= ' . $applicationId, 'success');

                $mail = array(
                    'email' => getUserEmail($return['employee_id']),
                    'name' => getUserFullName($return['employee_id']),
                    'subject' => 'Denied application',
                    'body' => '<h4>Dear ' . getUserFullName($return['employee_id']) . '</h4><p>Your leave application has been denied by ' . getUserFullName($watingForEmpId) . '</p>'
                );

                LeaveController::sendEmail($mail);

                return redirect('/dashboard')->with('success', 'Application deny');
            }
        } else if ($request->input('action') === 'approve') {

            $from = str_replace('/', '-', $request->input('from'));
            $to = str_replace('/', '-', $request->input('to'));
            $return = LeaveLib::approveLeave($watingForEmpId, $applicationId, $request->input('notificationId'), changeDateFormat($from, 'Y-m-d'), changeDateFormat($to, 'Y-m-d'), $request->input('leave_des'));
            // var_dump($return); die();
            if ($return['status'] === 'error') {
                \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), $return['message'] . '. Application id= ' . $applicationId, 'error');

                return redirect()->back()->withErrors($return['message']);
            } else {
                \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'Application Approved. Application id= ' . $applicationId, 'success');


                if ($return['nowWatingForWillBE'] === 'approved') {
                    $mail = array(
                        'email' => getUserEmail($return['employeeId']),
                        'name' => getUserFullName($return['employeeId']),
                        'subject' => 'Application approved',
                        'body' => '<h4>Dear ' . getUserFullName($return['employeeId']) . '</h4><p>Your leave application has been approved by ' . getUserFullName($watingForEmpId) . ' (' . $return['from'] . ' to ' . $return['to'] . ') for ' . $return['amount'] . ' day(s)</p>'
                    );

                    LeaveController::sendEmail($mail);
                } else {
                    $mail = array(
                        'email' => getUserEmail($return['nowWatingForWillBE']),
                        'name' => getUserFullName($return['nowWatingForWillBE']),
                        'subject' => 'Pending leave application',
                        'body' => '<h4>Dear ' . getUserFullName($return['nowWatingForWillBE']) . '</h4><p>You have a pending ' . $return['type'] . ' day ' . $return['purpose'] . ' leave application from ' . getUserFullName($return['employeeId']) . ' (' . $return['from'] . ' to ' . $return['to'] . ') for ' . $return['amount'] . ' day(s)</p>'
                    );

                    LeaveController::sendEmail($mail);
                }


                return redirect('/dashboard')->with('success', 'Application approved');
            }
        } else {
            return redirect()->back()->withErrors('Please select an action first');
        }
    }

    public function cancleLeaveApplication(Request $request, $applicationId, $employeeId) {

        $hasPrivilege = \App\Library\PrivilegeCheckLib::checkPrivilege(1, $request->session()->get('id'));
        if (!$hasPrivilege) {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'Access deny in cancleLeaveApplication. Application id= ' . $applicationId, 'error');
            return redirect()->back()->withErrors('Access deny');
        }

        $id = LeaveDetailsModel::select('id')->where(array('employee_login_id' => $employeeId, 'status' => 'approve'))->orderBy('apply_date', 'DESC')->first();
        if (!count($id) > 0) {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'Invalid application id in cancleLeaveApplication. Application id= ' . $applicationId, 'error');
            return redirect()->back()->withErrors('Invalid application id');
        }

        $apId = $id->id;
        if (trim($apId) !== trim($applicationId)) {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'Invalid application id in cancleLeaveApplication. Application id= ' . $applicationId, 'error');

            return redirect()->back()->withErrors('Invalid application id');
        }


        $name = EmployeeLib::getEmployeeName($request->session()->get('id'));

        if (LeaveDetailsModel::where(array('id' => $id->id))->update(array('status' => 'cancle', 'approve_deny_by' => $name))) {

            $notification = array(
                'employee_login_id' => $employeeId,
                'message' => 'Your approved leave application has been canceled by ' . $name,
                'url' => "/profile/leave-details",
                'purpose' => 'leave',
                'insert_date_time' => date('Y-m-d h:i:s a'),
                'update_date_time' => NUll,
                'status' => 'unseen',
                'type' => 'error',
                'related_id' => $applicationId
            );

            Notify::create($notification);
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'Application has been successfully canceled. Application id= ' . $applicationId, 'success');


            $mail = array(
                'email' => getUserEmail($employeeId),
                'name' => getUserFullName($employeeId),
                'subject' => 'Application cancelled',
                'body' => '<h4>Dear ' . getUserFullName($employeeId) . '</h4><p>Your approved leave application has been cancelled by ' . getUserFullName($request->session()->get('id')) . '</p>'
            );

            LeaveController::sendEmail($mail);

            if (Mail::failures()) {
                // return response showing failed emails
                return redirect()->back()->with('success', 'Application has been successfully canceled')->withErrors("Email sending failed");
            }


            return redirect()->back()->with('success', 'Application has been successfully canceled');
        } else {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'Unknown error occurred in cancleLeaveApplication. Application id= ' . $applicationId, 'error');

            return redirect()->back()->withErrors('Unknown error occurred. Please try again later');
        }
    }

    public function cahngeApproveApplicationDate(Request $request, $applicationId, $employeeId) {

        $hasPrivilege = \App\Library\PrivilegeCheckLib::checkPrivilege(1, $request->session()->get('id'));
        if (!$hasPrivilege) {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'Access deny cahngeApproveApplicationDate. Application id= ' . $applicationId, 'error');
            return redirect()->back()->withErrors('Access deny');
        }
        $id = LeaveDetailsModel::select('id', 'leave_purpose')->where(array('employee_login_id' => $employeeId, 'status' => 'approve'))->orderBy('apply_date', 'DESC')->first();
        if (!count($id) > 0) {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'Invalid application id in cahngeApproveApplicationDate. Application id= ' . $applicationId, 'error');

            return redirect()->back()->withErrors('Invalid application id');
        }

        $apId = $id->id;
        if (trim($apId) !== trim($applicationId)) {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'Invalid application id in cahngeApproveApplicationDate. Application id= ' . $applicationId, 'error');

            return redirect()->back()->withErrors('Invalid application id');
        }


        $name = EmployeeLib::getEmployeeName($request->session()->get('id'));

        $to = str_replace('/', '-', $request->input('leave_to_date'));
        $from = str_replace('/', '-', $request->input('leave_from_date'));

        $to = changeDateFormat($to, 'Y-m-d');
        $from = changeDateFormat($from, 'Y-m-d');

        $return = LeaveLib::changeApproveApplicationLeaveDate($from, $to, $applicationId, trim($id->leave_purpose), $employeeId);
        if ($return['status'] === 'error') {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), $return['message'] . ' in cahngeApproveApplicationDate. Application id= ' . $applicationId, 'error');

            $mail = array(
                'email' => getUserEmail($employeeId),
                'name' => getUserFullName($employeeId),
                'subject' => 'Application date changed',
                'body' => '<h4>Dear ' . getUserFullName($employeeId) . '</h4><p>Your approved leave application date has been changed by ' . getUserFullName($request->session()->get('id')) . '<br/>New date range are ' . $from . ' to ' . $to . '</p>'
            );

            LeaveController::sendEmail($mail);

            return redirect()->back()->withErrors($return['message']);
        } else {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), $return['message'] . ' in cahngeApproveApplicationDate. Application id= ' . $applicationId, 'success');

            return redirect()->back()->with('success', $return['message']);
        }
    }

    public function leaveHistroy(Request $request) {
        /* get all employee name and id */
        $selectedItem = array('employee_details.full_name', 'login.id');
        $where = array('status' => 'active');
        $employeeList = EmployeeLib::getAllActiveUserInfo($selectedItem, $where);

        if (!count($employeeList) > 0) {
            \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'No employee found in leaveHistroy', 'error');

            return redirect()->back()->withErrors("No employee found");
        }

        $i = 0;
        $employeeNameAndId = NULL;
        foreach ($employeeList as $row) {
            $employeeNameAndId[$i]['id'] = $row->id;
            $employeeNameAndId[$i++]['full_name'] = $row->full_name;
        }

        $company = \App\Library\EmployeeDetailsLib::get_company_name();
        $data = array(
            'title' => 'Leave History',
            'employeeNameAndId' => $employeeNameAndId,
            'company' => $company,
            'customJs' => 'report-leave-history'
        );

        \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'View leave histroy', 'success');

        return view('content.report-leave-history', $data);
    }

    public function leaveHistorySearch(Request $request) {

        $this->validate($request, [
            'from_date' => 'required',
            'to_date' => 'required',
        ]);


        $fromDate = str_replace('/', '-', $request->input('from_date'));
        $toDate = str_replace('/', '-', $request->input('to_date'));

        $fromDate = changeDateFormat($fromDate, 'Y-m-d');
        $toDate = changeDateFormat($toDate, 'Y-m-d');
        $id = $request->input('id');
        $leaveHistory = NULL;

        $company = \App\Library\EmployeeDetailsLib::get_company_name();

        if (is_numeric($id)) {
            if ($id === '0') {
                //no user selected so search by only date
                $leaveHistory = LeaveDetailsModel::select('employee_details.full_name', 'leave_details.status', 'leave_details.apply_date', 'leave_details.leave_from', 'leave_details.leave_to', 'leave_details.leave_amount', 'leave_details.leave_purpose', 'leave_details.leave_type')
                        ->join('employee_details', 'employee_details.employee_login_id', '=', 'leave_details.employee_login_id')
                        ->where('leave_details.leave_from', '>=', $fromDate)
                        ->where('leave_details.leave_to', '<=', $toDate)
                        ->orderBy('apply_date', 'DESC')
                        ->get();
            } else {
                //user selected so search by user and date also
                $leaveHistory = LeaveDetailsModel::select('employee_details.full_name', 'leave_details.status', 'leave_details.apply_date', 'leave_details.leave_from', 'leave_details.leave_to', 'leave_details.leave_amount', 'leave_details.leave_purpose', 'leave_details.leave_type')
                        ->join('employee_details', 'employee_details.employee_login_id', '=', 'leave_details.employee_login_id')
                        ->where('leave_details.leave_from', '>=', $fromDate)
                        ->where('leave_details.leave_to', '<=', $toDate)
                        ->where('leave_details.employee_login_id', $id)
                        ->orderBy('apply_date', 'DESC')
                        ->get();
            }
        } else {


            if (!in_array($id, $company)) {
                return redirect()->back()->withErrors('Invalid compnay name');
            }


            /* select company name */
            /* get employee id by using the compnay name */
            $employeeIds = \App\Library\EmployeeDetailsLib::getEmployeeNameByUsingCompanyName($id);
            if (count($employeeIds) > 0) {
                foreach ($employeeIds as $emp) {
                    $ids[] = $emp->employee_login_id;
                }

                $leaveHistory = LeaveDetailsModel::select('employee_details.full_name', 'leave_details.apply_date', 'leave_details.status', 'leave_details.leave_from', 'leave_details.leave_to', 'leave_details.leave_amount', 'leave_details.leave_purpose', 'leave_details.leave_type')
                        ->join('employee_details', 'employee_details.employee_login_id', '=', 'leave_details.employee_login_id')
                        ->where('leave_details.leave_from', '>=', $fromDate)
                        ->where('leave_details.leave_to', '<=', $toDate)
                        ->whereIn('leave_details.employee_login_id', $ids)
                        ->orderBy('apply_date', 'DESC')
                        ->get();
            } else {
                return redirect()->back()->withErrors("No employee found");
            }
        }






        /* get all employee name and id */
        $selectedItem = array('employee_details.full_name', 'login.id');
        $where = array('status' => 'active');
        $employeeList = EmployeeLib::getAllActiveUserInfo($selectedItem, $where);
        if (!count($employeeList) > 0) {
            return redirect()->back()->withErrors("No employee found");
        }

        $i = 0;
        $employeeNameAndId = NULL;
        foreach ($employeeList as $row) {
            $employeeNameAndId[$i]['id'] = $row->id;
            $employeeNameAndId[$i++]['full_name'] = $row->full_name;
        }

        $data = array(
            'title' => 'Leave History',
            'employeeNameAndId' => $employeeNameAndId,
            'company' => $company,
            'customJs' => 'report-leave-history',
            'leaveHistory' => $leaveHistory
        );


        \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'Search leaveHistroy', 'success');

        return view('content.report-leave-history', $data);
    }

    public function allPendingApplications(Request $request) {
        $pendingApplications = LeaveLib::getWatingApplicationShort($request->session()->get('id'));

        $data = array(
            'title' => 'All Pending Applications',
            'customJs' => 'pending-applications',
            'pendingApplications' => $pendingApplications
        );

        \App\Library\AuditTrailLib::addTrail($request->session()->get('name'), 'View allPendingApplications', 'success');

        return view('content.pending-applications', $data);
    }

    private static function sendEmail($data) {
        Mail::send('email-body.leave', $data, function ($message) use ($data) {
            $message->to($data['email'], $data['name'])->subject($data['subject']);
        });
    }

}
