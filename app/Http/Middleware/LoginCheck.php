<?php

namespace App\Http\Middleware;

use Closure;
use App\Library\PrivilegeCheckLib as PrivilegeCheckLib;
class LoginCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if(!$request->session()->get('is_login'))
        {
            /*redirect to login page*/
            return redirect('/')->withErrors('Please Login');
        }
        
        //now check is user is active or deactive
        if(!PrivilegeCheckLib::hasActive($request->session()->get('id')))
        {
            $request->session()->flush();
            return redirect('/')->withErrors('Your account has been deactivated');
        }
        
        /*Now check Privilege*/
        //PrivilegeCheckLib::hasPrivilege();
       
        return $next($request);
    }
}
