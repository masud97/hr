<?php

namespace App\Http\Middleware;

use Closure;
use App\Library\PrivilegeCheckLib as PrivilegeCheckLib;
use Illuminate\Support\Facades\Session;

class CheckPrivileges {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {


        if (!PrivilegeCheckLib::hasPrivilege(Session::get('id')))
        {
            return redirect()->back()->withErrors('Access deny');
        }

        return $next($request);
    }

}
