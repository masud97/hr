var Script = function () {

    $.validator.setDefaults({
       // submitHandler: function() { alert("submitted!"); }
    });

    $().ready(function() {
        // validate the comment form when it is submitted
        $("#commentForm").validate();

        // validate signup form on keyup and submit
        $("#signupForm").validate({
            rules: {
                firstname: "required",
                lastname: "required",
                phone:{
					required: true
				},
				o_email:{
				  required: false
				},
				pre_add:{
				  required: false
				},
				hig_deg:{
				  required: false
				},
				mobile_num_o:{
				  required: false
				},
				
				com_name:{
				  required: true
				},
				per_add:{
				  required: true
				},
				join_date:{
				  required: true
				},
				
				p_phone:{
				  required: true
				},
				
				join_date: {
					required: true
				},
                username: {
                    required: true,
                    minlength: 2
                },
                password: {
                    required: true,
                    minlength: 6
                },
                confirm_password: {
                    required: true,
                    minlength: 6,
                    equalTo: "#password"
                },
                email: {
                    required: true,
                    email: true
                },
				o_email:{
					required: true,
					email: true
				},
				emp_id:{
					required: true,
					maxlength: 6
				},
				
                topic: {
                    required: "#newsletter:checked",
                    minlength: 2
                },
				emer_con: {
					required: true
				},
                agree: "required"
            },
            messages: {
                firstname: "Please enter your firstname",
                lastname: "Please enter your lastname",
				
				phone: {
                    required: "Please enter Gurdian phone number"
                },
				
                
                username: {
                    required: "Please enter a username",
                    minlength: "Your username must consist of at least 2 characters"
                },
                password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 6 characters long"
                },
                confirm_password: {
                    required: "Please Re-enter your password",
                    minlength: "Your password must be at least 6 characters long",
                    equalTo: "Please enter the same password as above"
                },
                email: "Please enter a valid personal email address",
				emer_con: "This field is required",
				hig_deg: "This field is required",
				per_add: "Please enter your Valid Permanat Address",
				
			
				emp_id:"Please enter Valid ID",
				com_name:"Please enter Valid Company Name",
				join_date:"Please enter Valid Join Date",
				p_phone:"Please enter Valid phone number",
				
                agree: "Please accept our policy"
            }
        });

        // propose username by combining first- and lastname
        $("#username").focus(function() {
            var firstname = $("#firstname").val();
            var lastname = $("#lastname").val();
            if(firstname && lastname && !this.value) {
                this.value = firstname + "." + lastname;
            }
        });

        //code to hide topic selection, disable for demo
        var newsletter = $("#newsletter");
        // newsletter topics are optional, hide at first
        var inital = newsletter.is(":checked");
        var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
        var topicInputs = topics.find("input").attr("disabled", !inital);
        // show when newsletter is checked
        newsletter.click(function() {
            topics[this.checked ? "removeClass" : "addClass"]("gray");
            topicInputs.attr("disabled", !this.checked);
        });
    });


}();