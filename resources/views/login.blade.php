<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Arnob">
        <meta name="author-details" content="mdarnob007@gmail.com">
        <meta name="keyword" content="HR,Human Resorce,Big Web Technology Ltd">

        <title>Human Resource - Login</title>

        <!-- Bootstrap core CSS -->
        <link href="{{asset("assets/css/bootstrap.min.css")}}" rel="stylesheet">
        <link href="{{asset("assets/css/bootstrap-reset.css")}}" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="{{asset("assets/css/style.css")}}" rel="stylesheet">
        <link href="{{asset("assets/css/style-responsive.css")}}" rel="stylesheet" />

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
        <!--[if lt IE 9]>
        <script src="{{asset("js/html5shiv.js")}}"></script>
        <script src="{{asset("js/respond.min.js")}}"></script>
        <![endif]-->
    </head>

    <body class="login-body">

        <div class="container">

            <form class="form-signin" action="{{ url('login-check') }}" method="post">
                <h2 class="form-signin-heading">sign in now</h2>


                @include('common.error_and_success_alert')


                <div class="login-wrap">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="text" name="employee_id" class="form-control" placeholder="Employee ID" autofocus required>
                    <input type="password" name="password" class="form-control" placeholder="Password" required>

                    <button class="btn btn-lg btn-login btn-block" type="submit">Sign in</button>



                    <div class="registration">
                        <span class="pull-right">
                            <a data-toggle="modal" href="#myModal"> Forgot Password?</a>
                        </span>      
                    </div>

                </div>
            </form>

            <!-- Modal -->
            <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
                <div class="modal-dialog">
                    <form action="{{url('send-token')}}" method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Forgot Password ?</h4>
                            </div>
                            <div class="modal-body">

                                <p>Enter your e-mail address below to reset your password.</p>
                                <input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">

                            </div>
                            <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                                <!--<button data-dismiss="modal" class="btn btn-success" type="submit"><a href="" style="color: wheat;">Submit</a></button>-->
                                <button type="submit" class="btn btn-info">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- modal -->


        </div>



        <!-- js placed at the end of the document so the pages load faster -->
        <script src="{{asset("assets/js/jquery.js")}}"></script>
        <script src="{{asset("assets/js/bootstrap.min.js")}}" ></script>


    </body>
</html>