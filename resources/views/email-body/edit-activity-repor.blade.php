<div style="margin:0;padding:0" bgcolor="#FFFFFF">
    <table width="100%" height="100%" style="min-width:348px" border="0" cellspacing="0" cellpadding="0">
        <tbody>
            <tr height="32px"></tr>
            <tr align="center">
                <td width="32px"></td>
                <td>
                    <table border="0" cellspacing="0" cellpadding="0" style="max-width:600px">
                        <tbody>
                            <tr height="16"></tr>
                            <!--Header section-->
                            <tr>
                                <td>
                                    <table bgcolor="#f2ba56" width="100%" border="0" cellspacing="0" cellpadding="0" style="min-width:332px;max-width:600px;border:1px solid #e0e0e0;border-bottom:0;border-top-left-radius:3px;border-top-right-radius:3px">
                                        <tbody>
                                            <tr>
                                                <td width="72px" height="72px">
                                                    <img width="32" height="32" style="display:block;width:72px;min-height:72px;padding-left: 8px;padding-top: 8px;" src="http://hr.azizfoods.com/assets/img/logo.png" class="CToWUd">
                                                </td>
                                                <td height="72px" colspan="1" style="font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:24px;color:#000;line-height:1.25;padding-left: 10px ">
                                                    Aziz Food Products LTD
                                                </td>

                                            </tr>
                                            <tr>
                                                <td height="8px" colspan="3"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>

                            <!--header section end--> 

                            <tr>
                                <td>
                                    <table bgcolor="#FAFAFA" width="100%" border="0" cellspacing="0" cellpadding="0" style="min-width:332px;max-width:600px;border:1px solid #f0f0f0;border-bottom:1px solid #c0c0c0;border-top:0;border-bottom-left-radius:3px;border-bottom-right-radius:3px">
                                        <tbody>

                                            <tr height="16px">
                                                <td width="32px" rowspan="6"></td>
                                                <td></td>
                                                <td width="32px" rowspan="6"></td>
                                            </tr>

                                            <!--Top contain--> 
                                            <tr>
                                                <td>
                                                    <table style="min-width:500px" border="0" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                            <tr>
                                                                <td style="font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:13px;color:#202020;line-height:1.5">
                                                                    <b><br>Dear {{$name}},</b>
                                                                    <p>{{$emp_name}} has been edited an activity report.</p>
                                                                    <p><b>Updated at : {{$insert_date_time}}</b></p><br>
                                                                </td>
                                                            </tr>
                                                            <tr height="16px"></tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <!--top contain end--> 

                                            <!--    main contain table-->

                                            <tr>
                                                <td>
                                                    <table style="min-width:500px;font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:13px;color:#1d1a1a ;border-collapse: collapse" cellspacing="0" cellpadding="0" >
                                                        <thead style="border-bottom: 1px solid #f2ba56;text-align: left;line-height: 20px;font-weight: normal; font-size: 12px;margin-bottom: 10px;">
                                                            <tr>
                                                            <tr>
                                                                <th>Field Name</th>
                                                                <th>Old Data</th>
                                                                <th>New Data</th>

                                                            </tr>
                                                            </tr>
                                                        </thead>
                                                        <tbody >
                                                            @if($old_data->report_from != $new_data['report_from'])
                                                            <tr style="line-height: 20px;border-bottom: 1px solid #ddd;" >
                                                                <td>Report From</td>
                                                                <td>{{$old_data->report_from}}</td>
                                                                <td>{{$new_data['report_from']}}</td>
                                                            </tr >
                                                            @endif
                                                            @if($old_data->report_to != $new_data['report_to'])
                                                            <tr style="line-height: 20px;border-bottom: 1px solid #ddd;" >
                                                                <td>Report To</td>
                                                                <td>{{$old_data->report_to}}</td>
                                                                <td>{{$new_data['report_to']}}</td>
                                                            </tr>
                                                            @endif
                                                            @if($old_data->self_a != $new_data['self_a'])
                                                            <tr style="line-height: 20px;border-bottom: 1px solid #ddd;" >
                                                                <td>Self Assessment</td>
                                                                <td>{{$old_data->self_a}}</td>
                                                                <td>{{$new_data['self_a']}}</td>
                                                            </tr >
                                                            @endif
                                                            @if($old_data->activity_complete_this_week != $new_data['activity_complete_this_week'])
                                                            <tr style="line-height: 20px;border-bottom: 1px solid #ddd;" >
                                                                <td>Emergency Mobile</td>
                                                                <td>{{$old_data->activity_complete_this_week}}</td>
                                                                <td>{{$new_data['activity_complete_this_week']}}</td>
                                                            </tr>
                                                            @endif
                                                            @if($old_data->long_term_project != $new_data['long_term_project'])
                                                            <tr style="line-height: 20px;border-bottom: 1px solid #ddd;" >
                                                                <td>Long Term Project</td>
                                                                <td>{{$old_data->long_term_project}}</td>
                                                                <td>{{$new_data['long_term_project']}}</td>
                                                            </tr>
                                                            @endif
                                                            @if($old_data->issue_for_immediate_action != $new_data['issue_for_immediate_action'])
                                                            <tr style="line-height: 20px;border-bottom: 1px solid #ddd;" >
                                                                <td>Issue For Immediate Action</td>
                                                                <td>{{$old_data->issue_for_immediate_action}}</td>
                                                                <td>{{$new_data['issue_for_immediate_action']}}</td>
                                                            </tr>
                                                            @endif
                                                            @if($old_data->key_team != $new_data['key_team'])
                                                            <tr style="line-height: 20px;border-bottom: 1px solid #ddd;" >
                                                                <td>Key Team Dependencies</td>
                                                                <td>{{$old_data->key_team}}</td>
                                                                <td>{{$new_data['key_team']}}</td>
                                                            </tr>
                                                            @endif
                                                            @if($old_data->note != $new_data['note'])
                                                            <tr style="line-height: 20px;border-bottom: 1px solid #ddd;" >
                                                                <td>Note</td>
                                                                <td>{{$old_data->note}}</td>
                                                                <td>{{$new_data['note']}}</td>
                                                            </tr>
                                                            @endif

                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <!--    main contain table end -->
                                            <tr height="16"></tr>

                                            <!--footer section contain-->
                                            <tr  height="16">

                                            </tr>
                                            <!--footer section contain-->
                                            <tr>

                                            </tr>


                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr height="16"></tr>
                            <tr>
                                <td>
                                    <table style="font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:11px;color:#666666;line-height:18px;padding-bottom:10px">
                                        <tbody>
                                            <tr><td style="font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:13px;color:#202020;line-height:1.5" colspan="3">
                                                    <br><p>For More information please visit: <a href="http://hr.azizfoods.com" target="new">http://hr.azizfoods.com</a></p>
                                                </td></tr>
                                            <tr>
                                                <td><b>This is a auto generated message and does not require a reply.</b></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
                <td width="32px"></td>
            </tr>
            <tr height="32px"></tr>
        </tbody>
    </table>
</div>


