<div style="margin:0;padding:0" bgcolor="#FFFFFF">
    <table width="100%" height="100%" style="min-width:348px" border="0" cellspacing="0" cellpadding="0">
        <tbody>
            <tr height="32px"></tr>
            <tr align="center">
                <td width="32px"></td>
                <td>
                    <table border="0" cellspacing="0" cellpadding="0" style="max-width:600px">
                        <tbody>
                            <tr height="16"></tr>
                            <tr>
                                <td>
                                    <table bgcolor="#f2ba56" width="100%" border="0" cellspacing="0" cellpadding="0" style="min-width:332px;max-width:600px;border:1px solid #e0e0e0;border-bottom:0;border-top-left-radius:3px;border-top-right-radius:3px">
                                        <tbody>
                                            <tr>
                                                <td width="72px" height="72px">
                                                    <img width="32" height="32" style="display:block;width:72px;min-height:72px;padding-left: 8px;padding-top: 8px;" src="http://hr.azizfoods.com/assets/img/logo.png" class="CToWUd">
                                                </td>
                                                <td height="72px" colspan="1" style="font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:24px;color:#000;line-height:1.25;padding-left: 10px ">New notice from HR</td>

                                            </tr>
                                            <tr>
                                                <td height="8px" colspan="3"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table bgcolor="#FAFAFA" width="100%" border="0" cellspacing="0" cellpadding="0" style="min-width:332px;max-width:600px;border:1px solid #f0f0f0;border-bottom:1px solid #c0c0c0;border-top:0;border-bottom-left-radius:3px;border-bottom-right-radius:3px">
                                        <tbody>
                                            <tr height="16px">
                                                <td width="32px" rowspan="3"></td>
                                                <td></td>
                                                <td width="32px" rowspan="3"></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table style="min-width:300px" border="0" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                            <tr>
                                                                <td style="font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:13px;color:#202020;line-height:1.5">
                                                                    <br/>
                                                                    <?php echo strip_tags($body, '<br><b><i><h1><h2><h3><h4><h5><h6><u>') ?>
                                                                    <br/>
                                                                    <p>For More information please visit: <a href="http://hr.azizfoods.com" target="new">http://hr.azizfoods.com</a></p>
                                                                    <br/>
                                                                </td>
                                                            </tr>
                                                            <tr height="32px"></tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr height="32px"></tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr height="16"></tr>
                            <tr>
                                <td>
                                    <table style="font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:11px;color:#666666;line-height:18px;padding-bottom:10px">
                                        <tbody>
                                            <tr>
                                                <td><b>This is a auto generated message and does not require a reply.</b></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
                <td width="32px"></td>
            </tr>
            <tr height="32px"></tr>
        </tbody>
    </table>
</div>