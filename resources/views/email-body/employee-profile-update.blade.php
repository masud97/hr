<div style="margin:0;padding:0" bgcolor="#FFFFFF">
    <table width="100%" height="100%" style="min-width:348px" border="0" cellspacing="0" cellpadding="0">
        <tbody>
            <tr height="32px"></tr>
            <tr align="center">
                <td width="32px"></td>
                <td>
                    <table border="0" cellspacing="0" cellpadding="0" style="max-width:600px">
                        <tbody>
                            <tr height="16"></tr>
                            <!--Header section-->
                            <tr>
                                <td>
                                    <table bgcolor="#f2ba56" width="100%" border="0" cellspacing="0" cellpadding="0" style="min-width:332px;max-width:600px;border:1px solid #e0e0e0;border-bottom:0;border-top-left-radius:3px;border-top-right-radius:3px">
                                        <tbody>
                                            <tr>
                                                <td width="72px" height="72px">
                                                    <img width="32" height="32" style="display:block;width:72px;min-height:72px;padding-left: 8px;padding-top: 8px;" src="http://hr.azizfoods.com/assets/img/logo.png" class="CToWUd">
                                                </td>
                                                <td height="72px" colspan="1" style="font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:24px;color:#000;line-height:1.25;padding-left: 10px ">
                                                    Aziz Food Product LTD
                                                </td>

                                            </tr>
                                            <tr>
                                                <td height="8px" colspan="3"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>

                            <!--header section end--> 

                            <tr>
                                <td>
                                    <table bgcolor="#FAFAFA" width="100%" border="0" cellspacing="0" cellpadding="0" style="min-width:332px;max-width:600px;border:1px solid #f0f0f0;border-bottom:1px solid #c0c0c0;border-top:0;border-bottom-left-radius:3px;border-bottom-right-radius:3px">
                                        <tbody>

                                            <tr height="16px">
                                                <td width="32px" rowspan="6"></td>
                                                <td></td>
                                                <td width="32px" rowspan="6"></td>
                                            </tr>

                                            <!--Top contain--> 
                                            <tr>
                                                <td>
                                                    <table style="min-width:500px" border="0" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                            <tr>
                                                                <td style="font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:13px;color:#202020;line-height:1.5">
                                                                    <b><br>Dear {{$admin_name}},</b>
                                                                    <p>{{$name}} has updated @if($old_data_array->gender=='Male') his @else her @endif profile :</p><br>
                                                                </td>
                                                            </tr>
                                                            <tr height="16px"></tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <!--top contain end--> 

                                            <!--    main contain table-->

                                            <tr>
                                                <td>
                                                    <table style="min-width:500px;font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:13px;color:#1d1a1a ;border-collapse: collapse" cellspacing="0" cellpadding="0" >
                                                        <thead style="border-bottom: 1px solid #f2ba56;text-align: left;line-height: 20px;font-weight: normal; font-size: 12px;margin-bottom: 10px;">
                                                            <tr>
                                                            <tr>
                                                                <th>Field Name</th>
                                                                <th>Old Data</th>
                                                                <th>New Data</th>

                                                            </tr>
                                                            </tr>
                                                        </thead>
                                                        <tbody >
                                                            @if($old_data_array->full_name != $updateEmpDetails['full_name'])
                                                            <tr style="line-height: 20px;border-bottom: 1px solid #ddd;" >
                                                                <td>Full Name</td>
                                                                <td>{{$old_data_array->full_name}}</td>
                                                                <td>{{$updateEmpDetails['full_name']}}</td>
                                                            </tr >
                                                            @endif
                                                            @if($old_data_array->email_personal != $updateEmpDetails['email_personal'])
                                                            <tr style="line-height: 20px;border-bottom: 1px solid #ddd;" >
                                                                <td>Personal Email</td>
                                                                <td>{{$old_data_array->email_personal}}</td>
                                                                <td>{{$updateEmpDetails['email_personal']}}</td>
                                                            </tr>
                                                            @endif
                                                            @if($old_data_array->mobile_personal != $updateEmpDetails['mobile_personal'])
                                                            <tr style="line-height: 20px;border-bottom: 1px solid #ddd;" >
                                                                <td>Personal Mobile</td>
                                                                <td>{{$old_data_array->mobile_personal}}</td>
                                                                <td>{{$updateEmpDetails['mobile_personal']}}</td>
                                                            </tr >
                                                            @endif
                                                            @if($old_data_array->mobile_emergency != $updateEmpDetails['mobile_emergency'])
                                                            <tr style="line-height: 20px;border-bottom: 1px solid #ddd;" >
                                                                <td>Emergency Mobile</td>
                                                                <td>{{$old_data_array->mobile_emergency}}</td>
                                                                <td>{{$updateEmpDetails['mobile_emergency']}}</td>
                                                            </tr>
                                                            @endif
                                                            @if($old_data_array->emergency_contact_name_and_relation != $updateEmpDetails['emergency_contact_name_and_relation'])
                                                            <tr style="line-height: 20px;border-bottom: 1px solid #ddd;" >
                                                                <td>Emergency Contact Name And Relation</td>
                                                                <td>{{$old_data_array->emergency_contact_name_and_relation}}</td>
                                                                <td>{{$updateEmpDetails['emergency_contact_name_and_relation']}}</td>
                                                            </tr>
                                                            @endif
                                                            @if($old_data_array->persent_address != $updateEmpDetails['persent_address'])
                                                            <tr style="line-height: 20px;border-bottom: 1px solid #ddd;" >
                                                                <td>Present Address</td>
                                                                <td>{{$old_data_array->persent_address}}</td>
                                                                <td>{{$updateEmpDetails['persent_address']}}</td>
                                                            </tr>
                                                            @endif
                                                            @if($old_data_array->blood_group != $updateEmpDetails['blood_group'])
                                                            <tr style="line-height: 20px;border-bottom: 1px solid #ddd;" >
                                                                <td>Blood Group</td>
                                                                <td>{{$old_data_array->blood_group}}</td>
                                                                <td>{{$updateEmpDetails['blood_group']}}</td>
                                                            </tr>
                                                            @endif
                                                            @if($old_data_array->date_of_birth != $updateEmpDetails['date_of_birth'])
                                                            <tr style="line-height: 20px;border-bottom: 1px solid #ddd;" >
                                                                <td>Date of birth</td>
                                                                <td>{{changeDateFormat($old_data_array->date_of_birth, 'd-m-Y')}}</td>
                                                                <td>{{changeDateFormat($updateEmpDetails['date_of_birth'], 'd-m-Y')}}</td>
                                                            </tr>
                                                            @endif
                                                            @if($old_data_array->marital_status != $updateEmpDetails['marital_status'])
                                                            <tr style="line-height: 20px;border-bottom: 1px solid #ddd;" >
                                                                <td>Marital Status</td>
                                                                <td>{{$old_data_array->marital_status}}</td>
                                                                <td>{{$updateEmpDetails['marital_status']}}</td>
                                                            </tr>
                                                            @endif
                                                            @if($old_data_array->gender != $updateEmpDetails['gender'])
                                                            <tr style="line-height: 20px;border-bottom: 1px solid #ddd;" >
                                                                <td>Gender</td>
                                                                <td>{{$old_data_array->gender}}</td>
                                                                <td>{{$updateEmpDetails['gender']}}</td>
                                                            </tr>
                                                            @endif
                                                            @if($old_data_array->religion != $updateEmpDetails['religion'])
                                                            <tr style="line-height: 20px;border-bottom: 1px solid #ddd;" >
                                                                <td>Religion</td>
                                                                <td>{{$old_data_array->religion}}</td>
                                                                <td>{{$updateEmpDetails['religion']}}</td>
                                                            </tr>
                                                            @endif
                                                            @if($old_data_array->highest_degree_obtained != $updateEmpDetails['highest_degree_obtained'])
                                                            <tr style="line-height: 20px;border-bottom: 1px solid #ddd;" >
                                                                <td>Highest Degree Obtained</td>
                                                                <td>{{$old_data_array->highest_degree_obtained}}</td>
                                                                <td>{{$updateEmpDetails['highest_degree_obtained']}}</td>
                                                            </tr>
                                                            @endif
                                                            @if($old_data_array->image != $updateEmpDetails['image'])
                                                            <tr style="line-height: 20px;border-bottom: 1px solid #ddd;" >
                                                                <td>Profile Picture</td>
                                                                <td colspan="2">Changed</td>
                                                            </tr>
                                                            @endif
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <!--    main contain table end -->
                                            <tr height="16"></tr>

                                            <!--footer section contain-->
                                            <tr  height="16">
                                                
                                            </tr>
                                            <!--footer section contain-->
                                            <tr>
                                                
                                            </tr>


                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr height="16"></tr>
                            <tr>
                                <td>
                                    <table style="font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:11px;color:#666666;line-height:18px;padding-bottom:10px">
                                        <tbody>
                                            <tr><td style="font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:13px;color:#202020;line-height:1.5" colspan="3">
                                                   <br><p>For More information please visit: <a href="http://hr.azizfoods.com" target="new">http://hr.azizfoods.com</a></p>
                                                </td></tr>
                                            <tr>
                                                <td><b>This is a auto generated message and does not require a reply.</b></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
                <td width="32px"></td>
            </tr>
            <tr height="32px"></tr>
        </tbody>
    </table>
</div>


