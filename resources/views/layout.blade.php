<!DOCTYPE html>
<html lang="en">
    <head>
        @include('common.header')
    </head>

    <body>

        <section id="container" class="">
            <!--header start-->
            <header class="header white-bg">
                @include('common.topbar')
            </header>
            <!--header end-->
            <!--sidebar start-->
            <aside>
                @include('common.sidebar')
            </aside>
            <!--sidebar end-->
            <!--main content start-->
            <section id="main-content">
                @include('common.error_and_success_alert')
                @yield('content')
            </section>
            <!--main content end-->
            <!--footer start-->
            @include('common.footer')
            <!--footer end-->
        </section>

        @if(!isset($customJs))
        @include('common.default_js')
        @endif

        @if(isset($customJs))
        @include("common.js.$customJs")
        @endif

        @include('common.custom_script.ajax-calls')


        <!--script for this page-->
        <!--    <script src="assets/js/form-validation-script.js"></script>-->

        <!-- Modal -->
        <div class="modal fade" id="change-password-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Change Password</h4>
                    </div>
                    <div class="modal-body">

                        <div class="panel-body">
                            <form  method="post" action="{{url('change-password-submit')}}">
                                <input id="csrf_token" type="hidden" name="_token" value="{{csrf_token()}}">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Old Password</label>
                                    <input required id="file_title" name="old_password" type="password" class="form-control"  placeholder="Old Password">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">New Password</label>
                                    <input required id="file_title" name="new_password" type="password" class="form-control"  placeholder="New Password">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Confirm New Password</label>
                                    <input required id="file_title" name="confirm_new_password" type="password" class="form-control"  placeholder="Confirm New Password">
                                </div>


                        </div>

                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <input type="submit" value="Submit" class="btn btn-success">

                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- modal end-->


    </body>
</html>