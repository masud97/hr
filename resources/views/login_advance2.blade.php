<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Arnob">
        <meta name="author-details" content="mdarnob007@gmail.com">
        <meta name="keyword" content="HR,Human Resorce,Big Web Technology Ltd">

        <title>{{$title}}</title>

        <!-- Bootstrap core CSS -->
        <link href="{{url('assets/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{url('assets/css/bootstrap-reset.css')}}" rel="stylesheet">
        <!--external css-->
        <link href="{{url('assets/assets/font-awesome/css/font-awesome.css')}}" rel="stylesheet" />
        <!-- Custom styles for this template -->
        <link href="{{url('assets/css/style.css')}}" rel="stylesheet">
        <link href="{{url('assets/css/style-responsive.css')}}" rel="stylesheet" />
        <link href="{{url('assets/css/custom.css')}}" rel="stylesheet" />

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
        <!--[if lt IE 9]>
        <script src="{{url('assets/js/html5shiv.js')}}"></script>
        <script src="{{url('assets/js/respond.min.js')}}"></script>
        <![endif]-->
        <script src="{{url('assets/js/jquery.js')}}"></script>
        <script src="{{asset("assets/js/bootstrap.min.js")}}" ></script>
    </head>

    <body class="lock-screen" onload="startTime()">

        <div class="lock-wrapper">

            <div id="time"></div>


            <div style="height: 257px" class="lock-box text-center">
                <div id="image">
                    <img style="width: 75px; height: 75px" src="{{url('assets/img/nopic.png')}}" alt="lock avatar"/>
                </div>

                <h1 id="name"></h1>
                <div style="margin-bottom: 5px">
                    @include('common.error_and_success_alert')
                </div>
                <form class="form-inline" action="{{ url('login-check') }}" method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="form-group col-lg-12" style="padding-bottom: 10px">
                        <input type="number" placeholder="Employee ID" name="employee_id" id="empId" value="{{old('employee_id')}}" class="form-control lock-input">
                    </div>
                    <div class="form-group col-lg-12">
                        <input type="password" placeholder="Password" name="password" id="password" class="form-control lock-input" autocomplete="off">
                        <button class="btn btn-lock" type="submit">
                            <i class="fa fa-arrow-right"></i>
                        </button>
                    </div>
                </form>

                <div class="">
                    <span class="pull-right">
                        <br/>
                        <a data-toggle="modal" href="#myModal"> Forgot Password?</a>
                    </span>      
                </div>
            </div>
        </div>



        <!-- Modal -->
        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
            <div class="modal-dialog">
                <form action="{{url('send-token')}}" method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Forgot Password ?</h4>
                        </div>
                        <div class="modal-body">

                            <p>Enter your e-mail address below to reset your password.</p>
                            <input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">

                        </div>
                        <div class="modal-footer">
                            <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                            <!--<button data-dismiss="modal" class="btn btn-success" type="submit"><a href="" style="color: wheat;">Submit</a></button>-->
                            <button type="submit" class="btn btn-info">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- modal -->



        <script>
            function startTime()
            {

                var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
                var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];



                var today = new Date();
                var h = today.getHours() % 12 || 12;
                var m = today.getMinutes();
                var s = today.getSeconds();
                var day = days[ today.getDay() ];
                var month = months[ today.getMonth() ];
                var date = today.getDate();

                // add a zero in front of numbers<10
                m = checkTime(m);
                s = checkTime(s);
                document.getElementById('time').innerHTML = day+" "+month+" "+date+"<br/>"+h + ":" + m + ":" + s;
                t = setTimeout(function () {
                    startTime()
                }, 500);
            }

            function checkTime(i)
            {
                if (i < 10)
                {
                    i = "0" + i;
                }
                return i;
            }


            $(document).ready(function (e) {
//                        getUserImageAndName($("#empId").val());
                setTimeout(function () {
                    getUserImageAndName($("#empId").val());
                }, 200);

            });

            $("#empId").focusout(function (e) {
                getUserImageAndName($("#empId").val());
            });

            $("#password").focus(function (e) {
                getUserImageAndName($("#empId").val());
            });

            function getUserImageAndName(empId)
            {
                var url = "{{url('/get-name-and-image')}}/" + empId;
                $.ajax({
                    url: url,
                    context: document.body
                }).done(function (data) {




                    obj = JSON.parse(data);

                    if (obj['status'])
                    {
                        var fullName = obj['full_name'];
                        var image = obj['image'];
                        console.log(image);
                        $("#name").html(fullName);
                        var html = "<img style='width: 75px; height: 75px' src='" + image + "' alt='lock avatar'/>";
                        $("#image").html(html);
                    } else
                    {
                        $("#name").html('');
                        var html = '<img style="width: 75px; height: 75px" src="{{url("assets/img/nopic.png")}}" alt="lock avatar"/>';
                        $("#image").html(html);
                    }




                });
            }

        </script>
    </body>
</html>