<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="Arnob">
<meta name="author-details" content="mdarnob007@gmail.com">
<meta name="keyword" content="HR,Human Resorce,Bigweb Technology Ltd">
<link rel="shortcut icon" href="{{ asset('assets/img/fav.png')}}">

<title>{{ $title }}</title>

<!-- Bootstrap core CSS -->
<!--    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/bootstrap-reset.css')}}" rel="stylesheet">
    external css
    <link href="{{asset('assets/assets/font-awesome/css/font-awesome.css')}}" rel="stylesheet" />
     Custom styles for this template 
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/style-responsive.css')}}" rel="stylesheet" />-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
<!--[if lt IE 9]>
  <script src="{{asset('assets/js/html5shiv.js')}}"></script>
  <script src="{{asset('assets/js/respond.min.js')}}"></script>
<![endif]-->



<!-- Bootstrap core CSS -->
<link href="{{url('assets/css/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{url('assets/css/bootstrap-reset.css')}}" rel="stylesheet">
<!--external css-->
<link href="{{url('assets/assets/font-awesome/css/font-awesome.css')}}" rel="stylesheet" />
<!-- Custom styles for this template -->
<link href="{{url('assets/css/style.css')}}" rel="stylesheet">
<link href="{{url('assets/css/select2min.css')}}" rel="stylesheet">
<link href="{{url('assets/css/style-responsive.css')}}" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="{{url('assets/assets/bootstrap-fileupload/bootstrap-fileupload.css')}}" />
<!--    <link rel="stylesheet" type="text/css" href="css/datepicker.css" />-->
<link href="{{url('/assets/assets/jquery-ui/jquery-ui-1.10.1.custom.min.cs')}}s" rel="stylesheet"/>

<link href="{{url('assets/assets/fancybox/source/jquery.fancybox.css')}}" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="{{url('assets/css/gallery.css')}}" />

<link rel="stylesheet" type="text/css" href="{{url('assets/assets/bootstrap-datepicker/css/datepicker.css')}}" />

<link href="{{url('assets/css/custom.css')}}" rel="stylesheet">

<!--    advance table -->
<link href="{{url('assets/assets/advanced-datatable/media/css/demo_page.css')}}" rel="stylesheet" />
<link href="{{url('assets/assets/advanced-datatable/media/css/demo_table.css')}}" rel="stylesheet" />
<link rel="stylesheet" href="{{url('assets/assets/data-tables/DT_bootstrap.css')}}" />

<link rel="stylesheet" type="text/css" href="{{url('assets/assets/gritter/css/jquery.gritter.css')}}" />
<link rel="stylesheet" type="text/css" href="{{url('assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css')}}" />

<link href="{{url('/assets/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css')}}" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="{{url('/assets/assets/bootstrap-timepicker/compiled/timepicker.css')}}" />


<meta name="csrf-token" content="{{ csrf_token() }}" />

