<footer class="site-footer">
    <div class="text-center">
        2016 &copy;<a href="http://www.bigweb.com.bd/"> Big Web Technologies Ltd.</a>
        <a href="#" class="go-top">
            <i class="fa fa-angle-up"></i>
        </a>
    </div>
</footer>