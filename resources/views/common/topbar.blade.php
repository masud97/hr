<div class="sidebar-toggle-box">
    <div data-original-title="Toggle Navigation" data-placement="right" class="fa fa-bars tooltips"></div>
</div>
<!--logo start-->
<a href="{{ url('/')}}" class="logo" ><img style="height: 50px;margin-top: -9px;" src="{{asset('assets/img/logo.png')}}"></a>
<!--logo end-->

<div class="top-nav ">
    <ul class="nav pull-right top-menu">
        <li id="header_notification_bar" class="dropdown" style="padding-top: 5px;">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">

                <i class="fa fa-bell-o"></i>
                <span class="badge bg-warning" id="countNotifications"></span>
                
            </a>

            <ul class="dropdown-menu extended custom-notif" id="leave-notifications">

                <li>
<!--                    <a href="{{url('mark-all-notification-as-read')}}">Mark As Read</a>-->
                </li>
            </ul>


        </li>
        <li id="header_notification_bar" class="dropdown" style="padding-top: 5px;">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">

                <i class="fa fa-envelope-o" aria-hidden="true"></i>
                <span class="badge bg-warning" id="countNotificationsLeave"></span>
            </a>

            <ul class="dropdown-menu extended custom-notif" id="leave-notifications-2">

                <li>
                    <!--                        <a href="#">See all notifications</a>-->
                </li>
            </ul>


        </li>

        <!-- user login dropdown start-->
        <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <img style="height: 38px;border-radius: 20px" alt="" src="{{getUserImageUrl(Session::get('id'))}}">
<!--                <span class="username">{{Session::get('name')}}</span>-->
                <b class="caret"></b>
            </a>
            <ul class="dropdown-menu extended logout">
                <div class="log-arrow-up"></div>
                <li><a href="{{url('/profile')}}"><i class=" fa fa-suitcase"></i>Profile</a></li>
                <li><a id="change_password" href="#"><i class="fa fa-key"></i>Change Password</a></li>
                <li><a href="{{url('logout')}}"><i class="fa fa-sign-out"></i> Log Out</a></li>
            </ul>
        </li>
        <!-- user login dropdown end -->
    </ul>
</div>