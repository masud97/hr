<!-- js placed at the end of the document so the pages load faster -->
  <!--<script src="js/jquery.js"></script>-->
<script type="text/javascript" language="javascript" src="{{url('/assets/assets/advanced-datatable/media/js/jquery.js')}}"></script>
<script src="{{url('/assets/js/bootstrap.min.js')}}"></script>
<script class="include" type="text/javascript" src="{{url('/assets/js/jquery.dcjqaccordion.2.7.js')}}"></script>
<script src="{{url('/assets/js/jquery.scrollTo.min.js')}}"></script>
<script src="{{url('/assets/js/jquery.nicescroll.js')}}" type="text/javascript"></script>
<script type="text/javascript" language="javascript" src="{{url('/assets/assets/advanced-datatable/media/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{url('/assets/assets/data-tables/DT_bootstrap.js')}}"></script>
<script src="{{url('/assets/js/respond.min.js')}}" ></script>

<script type="text/javascript" src="{{url('/assets/js/jquery.confirm.min.js')}}"></script>
<!--common script for all pages-->
<script src="{{url('/assets/js/common-scripts.js')}}"></script>

<!--script for this page only-->

<script type="text/javascript" charset="utf-8">
$(document).ready(function () {
    $('#example').dataTable({
        "aaSorting": [[4, "desc"]]
    });
});
</script>


<script>

    function restore_employee(event, id) {

        event.preventDefault();

        var url = document.getElementById("restore-emp-" + id).href;

        $.confirm({
            text: "Are you sure,  you want to restore this employee ?",
            confirm: function (button) {
                location.href = url;
            }
        });

    }

</script>