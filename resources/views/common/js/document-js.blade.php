<script src="{{url('assets/js/jquery-1.8.3.min.js')}}"></script>
<script type="text/javascript" language="javascript" src="{{url('assets/assets/advanced-datatable/media/js/jquery.js')}}"></script>
<script type="text/javascript" language="javascript" src="{{url('assets/assets/advanced-datatable/media/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{url('assets/assets/data-tables/DT_bootstrap.js')}}"></script>
<script type="text/javascript" src="{{url('assets/js/jquery.confirm.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/assets/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{url('assets/js/advanced-form-components.js')}}"></script>