<!-- js placed at the end of the document so the pages load faster -->
<script src="{{url('/assets/js/jquery.js')}}"></script>

<script src="{{url('/assets/js/select2min.js')}}"></script>

<script type="text/javascript" src="{{url('/assets/assets/bootstrap-timepicker/js/bootstrap-timepicker.js')}}"></script>


<!--    script for date picker-->

<script type="text/javascript" src="{{url('/assets/assets/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{url('/assets/assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>




<script src="{{url('/assets/js/bootstrap.min.js')}}"></script>
<!--script for calender page only-->
<script src="{{url('assets/assets/fullcalendar/fullcalendar/fullcalendar.min.js')}}"></script>
<script src="{{url('/assets/js/external-dragging-calendar.js')}}"></script>

<script class="include" type="text/javascript" src="{{url('/assets/js/jquery.dcjqaccordion.2.7.js')}}"></script>
<script src="{{url('/assets/js/jquery.scrollTo.min.js')}}"></script>
<script src="{{url('/assets/js/jquery.nicescroll.js')}}" type="text/javascript"></script>
<script src="{{url('/assets/js/respond.min.js')}}"></script>
<!--common script for all pages-->
<script src="{{url('/assets/js/common-scripts.js')}}"></script>

<script>
$(document).ready(function () {

$(".notification-area").niceScroll({
cursorcolor: "#F8F8F8",
});
});</script>


<script type="text/javascript">
    $(".js-example-basic-multiple").select2();</script>




<script type="text/javascript" src="{{url('/assets/js/jquery.validate.min.js')}}"></script>

<!--this page  script only-->
<script src="{{url('/assets/js/advanced-form-components.js')}}"></script>

<!--script for this page-->
<script src="{{url('/assets/js/form-validation-script.js')}}"></script>


<script>
    $('.default-date-picker').datepicker({
    format: 'dd/mm/yyyy'

            });</script>



<script>

    $('#calendar1').fullCalendar({
    header: {
    left: 'prev,next today',
            center: 'title',
            right: 'month,basicWeek,basicDay'
    },
            editable: true,
            droppable: true, // this allows things to be dropped onto the calendar !!!
            drop: function (date, allDay) { // this function is called when something is dropped

            // retrieve the dropped element's stored Event Object
            var originalEventObject = $(this).data('eventObject');
            // we need to copy it, so that multiple events don't have a reference to the same object
            var copiedEventObject = $.extend({}, originalEventObject);
            // assign it the date that was reported
            copiedEventObject.start = date;
            copiedEventObject.allDay = allDay;
            // render the event on the calendar
            // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
            $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
            // is the "remove after drop" checkbox checked?
            if ($('#drop-remove').is(':checked')) {
            // if so, remove the element from the "Draggable Events" list
            $(this).remove();
            }

            },
            events: [

<?php if (isset($event_array)): ?>
    <?php if (!is_null($event_array)) : ?>
        <?php for ($k = 0; $k < sizeof($event_array); $k++) : ?>

                        {
                        title: "<?= $event_array[$k][2]; ?>",
                                start: "<?= changeDateFormat($event_array[$k][3] . " " . $event_array[$k][4], DATE_RFC2822); ?>",
                                end: "<?= changeDateFormat($event_array[$k][5] . " " . $event_array[$k][6], DATE_RFC2822); ?>",
                                // start: "<?= changeDateFormat("2016-08-16 00:00:00", 'D M j G:i:s T Y') ?>",
                                // end: "<?= changeDateFormat("2016-08-16 00:00:00", 'D M j G:i:s T Y') ?>",
                                color: "<?= $event_array[$k][8]; ?> !important"
                        },
        <?php endfor; ?>
    <?php endif; ?>
<?php endif; ?>



            ]
    });
</script>