
    <!-- js placed at the end of the document so the pages load faster -->
    <script src="{{url('assets/js/jquery.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/js/moment.min.js')}}"></script>

    <script src="{{url('assets/js/bootstrap.min.js')}}"></script>
    <script class="include" type="text/javascript" src="{{url('assets/js/jquery.dcjqaccordion.2.7.js')}}"></script>
     <script src="{{url('assets/assets/fancybox/source/jquery.fancybox.js')}}"></script>
    <script src="{{url('assets/js/jquery.scrollTo.min.js')}}"></script>
    <script src="{{url('assets/js/jquery.nicescroll.js')}}" type="text/javascript"></script>
    <script src="{{url('assets/js/respond.min.js')}}" ></script>

    <script src="{{url('assets/js/modernizr.custom.js')}}"></script>
    <script src="{{url('assets/js/toucheffects.js')}}"></script>
    

    <!--common script for all pages-->
    <script src="{{url('assets/js/common-scripts.js')}}"></script>

    
    <script type="text/javascript">
      $(function() {
        //    fancybox
          jQuery(".fancybox").fancybox();
      });

  </script>