<!-- js placed at the end of the document so the pages load faster -->
<script src="{{url('assets/js/jquery.js')}}"></script>
<script src="{{url('assets/js/bootstrap.min.js')}}"></script>
<script class="include" type="text/javascript" src="{{url('assets/js/jquery.dcjqaccordion.2.7.js')}}"></script>
<script src="{{url('assets/js/jquery.scrollTo.min.js')}}"></script>
<script src="{{url('assets/js/jquery.nicescroll.js')}}" type="text/javascript"></script>
<script src="{{url('assets/js/respond.min.js')}}"></script>
<script src="{{url('assets/js/select2min.js')}}"></script>

<!--this page plugins-->

<script type="text/javascript" src="{{url('assets/assets/fuelux/js/spinner.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/assets/bootstrap-fileupload/bootstrap-fileupload.js')}}"></script>
<script type="text/javascript" src="{{url('assets/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js')}}"></script>
<script type="text/javascript" src="{{url('assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js')}}"></script>
<script type="text/javascript" src="{{url('assets/assets/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{url('assets/assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>
<script type="text/javascript" src="{{url('assets/assets/bootstrap-daterangepicker/moment.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/assets/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<script type="text/javascript" src="{{url('assets/assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js')}}"></script>
<script type="text/javascript" src="{{url('assets/assets/bootstrap-timepicker/js/bootstrap-timepicker.js')}}"></script>
<script type="text/javascript" src="{{url('assets/assets/jquery-multi-select/js/jquery.multi-select.js')}}"></script>
<script type="text/javascript" src="{{url('assets/assets/jquery-multi-select/js/jquery.quicksearch.js')}}"></script>

<script type="text/javascript">
$(".js-example-basic-multiple").select2();
</script>

<script type="text/javascript">
               // When the document is ready
               $(document).ready(function () {
               
                       $('#publish_date').datepicker({
                               format: "dd/mm/yyyy"
                       });  

           
               });
       </script>
<!--common script for all pages-->
<script src="{{url('assets/js/common-scripts.js')}}"></script>
<!--this page  script only-->
<script src="{{url('assets/js/advanced-form-components.js')}}"></script>