<!-- js placed at the end of the document so the pages load faster -->
<script src="assets/js/jquery.js"></script>
<script src="assets/js/advanced-form-components.js"></script>

<script type="text/javascript" src="assets/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

<script src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/assets/bootstrap-fileupload/bootstrap-fileupload.js"></script>
<script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="assets/js/jquery.scrollTo.min.js"></script>
<script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
<script type="text/javascript" src="assets/js/jquery.validate.min.js"></script>


<!--common script for all pages-->
<script src="assets/js/common-scripts.js"></script>

<!--script for this page-->
<script src="assets/js/form-validation-script.js"></script>

<script src="assets/assets/jquery-knob/js/jquery.knob.js"></script>
     <script>

      //knob
      $(".knob").knob();

    </script>

