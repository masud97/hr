<!-- js placed at the end of the document so the pages load faster -->
<script src="{{url('assets/js/jquery.js')}}"></script>
<script src="{{url('assets/js/advanced-form-components.js')}}"></script>

<script type="text/javascript" src="{{url('assets/assets/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>

<script src="{{url('assets/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/assets/bootstrap-fileupload/bootstrap-fileupload.js')}}"></script>
<script class="include" type="text/javascript" src="{{url('assets/js/jquery.dcjqaccordion.2.7.js')}}"></script>
<script src="{{url('assets/js/jquery.scrollTo.min.js')}}"></script>
<script src="{{url('assets/js/jquery.nicescroll.js')}}"  type="text/javascript"></script>
<script type="text/javascript" src="{{url('assets/js/jquery.validate.min.js')}}"></script>


<!--common script for all pages-->
<script src="{{url('assets/js/common-scripts.js')}}"></script>

<!--script for this page-->
<script src="{{url('assets/js/form-validation-script.js')}}"></script>

<script src="{{url('assets/assets/jquery-knob/js/jquery.knob.js')}}"></script>
<script>

//knob
$(".knob").knob();

//$('#leave-from').oninput(function (e) {
//    var leavefrom = $('#leave-from').val();
//    var leaveto = $('#leave-to').val();
//    if(leaveto !== '')
//    {
//        var diff = get_difference(leavefrom,leaveto);
//        console.log(diff);
//    }
//});
//
//$('#leave-to').oninput(function (e) {
//    var leavefrom = $('#leave-from').val();
//    var leaveto = $('#leave-to').val();
//    if(leavefrom !== '')
//    {
//        var diff = get_difference(leavefrom,leaveto);
//        console.log(diff);
//    }
//});
//
//
//
//function get_difference(date1, date2)
//{
//
//    var start = new Date(date1), // date format year - month - day
//            end = new Date(date2),
//            diff = new Date(end - start),
//            days = diff / 1000 / 60 / 60 / 24;
//
//    return Math.round(days);
//}

</script>