<!-- js placed at the end of the document so the pages load faster -->
<script src="{{url('/assets/js/jquery.js')}}"></script>
<script type="text/javascript" src="{{url('/assets/js/moment.min.js')}}"></script>

<script src="{{url('assets/js/bootstrap.min.js')}}"></script>
<script class="include" type="text/javascript" src="{{url('/assets/js/jquery.dcjqaccordion.2.7.js')}}"></script>
<script src="{{url('/assets/js/jquery.scrollTo.min.js')}}"></script>
<script src="{{url('/assets/js/jquery.nicescroll.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{url('/assets/js/jquery.validate.min.js')}}"></script>

<script type="text/javascript" src="{{url('assets/js/jquery.confirm.min.js')}}"></script>
<!--common script for all pages-->
<script src="{{url('/assets/js/common-scripts.js')}}"></script>


<script>
function editMeritsDemerits(id, type) {


    var des_id = "#des_" + id;
    var des = $(des_id).text();

    $('.modal-title').text('Edit ' + type);
    $('#type').val(type);
    $('#details').html(des);
    $('#id').val(id);

    $('#myModal3').modal('show');
}
</script>


<script>

    function remove_merits_demerits(event, type, id)
    {
        
        event.preventDefault();
        
        var url = document.getElementById("merits-demerits-remove-confirmation-"+id).href;

        $.confirm({
            text: "Are you sure? You want to remove this " + type + " ?",
            confirm: function (button) {
                location.href = url;
            }
        });



    }
</script>