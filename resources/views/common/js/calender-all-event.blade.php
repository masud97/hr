<!--<script src="js/jquery.js"></script>-->
<script type="text/javascript" language="javascript" src="{{url('/assets/assets/advanced-datatable/media/js/jquery.js')}}"></script>
<script src="{{url('/assets/js/bootstrap.min.js')}}"></script>
<script class="include" type="text/javascript" src="{{url('/assets/js/jquery.dcjqaccordion.2.7.js')}}"></script>
<script src="{{url('/assets/js/jquery.scrollTo.min.js')}}"></script>
<script src="{{url('/assets/js/jquery.nicescroll.js')}}" type="text/javascript"></script>
<script src="{{url('/assets/js/respond.min.js')}}" ></script>
<script type="text/javascript" language="javascript" src="{{url('/assets/assets/advanced-datatable/media/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{url('/assets/assets/data-tables/DT_bootstrap.js')}}"></script>
<script type="text/javascript" src="{{url('/assets/assets/gritter/js/jquery.gritter.js')}}"></script>

<script src="{{url('/assets/assets/file-uploader/js/jquery.fileupload.js')}}"></script>
<script type="text/javascript" src="{{url('/assets/assets/bootstrap-fileupload/bootstrap-fileupload.js')}}"></script>

<script type="text/javascript" src="{{url('assets/js/jquery.confirm.min.js')}}"></script>
<!--common script for all pages-->
<script src="{{url('/assets/js/common-scripts.js')}}"></script>
<script src="{{url('/assets/js/config.js')}}"></script>

<script src="{{url('/assets/js/gritter.js')}}" type="text/javascript"></script>


<script type="text/javascript">
var url_to_go = "{{url('/remove-event/')}}";

/* Formating function for row details */
function fnFormatDetails(oTable, nTr)
{
    var aData = oTable.fnGetData(nTr);

//    console.log(aData);

    var sOut = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';

    sOut += '<tr><td>Description:</td><td>' + aData[8] + '</td></tr>';
    sOut += '<tr><td>Reminder For:</td><td>' + aData[9] + '</td></tr>';
    sOut += '<tr><td>Remind Before:</td><td>' + aData[10] + ' hour</td></tr>';
    sOut += '<tr><td>Action:</td>';
    sOut += '<td>';
   
    sOut += '<a id="remove-event-' + aData[7] + '" onclick="remove_event(' + "event" + ',' + aData[7] + ')" href="' + url_to_go + "/" + aData[7] + '"' + 'title="Remove This Event" class="btn btn-danger btn-xs terminate-emp"><i class="fa fa-trash-o "></i></a>';

    sOut += '</td></tr>';

    sOut += '</table>';

    return sOut;
}
$(document).ready(function () {
    /*
     * Insert a 'details' column to the table
     */
    var nCloneTh = document.createElement('th');
    var nCloneTd = document.createElement('td');
    nCloneTd.innerHTML = '<img src="assets/assets/advanced-datatable/examples/examples_support/details_open.png">';
    nCloneTd.className = "center";

    $('#hidden-table-info thead tr').each(function () {
        this.insertBefore(nCloneTh, this.childNodes[0]);
    });

    $('#hidden-table-info tbody tr').each(function () {
        this.insertBefore(nCloneTd.cloneNode(true), this.childNodes[0]);
    });

    /*
     * Initialse DataTables, with no sorting on the 'details' column
     */
    var oTable = $('#hidden-table-info').dataTable({
        "aoColumnDefs": [
            {"bSortable": false, "aTargets": [0]}
        ],
        "aaSorting": [[1, 'asc']]
    });

    /* Add event listener for opening and closing details
     * Note that the indicator for showing which row is open is not controlled by DataTables,
     * rather it is done here
     */
    $('#hidden-table-info tbody td img').live('click', function () {
        var nTr = $(this).parents('tr')[0];
        if (oTable.fnIsOpen(nTr))
        {
            /* This row is already open - close it */
            this.src = "assets/assets/advanced-datatable/examples/examples_support/details_open.png";
            oTable.fnClose(nTr);
        } else
        {
            /* Open this row */
            this.src = "assets/assets/advanced-datatable/examples/examples_support/details_close.png";
            oTable.fnOpen(nTr, fnFormatDetails(oTable, nTr), 'details');
        }
    });
});
</script>

<!--script for this page only-->



<script>

    function remove_event(event, id) {
//       alert(id);
        event.preventDefault()

        var url = document.getElementById("remove-event-" + id).href;

        $.confirm({
            text: "Are you sure,  you want to remove this event ?",
            confirm: function (button) {
                location.href = url;
            }
        });

    }

</script>





