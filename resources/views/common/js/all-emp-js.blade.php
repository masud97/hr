<!--<script src="js/jquery.js"></script>-->
<script type="text/javascript" language="javascript" src="{{url('/assets/assets/advanced-datatable/media/js/jquery.js')}}"></script>
<script src="{{url('/assets/js/bootstrap.min.js')}}"></script>
<script class="include" type="text/javascript" src="{{url('/assets/js/jquery.dcjqaccordion.2.7.js')}}"></script>
<script src="{{url('/assets/js/jquery.scrollTo.min.js')}}"></script>
<script src="{{url('/assets/js/jquery.nicescroll.js')}}" type="text/javascript"></script>
<script src="{{url('/assets/js/respond.min.js')}}" ></script>
<script type="text/javascript" language="javascript" src="{{url('/assets/assets/advanced-datatable/media/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{url('/assets/assets/data-tables/DT_bootstrap.js')}}"></script>
<script type="text/javascript" src="{{url('/assets/assets/gritter/js/jquery.gritter.js')}}"></script>

<script src="{{url('/assets/assets/file-uploader/js/jquery.fileupload.js')}}"></script>
<script type="text/javascript" src="{{url('/assets/assets/bootstrap-fileupload/bootstrap-fileupload.js')}}"></script>

<script type="text/javascript" src="{{url('assets/js/jquery.confirm.min.js')}}"></script>
<!--common script for all pages-->
<script src="{{url('/assets/js/common-scripts.js')}}"></script>
<script src="{{url('/assets/js/config.js')}}"></script>

<script src="{{url('/assets/js/gritter.js')}}" type="text/javascript"></script>


<script type="text/javascript">
var url_to_go = "{{url('/terminate-employee/')}}";

/* Formating function for row details */
function fnFormatDetails(oTable, nTr)
{
    var aData = oTable.fnGetData(nTr);

//console.log(aData);

    var sOut = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';

    sOut += '<tr><td>Office Number:</td><td>' + aData[8] + '</td></tr>';
    sOut += '<tr><td>Emergency Contact:</td><td>' + aData[9] + '</td></tr>';
    sOut += '<tr><td>Emergency Contact Name and Relation:</td><td>' + aData[10] + '</td></tr>';
    
    sOut += '<tr><td>Action:</td>';
    sOut += '<td>';
    @if(hasPrivilege(11))
    sOut += '<button onclick=" file_upload(' + aData[11] + ',' + aData[12] + ')" title="Click here to upload file" class="btn btn-success btn-xs"><i class="fa fa-upload" aria-hidden="true"></i></button>';
    @endif
    @if(hasPrivilege(5))
    sOut += '<button onclick=" merits_demerits(' + aData[11] + ')"  title="Click here to add merits/demerits" class="merits-demerits-btn btn btn-primary btn-xs"><i class="fa fa-plus-circle" aria-hidden="true"></i></button>';
    @endif
    @if(hasPrivilege(7))
    sOut += '<button onclick="reset_password(' + aData[11] + ',' + aData[12] + ')" title="Click here to reset password" class="btn btn-warning btn-xs"><i class="fa fa-key"></i></button>';
    @endif
    @if(hasPrivilege(6))
    sOut += '<a id="terminate-emp-' + aData[11] + '" onclick="terminate_employee(' + "event" + ',' + aData[11] + ')" href="' + url_to_go + "/" + aData[11] +'"' + 'title="Terminate this employee" class="btn btn-danger btn-xs terminate-emp"><i class="fa fa-trash-o "></i></a>';
    @endif
    sOut += '</td></tr>';

    sOut += '</table>';

    return sOut;
}
$(document).ready(function () {
    /*
     * Insert a 'details' column to the table
     */
    var nCloneTh = document.createElement('th');
    var nCloneTd = document.createElement('td');
    nCloneTd.innerHTML = '<img class="clickbtn" src="assets/assets/advanced-datatable/examples/examples_support/details_open.png">';
    nCloneTd.className = "center";

    $('#hidden-table-info thead tr').each(function () {
        this.insertBefore(nCloneTh, this.childNodes[0]);
    });

    $('#hidden-table-info tbody tr').each(function () {
        this.insertBefore(nCloneTd.cloneNode(true), this.childNodes[0]);
    });

    /*
     * Initialse DataTables, with no sorting on the 'details' column
     */
    var oTable = $('#hidden-table-info').dataTable({
        "aoColumnDefs": [
            {"bSortable": false, "aTargets": [0]}
        ],
        "aaSorting": [[1, 'asc']]
    });

    /* Add event listener for opening and closing details
     * Note that the indicator for showing which row is open is not controlled by DataTables,
     * rather it is done here
     */
    $('#hidden-table-info tbody td img.clickbtn').live('click', function () {
        var nTr = $(this).parents('tr')[0];
        if (oTable.fnIsOpen(nTr))
        {
            /* This row is already open - close it */
            this.src = "assets/assets/advanced-datatable/examples/examples_support/details_open.png";
            oTable.fnClose(nTr);
        } else
        {
            /* Open this row */
            this.src = "assets/assets/advanced-datatable/examples/examples_support/details_close.png";
            oTable.fnOpen(nTr, fnFormatDetails(oTable, nTr), 'details');
        }
    });
});
</script>

<!--script for this page only-->


<script>

    function merits_demerits(i) {

        $('#emp_id_merits_demerits').val(i);

        $('#merits-demerits-modal').modal('show');
    }

</script>
<script>

    function file_upload(i, rowId) {

        $('#emp_id_file_upload').val(i);
        $('#emp_row_id').val(rowId);

        $('#file-upload-modal').modal('show');
    }

</script>
<script>

    function reset_password(i, rowId) {
        $('#emp_id_reset_password').val(i);
        $('#emp_row_id_reset_password').val(rowId);

        $('#reset-password-modal').modal('show');
    }

</script>
<script>

    function terminate_employee(event, id) {
        event.preventDefault()

        var url = document.getElementById("terminate-emp-" + id).href;

        $.confirm({
            text: "Are you sure,  you want to terminate this employee ?",
            confirm: function (button) {
                location.href = url;
            }
        });

    }

</script>

<script>
    $("#merits-demerits-submit-btn").click(function () {
        $('#merits-demerits-modal').modal('hide');
        var form = $('#merits-demerits-form');

        $.ajax({
            type: "POST",
            url: form.attr('action'),
            data: form.serialize()
        })
                .done(function (msg) {
                    var abc = JSON.parse(msg);
                    if (abc['status']) {
                        $.gritter.add({
                            title: "Merits/Demerits",
                            text: abc['msg'],
                            // / notificationID: val.notification_id,
                            sticky: false,
                            // time: '10000'
                        });
                        $('#type').val(null);
                        $('#details').val(null);
                        $('#emp_id_merits_demerits').val(null);
                    } else {
                        $.gritter.add({
                            title: "Merits/Demerits",
                            text: abc['msg'],
                            // / notificationID: val.notification_id,
                            sticky: false,
                            // time: '10000'
                        });
                    }

                });


    });
</script>



