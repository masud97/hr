<!-- js placed at the end of the document so the pages load faster -->
<script src="{{url('/assets/js/jquery.js')}}"></script>


<script type="text/javascript" src="{{url('assets/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js')}}"></script>
<script type="text/javascript" src="{{url('assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js')}}"></script>


<script src="{{url('/assets/js/advanced-form-components.js')}}"></script>

<script type="text/javascript" src="{{url('/assets/assets/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{url('/assets/assets/jquery-ui/jquery-ui-1.10.1.custom.min.js')}}"></script>

<script src="{{url('/assets/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{url('/assets/assets/bootstrap-fileupload/bootstrap-fileupload.js')}}"></script>
<script class="include" type="text/javascript" src="{{url('/assets/js/jquery.dcjqaccordion.2.7.js')}}"></script>
<script src="{{url('/assets/js/jquery.scrollTo.min.js')}}"></script>
<script src="{{url('/assets/js/jquery.nicescroll.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{url('/assets/js/jquery.validate.min.js')}}"></script>


<!--common script for all pages-->
<script src="{{url('/assets/js/common-scripts.js')}}"></script>

<!--script for this page-->
<script src="{{url('/assets/js/form-validation-script.js')}}"></script>

<script src="{{url('/assets/assets/jquery-knob/js/jquery.knob.js')}}"></script>


<script>
    $(function(){
   var select = $( "#minbeds" );
   var slider = $( "<div id='slider'></div>" ).insertAfter( select ).slider({
       min: 1,
       max: 10,
       range: "min",
       value: select[ 0 ].selectedIndex + 1,
       slide: function( event, ui ) {
           select[ 0 ].selectedIndex = ui.value - 1;
       }
   });
   $( "#minbeds" ).change(function() {
       slider.slider( "value", this.selectedIndex + 1 );
   });
});
    
    </script>