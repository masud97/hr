<script src="{{url('assets/js/jquery-1.8.3.min.js')}}"></script>
<script type="text/javascript" language="javascript" src="{{url('assets/assets/advanced-datatable/media/js/jquery.js')}}"></script>
<script type="text/javascript" language="javascript" src="{{url('assets/assets/advanced-datatable/media/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{url('assets/assets/data-tables/DT_bootstrap.js')}}"></script>
<script type="text/javascript" src="{{url('assets/js/jquery.confirm.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/assets/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{url('assets/js/advanced-form-components.js')}}"></script>

<script type="text/javascript">
/* Formating function for row details */
function fnFormatDetails(oTable, nTr) {
    var aData = oTable.fnGetData(nTr);
    var url = "{{url('profile/leave-details/withdraw-appliaction')}}/" + aData[10];
    var url2 = "{{url('assets/leave-doc')}}/" + aData[9];
    
    var sOut = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';
    sOut += '<tr><td>Note:</td><td>' + aData[8] + '</td></tr>';

        sOut += '<tr><td>Action:</td>';
        sOut += '<td>';
        if (aData[9] != '')
        {

            sOut += '<a href='+url2+' class="btn btn-success btn-xs" title="View or download attachment" target="new"><i class="fa fa-download" aria-hidden="true"></i></a>';
        }
        
        @if(isset($employee_id) and isset($lastApprovedApplicationId))
            if(aData[10]=== "{{$lastApprovedApplicationId}}")
            {

               var cancleUrl = "{{url('profile/leave-details/cancle-approve-application')}}/"+aData[10]+"/{{$employee_id}}";
                if (aData[7] === 'Approve')
                {
                    sOut += '<a class="btn btn-danger btn-xs" id="cancleApplication_'+aData[10]+'" href=' + cancleUrl + ' onclick="cancleApplication(event,' + aData[10] + ')" title="Cancle application"><i class="fa fa-times" aria-hidden="true"></i></a>';
                    sOut += '<button class="btn btn-warning" onclick="cahngeApproveApplicationDate(event,'+aData[10]+')">Change Date</button>'

                }
            }
            
        @else
           // console.log("not isset");
            if (aData[7] === 'pending')
            {
                sOut += '<a class="btn btn-danger btn-xs" id="withdrawApplication_'+aData[10]+'" href=' + url + ' onclick="withdrawApplication(event,' + aData[10] + ')" title="Withdraw application"><i class="fa fa-times" aria-hidden="true"></i></a>';

            }
        @endif
        
        sOut += '</td></tr>';
  
    sOut += '</table>';

    return sOut;
}

$(document).ready(function () {
    /*
     * Insert a 'details' column to the table
     */
    var nCloneTh = document.createElement('th');
    var nCloneTd = document.createElement('td');
    var url = "{{url('/assets/assets/advanced-datatable/examples/examples_support/details_open.png')}}";
    nCloneTd.innerHTML = '<img src=' + url + '>';
    nCloneTd.className = "center";

    $('#hidden-table-info thead tr').each(function () {
        this.insertBefore(nCloneTh, this.childNodes[0]);
    });

    $('#hidden-table-info tbody tr').each(function () {
        this.insertBefore(nCloneTd.cloneNode(true), this.childNodes[0]);
    });

    /*
     * Initialse DataTables, with no sorting on the 'details' column
     */
    var oTable = $('#hidden-table-info').dataTable({
        "aoColumnDefs": [
            {
                "bSortable": false,
                "aTargets": [0]
            }
        ],
        "aaSorting": [[1, 'asc']]
    });

    /* Add event listener for opening and closing details
     * Note that the indicator for showing which row is open is not controlled by DataTables,
     * rather it is done here
     */
    $('#hidden-table-info tbody td img').live('click', function () {
        var nTr = $(this).parents('tr')[0];
        var url = "{{url('/assets/assets/advanced-datatable/examples/examples_support/details_open.png')}}";
        var urlClose = "{{url('/assets/assets/advanced-datatable/examples/examples_support/details_close.png')}}";
        if (oTable.fnIsOpen(nTr)) {
            /* This row is already open - close it */
            this.src = url;
            oTable.fnClose(nTr);
        } else {
            /* Open this row */
            this.src = urlClose;
            oTable.fnOpen(nTr, fnFormatDetails(oTable, nTr), 'details');
        }
    });
});
</script>

<script>

    function withdrawApplication(event, i)
    {
        event.preventDefault();
        var url = document.getElementById("withdrawApplication_"+i).href;

        $.confirm({
            text: "Are you sure? You want to withdraw this application?",
            confirm: function (button) {
                location.href = url;
            }
        });



    }
    
    function cancleApplication(event, i)
    {
        event.preventDefault();
        var url = document.getElementById("cancleApplication_"+i).href;

        $.confirm({
            text: "Are you sure? You want to cancle this application?",
            confirm: function (button) {
                location.href = url;
            }
        });



    }
    
    
    function cahngeApproveApplicationDate(event,i)
    {
        console.log(i);
          $("#myModal").modal('show');      
    }
</script>


<!-- js placed at the end of the document so the pages load faster -->

<script src="{{url('assets/js/bootstrap.min.js')}}"></script>
<script class="include" type="text/javascript" src="{{url('assets/js/jquery.dcjqaccordion.2.7.js')}}"></script>
<script src="{{url('assets/js/jquery.scrollTo.min.js')}}"></script>
<script src="{{url('assets/js/jquery.nicescroll.js')}}" type="text/javascript"></script>
<script src="{{url('assets/js/respond.min.js')}}"></script>



<!--common script for all pages-->
<script src="{{url('assets/js/common-scripts.js')}}"></script>

<!--script for pie chart-->
<script src="{{url('assets/assets/flot/jquery.flot.js')}}"></script>
<script src="{{url('assets/assets/flot/jquery.flot.resize.js')}}"></script>
<script src="{{url('assets/assets/flot/jquery.flot.pie.js')}}"></script>
<script src="{{url('assets/assets/flot/jquery.flot.stack.js')}}"></script>
<script src="{{url('assets/assets/flot/jquery.flot.crosshair.js')}}"></script>
<!--script for this page only-->
<script src="{{url('assets/js/flot-chart.js')}}"></script>


