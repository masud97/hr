<div id="sidebar"  class="nav-collapse ">
    <!-- sidebar menu start-->
    <ul class="sidebar-menu" id="nav-accordion">
        <li>
            @if(getCurrentUrl() === 'dashboard')
            <a href="{{url('/')}}" class="active">
                @else
                <a href="{{url('/')}}">
                    @endif
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard</span>
                </a>

        </li>

        @if(hasPrivilege(14))
        <li >
            @if(getCurrentUrl() === 'app-config')
            <a  href="{{url('/app-config')}}" class="active"><i class="fa fa-cogs "></i> App Config</a>
            @else
            <a  href="{{url('/app-config')}}"><i class="fa fa-cogs"></i> App Config</a>
            @endif
        </li>
        @endif

        <li class="sub-menu">
            @if(getCurrentUrl() === 'apply-for-leave' or getCurrentUrl() === 'leave-history' or getCurrentUrl() === 'leave/all-pending-applications')
            <a href="javascript:;" class="active">
                @else
                <a href="javascript:;">
                    @endif
                    <i class="fa fa-laptop"></i>
                    <span>Leave</span>
                </a>
                <ul class="sub">

                    @if(getCurrentUrl() === 'apply-for-leave')
                    <li class="active"><a  href="{{url('/apply-for-leave')}}" >Apply For Leave</a></li>
                    @else
                    <li><a  href="{{url('/apply-for-leave')}}">Apply For Leave</a></li>
                    @endif


                    @if(hasPrivilege(1))
                    @if(getCurrentUrl() === 'leave-history')
                    <li class="active"><a  href="{{url('/leave-history')}}">Leave History</a></li>
                    @else
                    <li><a  href="{{url('/leave-history')}}">Leave History</a></li>
                    @endif
                    @endif


                    @if(hasPrivilege(2))
                    @if(getCurrentUrl() === 'leave/all-pending-applications')
                    <li class="active"><a  href="{{url('/leave/all-pending-applications')}}">Pending Applications</a></li>
                    @else
                    <li><a  href="{{url('/leave/all-pending-applications')}}">Pending Applications</a></li>
                    @endif
                    @endif

                </ul>
        </li>

        <li class="sub-menu">
            @if(getCurrentUrl() === 'all-employee' or getCurrentUrl() === 'create-employee' or getCurrentUrl()==='archive-employee-list')
            <a href="javascript:;" class="active">
                @else
                <a href="javascript:;">

                    @endif
                    <i class="fa fa-users" aria-hidden="true"></i>
                    <span> Employee</span>
                </a>
                <ul class="sub">

                    @if(getCurrentUrl() === 'all-employee')
                    <li class="active"><a  href="{{url('/all-employee')}}">All employee</a></li>
                    @else
                    <li><a  href="{{url('/all-employee')}}">All employee</a></li>
                    @endif

                    @if(hasPrivilege(3))
                    @if(getCurrentUrl() === 'create-employee')
                    <li class="active"><a  href="{{url('/create-employee')}}">Create employee</a></li>
                    @else
                    <li><a  href="{{url('/create-employee')}}">Create employee</a></li>

                    @endif
                    @endif

                    @if(hasPrivilege(12))
                    @if(getCurrentUrl() === 'archive-employee-list')
                    <li class="active"><a  href="{{url('/archive-employee-list')}}">Archive employee</a></li>
                    @else
                    <li><a  href="{{url('/archive-employee-list')}}">Archive employee</a></li>
                    @endif

                    @endif
                </ul>
        </li>
        <li >
            @if(getCurrentUrl() === 'notice')
            <a  href="{{url('/notice')}}" class="active"><i class="fa fa-laptop "></i> Notice</a>
            @else
            <a  href="{{url('/notice')}}"><i class="fa fa-laptop"></i> Notice</a>
            @endif
        </li>
        <li >
            @if(getCurrentUrl() === 'calender')
            <a  href="{{url('/calender')}}" class="active"><i class="fa fa-calendar" aria-hidden="true"></i> Calender</a>
            @else
            <a  href="{{url('/calender')}}"><i class="fa fa-calendar" aria-hidden="true"></i> Calender</a>
            @endif

        </li>

        <li class="sub-menu">
            @if(getCurrentUrl() === 'admin-activity-report' or getCurrentUrl() === 'activity-report-employee/{id?}' or getCurrentUrl() ==='report-details/{id}/{employee_id?}/{notification_id?}' or getCurrentUrl() ==='pending-report/{id?}' or getCurrentUrl() ==='add-new-report')
            <a  href="{{url('/admin-activity-report')}}" class="active"><i class="fa fa-file-text-o" aria-hidden="true"></i> Activity Report</a>
            @else
            <a  href="{{url('/admin-activity-report')}}"><i class="fa fa-file-text-o" aria-hidden="true"></i> Activity Report</a>
            @endif

            <ul class="sub">
                


                 @if(getCurrentUrl() === 'pending-report/{id?}')
                <li class="active" ><a  href="{{url('/pending-report')}}" >Pending Report</a></li>
                @else
                <li ><a  href="{{url('/pending-report')}}" >Pending Report</a></li>
                @endif
                
                @if(getCurrentUrl() === 'admin-activity-report')
                <li class="active" ><a  href="{{url('/admin-activity-report')}}" >All Report's</a></li>
                @else
                <li ><a  href="{{url('/admin-activity-report')}}" >All Report's</a></li>
                @endif


                @if(getCurrentUrl() === 'activity-report-employee/{id?}')
                <li class="active" ><a   href="{{url('/activity-report-employee')}}">Own Report </a></li>
                @else
                <li><a  href="{{url('/activity-report-employee')}}">Own Report</a></li>
                @endif
                
                @if(getCurrentUrl() === 'add-new-report')
                <li class="active" ><a   href="{{url('/add-new-report')}}">Add New Report</a></li>
                @else
                <li><a  href="{{url('/add-new-report')}}">Add New Report</a></li>
                @endif
            </ul>
        </li>

        <!--multi level menu end-->

    </ul>


    <!-- sidebar menu end-->
</div>