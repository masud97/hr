<aside class="profile-nav col-lg-3">
    <section class="panel">
        <div class="user-heading round image-upload">

            <a href="#">
                @if(!is_null($userDetails->image))
                <img src="{{url('assets/img/user-image/')}}/{{$userDetails->image}}" alt="">
                @else
                <img src="{{url('assets/img/')}}/nopic.png" alt="">
                @endif
                
                @if(isset($only_profile))
                <i id="change_profile_pic_icon" class="fa fa-camera"  title="Change Your Image" aria-hidden="true"></i>
                @endif
                
                <br/>
                
             
                
            </a>
            
               <div>
                @if(isset($employee_id))
                <a title="Edit Profile" href="{{url("/admin-edit-employee-profile/".$employee_id)}}"> <i style="top:45px" class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                @else
                <a title="Edit Profile" href="{{url('/edit-employee-profile')}}"> <i style="top:45px" class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                @endif
                </div>
            
            
            
            <h1>
                @if(isset($employee_id))
                <a href="{{url("/admin-employee-profile-view/".$employee_id)}}" title="Profile">{{$userDetails->full_name}}</a>
                @else
                <a href="{{url('/profile')}}" title="Profile">{{$userDetails->full_name}}</a>
                @endif
                
            </h1>
            <p>{{$userDetails->email_work}}</p>
        </div>

        <ul class="nav nav-pills nav-stacked">
       
         @if(getCurrentUrl() === 'admin-employee-leave-details/{employee_id}' || getCurrentUrl() === 'profile/leave-details/{notificationId?}')
            <li class="active">
         @else
         <li>
         @endif
           
                @if(isset($employee_id))
                <a href="{{url('/admin-employee-leave-details/'.$employee_id)}}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i>Leave Details</a>

                @else
                <a href="{{url('/profile/leave-details')}}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i>Leave Details</a>

                @endif
            </li>
            
            
         @if(getCurrentUrl() === 'view-merits-demerits' || getCurrentUrl() === 'admin-view-merits-demerits/{employee_id}')
            <li class="active">
         @else
         <li>
         @endif
           
                @if(isset($employee_id))
                <a href="{{url('/admin-view-merits-demerits/'.$employee_id)}}"> <i class="fa fa-indent" aria-hidden="true"></i>Merits and Demerits</a>
                @else
                <a href="{{url('/view-merits-demerits')}}"> <i class="fa fa-indent" aria-hidden="true"></i>Merits and Demerits</a>

                @endif
            </li>
           
            @if(getCurrentUrl() === 'gallery/{employeeId?}')
            <li class='active'>
             @else
             <li>
             @endif
             
                @if(isset($employee_id))
                <a href="{{url('/gallery/'.$employee_id)}}"> <i class="fa  fa-th-large" aria-hidden="true"></i>Documents</a>
                @else
                <a href="{{url('/gallery')}}"> <i class="fa  fa-th-large" aria-hidden="true"></i>Documents</a>

                @endif
            </li>
           
            
           

<!--            <li>
                @if(isset($employee_id))
                <a href="{{url("/admin-edit-employee-profile/".$employee_id)}}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i>Edit Profile</a>
                @else
                <a href="{{url('/edit-employee-profile')}}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i>Edit Profile</a>
                @endif

            </li>-->
            
        </ul>

    </section>
</aside>


