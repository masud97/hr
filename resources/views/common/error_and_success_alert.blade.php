@if(count($errors)>0)
@if($title==='Human Resource - Login')
<div class="alert alert-block alert-danger fade in" style="margin-bottom: 0px;margin-top: 10px;font-size: 12px;">
    <strong>Error</strong>
    @foreach($errors->all() as $error)
    {{$error}}
    @endforeach
</div>
@else
<div class="alert alert-block alert-danger fade in" style="margin-bottom: 0px;margin-top: 25px;font-size: 18px;">
    <strong>Error</strong>
    @foreach($errors->all() as $error)
    {{$error}}
    @endforeach
</div>
@endif
@endif

@if (session('success'))
<div class="alert alert-block alert-success fade in" style="margin-bottom: 0px;margin-top: 25px;font-size: 18px;">
    {{ session('success') }}
</div>
@endif