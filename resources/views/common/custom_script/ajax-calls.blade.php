<script>

    //this function auto update the notification after 5sec
    setInterval(function () {
        checkNotifications();
        checkNotificationsLeave();
    }, 5000);
    function checkNotifications()
    {
        var url = "{{url('/ajax/get-notifications')}}";
        $.ajax({
            url: url,
            context: document.body
        }).done(function (data) {
            obj = JSON.parse(data);
            var url;
            if (obj['status'])
            {
                var details = obj['data'];
                $("#countNotifications").html(details.length);
                //data found
                var html = '<div class="notify-arrow notify-arrow-yellow"></div>'
                        + '<li>'
                        + '<p class="yellow">You have ' + details.length + ' new notifications</p>'
                        + '</li>';
                for (var i = 0; i < details.length; i++)
                {
                    url = "{{url('')}}" + details[i]['url'] + "/" + details[i]['id'];
                    html += '<li>'
                            + '<a href="' + url + '">';
                    if (details[i]['type'] === 'warning')
                    {
                        html += '<span class="label custom-lebel label-warning"><i  class="fa fa-bell"></i></span>';
                    } else if (details[i]['type'] === 'success')
                    {
                        html += '<span class="label custom-lebel label-success"><i  class="fa fa-bell"></i></span>';
                    } else if (details[i]['type'] === 'danger')
                    {
                        html += '<span class="label custom-lebel label-danger"><i  class="fa fa-bell"></i></span>';
                    } else
                    {
                        html += '<span class="label custom-lebel label-primary"><i  class="fa fa-bell"></i></span>';
                    }


                    html += '<span class="message"> '
                            + details[i]['message']
                            + '</span>'
                            + '</a>'
                            + '</li>';
                }

                var url = '{{url("mark-all-notification-as-read")}}';

                html += '<li style="text-align: center;" >'
                        + '<a title="Click here to mark all unread notifications as read at a time" href="' + url + '">Mark As Read</a>'
                        + '</li>';
                $("#leave-notifications").html(html);
            } else
            {

                //data found
                var html = '<div class="notify-arrow notify-arrow-yellow"></div>'
                        + '<li>'
                        + '<p class="yellow">You have no notifications</p>'
                        + '</li>';
                $("#leave-notifications").html(html);
            }
        });
    }


    function checkNotificationsLeave()
    {
        var url = "{{url('/ajax/get-notifications-leave')}}";
        $.ajax({
            url: url,
            context: document.body
        }).done(function (data) {
            obj = JSON.parse(data);
            var url;
            if (obj['status'])
            {
                var details = obj['data'];
                $("#countNotificationsLeave").html(details.length);
                //data found
                var html = '<div class="notify-arrow notify-arrow-yellow"></div>'
                        + '<li>'
                        + '<p class="yellow">You have ' + details.length + ' new leave applications</p>'
                        + '</li>';
                for (var i = 0; i < details.length; i++)
                {
                    url = "{{url('')}}" + details[i]['url'] + "/" + details[i]['id'];
                    html += '<li>'
                            + '<a href="' + url + '">';
                    if (details[i]['type'] === 'warning')
                    {
                        html += '<span class="label custom-lebel label-warning"><i  class="fa fa-bell"></i></span>';
                    } else if (details[i]['type'] === 'success')
                    {
                        html += '<span class="label custom-lebel label-success"><i  class="fa fa-bell"></i></span>';
                    } else if (details[i]['type'] === 'danger')
                    {
                        html += '<span class="label custom-lebel label-danger"><i  class="fa fa-bell"></i></span>';
                    } else
                    {
                        html += '<span class="label custom-lebel label-primary"><i  class="fa fa-bell"></i></span>';
                    }


                    html += '<span class="message"> '
                            + details[i]['message']
                            + '</span>'
                            + '</a>'
                            + '</li>';
                }


//                html += '<li>'
//                        + '<a href="#">See all notifications</a>'
//                        + '</li>';

                $("#leave-notifications-2").html(html);
            } else
            {

                //data found
                var html = '<div class="notify-arrow notify-arrow-yellow"></div>'
                        + '<li>'
                        + '<p class="yellow">You have no leave notifications</p>'
                        + '</li>';
                $("#leave-notifications-2").html(html);
            }
        });
    }


</script>


<script>
    $("#change_password").click(function (e) {
        e.preventDefault();
        $('#change-password-modal').modal('show');
    });

</script>