@extends('layout')


@section('content')

<section class="wrapper">    

    <div >
        <!--breadcrumbs start -->
        <ul class="breadcrumb new-bread">
            <li><a class="top-hover" href="{{url('/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a></li>
            <li><a class="top-hover" href="#">Leave</a></li>
            <li class="active">Leave History</li>
        </ul>
        <!--breadcrumbs end -->
    </div>


    <!-- page start-->
    <div class="row">
        <aside class="profile-info col-lg-12">
            <section class="panel">
                <header class="panel-heading"style="border-color: #DADFE1;">
                    Search Leave History 
                </header>
                <div class="panel-body">

                    <form class="cmxform form-horizontal tasi-form" id="signupForm" method="get" action="{{url('/leave-history/search')}}">
                        
                        <div class="form-group ">
                            <div class="col-xs-12 col-md-6  m-bot15">
                                <label for="username" class="control-label">Select Date Range</label>
                                <div class="input-group input-large" data-date="13/07/2013" data-date-format="dd/mm/yyyy">
                                    <input id="select-date" type="text" class="form-control dpd1" value="@if(isset($_GET['from_date'])){{$_GET['from_date']}}@endif" name="from_date">
                                    <span class="input-group-addon">To</span>
                                    <input id="select-date" type="text" class="form-control dpd2" value="@if(isset($_GET['to_date'])){{$_GET['to_date']}} @endif" name="to_date">
                                </div>	
                            </div> 
                            <div class="col-xs-12 col-md-6 ">
                                <label class="col-md-2 control-label col-lg-2" for="input_religion">Select Employee</label>
                                <select class="form-control m-bot15" name="id">
                                    <option value="0">Select</option>
                                    @if(!is_null($company))
                                        @foreach($company as $com)
                                            <option value="{{$com}}" @if(isset($_GET['id'])) @if($com === $_GET['id']) selected @endif @endif >{{$com}}</option>
                                        @endforeach
                                    @endif
                                    @if(!is_null($employeeNameAndId))
                                    @foreach($employeeNameAndId as $employee)
                                    <option value="{{$employee['id']}}" @if(isset($_GET['id'])) @if(trim($employee['id']) === trim($_GET['id'])) selected @endif @endif >{{ucfirst($employee['full_name'])}}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-offset-5 col-md-2">
                                <input class="btn btn-primary btn-shadow btn-lg btn-block" type="submit" value="Search">
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </aside>
    </div>
    <div class="row">

        <aside class="profile-info col-lg-12">

            <section class="panel">         	   	



                <section class="panel table-responsive">
                    <header class="panel-heading">
                        Leave History
                    </header>
                   <table  class="display table table-bordered table-striped" id="example">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Leave type</th>
                                <th>Leave purpose</th>
                                <th>Leave date range</th>
                                <th>Status</th>
                                <th>Apply date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($leaveHistory))
                            @if(!is_null($leaveHistory))
                            <?php $i=1; ?>
                            @foreach($leaveHistory as $history)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{ucfirst($history->full_name)}}</td>
                                    <td>{{ucfirst($history->leave_type)}}</td>
                                    <td>{{ucfirst($history->leave_purpose)}}</td>
                                    <td>{{$history->leave_from}} to {{$history->leave_to}}</td>
                                    @if($history->status === 'approve')
                                    <td>Approved</td>
                                    @elseif ($history->status === 'deny')
                                    <td>Denied</td>
                                    @elseif($history->status === 'withdraw')
                                    <td>Withdrawn</td>
                                    @elseif($history->status === 'cancle')
                                    <td>Canceled</td>
                                    @else
                                    <td>{{$history->status}}</td>
                                    @endif
                                    <td>{{$history->apply_date}}</td>
                                </tr>
                            @endforeach
                            @endif
                            @endif


                        </tbody>
                    </table>
                </section>
            </section>
        </aside> 
    </div>


</section>


@endsection