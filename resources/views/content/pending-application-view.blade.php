@extends('layout')


@section('content')
<section class="wrapper">

    <div >
        <!--breadcrumbs start -->
        <ul class="breadcrumb new-bread">
            <li ><a class="top-hover" href="{{url('/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a></li>
            <li><a class="top-hover" href="{{url('/leave/all-pending-applications')}}">Pending Application</a></li>
            <li class="active">Pending Application View</li>
        </ul>
        <!--breadcrumbs end -->
    </div>

    <!-- page start-->
    <div class="row">
        <aside class="profile-nav col-lg-3">
            <section class="panel">
                <div class="user-heading round">
                    <a href="#">
                        <img src="{{url('assets/img/user-image')}}/{{$leaveDetails->image}}" alt="">
                    </a>
                    <h1>{{$leaveDetails->full_name}}</h1>
                    <p>{{$leaveDetails->email_work}}</p>
                </div>

                <ul class="nav nav-pills nav-stacked">
                    @if(!is_null($leaveDetails->attachment))
                    <li class="active">
                        <a href="{{url('assets/leave-doc')}}/{{$leaveDetails->attachment}}" target="new"> <i class="fa fa-paperclip" aria-hidden="true"></i>Attachment</a>
                    </li>
                    @endif
                    @if(!is_null($leaveDetails->note))
                    <li>
                        <header class="panel-heading" style="border-color: #DADFE1;">
                            Note
                        </header>
                        <p class="note-paragraph"><?php echo strip_tags($leaveDetails->note,'<br>'); ?></p>
                    </li>
                    @endif

                </ul>

            </section>
        </aside>



        <aside class="profile-info col-lg-9">

            <div class="panel">

                <!--start of pie chart area-->


                <div class="col-md-4 profile-pic-chart">
                    <!--widget start-->
                    <div class="panel">
                        <div class="panel-body">
                            <div class="bio-chart">
                                <input class="knob" data-max="{{ $totalLeave['casual'] }}" value="{{$leaveTaken['casual']}}" data-width="85" data-height="85" data-displayPrevious=true  data-thickness=".2" data-fgColor="#4CC5CD" data-bgColor="#e8e8e8">
                            </div>
                            <div class="bio-desk">
                                <h4 class="terques">Casual</h4>
                                <p>Total : {{$totalLeave['casual']}}</p>
                                <p>Remaining : {{$totalLeave['casual']-$leaveTaken['casual']}}</p>
                            </div>
                        </div>
                    </div>
                    <!--widget end-->
                </div>
                <div class="col-md-4 profile-pic-chart">
                    <!--widget start-->
                    <div class="panel">
                        <div class="panel-body">
                            <div class="bio-chart">
                                <input class="knob" data-max="{{ $totalLeave['sick'] }}" value="{{$leaveTaken['sick']}}" data-width="85" data-height="85" data-displayPrevious=true  data-thickness=".2" data-fgColor="#4CC5CD" data-bgColor="#e8e8e8">
                            </div>
                            <div class="bio-desk">
                                <h4 class="terques">Sick</h4>
                                <p>Total : {{$totalLeave['sick']}}</p>
                                <p>Remaining : {{$totalLeave['sick']-$leaveTaken['sick']}}</p>
                            </div>
                        </div>
                    </div>
                    <!--widget end-->
                </div>
                <div class="col-md-4 profile-pic-chart"style="padding: 0;">
                    <!--widget start-->
                    <div class="panel">
                        <div class="panel-body">
                            <div class="bio-chart">
                                <input class="knob" data-max="{{ $totalLeave['half'] }}" value="{{$leaveTaken['half']}}" data-width="85" data-height="85" data-displayPrevious=true  data-thickness=".2" data-fgColor="#4CC5CD" data-bgColor="#e8e8e8">
                            </div>
                            <div class="bio-desk">
                                <h4 class="terques">Half</h4>
                                <p>Total : {{$totalLeave['half']}}</p>
                                <p>Remaining : {{$totalLeave['half']-$leaveTaken['half']}}</p>
                            </div>
                        </div>
                    </div>
                    <!--widget end-->
                </div>



                <!-- page end-->
            </div>
            <div class="clearfix"></div>




            <section class="panel">
                <header class="panel-heading" style="border-color: #DADFE1;">
                    Pending Leave Application
                </header>
                <div class="panel-body">

                    <form class="cmxform form-horizontal tasi-form" id="signupForm" method="post" action="{{url('/leave/approve-deny')}}">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="application_id" value="{{$applicatoinId}}">
                        <input type="hidden" name="notificationId" value="{{$notificationId}}">
                        <div class="form-group ">
                            <div class="col-md-2">
                                <label class="col-md-2 control-label col-lg-2" for="input_status">Leave Purpose</label>
                            </div>  
                            <div class="col-md-4">
                                <select class="form-control m-bot15" disabled>

                                    <option>{{ucfirst($leaveDetails->leave_purpose)}}</option>

                                </select>
                            </div> 
                            <div class="col-md-2">
                                <label class="col-md-2 control-label col-lg-2" for="input_religion">Leave Type</label>
                            </div>  
                            <div class="col-md-4">
                                <select class="form-control m-bot15" disabled>

                                    <option>{{ucfirst($leaveDetails->leave_type)}}</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group ">
                            <div class="col-md-2">
                                <label for="username" class="control-label col-md-2">Leave</label>
                            </div>  
                            <div class="col-md-4">

                                <div class="input-group input-large" data-date="{{changeDateFormat($leaveDetails->leave_from,'d/m/Y')}}" data-date-format="dd/mm/yyyy">
                                    <input type="text" id="leave-from"  class="form-control dpd1" value="{{changeDateFormat($leaveDetails->leave_from,'d/m/Y')}}" name="from">
                                    <span class="input-group-addon">To</span>
                                    <input type="text" id="leave-to" class="form-control dpd2" value="{{changeDateFormat($leaveDetails->leave_to,'d/m/Y')}}" name="to">
                                </div>


                            </div> 

                            <div class="col-md-2">
                                <label for="ccomment" class="control-label col-md-2">Amount</label>
                            </div>  
                            <div class="col-md-4">
                                <span style="font-size: 18px;font-weight: 500"> {{$leaveDetails->leave_amount}} day(s)</span>
                            </div> 

                        </div>
                        <div class="form-group ">
                            <div class="col-md-2">
                                <label class="col-md-2 control-label col-lg-2" for="input_status">Action</label>
                            </div>  
                            <div class="col-md-4">
                                <select class="form-control m-bot15" name="action">

                                    <option >Select</option>
                                    <option value="approve">Approve</option>
                                    <option value="deny">Deny</option>

                                </select>
                            </div> 
                            <div class="col-md-2">
                                <label for="ccomment" class="control-label col-md-2">Comment</label>
                            </div>  
                            <div class="col-md-4">
                                <textarea class="form-control " id="ccomment" name="leave_des"></textarea>
                            </div> 
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-5 col-md-7">
                                <input class="btn btn-danger" type="submit" value="Submit">

                            </div>
                        </div>


                    </form>


                </div>
            </section>



        </aside>


    </div>




</section>

@endsection