@extends('layout')

@section('content')
<section class="wrapper">

    <div >
        <!--breadcrumbs start -->
        <ul class="breadcrumb new-bread">
            <li><a class="top-hover" href="{{url('/dashboard')}}"><i class="fa fa-home"></i>Dashboard</a></li>

            <li><a class="top-hover" href="#">Employee</a></li>
            @if(isset($employee_id))
            <li><a class="top-hover" href="{{url('/all-employee')}}">All Employee</a></li>
            @endif
            <li class="active">Leave Details</li>
        </ul>
        <!--breadcrumbs end -->
    </div>


    <!-- page start-->
    <div class="row">
        @include('common.common_aside_profile')
        <aside class="profile-info col-lg-9">




            <!-- page start-->
            <section class="panel">
                <header class="panel-heading">
                    Leave Details
                </header>
                <div class="panel-body table-responsive">
                    <div class="adv-table">
                        <table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="hidden-table-info">
                            <thead>
                                <tr>
                                    <th>Sl.</th>
                                    <th>Type</th>
                                    <th>Purpose</th>
                                    <th>Leave Date</th>
                                    <th>Leave Amount</th>
                                    <th>Apply Date</th>
                                    <th>Status</th>
                                    <th style="display: none">Note</th>
                                    <th style="display: none">Attachment</th>
                                    <th style="display: none">id</th>
                                    <th>Waiting for</th>

                                </tr>
                            </thead>
                            <tbody>
                                @if(count($leaveDetails)>0)
                                <?php $i = 1; ?>
                                @foreach($leaveDetails as $row)
                                <tr class="gradeX">
                                    <td>{{$i++}}</td>
                                    <td>{{ucfirst($row->leave_type)}}</td>
                                    <td>{{ucfirst($row->leave_purpose)}}</td>
                                    <td>{{changeDateFormat($row->leave_from,'d-m-Y')}} to {{changeDateFormat($row->leave_to,'d-m-Y')}}</td>
                                    <td>{{$row->leave_amount}}</td>
                                    <td>{{changeDateFormat($row->apply_date,'d-m-Y')}}</td>
                                    
                                    @if($row->status === 'approve')
                                    <td>Approved</td>
                                    @elseif ($row->status === 'deny')
                                    <td>Denied</td>
                                    @elseif($row->status === 'withdraw')
                                    <td>Withdrawn</td>
                                    @elseif($row->status === 'cancle')
                                    <td>Canceled</td>
                                    @else
                                    <td>{{$row->status}}</td>
                                    @endif
                                    
                                    
                                    <td style="display: none"><?php echo strip_tags($row->note,'<br><b><i><h1><h2><h3><h4><h5><h6><u>') ?></td>
                                    <td style="display: none">{{$row->attachment}}</td>
                                    <td style="display: none">{{$row->id}}</td>
                                    @if($row->status === 'pending')
                                    <td>{{getUserFullName($row->waiting_for_employee_login_id)}}</td>
                                    @else
                                    <td></td>
                                    @endif
                                </tr>
                                @endforeach
                                @endif


                            </tbody>
                        </table>

                    </div>
                </div>
            </section>

        </aside>
    </div>


    <!-- page end-->
</section>



<!-- Modal -->
@if(isset($lastApprovedApplicationId))
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Select Date</h4>
            </div>
            <form action="{{url('profile/leave-details/change-approve-application-date')}}/{{$lastApprovedApplicationId}}/{{$employee_id}}" method="post">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="modal-body">

                    <div class="col-md-3">
                        <label for="username" class="control-label col-md-2">Leave</label>
                    </div> 
                    <div class="col-md-9">

                        <div class="input-group input-large" data-date="13/07/2013" data-date-format="mm/dd/yyyy">
                            <input id="select-date" type="text" value="{{changeDateFormat($leaveDate['leave_from'],'d/m/Y')}}" class="form-control dpd1" name="leave_from_date">
                            <span class="input-group-addon">To</span>
                            <input id="select-date" type="text" value="{{changeDateFormat($leaveDate['leave_to'],'d/m/Y')}}" class="form-control dpd2" name="leave_to_date">
                        </div>	

                    </div> 

                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success" value="Submit">
                </div>
            </form>
        </div>
    </div>
</div>
@endif
<!-- modal -->





@endsection