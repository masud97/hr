@extends('layout')

@section('content') 
<section class="wrapper site-min-height">
    <div >
        <!--breadcrumbs start -->
        <ul class="breadcrumb new-bread">
            <li><a class="top-hover" href="{{url('/dashboard')}}"><i class="fa fa-home"></i>Dashboard</a></li>

            <li><a class="top-hover" href="{{url('/calender')}}">Calender</a></li>

            <li class="active">All Event</li>
        </ul>
        <!--breadcrumbs end -->
    </div>
    <!-- page start-->
    <section class="panel">
        <header class="panel-heading">
            All Event
        </header>
        <div class="panel-body table-responsive">
            <div class="adv-table">
                <table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="hidden-table-info">
                    <thead>
                        <tr>

                            <th>Sl. No</th>
                            <th>Event Created By</th>
                            <th>Employee Name</th>
                            <th>Start Date & Time</th>
                            <th>End Date & Time</th>
                            <th>Status</th>
                            <th style="display : none;" >id</th>
                            <th style="display : none;" >Description</th>
                            <th style="display : none;" >Reminder For</th>
                            <th style="display : none;" >Reminder Before</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php for ($i = 0; $i < sizeof($event_array); $i++) : ?>
                            <tr class="gradeX" <?php if (!is_null($event_array[$i][11])) { echo 'style=" background-color : #E4E7BA !important;"'; } ?>
                                

                                >

                                <td><?= $i + 1 ?></td>
                                <td>{{$event_array[$i][2]}}</td>
                                <td>{{$event_array[$i][1]}}</td>
                                <td>{{$event_array[$i][3]." ".'('.$event_array[$i][4].')'}}</td>
                                <td>{{$event_array[$i][5]." ".'('.$event_array[$i][6].')'}}</td>
                                <td>{{$event_array[$i][10]}}</td>

                                <td style="display: none">{{$event_array[$i][0]}}</td>
                                <td style="display: none">{{$event_array[$i][7]}}</td>
                                <td style="display: none">{{$event_array[$i][8]}}</td>
                                <td style="display: none">{{$event_array[$i][9]}}</td>



                            </tr>
                        <?php endfor; ?>




                    </tbody>
                </table>





            </div>
        </div>
    </section>
    <!-- page end-->
</section>

@endsection