@extends('layout')

@section('content')
<section class="wrapper">



    <div >
        <!--breadcrumbs start -->
        <ul class="breadcrumb new-bread">
            <li ><a class="top-hover" href="{{url('/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a></li>
            <!--            <li><a class="top-hover" href="#"></a></li>-->
            <li class="active">Calendar</li>
        </ul>
        <!--breadcrumbs end -->
    </div>
    <div class="row">

        <aside class="col-lg-12">
            <section class="panel">
                <div class="link-body">
                    <a class="btn btn-success event-buutton" href="{{url('/all-event')}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                    <a title="" data-placement="right" data-toggle="modal" href="#myModal2" data-original-title="See all event" class="btn btn-success tooltips" href="#"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                </div>
                <div class="panel-body">

                    <div id="calendar1" class="has-toolbar"></div>
                </div>
            </section>
        </aside>
    </div>

    <div class="row">
        <aside class="col-lg-12">
            <section class="panel">
                <div class="panel-body">
                    <!-- Modal -->
                    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-data-table">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Add New Event</h4>
                                </div>
                                <div class="modal-body">

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <section class="panel">

                                                <div class="panel-body">



                                                    <form class="cmxform form-horizontal tasi-form tasi-form2" method="post" action="{{url('/create-calender')}}">
                                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                        <div class="form-group ">
                                                            <div class="col-md-6">
                                                                <label class="control-label" for="input_status">Select Employee to Remind</label>
                                                                <select required class="js-example-basic-multiple" name="selected_emp[]" multiple="multiple">
                                                                    <option  value="all">All Employee</option>
                                                                    @if(!is_null($company))
                                                                    @foreach($company as $com)
                                                                    <option>{{$com}}</option>
                                                                    @endforeach
                                                                    @endif

                                                                    @if(!is_null($activeEmployee))
                                                                    @foreach($activeEmployee as $emp)
                                                                    <option value="{{$emp['id']}}">{{$emp['name']}}</option>
                                                                    @endforeach
                                                                    @endif
                                                                </select>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label class="control-label" for="input_religion">Remind Before</label>
                                                                <select name="remind_before" class="form-control m-bot15">
                                                                    <option <?php
                                                                    if (old('remind_before') == '1') {
                                                                        echo 'selected';
                                                                    }
                                                                    ?> >1 Hour</option>
                                                                    <option <?php
                                                                    if (old('remind_before') == '2') {
                                                                        echo 'selected';
                                                                    }
                                                                    ?> >2 Hour</option>
                                                                    <option <?php
                                                                        if (old('remind_before') == '3') {
                                                                            echo 'selected';
                                                                        }
                                                                    ?> >3 Hour</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group ">
                                                            <div class="col-md-6" >
                                                                <label class="control-label">Start Date</label>
                                                                <input data-date-format="dd/mm/yyyy"  name="start_date" class="form-control form-control-inline input-medium default-date-picker" size="16" type="text" value="{{old('start_date')}}" />
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label class="control-label">End Date</label>
                                                                <input name="end_date" class="form-control form-control-inline input-medium default-date-picker" size="16" type="text" value="{{old('end_date')}}" />

                                                            </div>
                                                        </div>
                                                        <div class="form-group ">
                                                            <div class="col-md-6">
                                                                <label class="control-label">Start Time</label>
                                                                <div class="input-group bootstrap-timepicker">
                                                                    <input type="text" class="form-control timepicker-default" name="start_time" value="{{old('start_time')}}">
                                                                    <span class="input-group-btn">
                                                                        <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label class="control-label col-md-2">End Time</label>
                                                                <div class="input-group bootstrap-timepicker">
                                                                    <input value="{{old('end_time')}}" name="end_time" type="text" class="form-control timepicker-default">
                                                                    <span class="input-group-btn">
                                                                        <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
                                                                    </span>
                                                                </div>
                                                            </div>



                                                        </div>



                                                        <div class="form-group ">
                                                            <div class="col-md-12">
                                                                <label class="control-label" for="input_religion">Event Title</label>
                                                                <input value="{{old('title')}}" class="form-control " id="hig_deg" name="title" type="text" />
                                                            </div>

                                                        </div>
                                                         <div class="form-group ">
                                                            
                                                            <div class="col-md-12">
                                                                <label class="control-label" for="input_status">Description</label>
                                                                <textarea class="form-control " id="description1" name="des" required>{{old('des')}}</textarea>
                                                            </div>

                                                        </div>
                                                        
                                                        <div class="form-group ">
                                                            <center>
                                                                <p> Please do not use these color codes 

                                                                </p>
                                                                <div style="    display: inline-block;    margin-right: 10px;" class="color1"><p style="display: block;
                                                                                                                                                 background-color: #F89406;
                                                                                                                                                 margin-top: 3px;"></p><span>#F89406</span></div>
                                                                <div style="    display: inline-block;    margin-right: 10px;" class="color2"><p style="display: block;
                                                                                                                                                 background-color: #396CCF;
                                                                                                                                                 margin-top: 3px;"></p><span>#396CCF</span></div>
                                                                <div style="    display: inline-block;    margin-right: 10px;" class="color3"><p style="display: block;
                                                                                                                                                 background-color: #ff0000;
                                                                                                                                                 margin-top: 3px;"></p><span>#FF0000</span></div>

                                                            </center>
                                                            <br>
                                                            <center>
                                                                <label>Choose event color</label>
                                                                <input name="event_color" type="color" value="{{old('event_color')}}">
                                                            </center>

                                                        </div>



                                                </div>
                                            </section>
                                        </div>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                    <input type="submit" class="btn btn-success">
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- modal -->

            </section>
        </aside>
    </div>

</div>

</section>

@endsection