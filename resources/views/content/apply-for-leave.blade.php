@extends('layout')

@section('content')
<section class="wrapper" style="margin-top: 0;">

    <div >
        <!--breadcrumbs start -->
        <ul class="breadcrumb new-bread">
            <li><a class="top-hover" href="{{url('/dashboard')}}"><i class="fa fa-home"></i> Dashaboard</a></li>
            <li><a class="top-hover" href="#">Leave</a></li>
            <li class="active">Apply For Leave</li>
        </ul>
        <!--breadcrumbs end -->
    </div>	

    <!--start of pie chart area-->

    <div class="row">
        <div class="col-lg-4">
            <div class="panel">
                <div class="panel-body">
                    <div class="bio-chart">
                        <input readonly title="Casual Leave Taken" class="knob" data-max="{{$totalLeave['casual']}}" value="{{$leaveTaken['casual']}}" data-width="100" data-height="100" data-displayPrevious=true  data-thickness=".2"  data-fgColor="#96be4b" data-bgColor="#e8e8e8">
                    </div>
                    <div class="bio-desk">
                        <h4 class="red">Casual Leave</h4>
                        <p >Total : {{$totalLeave['casual']}}</p>
                        <p>Remaining : {{$totalLeave['casual']-$leaveTaken['casual']}}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="panel">
                <div class="panel-body">
                    <div class="bio-chart">
                        <input readonly title="Sick Leave Taken" class="knob" data-max="{{$totalLeave['sick']}}" value="{{$leaveTaken['sick']}}" data-width="100" data-height="100" data-displayPrevious=true  data-thickness=".2" data-fgColor="#e06b7d" data-bgColor="#e8e8e8">
                    </div>
                    <div class="bio-desk">
                        <h4 class="terques">Sick Leave </h4>
                        <p>Total : {{$totalLeave['sick']}}</p>
                        <p>Remaining : {{$totalLeave['sick']-$leaveTaken['sick']}}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="panel">
                <div class="panel-body">
                    <div class="bio-chart">
                        <input readonly title="Half Day Leave Taken" class="knob" data-max="{{$totalLeave['half']}}" value="{{$leaveTaken['half']}}" data-width="100" data-height="100" data-displayPrevious=true  data-thickness=".2"  data-fgColor="#4CC5CD" data-bgColor="#e8e8e8">
                    </div>
                    <div class="bio-desk">
                        <h4 class="green">Half Day</h4>
                        <p>Total : {{$totalLeave['half']}}</p>
                        <p>Remaining : {{$totalLeave['half']-$leaveTaken['half']}}</p>
                    </div>
                </div>
            </div>
        </div>

    </div>	

    <!--end of pie chart area-->


    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading"style="border-color: #DADFE1;">
                    Leave Application Form
                </header>
                <div class="panel-body">
                    <form class="cmxform form-horizontal tasi-form" id="signupForm" method="post" action="{{url('/apply-for-leave/submit')}}" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        
                        <div class="form-group ">
                            <div class="col-md-6">
                                <label class="control-label" for="input_status">Leave Type</label>
                                <select class="form-control m-bot15" required name="leave_type">
                                    <option value="full">Full day</option>
                                    <option value="half">Half day</option>
                                    <option value="special">Special</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label class=" control-label " for="input_religion">Leave Purpose</label>
                                <select class="form-control m-bot15" required name="leave_purpose">

                                    <option value="casual">Casual</option>
                                    <option value="sick">Sick</option>
                                    <option value="special">Special</option>
                                </select>
                            </div> 
                        </div>

                        <div class="form-group ">
                            <div class="col-md-8">
                                <label for="username" class="control-label">Leave</label>
                                <div class="input-group input-large m-bot15" data-date="01/08/2016" data-date-format="dd/mm/yyyy">
                                    <input type="text" class="form-control dpd1" name="leave_from" value="{{old('leave_from')}}" required>
                                    <span class="input-group-addon">To</span>
                                    <input type="text" class="form-control dpd2" name="leave_to" value="{{old('leave_to')}}" required>
                                </div>
                            </div> 
                            <div class="col-md-4 text-center">
                                <label class="control-label" for="inputSuccess" style="float: none;text-align: center;">File Upload</label>
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <span class="btn btn-white btn-file">
                                        <span class="fileupload-new"><i class="fa fa-upload" aria-hidden="true"></i> Select file</span>
                                        <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                        <input type="file" class="default" name="attachment" />
                                    </span>
                                    <span class="fileupload-preview" style="margin-left:5px;"></span>
                                    <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                                </div>
                            </div>
                            
                        </div>
                         <div class="form-group ">
                            <div class="col-md-12">
                                <label for="ccomment" class="control-label col-md-2">Description</label>
                                <textarea class="form-control" cols="" rows="2" id="ccomment" name="leave_note">{{old('leave_note')}}</textarea>
                            </div> 
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-5 col-md-2">
                                <input type="submit" class="btn btn-primary btn-shadow btn-lg btn-block" value="Apply">
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>

</section>
@endsection