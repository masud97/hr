@extends('layout')

@section('content') 
<!--main content start-->

<section class="wrapper" style="margin-top: 0;">

    <div >
        <!--breadcrumbs start -->
        <ul class="breadcrumb new-bread">
            <li ><a class="top-hover" href="{{url('/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a></li>
            <li><a class="top-hover" href="activity-report-employee">Own report</a></li>
            <li class="active">Add New Report</li>
        </ul>
        <!--breadcrumbs end -->
    </div>	

    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading"style="border-color: #DADFE1;">
                    Add New Activity Report
                </header>
                <div class="panel-body">
                    <form class="cmxform form-horizontal tasi-form" id="signupForm" enctype="multipart/form-data" method="post" action="{{url('activity-report-submit')}}">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">

                        <div class="form-group ">
                            <div class="col-md-2">
                                <label class="col-md-1 control-label col-lg-1" for="input_religion"><span class="special-required">*</span>Date Range</label>
                            </div>  
                            <div class="col-md-6">
                                <div class="input-group input-large" data-date="01/08/2016" data-date-format="dd/mm/yyyy">
                                    <input required type="text" class="form-control dpdrang1" name="report_from" value="{{old('report_from')}}" required>
                                    <span class="input-group-addon">To</span>
                                    <input required type="text" class="form-control dpdrang2" name="report_to" value="{{old('report_to')}}" required>
                                </div>
                            </div> 
                        </div>

                        <div class="form-group ">
                            <div class="col-md-2">
                                <label class="col-md-1 control-label col-lg-1" for="input_status">Self Assessment </label>
                            </div>  
                            <div class="col-md-6">
                                <div class="radios">
                                    <label class="label_radio r_on" for="">
                                        <input required <?php
                                        if (old('self_a') == 'red') {
                                            echo 'checked';
                                        }
                                        ?> name="self_a" id="radio-01" value="red" type="radio" >Red&nbsp;&nbsp;
                                        <input required <?php
                                        if (old('self_a') == 'green') {
                                            echo 'checked';
                                        }
                                        ?> name="self_a" id="radio-02" value="green" type="radio">Green&nbsp;&nbsp;
                                        <input required <?php
                                        if (old('self_a') == 'yellow') {
                                            echo 'checked';
                                        }
                                        ?> name="self_a" id="radio-02" value="yellow" type="radio">Yellow&nbsp;&nbsp;
                                    </label>


                                </div>
                            </div> 
                        </div>

                        <div class="form-group ">

                            <div class="col-md-2">
                                <label class="control-label col-md-3">Activities Completed This week</label>
                            </div>
                            <div class="col-md-6">
                                <textarea class="activity_report_editor form-control" name="activity_complete_this_week" rows="8" required>{{old('activity_complete_this_week')}}</textarea>
                            </div>

                        </div>

                        <div class="form-group ">
                            <div class="col-md-2">
                                <label class="col-md-1 control-label col-lg-1" for="input_status">Long Term Project</label>
                            </div>  
                            <div class="col-md-6">
                                <textarea class="form-control " id="pre_add" name="long_term_project" >{{old('long_term_project')}}</textarea>
                            </div> 
                        </div>	

                        <div class="form-group ">
                            <div class="col-md-2">
                                <label class="col-md-1 control-label col-lg-1" for="input_status">Issue For Immediate Action</label>
                            </div>  
                            <div class="col-md-6">
                                <textarea class="form-control " id="pre_add" name="issue_for_immediate_action" >{{old('issue_for_immediate_action')}}</textarea>
                            </div> 

                        </div>	
                        <div class="form-group ">
                            <div class="col-md-2">
                                <label class="col-md-1 control-label col-lg-1" for="input_status">Key Team Dependencies</label>
                            </div>  
                            <div class="col-md-6">
                                <textarea class="form-control " id="pre_add" name="key_team_" >{{old('key_team_')}}</textarea>
                            </div> 
                        </div>	

                        <div class="form-group ">
                            <div class="col-md-2">
                                <label class="col-md-1 control-label col-lg-1" for="input_status">Note</label>
                            </div>  
                            <div class="col-md-6">
                                <textarea class="form-control " id="pre_add" rows="5" name="note_" >{{old('note_')}}</textarea>
                            </div> 
                        </div>	
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-2">
                                <input type="submit" class="btn btn-primary btn-shadow btn-lg btn-block" value="Submit">
                            </div>
                        </div>

                    </form>
                </div>
            </section>
        </div>
    </div>

</section>	

@endsection
