@extends('layout')

@section('content')

<section class="wrapper site-min-height">

    <div >
        <!--breadcrumbs start -->
        <ul class="breadcrumb new-bread">
            <li ><a class="top-hover" href="{{url('/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a></li>
            <li ><a class="top-hover" href="#">Activity Report</a></li>
            @if(!is_null($emp))
            <li><a class="top-hover" href="{{url('/admin-activity-report')}}">Employee List</a></li>
            @endif
            @if(!is_null($emp))
            <li ><a class="top-hover" href="{{url('/admin-activity-report')}}">Report List</a></li>
            @else
            <li ><a class="top-hover" href="{{url('/activity-report-employee')}}">Report List</a></li>

            @endif
            <li class="active" >Details</li>
        </ul>
        <!--breadcrumbs end -->
    </div>

    <section class="panel">


        <header class="panel-heading" style="border-color: #DADFE1;height: 50px">
            <div class="col-md-8">
                Report Details
            </div>

            <div class="col-md-4 text-right">
                <p class="date" style="font-size: 12px;
    color: #9c9c9c;"><b>Published On:</b> {{$report_list_array->insert_date }}</p>
            </div>

        </header>

        <div class="heading-inbox row">



        </div>


        <div class="panel-body table-responsive">

            <div class="inbox-body">

                <div class="sender-info">
                    <div class="row">
                        <div class="col-md-12">
                            <img alt="" src="{{url('assets/img/user-image')}}/{{$publish_by_name_and_image->image}}">
                            <strong>{{$publish_by_name_and_image->full_name}}</strong>

                        </div>
                    </div>
                </div>

                <div class="view-mail">

                    <table class="table borderless" border="0">
                        <tbody>
                            <tr>
                                <th>Date Range</th>
                                <td style="width: 10px;">:</td>
                                <td> {{ $report_list_array->report_from }} TO  {{ $report_list_array->report_to }}</td>

                            </tr>
                            <tr>
                                <th>Self Assesment</th>
                                <td style="width: 10px;">:</td>
                                <td> {{$report_list_array->self_a}}</td>

                            </tr>
                            <tr>
                                <th>Activities completed this week</th>
                                <td style="width: 10px;">:</td>
                                <td> <?php echo strip_tags($report_list_array->activity_complete_this_week, '<br><b><i><u><li><ol><ul>') ?></td>

                            </tr>
                            <tr>
                                <th>Long Term Project</th>
                                <td style="width: 10px;">:</td>
                                <td>{{$report_list_array->long_term_project}}</td>

                            </tr>
                            <tr>
                                <th>Issued for immediate attention</th>
                                <td style="width: 10px;">:</td>
                                <td>{{$report_list_array->issue_for_immediate_action}}</td>

                            </tr>
                            <tr>
                                <th>Key team inter dependencies</th>
                                <td style="width: 10px;">:</td>
                                <td>{{$report_list_array->key_team}}</td>

                            </tr>
                            <tr>
                                <th>Note</th>
                                <td style="width: 10px;">:</td>
                                <td>{{$report_list_array->note}}</td>

                            </tr>
                            <tr>
                                <th>Rating</th>
                                <td style="width: 10px;">:</td>
                                <td>{{$report_list_array->rating}}</td>

                            </tr>

                        </tbody>
                    </table>



                </div>


            </div>


        </div>
    </section>
    @if(is_null($report_list_array->rating))
    @if(Session::get('id') == $report_list_array->who_will_rate)
    <section class="panel">
        <header class="panel-heading">
            Report Rating
        </header>
        <div class="panel-body">
            <table class="table sliders">
                <tbody> 

                <form method="post" action="{{url('/rate-report')}}">
                    <input type="hidden" value="{{csrf_token()}}" name="_token" >
                    <input type="hidden" value="{{$report_list_array->id}}" name="id" >
                    <input type="hidden" value="{{$report_list_array->who_will_rate}}" name="who_will_rate" >
                    <input type="hidden" value="{{$report_list_array->employee_login_id}}" name="employee_login_id" >
                    <tr>
                        <td style="width:14%">Bound to Select</td>
                        <td>
                            <select name="rate" id="minbeds" class="form-control bound-s">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option selected>5</option>
                                <option>6</option>
                                <option>7</option>
                                <option>8</option>
                                <option>9</option>
                                <option>10</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td >
                            <input class="btn btn-primary" type="submit" value="Submit">
                        </td>
                    </tr>
                </form>
                </tbody>
            </table>
        </div>
    </section>
    @endif
    @endif
    <!-- page end-->

</section>

@endsection