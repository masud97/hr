@extends('layout')

@section('content')

<section class="wrapper site-min-height">

    <div >
        <!--breadcrumbs start -->
        <ul class="breadcrumb new-bread">
            <li ><a class="top-hover" href="{{url('/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a></li>

            <li >Activity Report</li>
            <li class="active">Pending Report</li>
        </ul>
        <!--breadcrumbs end -->
    </div>

    <!-- page start-->
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    Activity Report

                </header>

                <div class="panel-body table-responsive">
                    <div class="adv-table">
                        <table  class="display table table-bordered table-striped" id="example">
                            <thead class="alignment">
                                <tr>
                                    <th>Sr</th>
                                    <th>Date Range</th>
                                    <th>Insert Date & Time</th>


                                </tr>
                            </thead>
                            <tbody>
                                @for($i=0;$i<sizeof($report_list_array);$i++)

                                <tr>
                                    <td>{{$i+1}}</td>

                                    <td><a href="{{url('/report-details/'.$report_list_array[$i]['id']."/".$report_list_array[$i]['employee_login_id'])}}">{{$report_list_array[$i]['report_from']}} TO {{$report_list_array[$i]['report_to']}}</a></td>


                                    <td>{{$report_list_array[$i]['insert_date']}}</td>


                                </tr>


                                @endfor

                                </tfoot>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- page end-->
</section>


@endsection