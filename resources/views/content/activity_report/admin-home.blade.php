@extends('layout')

@section('content')

<section class="wrapper site-min-height">

    <div >
        <!--breadcrumbs start -->
        <ul class="breadcrumb new-bread">
            <li ><a class="top-hover" href="{{url('/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a></li>
             <li><a class="top-hover" href="#">Activity Report</a></li>
            <li class="active">Employee List</li>
        </ul>
        <!--breadcrumbs end -->
    </div>

    <!-- page start-->
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    Activity Report
                    <!--                    <a title="" data-placement="left" data-toggle="tooltip" data-original-title="New Notice" class="btn btn-success link-all-notice tooltips" href="{{url('/add-new-report')}}">
                                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                        </a>-->

                </header>

                <div class="panel-body table-responsive">
                    <div class="adv-table">
                        <table  class="display table table-bordered table-striped" id="example">
                            <thead class="alignment">
                                <tr>
                                    <th>Sr</th>
                                    <th></th>
                                    <th>Employee Name</th>
                                    <th>Company Name</th>
                                    <th>Rate</th>


                                </tr>
                            </thead>
                            <tbody>
                                @for($i=0;$i<sizeof($emp_list_array);$i++)

                                <tr>
                                    <td>{{$i+1}}</td>
                                    @if(!is_null($emp_list_array[$i][4]))
                                    <td><img height="50px" width="40px" class="profile-pic" src="assets/img/user-image/<?= $emp_list_array[$i][4] ?>"></td>
                                    @else
                                    <td><img height="50px" width="40px" class="profile-pic" src="assets/img/nopic.png"></td>

                                    @endif
                                    <td><a href="{{url('/activity-report-employee/'.$emp_list_array[$i][0])}}">{{$emp_list_array[$i][1]}}</a></td>
                                    <td>{{$emp_list_array[$i][5]}}</td>
                                    <td>{{$emp_list_array[$i][2]}} out of {{$emp_list_array[$i][3]}}</td>
                                </tr>


                                @endfor

                                </tfoot>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- page end-->
</section>


@endsection