@extends('layout')

@section('content')

<section class="wrapper site-min-height">

    <div >
        <!--breadcrumbs start -->
        <ul class="breadcrumb new-bread">
            <li ><a class="top-hover" href="{{url('/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a></li>
            <li ><a class="top-hover" href="#">Activity Report</a></li>
            @if(!is_null($emp))
            <li><a class="top-hover" href="{{url('/admin-activity-report')}}">All reports</a></li>
            @endif
            <li class="active">Report List</li>

        </ul>
        <!--breadcrumbs end -->
    </div>

    <!-- page start-->
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    Activity Report
                    @if(is_null($emp))
                    <a title="" data-placement="left" data-toggle="tooltip" data-original-title="New Notice" class="btn btn-success link-all-notice tooltips" href="{{url('/add-new-report')}}">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                    </a>
                    @endif
                </header>

                <div class="panel-body table-responsive">
                    <div class="adv-table">
                        <table  class="display table table-bordered table-striped" id="example">
                            <thead class="alignment">
                                <tr>
                                    <th>Sr</th>
                                    <th>Date Range</th>
                                    <th>Insert Date & Time</th>
                                    <th>Rating</th>
                                    <th>Action</th>

                                </tr>
                            </thead>
                            <tbody>
                                @for($i=0;$i<sizeof($report_list_array);$i++)

                                <tr>
                                    <td>{{$i+1}}</td>
                                    @if(is_null($emp))
                                    <td><a href="{{url('/report-details/'.$report_list_array[$i]['id'])}}">{{$report_list_array[$i]['report_from']}} TO {{$report_list_array[$i]['report_to']}}</a></td>
                                    @else
                                    <td><a href="{{url('/report-details/'.$report_list_array[$i]['id']."/".$emp)}}">{{$report_list_array[$i]['report_from']}} TO {{$report_list_array[$i]['report_to']}}</a></td>

                                    @endif
                                    <td>{{$report_list_array[$i]['insert_date']}}</td>
                                    <td>{{$report_list_array[$i]['rating']}}</td>
                                    <td>
                                        @if(is_null($emp))
                                        @if($report_list_array[$i]['rating']==='-')
                                        <a class="btn  btn-sm" href="{{url('/edit-report/'.$report_list_array[$i]['id'])}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                        @endif
                                        @endif

                                    </td>

                                </tr>


                                @endfor

                                </tfoot>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- page end-->
</section>


@endsection