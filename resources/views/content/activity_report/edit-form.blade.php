@extends('layout')

@section('content') 
<!--main content start-->

<section class="wrapper" style="margin-top: 0;">

    <div >
        <!--breadcrumbs start -->
        <ul class="breadcrumb new-bread">
            <li ><a class="top-hover" href="{{url('/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a></li>
            <li><a class="top-hover" href="#">Activity Report</a></li>
            <li><a class="top-hover" href="{{url('/activity-report-employee')}}">Report List</a></li>
            <li class="active">Edit Activity Report ({{ Session::get('name')}})</li>
        </ul>
        <!--breadcrumbs end -->
    </div>	

    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading"style="border-color: #DADFE1;">
                    Edit Activity Report
                </header>
                <div class="panel-body">



                    <form class="cmxform form-horizontal tasi-form" id="signupForm" enctype="multipart/form-data" method="post" action="{{url('edit-report-submit')}}">

                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="id" value="{{$report_list_array->id}}">
                        <input type="hidden" name="who_will_rate" value="{{$report_list_array->who_will_rate}}">



                        <div class="form-group ">


                            <div class="col-md-2">
                                <label class="col-md-1 control-label col-lg-1" for="input_religion"><span class="special-required">*</span>Date Range</label>
                            </div>  


                            <div class="col-md-6">

                                <div class="input-group input-large" data-date="01/08/2016" data-date-format="mm/dd/yyyy">
                                    <input type="text" class="form-control dpd1" name="report_from" value="{{$report_list_array->report_from}}" required>
                                    <span class="input-group-addon">To</span>
                                    <input type="text" class="form-control dpd2" name="report_to" value="{{$report_list_array->report_to}}" required>
                                </div>


                            </div> 



                        </div>

                        <div class="form-group ">
                            <div class="col-md-2">
                                <label class="col-md-1 control-label col-lg-1" for="input_status">Self Assessment </label>
                            </div>  
                            <div class="col-md-6">
                                <div class="radios">
                                    <label class="label_radio r_on" for="radio-01">
                                        <input <?php
                                        if ($report_list_array->self_a == 'red') {
                                            echo 'checked';
                                        }
                                        ?> name="self_a" id="radio-01" value="red" type="radio" >Red&nbsp;&nbsp;
                                        <input <?php
                                        if ($report_list_array->self_a == 'green') {
                                            echo 'checked';
                                        }
                                        ?> name="self_a" id="radio-02" value="green" type="radio">Green&nbsp;&nbsp;
                                        <input <?php
                                        if ($report_list_array->self_a == 'yellow') {
                                            echo 'checked';
                                        }
                                        ?> name="self_a" id="radio-02" value="yellow" type="radio">Yellow&nbsp;&nbsp;
                                    </label>


                                </div>
                            </div> 






                        </div>

                        <div class="form-group ">

                            <div class="col-md-2">
                                <label class="control-label col-md-3">Activities Completed This week</label>
                            </div>
                            <div class="col-md-8">
                                <textarea class="activity_report_editor form-control" name="activity_complete_this_week" rows="8" >{{$report_list_array->activity_complete_this_week}}</textarea>
                            </div>

                        </div>



                        <div class="form-group ">


                            <div class="col-md-2">
                                <label class="col-md-1 control-label col-lg-1" for="input_status">Long Term Project</label>
                            </div>  
                            <div class="col-md-6">
                                <textarea class="form-control " id="pre_add" name="long_term_project" >{{$report_list_array->long_term_project}}</textarea>
                            </div> 

                        </div>	

                        <div class="form-group ">


                            <div class="col-md-2">
                                <label class="col-md-1 control-label col-lg-1" for="input_status">Issue For Immediate Action</label>
                            </div>  
                            <div class="col-md-6">
                                <textarea class="form-control " id="pre_add" name="issue_for_immediate_action" >{{$report_list_array->issue_for_immediate_action}}</textarea>
                            </div> 

                        </div>	
                        <div class="form-group ">


                            <div class="col-md-2">
                                <label class="col-md-1 control-label col-lg-1" for="input_status">Key Team Dependencies</label>
                            </div>  
                            <div class="col-md-6">
                                <textarea class="form-control " id="pre_add" name="key_team_">{{$report_list_array->key_team}}</textarea>
                            </div> 

                        </div>	

                        <div class="form-group ">


                            <div class="col-md-2">
                                <label class="col-md-1 control-label col-lg-1" for="input_status">Note</label>
                            </div>  
                            <div class="col-md-6">
                                <textarea class="form-control " id="pre_add" rows="5" name="note_" >{{$report_list_array->note}}</textarea>
                            </div> 

                        </div>	




                        <div class="form-group">
                            <div class="col-md-offset-5 col-md-7">
                                <input type="submit" class="btn btn-danger" value="Submit">
                            </div>
                        </div>


                    </form>




                </div>
            </section>
        </div>
    </div>

</section>	

@endsection
