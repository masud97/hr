@extends('layout')

@section('content')



<section class="wrapper">

    <div >
        <!--breadcrumbs start -->
        <ul class="breadcrumb new-bread">
            <li ><a class="top-hover" href="{{url('/dashboard')}}"><i class="fa fa-home"></i>Dashboard</a></li>
            <li><a class="top-hover" href="#">Employee</a></li>
            @if(isset($employee_id))
            <li><a class="top-hover" href="{{url('/archive-employee-list')}}">Archive Employee</a></li>
            @endif
            <li class="active">Profile ({{$userDetails->full_name}})</li>
        </ul>
        <!--breadcrumbs end -->
    </div>


    <!-- page start-->
    <div class="row">
        <aside class="profile-nav col-lg-3">
            <section class="panel">
                <div class="user-heading round 
                     image-upload">

                    <a href="#">
                        @if(!is_null($userDetails->image))
                        <img src="{{url('assets/img/user-image/')}}/{{$userDetails->image}}" alt="">
                        @else
                        <img src="{{url('assets/img/')}}/nopic.png" alt="">
                        @endif

                    </a>
                    <h1>{{$userDetails->full_name}}</h1>
                    <p>{{$userDetails->email_work}}</p>
                </div>

                <ul class="nav nav-pills nav-stacked">
                    <li class="active">
                        @if(isset($employee_id))
                        <a href="{{url("/archive-employee-profile-view/".$employee_id)}}"> <i class="fa fa-user"></i> Profile</a>
                        @else
                        <a href="{{url('/profile')}}"> <i class="fa fa-user"></i> Profile</a>
                        @endif

                    </li>

                    <li>
                        @if(isset($employee_id))
                        <a href="{{url('/archive-view-merits-demerits/'.$employee_id)}}"> <i class="fa fa-indent" aria-hidden="true"></i>Merits and Demerits</a>


                        @endif
                    </li>




                </ul>

            </section>
        </aside>



        <aside class="profile-info col-lg-9">


            <section class="panel">

                <div class="panel-body bio-graph-info">
                    <header class="panel-heading" style="border-color: #DADFE1;">
                        Bio Graph
                    </header>

                    <div class="panel-body">

                        <div class="bio-row">
                            <p><span>Employee Id </span>: {{$emp_id}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>Company Name </span>: {{$userDetails->company_name}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>Designation </span>: {{$userDetails->designation}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>Type </span>: {{$userDetails->employee_category}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>Joining Date </span>: {{$userDetails->joining_date}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>Office Mobile </span>: {{$userDetails->mobile_work}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>Personal Mobile </span>: {{$userDetails->mobile_personal}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>Emergency Contact </span>: {{$userDetails->mobile_emergency}} ({{$userDetails->emergency_contact_name_and_relation}})</p>
                        </div>
                        <div class="bio-row">
                            <p><span>Office Email </span>: {{$userDetails->email_work}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>Personal Email </span>: {{$userDetails->email_personal}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>Present Address </span>: {{$userDetails->persent_address}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>Permanent Address </span>: {{$userDetails->permanent_address}}</p>
                        </div>

                        <div class="bio-row">
                            <p><span>Gender </span>: {{$userDetails->gender}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>Marital Status </span>: {{$userDetails->marital_status}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>Religion </span>: {{$userDetails->religion}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>Blood Group </span>: {{$userDetails->blood_group}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>Date Of Birth </span>: {{$userDetails->date_of_birth}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>Highest Degree Obtained </span>: {{$userDetails->highest_degree_obtained}}</p>
                        </div>
                    </div>
                </div>
            </section>

            <section class="panel">
                <div class="panel-body bio-graph-info">
                    <header class="panel-heading" style="border-color: #DADFE1;">
                        Approver

                    </header>
                    <div class="panel-body">

                        <div class="col-md-4">
                            <p><span>Supervisor</span>: {{$assignApprover['supervisor']['name']}}</p>
                        </div>
                        <div class="col-md-4">
                            <p><span>Approver</span>: {{$assignApprover['approver']['name']}}</p>
                        </div>
                        <div class="col-md-4">
                            <p><span>Final Approver</span>: {{$assignApprover['finalApprover']['name']}}</p>
                        </div>

                    </div>
                </div>
            </section>


            <div class="panel">

                <!--                start of pie chart area-->


                <div class="col-md-4" style="padding: 0;">
                    <!--                    widget start-->
                    <div class="panel">
                        <div class="panel-body">
                            <div class="bio-chart">
                                <input class="knob" data-max="{{$totalLeave['casual']}}" value="{{$leaveTaken['casual']}}" data-width="80" data-height="90" data-displayPrevious=true  data-thickness=".2"  data-fgColor="#96be4b" data-bgColor="#e8e8e8">
                            </div>
                            <div class="bio-desk">
                                <h4 class="red">Casual Leave</h4>
                                <p>Total : {{$totalLeave['casual']}}</p>
                                <p>Taken : {{$leaveTaken['casual']}}</p>
                            </div>
                        </div>
                    </div>
                    <!--                    widget end-->
                </div>
                <div class="col-md-4">
                    <!--                    widget start-->
                    <div class="panel">
                        <div class="panel-body">
                            <div class="bio-chart">
                                <input class="knob" data-max="{{$totalLeave['sick']}}" value="{{$leaveTaken['sick']}}" data-width="80" data-height="90" data-displayPrevious=true  data-thickness=".2" data-fgColor="#e06b7d" data-bgColor="#e8e8e8">
                            </div>
                            <div class="bio-desk">
                                <h4 class="terques">Sick Leave </h4>
                                <p>Total : {{$totalLeave['sick']}}</p>
                                <p>Taken : {{$leaveTaken['sick']}}</p>
                            </div>
                        </div>
                    </div>
                    <!--                    widget end-->
                </div>
                <div class="col-md-4" style="padding: 0;">
                    <!--                    widget start-->
                    <div class="panel">
                        <div class="panel-body">
                            <div class="bio-chart">
                                <input class="knob" data-max="{{$totalLeave['half']}}" value="{{$leaveTaken['half']}}" data-width="80" data-height="90" data-displayPrevious=true  data-thickness=".2"  data-fgColor="#4CC5CD" data-bgColor="#e8e8e8">
                            </div>
                            <div class="bio-desk">
                                <h4 class="green">Half Day</h4>
                                <p>Total : {{$totalLeave['half']}}</p>
                                <p>Taken : {{$leaveTaken['half']}}</p>
                            </div>
                        </div>
                    </div>
                    <!--                    widget end-->
                </div>



                <!--                 page end-->
            </div>


        </aside>









    </div>




</section>




@endsection
