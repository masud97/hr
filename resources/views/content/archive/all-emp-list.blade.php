@extends('layout')

@section('content') 
<section class="wrapper site-min-height">
    <div >
        <!--breadcrumbs start -->
        <ul class="breadcrumb new-bread">
            <li ><a class="top-hover" href="{{url('/dashboard')}}"><i class="fa fa-home"></i>Dashboard</a></li>
            <li><a class="top-hover" href="#">Employee</a></li>

            <li class="active">Archive Employee</li>


        </ul>
        <!--breadcrumbs end -->
    </div>
    <!-- page start-->
    <section class="panel">
        <header class="panel-heading">
            Archive Employee
        </header>
        <div class="panel-body table-responsive">
            <div class="adv-table">

                <table  class="display table table-bordered table-striped" id="example">
                    <thead>
                        <tr>
                            <th>Sl. NO.</th>
                            <th></th>
                            <th>Employee Name</th>
                            <th>Employee ID</th>
                            <th>Company Name</th>
                            <th class="hidden-phone">Designation</th>
                            @if(hasPrivilege(12))
                            <th class="hidden-phone">Action</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        <?php for ($i = 0; $i < sizeof($all_emp_val); $i++) : ?>
                            <tr class="gradeX">
                                <td><?= $i + 1; ?></td>
                                @if(!is_null($all_emp_val[$i]['image']))
                                <td><img height="50px" width="40px" class="profile-pic" src="assets/img/user-image/<?= $all_emp_val[$i]['image'] ?>"></td>
                                @else
                                <td><img height="50px" width="40px" class="profile-pic" src="assets/img/nopic.png"></td>
                                @endif
                                <td><a title="Click here to view profile " href="{{url("/archive-employee-profile-view/".$all_emp_val[$i]['employee_id'])}}"><?= $all_emp_val[$i]['full_name'] ?></a></td>
                                <td><?= $all_emp_val[$i]['employee_login_id'] ?></td>
                                <td><?= $all_emp_val[$i]['company_name'] ?></td>
                                <td><?= $all_emp_val[$i]['designation'] ?></td>
                                @if(hasPrivilege(12))
                                <td><a id="restore-emp-{{$all_emp_val[$i]['employee_id']}}" onclick="restore_employee(event,{{$all_emp_val[$i]['employee_id']}})" title="Click here to restore this employee" href="{{url("/restore-employee/".$all_emp_val[$i]['employee_id'])}}"> <button class="btn btn-warning" >Restore</button> </a></td>
                                @endif
                            </tr>


                        <?php endfor; ?>




                    </tbody>

                </table>



            </div>
        </div>
    </section>
    <!-- page end-->
</section>

@endsection