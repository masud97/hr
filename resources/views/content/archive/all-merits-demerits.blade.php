@extends('layout') 
@section('content')
<section class="wrapper">

    <div>
        <!--breadcrumbs start -->
        <ul class="breadcrumb new-bread">
            <li><a class="top-hover" href="{{url('/dashboard')}}"><i class="fa fa-home"></i>Dashboard</a></li>
            <li><a class="top-hover" href="#">Employee</a></li>
            @if(isset($employee_id))
            <li><a class="top-hover" href="{{url('/archive-employee-list')}}">Archive Employee</a></li>
            @endif
            <li class="active">Merits/Demerits</li>
        </ul>
        <!--breadcrumbs end -->
    </div>


    <!-- page start-->
    <div class="row">
        <aside class="profile-nav col-lg-3">
            <section class="panel">
                <div class="user-heading round 
                     image-upload">

                    <a href="#">
                        @if(!is_null($userDetails->image))
                        <img src="{{url('assets/img/user-image/')}}/{{$userDetails->image}}" alt="">
                        @else
                        <img src="{{url('assets/img/')}}/nopic.png" alt="">
                        @endif

                    </a>
                    <h1>{{$userDetails->full_name}}</h1>
                    <p>{{$userDetails->email_work}}</p>
                </div>

                <ul class="nav nav-pills nav-stacked">
                    <li class="active">
                        @if(isset($employee_id))
                        <a href="{{url("/archive-employee-profile-view/".$employee_id)}}"> <i class="fa fa-user"></i> Profile</a>
                        @else
                        <a href="{{url('/profile')}}"> <i class="fa fa-user"></i> Profile</a>
                        @endif

                    </li>

                    <li>
                        @if(isset($employee_id))
                        <a href="{{url('/archive-view-merits-demerits/'.$employee_id)}}"> <i class="fa fa-indent" aria-hidden="true"></i>Merits and Demerits</a>


                        @endif
                    </li>





                </ul>

            </section>
        </aside>

        <aside class="profile-info col-lg-9">

            <section class="panel">



                <!--widget start-->
                <section class="panel">
                    <header class="panel-heading tab-bg-dark-navy-blue">
                        <ul class="nav nav-tabs nav-justified ">
                            <li class="active">
                                <a href="#merits" data-toggle="tab">
                                    Merits
                                </a>
                            </li>
                            <li>
                                <a href="#demerits" data-toggle="tab">
                                    Demerits
                                </a>
                            </li>
                        </ul>
                    </header>
                    <div class="panel-body">
                        <div class="tab-content tasi-tab">

                            <div class="tab-pane active" id="merits">

                                @for($i=0;$i<sizeof($merits_demerits_array);$i++)
                                    @if($merits_demerits_array[$i]['type']=='Merits')
                                    <article class="media">
                                        <!--                                    <a class="pull-left thumb p-thumb">
                                                                                <img src="img/product1.jpg">
                                                                            </a>-->
                                        <div class="media-body">
                                            <!--                                        <a class=" p-head" href="#">Item One Tittle</a>-->

                                            <p id="des_{{$merits_demerits_array[$i]['id']}}">{{$merits_demerits_array[$i]['description']}}&nbsp;&nbsp;</p>


                                        </div>
                                    </article>

                                    @endif
                                    @endfor

                            </div>

                            <div class="tab-pane" id="demerits">

                                @for($i=0;$i<sizeof($merits_demerits_array);$i++)
                                    @if($merits_demerits_array[$i]['type']=='Demerits')
                                    <article class="media">
                                        <!--                                    <a class="pull-left thumb p-thumb">
                                                                                <img src="img/product1.jpg">
                                                                            </a>-->
                                        <div class="media-body">
                                            <!--                                        <a class=" p-head" href="#">Item One Tittle</a>-->
                                            <p id="des_{{$merits_demerits_array[$i]['id']}}">{{$merits_demerits_array[$i]['description']}}&nbsp;&nbsp;</p>

                                        </div>
                                    </article>

                                    @endif
                                    @endfor

                            </div>

                        </div>
                    </div>
                </section>
                <!--widget end-->



            </section>

        </aside>

    </div>

</section>

@endsection