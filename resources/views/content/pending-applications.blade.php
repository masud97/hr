@extends('layout')

@section('content')

<section class="wrapper site-min-height">

    <div >
        <!--breadcrumbs start -->
        <ul class="breadcrumb new-bread">
            <li ><a class="top-hover" href="{{url('/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a></li>

            <li class=""><a href="#" class="top-hover">Leave</a></li>
            <li class="active">Pending Leave Applications</li>
        </ul>
        <!--breadcrumbs end -->
    </div>

    <!-- page start-->
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    {{ucfirst($title)}}
                </header>

                <div class="panel-body table-responsive">
                    <div class="adv-table">
                        <table  class="display table table-bordered table-striped" id="example">
                            <thead class="alignment">
                                <tr>
                                    <th>Sr</th>
                                    <th>Name</th>
                                    <th>Leave type</th>
                                    <th>Leave Purpose</th>
                                    <th>Leave Amount</th>
                                    <th>Leave date</th>
                                    <th>Apply date</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($pendingApplications)>0)
                                <?php $i=1; ?>
                                @foreach($pendingApplications as $application)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td><a href="{{url('pending-applicatoins/application-view')}}/{{$application->id}}">{{$application->full_name}}</a></td>
                                    <td>{{ucfirst($application->leave_type)}}</td>
                                    <td>{{ucfirst($application->leave_purpose)}}</td>
                                    <td>{{$application->leave_amount}}</td>
                                    <td>{{changeDateFormat($application->leave_from,'d-m-Y')}} to {{changeDateFormat($application->leave_to,'d-m-Y')}}</td>
                                    <td>{{changeDateFormat($application->apply_date,'l jS \of F Y')}}</td>
                                </tr>
                                @endforeach
                                @endif
                               


                                </tfoot>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- page end-->
</section>


@endsection