@extends('layout') 
@section('content')
<section class="wrapper">

    <div >
        <!--breadcrumbs start -->
        <ul class="breadcrumb new-bread">
            <li "><a class="top-hover" href="{{url('/dashboard')}}"><i class="fa fa-home"></i>Dashboard</a></li>
            <li><a class="top-hover" href="#">Employee</a></li>
            @if(isset($employee_id))
            <li><a class="top-hover" href="{{url('/all-employee')}}">All Employee</a></li>
            @endif
            <li class="active">Merits/Demerits</li>
        </ul>
        <!--breadcrumbs end -->
    </div>


    <!-- page start-->
    <div class="row">
        @include('common.common_aside_profile')

        <aside class="profile-info col-lg-9">

            <section class="panel">



                <!--widget start-->
                <section class="panel">
                    <header class="panel-heading tab-bg-dark-navy-blue">
                        <ul class="nav nav-tabs nav-justified ">
                            <li class="active">
                                <a href="#merits" data-toggle="tab">
                                    Merits
                                </a>
                            </li>
                            <li>
                                <a href="#demerits" data-toggle="tab">
                                    Demerits
                                </a>
                            </li>
                        </ul>
                    </header>
                    <div class="panel-body">
                        <div class="tab-content tasi-tab">

                            <div class="tab-pane active" id="merits">

                                @for($i=0;$i<sizeof($merits_demerits_array);$i++)
                                    @if($merits_demerits_array[$i]['type']=='Merits')
                                    <article class="media">
                                        <!--                                    <a class="pull-left thumb p-thumb">
                                                                                <img src="img/product1.jpg">
                                                                            </a>-->
                                        <div class="media-body">
                                            <!--                                        <a class=" p-head" href="#">Item One Tittle</a>-->

                                            <p id="des_{{$merits_demerits_array[$i]['id']}}">{{$merits_demerits_array[$i]['description']}}&nbsp;&nbsp;</p>
                                            @if(hasPrivilege(5))
                                            <button onclick="editMeritsDemerits({{$merits_demerits_array[$i]['id']}}, 'merits')" title="Edit this merits" data-placement="top" data-toggle="tooltip"  class="btn btn-sm tooltips"><i class="fa fa-pencil" aria-hidden="true"></i></button>  
                                            <a onclick="remove_merits_demerits('event','merits',{{$merits_demerits_array[$i]['id']}})" href="{{url('/remove-merits-demerits/'.$merits_demerits_array[$i]['id'])}}" title="Remove this merits" class="btn btn-sm tooltips"><i class="fa fa-trash-o"></i></a>
                                            @endif
                                        </div>
                                    </article>

                                    @endif
                                    @endfor

                            </div>

                            <div class="tab-pane" id="demerits">

                                @for($i=0;$i<sizeof($merits_demerits_array);$i++)
                                    @if($merits_demerits_array[$i]['type']=='Demerits')
                                    <article class="media">
                                        <!--                                    <a class="pull-left thumb p-thumb">
                                                                                <img src="img/product1.jpg">
                                                                            </a>-->
                                        <div class="media-body">
                                            <!--                                        <a class=" p-head" href="#">Item One Tittle</a>-->
                                            <p id="des_{{$merits_demerits_array[$i]['id']}}">{{$merits_demerits_array[$i]['description']}}&nbsp;&nbsp;</p><button onclick="editMeritsDemerits({{$merits_demerits_array[$i]['id']}}, 'demerits')" title="Edit this demerits" data-placement="top" data-toggle="tooltip" data-original-title="Delete" class="btn btn-sm tooltips"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                                            <a id="merits-demerits-remove-confirmation-{{$merits_demerits_array[$i]['id']}}" onclick="remove_merits_demerits(event,'demerits',{{$merits_demerits_array[$i]['id']}})" href="{{url('/remove-merits-demerits/'.$merits_demerits_array[$i]['id'])}}" title="Remove this demerits" class="btn btn-sm tooltips"><i class="fa fa-trash-o"></i></a>
                                        </div>
                                    </article>

                                    @endif
                                    @endfor

                            </div>

                        </div>
                    </div>
                </section>
                <!--widget end-->



            </section>

        </aside>
        <!-- Modal -->
        <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Modal Tittle</h4>
                    </div>
                    <div class="modal-body">




                        <div class="panel-body">
                            <form  method="post" action="{{url('edit-merits-demerits')}}">
                                <input id="csrf_token" type="hidden" name="_token" value="{{csrf_token()}}">
                                <input id="id" type="hidden" name="id">
                                <input id="type" type="hidden" name="type">


                                <div class="form-group">

                                    <textarea class="form-control " id="details" name="details" required></textarea>

                                </div>
                        </div>




                        <div class="modal-footer">
                            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                            <input class="btn btn-default"  type="submit" value="Submit" >

                        </div>
                        </form>
                    </div>


                </div>
            </div>
        </div>
        <!-- modal end-->

        <!-- modal -->
    </div>

</section>

@endsection