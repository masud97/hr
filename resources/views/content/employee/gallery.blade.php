@extends('layout')

@section('content')


<section class="wrapper">

    <div >
        <!--breadcrumbs start -->
        <ul class="breadcrumb new-bread">
            <li ><a class="top-hover" href="{{url('/')}}"><i class="fa fa-home"></i> Dashboard</a></li>
           
            @if(isset($employee_id))
            <li><a class="top-hover" href="{{url('/all-employee')}}">All Employee</a></li>
            <li><a class="top-hover" href="{{url('/admin-employee-profile-view')}}/{{$employee_id}}">Profile</a></li>
            @else
            <li><a class="top-hover" href="{{url('/profile')}}">Profile</a></li>
            @endif
            <li class="active">Gallery</li>
        </ul>
        <!--breadcrumbs end -->
    </div>


    <!-- page start-->
    <div class="row">
         @include('common.common_aside_profile')
       
        <aside class="profile-info col-lg-9">

            <!-- page start-->
            <section class="panel">
                <header class="panel-heading">
                    Image Galley
                </header>
                <div class="panel-body">
                    <ul class="grid cs-style-3">
                        @if(!is_null($documents))
                            @foreach($documents as $doc)
                                <?php $type = explode('.',$doc['file']); ?>
                                <li>
                                    <figure>
                                        @if($type[1] === 'pdf')
                                        <img src="{{url('assets/img/pdflogo.png')}}" >
                                        <figcaption>
                                            <h3>{{$doc['Title']}}</h3>
                                            <span>{{$doc['Category']}} </span>
                                            <a class="fancybox123" rel="group" target="new" href="{{url('assets/user-doc')}}/{{$doc['file']}}">Take a look</a>
                                        </figcaption>
                                        @else
                                        <img src="{{url('assets/user-doc')}}/{{$doc['file']}}" >
                                        <figcaption>
                                            <h3>{{$doc['Title']}}</h3>
                                            <span>{{$doc['Category']}} </span>
                                            <a class="fancybox" rel="group" href="{{url('assets/user-doc')}}/{{$doc['file']}}">Take a look</a>
                                        </figcaption>
                                        @endif
                                        
                                    </figure>
                                </li>
                            @endforeach
                        @endif
                        
                        
                        @if(count($attachments)>0)
                            @foreach($attachments as $doc)
                                <li>
                                    <figure>
                                        <img src="{{url('assets/leave-doc')}}/{{$doc}}" alt="img04">
                                        <figcaption>
                                            <h3>Attachment</h3>
                                            <span>Leave attachment</span>
                                            <a class="fancybox" rel="group" href="{{url('assets/leave-doc')}}/{{$doc}}">Take a look</a>
                                        </figcaption>
                                    </figure>
                                </li>
                            @endforeach
                        @endif
                       
                    </ul>

                </div>
            </section>
    </div>


</aside>


</div>




</section>

 @endsection
