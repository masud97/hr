@extends('layout')

@section('content') 
<!--main content start-->

<section class="wrapper" style="margin-top: 0;">

    <div class="">
        <!--breadcrumbs start -->
        <ul class="breadcrumb new-bread">
            <li><a class="top-hover" href="{{url('/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a></li>
            <li><a class="top-hover" href="#">Employee</a></li>
            <li class="active">Create Employee</li>
        </ul>
        <!--breadcrumbs end -->
    </div>	

    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading"style="border-color: #DADFE1;">
                    Create Employee
                </header>
                <div class="panel-body">



                    <form class="cmxform form-horizontal tasi-form" id="signupForm" enctype="multipart/form-data" method="post" action="{{url('create-employee-submit')}}">

                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="form-group ">
                            <div class="col-md-4">
                                <label class="control-label " for="input_status"><span class="special-required">*</span>Employee Name</label>
                                <input value="{{old('full_name')}}" class=" form-control" id="firstname" name="full_name" type="text" />
                            </div> 
                            <div class="col-md-4">
                                <label class="control-label " for="input_religion"><span class="special-required">*</span>Employee ID</label>
                                <input class="form-control " id="emp_id" type="number" value="{{old('employee_id')}}" name="employee_id" required />
                            </div>  
                            <div class="col-md-4">
                                <label class="control-label " for="input_religion">Designation</label>
                                <input class="form-control " id="deg" value="{{old('designation')}}" name="designation" type="text" />
                            </div>

                        </div>


                        <div class="form-group ">
                            <div class="col-md-4">
                                <label class="control-label" for="input_religion"><span class="special-required">*</span>Joining Date</label>
                                <div data-date-viewmode="years" data-date-format="dd-mm-yyyy" data-date="12-02-2012"  class="input-append date dpYears">
                                    <input type="text" readonly="" value="{{changeDateFormat(old('joining_date'), $format = 'd-m-Y')}}" size="16" id="join_date" name="joining_date" class="form-control create_emp_date_picker">
                                    <span class="input-group-btn add-on">
                                        <button class="btn" type="button" style="margin-left: -30px;"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                                <span class="help-block hidden">Select date</span>
                            </div>
                            <div class="col-md-4">
                                <label class="control-label" for="input_status">Employee category</label>
                                <select name="employee_category" class="form-control ">
                                    <option >Select</option>
                                    <option <?php
                                    if (old('employee_category') == 'Part Time') {
                                        echo 'selected';
                                    }
                                    ?> >Part Time</option>
                                    <option <?php
                                    if (old('employee_category') == 'Full Time') {
                                        echo 'selected';
                                    }
                                    ?> >Full Time</option>
                                    <option <?php
                                    if (old('employee_category') == 'Provision') {
                                        echo 'selected';
                                    }
                                    ?> >Provision</option>
                                </select>
                            </div> 
                            <div class="col-md-4">
                                <label class="control-label" for="input_religion"><span class="special-required">*</span>Company name</label>
                                <select class="form-control " name="company_name">

                                    <option value="">Select Company</option>
                                    <option <?php
                                    if (old('company_name') == 'Aziz Food Products Ltd') {
                                        echo 'selected';
                                    }
                                    ?> >Aziz Food Products Ltd</option>
                                    <option <?php
                                    if (old('company_name') == 'Big Web Technologies Ltd') {
                                        echo 'selected';
                                    }
                                    ?> >Big Web Technologies Ltd</option>
                                    <option <?php
                                    if (old('company_name') == 'M.H. Global Trading') {
                                        echo 'selected';
                                    }
                                    ?> >M.H. Global Trading</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group "> 
                            <div class="col-md-4">
                                <label class="control-label" for="input_status">Superviser</label>
                                <select class="form-control " name="supervisor_login_id">
                                    <option value="0">Select Employee</option>
                                    @for($i=0;$i<sizeof($all_emp);$i++)

                                        <option <?php
                                            if (old('supervisor_login_id') == $all_emp[$i]['id']) {
                                                echo 'selected';
                                            }
                                            ?> value="{{$all_emp[$i]['id']}}">{{$all_emp[$i]['name']}}</option>

                                        @endfor
                                </select>
                            </div> 
                            <div class="col-md-4">
                                <label class="control-label" for="input_religion">Approver</label>
                                <select class="form-control " name="approver_login_id">
                                    <option value="0">Select Employee</option>
                                    @for($i=0;$i<sizeof($all_emp);$i++)

                                        <option <?php
                                            if (old('approver_login_id') == $all_emp[$i]['id']) {
                                                echo 'selected';
                                            }
                                            ?> value="{{$all_emp[$i]['id']}}">{{$all_emp[$i]['name']}}</option>

                                        @endfor
                                </select>
                            </div>  
                            <div class="col-md-4">
                                <label class="control-label" for="input_religion">Final Approver</label>
                                <select class="form-control " name="final_approver_login_id">
                                    <option value="0">Select Employee</option>
                                    @for($i=0;$i<sizeof($all_emp);$i++)

                                        <option <?php
                                            if (old('final_approver_login_id') == $all_emp[$i]['id']) {
                                                echo 'selected';
                                            }
                                            ?> value="{{$all_emp[$i]['id']}}">{{$all_emp[$i]['name']}}</option>

                                        @endfor
                                </select>
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="col-md-4">
                                <label class="control-label" for="input_status">Office E-Mail</label>
                                <input class="form-control "  type="email" name="email_work" value="{{old('email_work')}}" />
                            </div>
                            <div class="col-md-4">
                                <label class="control-label" for="input_religion"><span class="special-required">*</span>Personal E-Mail </label>
                                <input class="form-control " id="cemail" type="email" name="email_personal" value="{{old('email_personal')}}" required />
                            </div>
                            <div class="col-md-4">
                                <label class=" control-label" for="input_religion">Office Mobile</label>
                                <input class="form-control " id="mobile_num_o" type="phone" name="mobile_work" value="{{old('mobile_work')}}"  />
                            </div>

                        </div>


                        <div class="form-group ">
                            <div class="col-md-4">
                                <label class="control-label " for="input_status"> <span class="special-required">*</span> Personal Mobile</label>
                                <input class="form-control " id="p_phone" type="phone" name="mobile_personal" value="{{old('mobile_personal')}}" required />
                            </div> 
                            <div class="col-md-4">
                                <label class="control-label" for="input_religion"><span class="special-required">*</span>Emergency Mobile</label>
                                <input class="form-control " id="mobile_num" type="phone" name="mobile_emergency" required value="{{old('mobile_emergency')}}" />
                            </div>
                            <div class="col-md-4">
                                <label class="control-label" for="input_religion"><span class="special-required">*</span>Emergency Contact name and Relation </label>
                                <input class="form-control " id="emer_con" name="emergency_contact_name_and_relation" value="{{old('emergency_contact_name_and_relation')}}" type="text" />
                            </div>

                        </div>

                        <div class="form-group ">
                            <div class="col-md-4 gender-inline">
                                <label class="control-label" for="input_status">Gender</label>
                                <div class="radios">
                                    <label class="label_radio r_on" for="radio-01">
                                        <input <?php
                                        if (old('gender') == 'Male') {
                                            echo 'checked';
                                        }
                                        ?> name="gender" id="radio-01" value="Male" type="radio" >Male
                                    </label>
                                    <label class="label_radio r_off" for="radio-02">
                                        <input <?php
                                        if (old('gender') == 'Female') {
                                            echo 'checked';
                                        }
                                        ?> name="gender" id="radio-02" value="Female" type="radio">Female
                                    </label>
                                    <label class="label_radio r_off" for="radio-02">
                                        <input <?php
                                        if (old('gender') == 'Others') {
                                            echo 'checked';
                                        }
                                        ?> name="gender" id="radio-02" value="Others" type="radio">Others
                                    </label>

                                </div>
                            </div> 


                            <div class="col-md-4">
                                <label class="control-label" for="input_religion"> <span class="special-required">*</span> Date Of Birth</label>
                                <div data-date-viewmode="years" data-date-format="dd-mm-yyyy" data-date="12-02-2012"  class="input-append date dpYears">
                                    <input name="date_of_birth" type="text" readonly="" value="{{changeDateFormat(old('date_of_birth'), $format = 'd-m-Y')}}" size="16" class="form-control create_emp_date_picker">
                                    <span class="input-group-btn add-on">
                                        <button class="btn" type="button" style="margin-left: -30px;"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                                <span class="help-block hidden">Select date</span>
                            </div>
                            <div class="col-md-4">
                                <label class="control-label" for="input_religion">  <span class="special-required">*</span> Blood Group</label>
                                <select class="form-control " name="blood_group">
                                    <option>Select</option>
                                    <option <?php
                                    if (old('blood_group') == 'A+') {
                                        echo 'selected';
                                    }
                                    ?> >A+</option>
                                    <option <?php
                                    if (old('blood_group') == 'A-') {
                                        echo 'selected';
                                    }
                                    ?> >A-</option>
                                    <option <?php
                                    if (old('blood_group') == 'B+') {
                                        echo 'selected';
                                    }
                                    ?> >B+</option>
                                    <option <?php
                                    if (old('blood_group') == 'B-') {
                                        echo 'selected';
                                    }
                                    ?> >B-</option>
                                    <option <?php
                                    if (old('blood_group') == 'AB+') {
                                        echo 'selected';
                                    }
                                    ?> >AB+</option>
                                    <option <?php
                                    if (old('blood_group') == 'AB-') {
                                        echo 'selected';
                                    }
                                    ?> >AB-</option>
                                    <option <?php
                                    if (old('blood_group') == 'O+') {
                                        echo 'selected';
                                    }
                                    ?> >O+</option>
                                    <option <?php
                                    if (old('blood_group') == 'O-') {
                                        echo 'selected';
                                    }
                                    ?> >O-</option>
                                </select>
                            </div>


                        </div>
                        <div class="form-group ">

                            <div class="col-md-4">
                                <label class="control-label" for="input_status">Marital Status</label>
                                <select name="marital_status" class="form-control">
                                    <option <?php
                                    if (old('marital_status') == 'Single') {
                                        echo 'selected';
                                    }
                                    ?> >Single</option>
                                    <option <?php
                                    if (old('marital_status') == 'Married') {
                                        echo 'selected';
                                    }
                                    ?> >Married</option>
                                    <option <?php
                                    if (old('marital_status') == 'Widowed') {
                                        echo 'selected';
                                    }
                                    ?> >Widowed</option>
                                    <option <?php
                                    if (old('marital_status') == 'Separated') {
                                        echo 'selected';
                                    }
                                    ?> >Separated</option>
                                    <option <?php
                                    if (old('marital_status') == 'Divorced') {
                                        echo 'selected';
                                    }
                                    ?> >Divorced</option>
                                </select>
                            </div> 
                            <div class="col-md-4">
                                <label class="control-label" for="input_religion">Religion</label>
                                <select name="religion" class="form-control ">
                                    <option <?php
                                    if (old('religion') == 'Islam') {
                                        echo 'selected';
                                    }
                                    ?> >Islam</option>
                                    <option <?php
                                    if (old('religion') == 'Hindu') {
                                        echo 'selected';
                                    }
                                    ?> >Hindu</option>
                                    <option <?php
                                    if (old('religion') == 'Christian') {
                                        echo 'selected';
                                    }
                                    ?> >Christian</option>
                                    <option <?php
                                    if (old('religion') == 'Buddhism') {
                                        echo 'selected';
                                    }
                                    ?> >Buddhism</option>
                                    <option <?php
                                    if (old('religion') == 'Others') {
                                        echo 'selected';
                                    }
                                    ?> >Others</option>
                                </select>
                            </div>  
                            <div class="col-md-4">
                                <label class="control-label" for="input_religion">Highest degree obtained</label>
                                <input class="form-control " id="hig_deg" value="{{old('highest_degree_obtained')}}" name="highest_degree_obtained" type="text" />
                            </div>

                        </div>
                        <div class="form-group ">
                            
                            <div class="col-md-4">
                                <label class=" control-label" for="input_status">Department</label>
                                <input class="form-control " id="department" name="department" type="text" value="{{old('department')}}">
                            </div> 
                            <div class="col-md-4">
                                <label class=" control-label" for="input_status">Password</label>
                                <input class="form-control " id="password" name="password" type="password">
                            </div> 
                           

                            <div class="col-md-4">
                                <label class="control-label" for="input_religion">Confirm Password</label>
                                <input class="form-control " id="confirm_password" name="confirm_password" type="password">
                            </div>


                        </div>
                        <div class="form-group ">

                            <div class="col-md-4">
                                <label class=" control-label" for="input_status">Present Address</label>
                                <textarea class="form-control " rows="6" id="pre_add" name="persent_address" required>{{old('persent_address')}}</textarea>
                            </div> 
                            <div class="col-md-4">
                                <label class=" control-label" for="input_religion"><span class="special-required">*</span>Permanat Address</label>
                                <textarea class="form-control " rows="6" id="comment" name="permanent_address" required>{{old('permanent_address')}}</textarea>
                            </div>
                            <div class="col-md-4">
                                <label class="control-label" for="input_religion">Image Upload</label>
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <div class="fileupload-new thumbnail" style="width: 113px; height: 100px;">
                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                    </div>
                                    <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 100px; max-height: 80px; line-height: 20px;"></div>
                                    <div>
                                        <span class="btn btn-white btn-file">
                                            <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                            <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                            <input name="profile_img" type="file" class="default" />
                                        </span>				
                                    </div>
                                </div>
                            </div>

                        </div>	
                        <div class="form-group">
                            <div class="col-md-offset-5 col-md-2">
                                <input type="submit" class="btn btn-primary btn-shadow btn-lg btn-block" value="Submit">
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>

</section>	

@endsection
