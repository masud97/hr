@extends('layout')

@section('content') 
<!--main content start-->

<section class="wrapper" style="margin-top: 0;">

    <div>
        <!--breadcrumbs start -->
        <ul class="breadcrumb new-bread">
            <li><a class="top-hover" href="{{url('/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a></li>
            <li><a class="top-hover" href="#">Employee</a></li>
            @if(isset($employee_id))
            <li><a class="top-hover" href="{{url('/all-employee')}}">All Employee</a></li>
            @endif

            <li class="active">Edit Employee ({{$employee_data->full_name}})</li>
        </ul>
        <!--breadcrumbs end -->
    </div>	

    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading"style="border-color: #DADFE1;">
                    Edit Employee ({{$employee_data->full_name}})
                </header>
                <div class="panel-body">


                    @if(isset($employee_id))
                    <form class="cmxform form-horizontal tasi-form" id="signupForm" enctype="multipart/form-data" method="post" action="{{url('edit-employee-submit')}}">
                        @else
                        <form class="cmxform form-horizontal tasi-form" id="signupForm" enctype="multipart/form-data" method="post" action="{{url('edit-employee-by-emp-submit')}}">
                            @endif
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="id" value="{{$employee_data->id}}">
                            <input type="hidden" name="existing_image" value="{{$employee_data->image}}">
                            @if(isset($employee_id))
                            <input type="hidden" name="employee_login_id" value="{{$employee_data->employee_login_id}}">

                            <input type="hidden" name="existing_employee_id" value="{{$employeeId}}">

                            <input type="hidden" name="approver_row_id" value="{{$assign_approver->id}}">
                            
                            @endif
                            
                            @if(isset($notificationId))
                            <input type="hidden" name="notification_id" value="{{$notificationId}}">
                            @endif

                            <div class="form-group ">
                                <div class="col-md-4">
                                    <label class="control-label" for="input_status"><span class="special-required">*</span>Employee Name</label>
                                    <input required value="{{$employee_data->full_name}}" class=" form-control" id="firstname" name="full_name" type="text" />
                                </div> 
                                @if(isset($employee_id))
                                <div class="col-md-4">
                                    <label class="control-label" for="input_religion"><span class="special-required">*</span>Employee ID</label>
                                    <input class="form-control " id="emp_id" type="number" value="{{$employeeId}}" name="employee_id" required />
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label" for="input_religion"><span class="special-required">*</span>Designation</label>
                                    <input required class="form-control " id="deg" value="{{$employee_data->designation}}" name="designation" type="text" />
                                </div>
                                @endif
                            </div>
                            @if(isset($employee_id))

                            <div class="form-group ">  
                                <div class="col-md-4">
                                    <label class="control-label" for="input_religion"><span class="special-required">*</span>Joining Date</label>
                                    <div data-date-viewmode="years" data-date-format="dd-mm-yyyy" data-date="12-02-2012"  class="input-append date dpYears">
                                        <input required type="text" readonly="" value="{{changeDateFormat($employee_data->joining_date,'d-m-Y')}}" size="16" id="join_date" name="joining_date" class="form-control edit_emp_date_picker">
                                        <span class="input-group-btn add-on">
                                            <button class="btn btn-danger" type="button" style="margin-left: -30px;"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>
                                    <span class="help-block">Select date</span>
                                </div>
                                <div class="col-md-4">
                                    <label class=" control-label " for="input_status"><span class="special-required">*</span>Employee category</label>
                                    <select required name="employee_category" class="form-control m-bot15">
                                        <option >Select</option>
                                        <option <?php
                                        if ($employee_data->employee_category == 'Part Time') {
                                            echo 'selected';
                                        }
                                        ?> >Part Time
                                        </option>
                                        <option <?php
                                        if ($employee_data->employee_category == 'Full Time') {
                                            echo 'selected';
                                        }
                                        ?> >Full Time</option>
                                        <option <?php
                                        if ($employee_data->employee_category == 'Provision') {
                                            echo 'selected';
                                        }
                                        ?> >Provision</option>
                                        <option <?php
                                        if ($employee_data->employee_category == 'Intern') {
                                            echo 'selected';
                                        }
                                        ?> >Intern</option>
                                    </select>
                                </div> 

                                <div class="col-md-4">
                                    <label class="control-label" for="input_religion"><span class="special-required">*</span>Company name</label>
                                    <select required class="form-control m-bot15" name="company_name">

                                        <option value="">Select Company</option>
                                        <option <?php
                                        if ($employee_data->company_name == 'Aziz Food Products Ltd') {
                                            echo 'selected';
                                        }
                                        ?> >Aziz Food Products Ltd</option>
                                        <option <?php
                                        if ($employee_data->company_name == 'Big Web Technologies Ltd') {
                                            echo 'selected';
                                        }
                                        ?> >Big Web Technologies Ltd</option>
                                        <option <?php
                                        if ($employee_data->company_name == 'M.H. Global Trading') {
                                            echo 'selected';
                                        }
                                        ?> >M.H. Global Trading</option>


                                    </select>
                                </div>
                            </div>
                            @endif

                            @if(isset($employee_id))
                            <div class="form-group ">
                                <div class="col-md-4">
                                    <label class="control-label" for="input_status">Superviser</label>
                                    <select  class="form-control m-bot15" name="supervisor_login_id">
                                        <option value="0">Select Employee</option>
                                        @for($i=0;$i<sizeof($all_emp);$i++)

                                            <option <?php
                                                if ($all_emp[$i]['id'] == $assign_approver->supervisor_login_id) {
                                                    echo 'selected';
                                                }
                                                ?> value="<?= $all_emp[$i]['id'] ?>" >{{$all_emp[$i]['name']}}</option>

                                            @endfor
                                    </select>
                                </div> 

                                <div class="col-md-4">
                                    <label class="control-label" for="input_religion">Approver</label>
                                    <select class="form-control m-bot15" name="approver_login_id">
                                        <option value="0">Select Employee</option>
                                        @for($i=0;$i<sizeof($all_emp);$i++)

                                            <option <?php
                                                if ($all_emp[$i]['id'] == $assign_approver->approver_login_id) {
                                                    echo 'selected';
                                                }
                                                ?> value="{{$all_emp[$i]['id']}}">{{$all_emp[$i]['name']}}</option>

                                            @endfor
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label class="col-md-1 control-label col-lg-1" for="input_religion"><span class="special-required">*</span>Final Approver</label>
                                    <select required class="form-control m-bot15" name="final_approver_login_id">
                                        <option value="0">Select Employee</option>
                                        @for($i=0;$i<sizeof($all_emp);$i++)

                                            <option <?php
                                                if ($all_emp[$i]['id'] == $assign_approver->final_approver_login_id) {
                                                    echo 'selected';
                                                }
                                                ?> value="{{$all_emp[$i]['id']}}">{{$all_emp[$i]['name']}}</option>

                                            @endfor
                                    </select>
                                </div>

                            </div>
                            @endif

                            <div class="form-group ">
                               

                                <div class="col-md-4">
                                    <label class=" control-label" for="input_status"><span class="special-required">*</span>Office E-Mail</label>
                                    <input required class="form-control " value="{{$employee_data->email_work}}"  type="email" name="email_work"  />
                                </div> 
                                

                                <div class="col-md-4">
                                    <label class="control-label" for="input_religion"><span class="special-required">*</span><span class="special-required">*</span>Personal E-Mail </label>
                                    <input required class="form-control " value="{{$employee_data->email_personal}}" id="cemail" type="email" name="email_personal" required />
                                </div>
                                

                                <div class="col-md-4">
                                    <label class="control-label" for="input_religion"><span class="special-required">*</span>Office Mobile</label>
                                    <input required class="form-control " value="{{$employee_data->mobile_work}}" id="mobile_num_o" type="phone" name="mobile_work"  />
                                </div>
                                
                            </div>


                            <div class="form-group ">

                                <div class="col-md-4">
                                    <label class=" control-label " for="input_status"> <span class="special-required">*</span>Personal Mobile</label>
                                    <input required class="form-control " id="p_phone" value="{{$employee_data->mobile_personal}}" type="phone" name="mobile_personal" required />
                                </div> 
                                <div class="col-md-4">
                                    <label class="control-label " for="input_religion"><span class="special-required">*</span>Emergency Mobile</label>
                                    <input required class="form-control " id="mobile_num" value="{{$employee_data->mobile_emergency}}" type="phone" name="mobile_emergency" required />
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label" for="input_religion"><span class="special-required">*</span>Emergency Contact name and Relation </label>
                                    <input required class="form-control " value="{{$employee_data->emergency_contact_name_and_relation}}" id="emer_con" name="emergency_contact_name_and_relation" type="text" />
                                </div>

                            </div>

                            <div class="form-group ">

                                <div class="col-md-4 gender-inline">
                                    <label class="control-label" for="input_status"><span class="special-required">*</span>Gender</label>
                                    <div class="radios">
                                        <label class="label_radio r_on" for="radio-01">
                                            <input required name="gender" id="radio-01" value="Male" type="radio" <?php
                                            if ($employee_data->gender == 'Male') {
                                                echo 'checked';
                                            }
                                            ?> >Male
                                        </label>
                                        <label class="label_radio r_off" for="radio-02">
                                            <input required name="gender" id="radio-02" value="Female" type="radio" <?php
                                            if ($employee_data->gender == 'Female') {
                                                echo 'checked';
                                            }
                                            ?> >Female
                                        </label>
                                        <label class="label_radio r_off" for="radio-02">
                                            <input required name="gender" id="radio-02" value="Others" type="radio" <?php
                                            if ($employee_data->gender == 'Others') {
                                                echo 'checked';
                                            }
                                            ?> >Others
                                        </label>

                                    </div>
                                </div> 
                                <div class="col-md-4">
                                    <label class="control-label" for="input_religion"> <span class="special-required">*</span>Date Of Birth</label>
                                    <div data-date-viewmode="years" data-date-format="dd-mm-yyyy" data-date="12-02-2012"  class="input-append date dpYears">
                                        <input required name="date_of_birth" type="text" readonly="" value="{{changeDateFormat($employee_data->date_of_birth,'d-m-Y')}}" size="16" class="form-control edit_emp_date_picker">
                                        <span class="input-group-btn add-on">
                                            <button class="btn " type="button" style="margin-left: -30px;"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>
                                    <span class="help-block hidden">Select date</span>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label" for="input_religion">  <span class="special-required">*</span>Blood Group</label>
                                    <select required class="form-control m-bot15" name="blood_group">
                                        <option >Select</option>
                                        <option <?php
                                        if ($employee_data->blood_group == 'A+') {
                                            echo 'selected';
                                        }
                                        ?>>A+</option>
                                        <option <?php
                                        if ($employee_data->blood_group == 'A-') {
                                            echo 'selected';
                                        }
                                        ?>>A-</option>
                                        <option
                                        <?php
                                        if ($employee_data->blood_group == 'B+') {
                                            echo 'selected';
                                        }
                                        ?>>B+</option>
                                        <option <?php
                                        if ($employee_data->blood_group == 'B-') {
                                            echo 'selected';
                                        }
                                        ?>>B-</option>
                                        <option
                                        <?php
                                        if ($employee_data->blood_group == 'AB+') {
                                            echo 'selected';
                                        }
                                        ?>>AB+</option>
                                        <option <?php
                                        if ($employee_data->blood_group == 'AB-') {
                                            echo 'selected';
                                        }
                                        ?> >AB-</option>
                                        <option <?php
                                        if ($employee_data->blood_group == 'O+') {
                                            echo 'selected';
                                        }
                                        ?>>O+</option>
                                        <option <?php
                                        if ($employee_data->blood_group == 'O-') {
                                            echo 'selected';
                                        }
                                        ?>>O-</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group ">
                                <div class="col-md-4">
                                    <label class="control-label" for="input_status"><span class="special-required">*</span>Marital Status</label>
                                    <select required name="marital_status" class="form-control m-bot15">
                                        <option <?php
                                        if ($employee_data->marital_status == 'Single') {
                                            echo 'selected';
                                        }
                                        ?> >Single</option>
                                        <option <?php
                                        if ($employee_data->marital_status == 'Married') {
                                            echo 'selected';
                                        }
                                        ?> >Married</option>
                                        <option <?php
                                        if ($employee_data->marital_status == 'Widowed') {
                                            echo 'selected';
                                        }
                                        ?> >Widowed</option>
                                        <option <?php
                                        if ($employee_data->marital_status == 'Separated') {
                                            echo 'selected';
                                        }
                                        ?> >Separated</option>
                                        <option <?php
                                        if ($employee_data->marital_status == 'Divorced') {
                                            echo 'selected';
                                        }
                                        ?> >Divorced</option>
                                    </select>
                                </div> 
                                <div class="col-md-4">
                                    <label class="control-label " for="input_religion"><span class="special-required">*</span>Religion</label>
                                    <select required name="religion" class="form-control m-bot15">
                                        <option <?php
                                        if ($employee_data->religion == 'Islam') {
                                            echo "selected";
                                        }
                                        ?> >Islam</option>
                                        <option <?php
                                        if ($employee_data->religion == 'Hindu') {
                                            echo "selected";
                                        }
                                        ?> >Hindu</option>
                                        <option <?php
                                        if ($employee_data->religion == 'Christian') {
                                            echo "selected";
                                        }
                                        ?> >Christian</option>
                                        <option <?php
                                        if ($employee_data->religion == 'Buddhism') {
                                            echo "selected";
                                        }
                                        ?> >Buddhism</option>
                                        <option> <?php
                                            if ($employee_data->religion == 'Others') {
                                                echo "selected";
                                            }
                                            ?> Others</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label" for="input_religion"><span class="special-required">*</span>Highest degree obtained</label>
                                    <input required class="form-control " id="hig_deg" value="{{$employee_data->highest_degree_obtained}}" name="highest_degree_obtained" type="text" />
                                </div>

                            </div>

                            <div class="form-group ">
                                <div class="col-md-6">
                                    <label class=" control-label" for="input_status"><span class="special-required">*</span>Present Address</label>
                                    <textarea required class="form-control " rows="2" id="pre_add" name="persent_address" required>{{$employee_data->persent_address}}</textarea>
                                </div> 
                                @if(isset($employee_id))
                                <div class="col-md-6">
                                    <label class="control-label" for="input_religion"><span class="special-required">*</span>Permanent Address</label>
                                    <textarea required class="form-control " rows="2" id="comment" name="permanent_address" required>{{$employee_data->permanent_address}}</textarea>
                                </div>

                                @endif

                            </div>	
                            <div class="form-group ">
                                
                                @if(isset($employee_id))
                                <div class="col-md-6">
                                    <label class="control-label" for="input_religion"><span class="special-required">*</span>Department</label>
                                    <input required type="text" class="form-control" value="{{$employee_data->department}}" name="department" id="department">
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label" for="input_religion"><span class="special-required">*</span>National Id</label>
                                    <input required type="text" class="form-control" value="{{$employee_data->national_id}}" name="national_id" id="department">
                                </div>

                                @endif

                            </div>	
                            <div class="form-group ">

                                <div class="col-md-6">
                                    <label class=" control-label" for="input_religion">Image Upload</label>
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="fileupload-new thumbnail" style="width: 113px; height: 100px;">
                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                        </div>
                                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 100px; max-height: 80px; line-height: 20px;"></div>
                                        <div>
                                            <span class="btn btn-white btn-file">
                                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                                <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                                <input name="profile_img" type="file" class="default" />
                                            </span>
    <!--									  <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>-->
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label class=" control-label " for="input_religion">Existing profile picture</label>
                                    @if(!is_null($employee_data->image))
                                    <img width="113px" height="100px" src="{{url('/assets/img/user-image/'.$employee_data->image)}}" alt="Image loading">
                                    @else
                                    <img width="113px" height="100px" src="{{url('/assets/img/nopic.png')}}" alt="Image loading">
                                    @endif
                                </div>

                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-5 col-md-2">
                                    <input type="submit" class="btn btn-primary btn-shadow btn-lg btn-block" value="Submit">
                                </div>
                            </div>
                        </form>
                </div>
            </section>
        </div>
    </div>

</section>	

@endsection
