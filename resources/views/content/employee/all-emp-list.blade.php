@extends('layout')

@section('content') 
<section class="wrapper site-min-height">
    <!-- page start-->
    <div>
        <!--breadcrumbs start -->
        <ul class="breadcrumb new-bread">
            <li><a class="top-hover" href="{{url('/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a></li>
            <li><a class="top-hover" href="#">Employee</a></li>
            <li class="active">All Employee</li>
        </ul>
        <!--breadcrumbs end -->
    </div>
    <section class="panel">
        <header class="panel-heading">
            All Employee
        </header>
        <div class="panel-body table-responsive">
            <div class="adv-table">
                <table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="hidden-table-info">
                    <thead>
                        <tr>
                            <th>Sl. No</th>
                            <th></th>
                            <th>Employee Name</th>
                            <th>Employee Id</th>
                            <th>Company Name</th>
                            <th >Designation </th>
                            <th >Blood Group </th>

                            <th style="display : none;" >Office Mobile No</th>
                            <th style="display : none;" >Emergency Contact No</th>
                            <th style="display : none;" >Emergency Contact Person Name And Relation</th>
                            <th style="display : none;" >Employee Login Id</th>
                            <th style="display : none;" >Row id</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php for ($i = 0; $i < sizeof($all_emp_val); $i++) : ?>
                            <tr class="gradeX">
                                <td><?= $i + 1 ?></td>
                                @if(!is_null($all_emp_val[$i]['image']))
                                <td><img height="50px" width="40px" class="profile-pic" src="assets/img/user-image/<?= $all_emp_val[$i]['image'] ?>"></td>
                                @else
                                <td><img height="50px" width="40px" class="profile-pic" src="assets/img/nopic.png"></td>

                                @endif
                                @if (hasPrivilege(8))
                                <td><a title="Click here to view profile " href="{{url("/admin-employee-profile-view/".$all_emp_val[$i]['employee_login_id'])}}"><?= $all_emp_val[$i]['full_name'] ?></a></td>
                                @else
                                <td><?= $all_emp_val[$i]['full_name'] ?></td>

                                @endif
                                <td><?= $all_emp_val[$i]['employee_id'] ?></td>
                                <td><?= $all_emp_val[$i]['company_name'] ?></td>
                                <td><?= $all_emp_val[$i]['designation'] ?></td>
                                <td><?= $all_emp_val[$i]['blood_group'] ?></td>

                                <td style="display: none"><?= $all_emp_val[$i]['mobile_work'] ?></td>
                                <td style="display: none"><?= $all_emp_val[$i]['mobile_emergency'] ?></td>
                                <td style="display: none"><?= $all_emp_val[$i]['emergency_contact_name_and_relation'] ?></td>
                                <td style="display: none"><?= $all_emp_val[$i]['employee_login_id'] ?></td>
                                <td style="display: none"><?= $all_emp_val[$i]['id'] ?></td>



                            </tr>
                        <?php endfor; ?>




                    </tbody>
                </table>



                <!-- Modal -->
                <div class="modal fade" id="merits-demerits-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Add Merits/Demerits</h4>
                            </div>
                            <div class="modal-body">

                                <div class="panel-body">
                                    <form id="merits-demerits-form" method="post" action="{{url('add-merits-demerits')}}">
                                        <input id="csrf_token" type="hidden" name="_token" value="{{csrf_token()}}">
                                        <input id="emp_id_merits_demerits" type="hidden" name="employee_id">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"> Select One</label>
                                            <select id="type" name="type" class="form-control m-bot15">
                                                <option>Select</option>
                                                <option>Merits</option>
                                                <option>Demerits</option>
                                            </select>
                                        </div>

                                        <div class="form-group">

                                            <textarea class="form-control " id="details" name="details" required></textarea>

                                        </div>




                                </div>




                            </div>
                            <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                <button id="merits-demerits-submit-btn"  class="btn btn-success" type="button">Submit</button>

                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- modal end-->
                <!-- Modal file upload-->
                <div class="modal fade" id="file-upload-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">File Upload</h4>
                            </div>
                            <div class="modal-body">

                                <div class="panel-body">
                                    <form id="file-upload-form" method="post" action="{{url('file-upload-admin')}}" enctype="multipart/form-data">
                                        <input id="csrf_token" type="hidden" name="_token" value="{{csrf_token()}}">
                                        <input id="emp_id_file_upload" type="hidden" name="employee_id">
                                        <input id="emp_row_id" type="hidden" name="row_id">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Title</label>
                                            <input  name="file_title" type="text" class="form-control"  placeholder="Title">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Category</label>
                                            <select id="file_category" name="file_category" class="form-control m-bot15">
                                                <option>CV Related</option>
                                                <option>Application</option>
                                                <option>Others</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <input id="file_upload" name="file_upload" type="file" class="default" />

                                        </div>

                                </div>




                            </div>
                            <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                <input type="submit" value="Submit" class="btn btn-success">


                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- modal end-->
                <!-- Modal reset passsword-->
                <div class="modal fade" id="reset-password-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Reset Password</h4>
                            </div>
                            <div class="modal-body">

                                <div class="panel-body">
                                    <form id="file-upload-form" method="post" action="{{url('reset-password-submit')}}" enctype="multipart/form-data">
                                        <input id="csrf_token" type="hidden" name="_token" value="{{csrf_token()}}">
                                        <input id="emp_id_reset_password" type="hidden" name="employee_id">
                                        <input id="emp_row_id_reset_password" type="hidden" name="row_id">

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">New Password</label>
                                            <input  name="new_password" type="password" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Confirm New Password</label>
                                            <input  name="confirm_new_password" type="password" class="form-control">
                                        </div>

                                </div>




                            </div>
                            <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                <input type="submit" value="Submit" class="btn btn-success">


                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- modal end-->



            </div>
        </div>
    </section>
    <!-- page end-->
</section>

@endsection