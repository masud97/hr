@extends('layout')

@section('content') 

<div class="col-md-12">
    <!--breadcrumbs start -->
    <ul class="breadcrumb new-bread">
        <li><a href="#"><i class="fa fa-home"></i>Dashboard</a></li>
        <li class="active">Change Password</li>
    </ul>
    <!--breadcrumbs end -->
</div>


<header class="panel-heading form-reg-hed">
    Change Password
</header>
<form class="cmxform form-horizontal tasi-form"  method="post" action="{{ url('change-password-submit') }}">
<div class="form-group ">
						<div class="col-md-1">
							<label class="col-md-1 control-label col-lg-1" for="input_status">Superviser</label>
						</div>  
						<div class="col-md-3">
							<select class="form-control m-bot15">
							  <option>Select</option>
							  <option>1</option>
							  <option>1</option>
							  <option>3</option>
						  </select>
						</div> 
						<div class="col-md-1">
							 <label class="col-md-1 control-label col-lg-1" for="input_religion">Approver</label>
						</div>  
						<div class="col-md-3">
							<select class="form-control m-bot15">
							  <option>Select</option>
							  <option>A</option>
							  <option>B</option>
						    </select>
					    </div>
					    
					    <div class="col-md-1">
							 <label class="col-md-1 control-label col-lg-1" for="input_religion">Final Approver</label>
						</div>  
						<div class="col-md-3">
							<select class="form-control m-bot15">
							  <option>Select</option>
							  <option>A</option>
							  <option>B</option>
						    </select>
					    </div>
					    
					</div>
</form>
@endsection