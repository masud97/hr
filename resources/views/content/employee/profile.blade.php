@extends('layout')

@section('content')



<section class="wrapper">

    <div >
        <!--breadcrumbs start -->
        <ul class="breadcrumb new-bread">
            <li ><a class="top-hover" href="{{url('/dashboard')}}"><i class="fa fa-home"></i>Dashboard</a></li>
            <li><a class="top-hover" href="#">Employee</a></li>
            @if(isset($employee_id))
            <li><a class="top-hover" href="{{url('/all-employee')}}">All Employee</a></li>
            @endif
            <li class="active">Profile</li>
        </ul>
        <!--breadcrumbs end -->
    </div>


    <!-- page start-->
    <div class="row">
        @include('common.common_aside_profile')
        <aside class="profile-info col-lg-9">
            <section class="panel">
                <div class="panel-body bio-graph-info">
                    <header class="panel-heading" style="border-color: #DADFE1;">
                        Bio Graph
                    </header>
                    <div class="row">
                        <div class="col-xs-12 col-lg-6 table-responsive">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td>Employee Id </td>
                                        <td>{{$emp_id}}</td>
                                    </tr>
                                    <tr>
                                        <td>Company Name</td>
                                        <td> {{$userDetails->company_name}}</td>
                                    </tr>
                                    <tr>
                                        <td>Department </td>
                                        <td>{{$userDetails->department}}</td>
                                    </tr>
                                    <tr>
                                        <td>Designation </td>
                                        <td>{{$userDetails->designation}}</td>
                                    </tr>
                                    <tr>
                                        <td>Type</td>
                                        <td>{{$userDetails->employee_category}}</td>
                                    </tr>
                                    <tr>
                                        <td>Joining Date </td>
                                        <td>{{$userDetails->joining_date}}</td>
                                    </tr>
                                    <tr>
                                        <td>Office Mobile</td>
                                        <td> {{$userDetails->mobile_work}}</td>
                                    </tr>
                                    <tr>
                                        <td>Personal Mobile </td>
                                        <td>{{$userDetails->mobile_personal}}</td>
                                    </tr>
                                    <tr>
                                        <td>Emergency Contact </td>
                                        <td>{{$userDetails->mobile_emergency}} ({{$userDetails->emergency_contact_name_and_relation}})</td>
                                    </tr>
                                    <tr>
                                        <td>Office Email </td>
                                        <td>{{$userDetails->email_work}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-xs-12  col-lg-6 table-responsive">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td>Personal Email </td>
                                        <td>{{$userDetails->email_personal}}</td>
                                    </tr>
                                    <tr>
                                        <td>Present Address </td>
                                        <td> {{$userDetails->persent_address}}</td>
                                    </tr>
                                    <tr>
                                        <td>Permanent Address </td>
                                        <td>{{$userDetails->permanent_address}}</td>
                                    </tr>
                                    <tr>
                                        <td>Gender</td>
                                        <td> {{$userDetails->gender}}</td>
                                    </tr>
                                    <tr>
                                        <td>Marital Status  </td>
                                        <td> {{$userDetails->marital_status}}</td>
                                    </tr>
                                    <tr>
                                        <td>Religion</td>
                                        <td>{{$userDetails->religion}}</td>
                                    </tr>
                                    <tr>
                                        <td>Blood Group </td>
                                        <td>{{$userDetails->blood_group}}</td>
                                    </tr>
                                    <tr>
                                        <td>Date Of Birth</td>
                                        <td>{{$userDetails->date_of_birth}}</td>
                                    </tr>
                                    <tr>
                                        <td>Highest Degree Obtained</td>
                                        <td>{{$userDetails->highest_degree_obtained}}</td>
                                    </tr>
                                    <tr>
                                        <td>National Id</td>
                                        <td>{{$userDetails->national_id}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--                    <div class="">
                                        <div class="bio-row">
                                            <p><span>Employee Id </span>: {{$emp_id}}</p>
                                        </div>
                                        <div class="bio-row">
                                            <p><span>Company Name </span>: {{$userDetails->company_name}}</p>
                                        </div>
                                        <div class="bio-row">
                                            <p><span>Department </span>: {{$userDetails->department}}</p>
                                        </div>
                                        <div class="bio-row">
                                            <p><span>Designation </span>: {{$userDetails->designation}}</p>
                                        </div>
                                        <div class="bio-row">
                                            <p><span>Type </span>: {{$userDetails->employee_category}}</p>
                                        </div>
                                        <div class="bio-row">
                                            <p><span>Joining Date </span>: {{$userDetails->joining_date}}</p>
                                        </div>
                                        <div class="bio-row">
                                            <p><span>Office Mobile </span>: {{$userDetails->mobile_work}}</p>
                                        </div>
                                        <div class="bio-row">
                                            <p><span>Personal Mobile </span>: {{$userDetails->mobile_personal}}</p>
                                        </div>
                                        <div class="bio-row">
                                            <p><span>Emergency Contact </span>: {{$userDetails->mobile_emergency}} ({{$userDetails->emergency_contact_name_and_relation}})</p>
                                        </div>
                                        <div class="bio-row">
                                            <p><span>Office Email </span>: {{$userDetails->email_work}}</p>
                                        </div>
                                        <div class="bio-row">
                                            <p><span>Personal Email </span>: {{$userDetails->email_personal}}</p>
                                        </div>
                                        <div class="bio-row">
                                            <p><span>Present Address </span>: {{$userDetails->persent_address}}</p>
                                        </div>
                                        <div class="bio-row">
                                            <p><span>Permanent Address </span>: {{$userDetails->permanent_address}}</p>
                                        </div>
                
                                        <div class="bio-row">
                                            <p><span>Gender </span>: {{$userDetails->gender}}</p>
                                        </div>
                                        <div class="bio-row">
                                            <p><span>Marital Status </span>: {{$userDetails->marital_status}}</p>
                                        </div>
                                        <div class="bio-row">
                                            <p><span>Religion </span>: {{$userDetails->religion}}</p>
                                        </div>
                                        <div class="bio-row">
                                            <p><span>Blood Group </span>: {{$userDetails->blood_group}}</p>
                                        </div>
                                        <div class="bio-row">
                                            <p><span>Date Of Birth </span>: {{$userDetails->date_of_birth}}</p>
                                        </div>
                                        <div class="bio-row">
                                            <p><span>Highest Degree Obtained </span>: {{$userDetails->highest_degree_obtained}}</p>
                                        </div>
                                    </div>-->
                </div>
            </section>

            <section class="panel">
                <div class="panel-body bio-graph-info">
                    <header class="panel-heading" style="border-color: #DADFE1;">
                        Approver
                        @if(isset($employee_id))
                        <button id="change-approver-btn" style="float: right" class="btn btn-success btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                        @endif
                    </header>
                    <div class="panel-body">

                        <div class="col-md-4">
                            <p><span>Supervisor</span>: {{$assignApprover['supervisor']['name']}}</p>
                        </div>
                        <div class="col-md-4">
                            <p><span>Approver</span>: {{$assignApprover['approver']['name']}}</p>
                        </div>
                        <div class="col-md-4">
                            <p><span>Final Approver</span>: {{$assignApprover['finalApprover']['name']}}</p>
                        </div>

                    </div>
                </div>
            </section>


            <div class="panel">
                <!--                start of pie chart area-->
                <div class="col-md-4" style="padding: 0;">
                    <!--                    widget start-->
                    <div class="panel">
                        <div class="panel-body">
                            <div class="bio-chart">
                                <input readonly title="Casual Leave Taken" class="knob" data-max="{{$totalLeave['casual']}}" value="{{$leaveTaken['casual']}}" data-width="80" data-height="90" data-displayPrevious=true  data-thickness=".2"  data-fgColor="#96be4b" data-bgColor="#e8e8e8">
                            </div>
                            <div class="bio-desk">
                                <h4 class="red">Casual Leave</h4>
                                <p>Total : {{$totalLeave['casual']}}</p>
                                <p>Remaining : {{$totalLeave['casual']-$leaveTaken['casual']}}</p>
                            </div>
                        </div>
                    </div>
                    <!--                    widget end-->
                </div>
                <div class="col-md-4">
                    <!--                    widget start-->
                    <div class="panel">
                        <div class="panel-body">
                            <div class="bio-chart">
                                <input  readonly title="Sick Leave Taken" class="knob" data-max="{{$totalLeave['sick']}}" value="{{$leaveTaken['sick']}}" data-width="80" data-height="90" data-displayPrevious=true  data-thickness=".2" data-fgColor="#e06b7d" data-bgColor="#e8e8e8">
                            </div>
                            <div class="bio-desk">
                                <h4 class="terques">Sick Leave </h4>
                                <p>Total : {{$totalLeave['sick']}}</p>
                                <p>Remaining : {{$totalLeave['sick']-$leaveTaken['sick']}}</p>
                            </div>
                        </div>
                    </div>
                    <!--                    widget end-->
                </div>
                <div class="col-md-4" style="padding: 0;">
                    <!--                    widget start-->
                    <div class="panel">
                        <div class="panel-body">
                            <div class="bio-chart">
                                <input readonly title="Half Day Leave Taken" class="knob" data-max="{{$totalLeave['half']}}" value="{{$leaveTaken['half']}}" data-width="80" data-height="90" data-displayPrevious=true  data-thickness=".2"  data-fgColor="#4CC5CD" data-bgColor="#e8e8e8">
                            </div>
                            <div class="bio-desk">
                                <h4 class="green">Half Day</h4>
                                <p>Total : {{$totalLeave['half']}}</p>
                                <p>Remaining : {{$totalLeave['half']-$leaveTaken['half']}}</p>
                            </div>
                        </div>
                    </div>
                    <!--                    widget end-->
                </div>
                <!--                 page end-->
            </div>
        </aside>
    </div>
</section>

<!-- Modal file upload-->
<div class="modal fade" id="change_profile_picture_modal"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Change profile picture</h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    @if(isset($employee_id))
                    <form  method="post" action="{{url('change-profile-picture/'.$employee_id)}}" enctype="multipart/form-data">
                        @else
                        <form method="post" action="{{url('change-profile-picture')}}" enctype="multipart/form-data">
                            @endif 
                            <input id="csrf_token" type="hidden" name="_token" value="{{csrf_token()}}">
                            <input  value="{{$userDetails->image}}" type="hidden" name="existing_image">
                            <input  value="{{$userDetails->id}}" type="hidden" name="id">
                            <div class="form-group">
                                <label class="col-md-1 control-label col-lg-1" for="input_religion">Image Upload</label>
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <div class="fileupload-new thumbnail" style="width: 113px; height: 100px;">
                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                    </div>
                                    <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 100px; max-height: 80px; line-height: 20px;"></div>
                                    <div>
                                        <span class="btn btn-white btn-file">
                                            <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                            <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                            <input name="profile_img" type="file" class="default" />
                                        </span>
<!--									  <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>-->
                                    </div>
                                </div>


                            </div>

                            </div>




                            </div>
                            <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                <input type="submit" value="Submit" class="btn btn-success">


                            </div>
                        </form>
                </div>
            </div>
        </div>
        <!-- modal end-->
        @if(isset($employee_id))
        <!-- Modal file upload-->
        <div class="modal fade" id="edit-approver-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Change Approver</h4>
                    </div>
                    <div class="modal-body">

                        <div class="panel-body">

                            <form  method="post" action="{{url('edit-approver/'.$employee_id)}}" enctype="multipart/form-data">


                                <input id="csrf_token" type="hidden" name="_token" value="{{csrf_token()}}">

                                <input  value="{{$assignApprover['row_id']}}" type="hidden" name="row_id">


                                <div class="form-group">
                                    <label for="exampleInputEmail1">Supervisor</label>
                                    <select  name="supervisor_login_id" class="form-control m-bot15">
                                        <option value="0">Select</option>
                                        @for($i=0;$i<sizeof($all_emp);$i++)
                                            <option <?php
                                                if ($all_emp[$i]['id'] == $assignApprover['supervisor']['id']) {
                                                    echo 'selected';
                                                }
                                                ?> value="{{$all_emp[$i]['id']}}">{{$all_emp[$i]['name']}}</option>
                                            @endfor
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Approver</label>
                                    <select name="approver_login_id" class="form-control m-bot15">
                                        <option value="0">Select</option>
                                        @for($i=0;$i<sizeof($all_emp);$i++)
                                            <option <?php
                                                if ($all_emp[$i]['id'] == $assignApprover['approver']['id']) {
                                                    echo 'selected';
                                                }
                                                ?> value="{{$all_emp[$i]['id']}}">{{$all_emp[$i]['name']}}</option>
                                            @endfor
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Final Approver</label>
                                    <select  name="final_approver_login_id" class="form-control m-bot15">
                                        <option value="0">Select</option>
                                        @for($i=0;$i<sizeof($all_emp);$i++)
                                            <option <?php
                                                if ($all_emp[$i]['id'] == $assignApprover['finalApprover']['id']) {
                                                    echo 'selected';
                                                }
                                                ?> value="{{$all_emp[$i]['id']}}">{{$all_emp[$i]['name']}}</option>
                                            @endfor

                                    </select>
                                </div>

                        </div>




                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <input type="submit" value="Submit" class="btn btn-success">


                    </div>
                    </form>
                </div>
            </div>
        </div>
        @endif
        <!-- modal end-->


        @endsection
