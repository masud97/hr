@extends('layout')

@section('content') 
<!--main content start-->

<section class="wrapper" style="margin-top: 0;">

    <div>
        <!--breadcrumbs start -->
        <ul class="breadcrumb new-bread">
            <li><a class="top-hover" href="{{url('/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a></li>
            <li><a class="top-hover" href="#">Employee</a></li>
            <li><a class="top-hover" href="{{url('/all-employee')}}">All Employee</a></li>
            <li class="active">Terminate Employee</li>
        </ul>
        <!--breadcrumbs end -->
    </div>	

    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading"style="border-color: #DADFE1;">
                    Terminate Employee
                </header>
                <div class="panel-body">



                    <form class="cmxform form-horizontal tasi-form" id="signupForm" enctype="multipart/form-data" method="post" action="{{url('terminate-employee-submit')}}">
                        
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="employee_id" value="{{$employee_id}}">
                        
                        <div class="form-group ">
                            <div class="col-md-2">
                                <label class="col-md-1 control-label col-lg-1" for="input_status"><span class="special-required">*</span>Employee Name</label>
                            </div>  
                            <div class="col-md-3">
                                <input readonly value="{{$employee_name}}" class=" form-control" name="full_name" type="text" />
                            </div>  


                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                                <label class="col-md-1 control-label col-lg-1" for="input_status"><span class="special-required">*</span>Transfer Privilige To </label>
                            </div> 
                            <div class="col-md-3">
                                <select class="form-control m-bot15" name="replace_login_id">
                                    <option value="0">Select Employee</option>
                                    @for($i=0;$i<sizeof($all_emp);$i++)
                                        
                                        <option <?php
                                        if (old('approver_login_id') == $all_emp[$i]['id']) {
                                            echo 'selected';
                                        }
                                        ?> value="{{$all_emp[$i]['id']}}">{{$all_emp[$i]['name']}}</option>

                                        @endfor
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-5 col-md-7">
                                <input type="submit" class="btn btn-danger" value="Submit">
                            </div>
                        </div>


                    </form>




                </div>
            </section>
        </div>
    </div>

</section>	

@endsection
