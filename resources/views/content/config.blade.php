@extends('layout')

@section('content')
<section class="wrapper" style="margin-top: 0;">

    <div >
        <!--breadcrumbs start -->
        <ul class="breadcrumb new-bread">
            <li><a class="top-hover" href="{{url('/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a></li>
            <li class="active">App Config</li>
        </ul>
        <!--breadcrumbs end -->
    </div>	

    <!--start of pie chart area-->



    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading"style="border-color: #DADFE1;">
                    Leave Settings
                </header>
                <div class="panel-body">

                    <form class="cmxform form-horizontal tasi-form" id="signupForm" method="post" action="{{url('/app-config/submit')}}" enctype="multipart/form-data">

                        <input type="hidden" name="_token" value="{{csrf_token()}}">

                        

                        <div class="form-group ">
                            <div class="col-md-2">
                                <label for="username" class="control-label col-md-2">Casual Leave</label>
                            </div>  
                            <div class="col-md-1">
                                <input type="number" class="form-control" id="casual-leave" name="casual-leave" value="{{$totalLeave['casual']}}">

                            </div> 
                            
                            
                            <div class="col-md-1">
                                <label for="username" class="control-label col-md-2">Sick Leave</label>
                            </div>  
                            <div class="col-md-1">
                                <input type="number" class="form-control" id="sick-leave" name="sick-leave" value="{{$totalLeave['sick']}}">

                            </div> 
                            
                            <div class="col-md-1">
                                <label for="username" class="control-label col-md-2">Half Day</label>
                            </div>  
                            <div class="col-md-1">
                                <input type="number" class="form-control" id="halfday" name="half-day" value="{{$totalLeave['half']}}">

                            </div> 
                            
                             <div class="col-md-1">
                                <input type="submit" class="btn btn-primary" value="Submit">
                            </div>
                          
                          

                        </div>

                        

                    </form>


                </div>
            </section>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading"style="border-color: #DADFE1;">
                    Company Setting
                </header>
                <div class="panel-body">
                    <form class="cmxform form-horizontal tasi-form" id="signupForm" method="post" action="{{url('/create-new-company')}}" enctype="multipart/form-data">

                        <input type="hidden" name="_token" value="{{csrf_token()}}">




                        <div class="form-group ">
                            <div class="col-md-8">
                                @if(is_null($totalCom))
                                <input required name="companyName" id="tagsinput" class="tagsinput" value="" title="Insert New Company Name" />
                                @else
                                <input required name="companyName" id="tagsinput" class="tagsinput" value="{{$totalCom->companyName}}" />
                                @endif
                            </div>
                            <div class="col-md-1">
                                @if(is_null($totalCom))
                                <input type="submit" class="btn btn-primary" value="Create">
                                @else
                                <input type="submit" class="btn btn-primary" value="Update">
                                @endif

                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>



</section>
@endsection