@extends('layout')

@section('content')
<section class="wrapper">

    <div class="">
        <!--breadcrumbs start -->
        <ul class="breadcrumb new-bread">
            <li><a class="top-hover" href="{{url('/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a></li>
            <li><a class="top-hover" href="{{url('/notice')}}">Notice</a></li>
            <li class="active">New Notice</li>
        </ul>
        <!--breadcrumbs end -->
    </div>


    <!-- page start-->
    <div class="row">

        <aside class="profile-info col-lg-12">

            <section class="panel">
                <header class="panel-heading panel-heading-2" style="border-color: #DADFE1;">
                    New Notice
                    
                </header>
                <div class="panel-body">

                    <form class="cmxform form-horizontal tasi-form" id="signupForm" method="post" action="{{url('/notice/new/submit')}}">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="form-group ">
                            <div class="col-md-2">
                                <label class="control-label" for="input_status">Select employee or company</label>
                            </div>
                            <div class="col-md-10">

                                <select class="js-example-basic-multiple" name="employee_or_company[]" multiple="multiple">
                                    <option value="all">All Employee</option>
                                    @if(!is_null($company))
                                        @foreach($company as $com)
                                            <option>{{$com}}</option>
                                        @endforeach
                                    @endif
                                    
                                    @if(!is_null($activeEmployee))
                                        @foreach($activeEmployee as $emp)
                                        <option value="{{$emp['id']}}">{{$emp['name']}}</option>
                                        @endforeach
                                    @endif
                                </select>


                            </div>

                        </div>


                        <div class="form-group ">
                            <div class="col-md-2">
                                <label class="col-md-1 control-label col-lg-1" for="input_religion">Subject</label>
                            </div>
                            <div class="col-md-10">
                                <input class="form-control " id="com_name" name="subject" type="text" value="{{old('subject')}}" />
                            </div>

                        </div>


                        <div class="form-group ">

                            <div class="col-md-2">
                                <label for="username" class="control-label">Publish Date</label>
                            </div>
                            <div class="col-md-10">
                                <input id="publish_date" class="form-control form-control-inline input-medium default-date-picker" size="16" type="text" name="publish_date" value="{{old('publish_date')}}" />
                            </div>

<!--                            <div class="col-md-2">
                                <label class="control-label" for="input_status">Priority</label>
                            </div>
                            <div class="col-md-4">

                                <div class="radios prio-radio">
                                    <label class="label_radio r_on" for="radio-01">
                                        <input name="priority" id="radio-01" value="1" type="radio"  @if(old('priority') === '1') checked @endif>High
                                    </label>
                                    <label class="label_radio r_off" for="radio-02">
                                        <input name="priority" id="radio-02" value="2" type="radio" @if(old('priority') === '2') checked @endif>Medium
                                    </label>
                                    <label class="label_radio r_off" for="radio-02">
                                        <input name="priority" id="radio-02" value="3" type="radio" @if(old('priority') === '3') checked @endif>Low
                                    </label>

                                </div>
                            </div>-->

                        </div>



                        <div class="form-group ">

                            <div class="col-md-2">
                                <label class="control-label col-md-3">Notice</label>
                            </div>
                            <div class="col-md-10">
                                <textarea class="wysihtml5 form-control" name="notice_body" rows="10" >{{old('notice_body')}}</textarea>
                            </div>

                        </div>


                </div>

                <div class="form-group">
                    <div class="col-md-offset-5 col-md-7">
                        <button class="btn btn-danger" type="submit">Submit</button>
                    </div>
                </div>


                </form>


                </div>
            </section>






        </aside>


    </div>




</section>
@endsection