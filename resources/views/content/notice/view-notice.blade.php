@extends('layout')

@section('content')

<section class="wrapper site-min-height">

    <div class="">
        <!--breadcrumbs start -->
        <ul class="breadcrumb new-bread">
            <li ><a class="top-hover" href="{{url('/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a></li>
            <li><a class="top-hover" href="{{url('/notice')}}">Notice</a></li>
            <li class="active">View Notice</li>
        </ul>
        <!--breadcrumbs end -->
    </div>

    <section class="panel">

        <div class="panel-body">


            <div class="inbox-body">
                <div class="heading-inbox row">
                    <div class="col-md-8">
                        <div class="compose-btn">                                                                   

                        </div>

                    </div>
                    <div class="col-md-4 text-right">
                        <p class="date"><b>Published On:</b> {{ changeDateFormat($notice->publish_on,'l jS \of F Y')}}</p>
                    </div>
                    <div class="col-md-12">
                        <h4> {{$notice->subject}}</h4>
                    </div>
                </div>
                <div class="sender-info">
                    <div class="row">
                        <div class="col-md-12">
                            <img alt="" src="{{url('assets/img/user-image')}}/{{$publish_by_name_and_image->image}}">
                            <strong>{{$publish_by_name_and_image->full_name}}</strong>

                        </div>
                    </div>
                </div>
                <div class="view-mail">
                    <p 	class="date-specify" style="text-align: justify"><?php echo strip_tags($notice->body,'<br><b><i><h1><h2><h3><h4><h5><h6><u>') ?></p>

                </div>
                <br/>
                @if(hasPrivilege(9))
                <div>
                    <h4>Notice For: </h4>
                    <p>{{$noticeFor}}</p>
                </div>
                @endif


            </div>


        </div>
    </section>
    <!-- page end-->

</section>

@endsection