@extends('layout')

@section('content')

<section class="wrapper site-min-height">

    <div class="">
        <!--breadcrumbs start -->
        <ul class="breadcrumb new-bread">
            <li><a class="top-hover" href="{{url('/dashboard')}}"><i class="fa fa-home"></i> Dashboard</a></li>

            <li class="active">Notice</li>
        </ul>
        <!--breadcrumbs end -->
    </div>

    <!-- page start-->
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    {{$title}}

                    @if($hasPrivilege)
                    <a title="" data-placement="left" data-toggle="tooltip" data-original-title="New Notice" class="btn btn-success link-all-notice tooltips" href="{{url('/notice/new')}}">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                    </a>
                    @endif
                </header>

                <div class="panel-body table-responsive">
                    <div class="adv-table">
                        <table  class="display table table-bordered table-striped" id="example">
                            <thead class="alignment">
                                <tr>
                                    <th>Sr</th>
                                    <th>Title</th>
                                    <th>Publish Date</th>
                                    <th>Insert Date</th>
                                    <th class="hidden-phone">Status</th>
                                    @if($hasPrivilege)
                                    <th class="hidden-phone">Action</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>

                                @if(count($allNotice)>0)

                                <?php $i = 1; ?>
                                @foreach($allNotice as $notice)
                                @if($hasPrivilege)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td><a href="{{url('/notice/view')}}/{{$notice->id}}">{{$notice->subject}}</a></td>
                                    <td>{{changeDateFormat($notice->publish_on,'l jS \of F Y')}}</td>
                                    <td>{{changeDateFormat($notice->insert_date,'l jS \of F Y')}}</td>
                                    <td>{{$notice->status}}</td>
                                    @if($hasPrivilege)
                                    <td class="center hidden-phone">
                                        <!--<a href="{{url('/notice/reissue')}}/{{$notice->id}}" title="Reissue This Notice" data-placement="top" data-toggle="tooltip" data-original-title="Reissue This Notice" class="btn btn-success btn-xs tooltips"><i class="fa fa-refresh" aria-hidden="true"></i></a>-->
                                        @if($notice->status !== 'withdrawn')
                                        <a href="{{url('/notice/withdraw')}}/{{$notice->id}}" title="Withdraw This Notice" data-placement="top" data-toggle="tooltip" data-original-title="Withdraw This Notice" class="btn btn-danger btn-xs tooltips confirm"><i class="fa fa-times" aria-hidden="true"></i></a>
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                @elseif($notice->status !== 'withdrawn')
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td><a href="{{url('/notice/view')}}/{{$notice->id}}">{{$notice->subject}}</a></td>
                                    <td>{{changeDateFormat($notice->publish_on,'l jS \of F Y')}}</td>
                                    <td>{{changeDateFormat($notice->insert_date,'l jS \of F Y')}}</td>
                                    <td>{{$notice->status}}</td>
                                </tr>
                                @endif
                                @endforeach

                                @endif



                                </tfoot>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- page end-->
</section>


@endsection