<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Arnob">
        <meta name="author-details" content="mdarnob007@gmail.com">
        <meta name="keyword" content="HR,Human Resorce,Big Web Technology Ltd">

        <title>Human Resource - Login</title>

        <!-- Bootstrap core CSS -->
        <link href="{{asset("assets/css/bootstrap.min.css")}}" rel="stylesheet">
        <link href="{{asset("assets/css/bootstrap-reset.css")}}" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="{{asset("assets/css/style.css")}}" rel="stylesheet">
        <link href="{{asset("assets/css/style-responsive.css")}}" rel="stylesheet" />

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
        <!--[if lt IE 9]>
        <script src="{{asset("js/html5shiv.js")}}"></script>
        <script src="{{asset("js/respond.min.js")}}"></script>
        <![endif]-->
    </head>

    <body class="login-body">

        <div class="container">

            <form class="form-signin" action="{{ url('forget-password-reset-submit') }}" method="post">
                <h2 class="form-signin-heading">Reset Forget Password</h2>


                @if(count($errors)>0)
                <div class="alert alert-block alert-danger fade in" style="margin-bottom: 0px;">
                    <strong>Error</strong>
                    @foreach($errors->all() as $error)
                    {{$error}}
                    @endforeach
                </div>
                @endif


                <div class="login-wrap">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="text" name="token" class="form-control" placeholder="Token" autofocus required>
                    <input type="password" name="new_password" class="form-control" placeholder="Password" required>
                    <input type="password" name="new_confirm_password" class="form-control" placeholder="Confirm Password" required>

                    <button class="btn btn-lg btn-login btn-block" type="submit">Submit</button>
                </div>
            </form>



        </div>



        <!-- js placed at the end of the document so the pages load faster -->
        <script src="{{asset("assets/js/jquery.js")}}"></script>
        <script src="{{asset("assets/js/bootstrap.min.js")}}" ></script>


    </body>
</html>


<!--------->

